<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuariosModel extends Model
{
    //
     protected $table = 'users';
     protected $hidden = array('password', 'remember_token');
     public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'name'=>'required',
                'cedula'=>'required',
                //'cedula'=>'required|min:10|unique:users'. ($id ? ",id,$id" : ''),                
                'email'=>'required|email|unique:users'. ($id ? ",id,$id" : '')
            ], $merge);
        }
         public static function rulescon ($id=0, $merge=[]) {
            return array_merge(
            [                
                'email'=>'required|email|unique:users'. ($id ? ",id,$id" : ''),
                'password'=>'required|alpha_num|min:4|confirmed',
                'password_confirmation'=>'required|alpha_num|min:4',
                'name'=>'required',
                'id_direccion'=>'required',
                'cargo'=>'required'
            ], $merge);
        }       
}
