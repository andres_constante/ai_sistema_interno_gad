<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuPerfilModel extends Model
{
    //
    protected $table = 'ad_menu_perfil';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'id_perfil'=>'required',
                'id_modulo'=>'required'
            ], $merge);
        }
}
