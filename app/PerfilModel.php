<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerfilModel extends Model
{
    //
    protected $table = 'ad_perfil';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'nombre_perfil'=>'required|unique:ad_perfil'. ($id ? ",id,$id" : ''),
            ], $merge);
        } 
}
