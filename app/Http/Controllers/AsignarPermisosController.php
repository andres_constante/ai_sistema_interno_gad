<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
//
use App\MenuModel;
use App\AsignarPermisosModel;
use App\UsuariosModel;
use App\PerfilPermisosModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;

class AsignarPermisosController extends Controller
{
    var $configuraciongeneral = array ("Asignar Permisos", "asignarpermisos", "crear","permisostipousu");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[ 
        {"Tipo":"select2","Descripcion":"Usuario","Nombre":"name","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }
                  ]'; 
    public function __construct() {
        $this->middleware('auth');
    } 
    public function permisostipousu(){
        $id=Input::get("id");        
        $menutodo=intval(Input::get("menutodo"));
        $tipobus=Input::get("tipobus");
        $ruta=Input::get("ruta");
        $submit=0;
        //return $id;
        if($menutodo==1)
        {
            $idmainper=$id;
             $tabla=  MenuModel::orderby("orden")->get();
             $submit=1;
        }
         else
         {  $idmainper=0;
            if($tipobus=="usuario")
            {
              $tabla= MenuModel::whereIn("id",function($query) use($id){
                $query->select('idmenu')
                ->from(with(new AsignarPermisosModel)->getTable())                
                ->where('idusuario', $id);
              })->orderby("orden")->get();
            }else{
              $tabla= MenuModel::whereIn("id",function($query) use($id){
                $query->select('idmenu')
                ->from(with(new PerfilPermisosModel)->getTable())                
                ->where('idperfil', $id);
              })->orderby("orden")->get();
            }           
        }
        //show($tabla->toarray());
            /*echo "<pre>";
            print_r($tabla);
            die();*/
            //var_dump($menupermiso);
            // show the view and pass the nerd to it
            return view('permisosmenu.mainshow',[
                "total"=>$tabla->count(),
                "tabla"=>$tabla,
                "idmainper"=>$idmainper,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "submit"=>$submit,
                "idsi"=>$id,
                "ruta"=>$ruta,
                "create"=>'si',
                "delete"=>'si'
                ]);
    }
    public function guardar($id)
        {            
            $input=Input::all();
            //show($input);

            $ruta=$this->configuraciongeneral[1];
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new AsignarPermisosModel;
                 $msg="Registro Creado Exitosamente...!";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= AsignarPermisosModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
            }
            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, AsignarPermisosModel::rules());
            
            if ($validator->fails()) {
                //die($ruta);
              if(!Input::has("idmenumain") && Input::has("name"))
              { 
                $del=AsignarPermisosModel::where('idusuario', '=', Input::get('name'))->delete(); 
                Session::flash('message', "Todos los permisos fueron eliminados para el Tipo de Usuario");
                return Redirect::to($this->configuraciongeneral[1]);
              }else{
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
              }
            }else {
                //die($ruta);
                        $sw=0;
            $permimenu = Input::get('idmenumain');
                        $tipo=Input::get('name');
                        /*echo "<pre>";
                        echo "Usuario: $usuario <br>";
                        print_r($permimenu);
                        die();*/
                        //DB::table('menutipousuario')->where('idtipousuario', '=', $tipo)->delete();
                        $del=AsignarPermisosModel::where('idusuario', '=', $tipo)->delete();
                        //print_r($permimenu);
//                        if(is_array($permimenu))
  //                      {
                            foreach ($permimenu as $valor) {
                                echo "Menu:  $valor <br>";
                                $permisos = new AsignarPermisosModel;
                                $permisos->idusuario  = $tipo;
                                $permisos->idmenu     = $valor;
                                /*$verpa= DB::table('menutipousuario')->where(function($query)use($valor,$tipo){
                                    $query->where("idtipousuario",$tipo)->where("idmenu",$valor);*/
                                $verpa= AsignarPermisosModel::where(function($query)use($valor,$tipo){
                                    $query->where("idusuario",$tipo)->where("idmenu",$valor);

                                })->first();
                                if(!$verpa)
                                {
                                    $permisos->save();
                                }
                                $sw=1;
                                /*Ver padre del hijo*/
                                //$verpa= DB::table('menu')->where("id",$valor)->first();
                                $verpa=MenuModel::where("id",$valor)->first();
                                $idpa=intval($verpa->idmain);
                                $verpa= AsignarPermisosModel::where(function($query)use($idpa,$tipo){
                                    $query->where("idusuario",$tipo)->where("idmenu",$idpa);
                                })->first();
                                
                                if(!$verpa && $idpa!=0)
                                {
                                    $permisos = new AsignarPermisosModel;
                                    $permisos->idusuario  = $tipo;
                                    $permisos->idmenu     = $idpa;
                                    
                                    $permisos->save();
                                }
                                  
                            }
                 //MisFunciones::auditoria("Usuarios Nuevo: ",Input::get('name'));
                // redirect
            Auditoria("Asignación de Permisos por Usuario"." - ID: ". $tipo);
             Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
            }             
        }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $main=UsuariosModel::orderby("name")->lists("name","id")->all();
        $objetos=json_decode($this->objetos);
        $objetos[0]->Valor= $this->escoja + $main;
        //show($objetos);        
        //$objetos=array_values($objetos);                
        $tabla=array();//->paginate(500);
        return view('permisosmenu.createindex',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,                
                //"checkmenu"=>"si",
                "validarjs"=>array()
                ]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $idbus=Input::get("id");
        //$idbus=Tipo_Usuario::where("id",$idbus)->lists("id")->all();
        //show($idbus);
        $main=UsuariosModel::orderby("name")->lists("name","id")->all();
        $objetos=json_decode($this->objetos);
        $objetos[0]->Valor= $this->escoja + $main;
        $objetos[0]->ValorAnterior= $idbus;

        //show($objetos[0]);
        //$objetos=array_values($objetos);                
        $tabla=array();//->paginate(500);
        return view('permisosmenu.createindex',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "checkmenu"=>"si",
                "validarjs"=>array(),
                "menutodo"=>"si",
                "idbus"=>$idbus
                ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
