<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Config;
use DB;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Auth;
//use App\ReportesJasperController;
class ReportController extends Controller
{        
    /**
     * ReportController constructor. middleware for authenticate users
     */
    var $escoja=array(0=>"Escoja opción...") ;
    protected $ReportJasper;
    public function __construct(ReportesJasperController $ReportJasper) {
        $this->middleware('auth',['except' => ['reportescoordinacionactividades']]);
         $this->ReportJasper = $ReportJasper;
    }
    public function generarreporte()
    {
        $input=Input::all();        
        $data=json_decode(Input::get("data"));
        $namedata=Crypt::decrypt(Input::get("namedata"));
        $paramadi=Crypt::decrypt(Input::get("paramadi"));
        //show(array($paramadi,$data,$namedata));        
        $paramsok=array();
        foreach ($data as $key => $value) {
            # code...
            //show($value);
            if($value->name!="_token")
                $paramsok[$value->name]=$value->value;
        }
        //show($paramsok);
        //$name, $params, $format, $filename, $download, $download_is_attachment = true
        return $this->ReportJasper->generateReport($namedata,$paramsok+$paramadi,"pdf",$namedata,true,false);
    }
    public function reportescoordinacionactividades()
    {
        if(Input::get("token"))
            return $this->generarreporte();
        //Objetos a Armar
        $objects = json_decode('[
            {"Tipo": "select", "Descripcion": "Dirección", "Nombre": "IDDIRECCION", "Clase": "chosen-select", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Prioridad", "Nombre": "PRIORIDAD", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "select", "Descripcion": "Estado", "Nombre": "ESTADO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha Inicio", "Nombre": "FECHAINICIO", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Fecha hasta", "Nombre": "FECHAFINAL", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Actualización Desde", "Nombre": "FECHAACTUDESDE", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"},
            {"Tipo": "datetext", "Descripcion": "Actualización Hasta", "Nombre": "FECHAACTUHASTA", "Clase": "Null", "Valor": "Null", "ValorAnterior": "Null"}
        ]');
        /*COMBOS*/
        $direcciones=direccionesModel::join("coor_tmov_cronograma_cab as a","a.id_direccion","=","tmae_direcciones.id")
        ->select("tmae_direcciones.direccion","tmae_direcciones.id",DB::raw("count(*) as total"))
        ->groupBy("tmae_direcciones.direccion","tmae_direcciones.id")
        ->orderby("tmae_direcciones.direccion")
        ->lists("direccion","id")
        ->all();

        $prioridad=explodewords(ConfigSystem("prioridad"),"|");
        
        $estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
        $fechaini=date('Y')."-01-01";
        $fechafin=date('Y')."-12-31";
        $objects[0]->Valor=$this->escoja + $direcciones;
        $objects[1]->Valor=$this->escoja + $prioridad;
        $objects[2]->Valor=$this->escoja + $estadoobras;
        $objects[3]->Valor=$fechaini;
        $objects[4]->Valor=$fechafin;
        $objects[5]->Valor=$fechaini;
        $objects[6]->Valor=$fechafin;        
        /**/
        $params=array();
        foreach ($objects as $key => $value) {
            # code...
            $params[]=$value->Nombre;
        }
        //show($params);
        $validate_js = [];        
        $general_config = ['Reporte general de Actividades por Rendimiento', 'reportescoordinacionactividades','rpt_actividades'];                
        $paramadi=array("TITULO"=>$general_config[0],"USUARIO"=>Auth::user()->name);
        return view('reports.create',[
            'objects' => $objects,
            'general_config' => $general_config,
            'validarjs' => $validate_js,
            'paramadi' =>Crypt::encrypt($paramadi)
        ]);
        //Crypt::decrypt
    }
}
