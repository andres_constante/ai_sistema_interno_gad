<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\ArchivosModel;
use Image;

class ArchivosController extends Controller
{
    //
    var $configuraciongeneral = array ("Gestor de Archivos", "subirarchivos", "index");
    private $archivos_path;
    public function __construct() {
        $this->middleware('auth');
        $this->archivos_path = public_path('archivos_sistema');

    }
    
    /**
     * Display all of the images.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $archivos = Upload::all();
        // return view('vistas.createarchivos', compact('archivos'));
    }
 
    /**
     * Show the form for creating uploading new images.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->configuraciongeneral[4]=$request->id;
        $this->configuraciongeneral[5]=$request->tipo;
        return view('vistas.createarchivos',[
            "configuraciongeneral"=>$this->configuraciongeneral
    ]);
    }
 
    /** 
     * Saving images uploaded through XHR Request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // show("aqui");   
        $archivos = $request->file('file');
        //show($archivos);
        if (!is_array($archivos)) {
            $archivos = [$archivos];
        }
 
        if (!is_dir($this->archivos_path)) {
            mkdir($this->archivos_path, 0777);
        }
 
        for ($i = 0; $i < count($archivos); $i++) {
            $archivo = $archivos[$i];
            $name = 'Archivo_Ref_'.$request->id_referencia.'_tipo_'.$request->tipo.'_'.sha1(date('YmdHis') . str_random(10));
            $save_name = $name . '.' . $archivo->getClientOriginalExtension();
            $resize_name = $name . str_random(2) . '.' . $archivo->getClientOriginalExtension();
 
            $tipo_archivo=$archivo->getClientOriginalExtension();
            if($tipo_archivo=='png'||$tipo_archivo=='jpg'){
                $ima=Image::make($archivo)
                ->resize(1124,null, function ($constraints) {
                    $constraints->aspectRatio();
                })
                ->save($this->archivos_path . '/' . $resize_name);

            }else{
               $archivo->move($this->archivos_path,$resize_name);
            }
            
            
                
           
 
            $upload = new ArchivosModel();
            $upload->tipo =  $request->tipo;
            $upload->id_referencia =  $request->id_referencia;
            $upload->nombre = basename($archivo->getClientOriginalName());
            $upload->tipo_archivo=$archivo->getClientOriginalExtension();;
            $upload->ruta=$resize_name;
            $upload->save();
        }
        return response()-> json([
            'message' => 'OK'
        ], 200);
    }
 
    /**
     * Remove the images from the storage.
     *
     * @param Request $request
     */
    public function destroy(Request $request)
    {
        $filename = $request->id;
        $uploaded_image = Upload::where('original_name', basename($filename))->first();
 
        if (empty($uploaded_image)) {
            return Response::json(['message' => 'Sorry file does not exist'], 400);
        }
 
        $file_path = $this->archivos_path . '/' . $uploaded_image->filename;
        $resized_file = $this->archivos_path . '/' . $uploaded_image->resized_name;
 
        if (file_exists($file_path)) {
            unlink($file_path);
        }
 
        if (file_exists($resized_file)) {
            unlink($resized_file);
        }
 
        if (!empty($uploaded_image)) {
            $uploaded_image->delete();
        }
 
        return Response::json(['message' => 'File successfully delete'], 200);
    }




    
    


}
