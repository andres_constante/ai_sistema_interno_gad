<?php

namespace App\Http\Controllers;

use PHPJasper\PHPJasper;
use Config;
use Illuminate\Support\Facades\Input;

/**
 * Class Report
 * This class is a helper to generate reports using Jasper Reports from PHP.
 * @package app\controllers
 */
class Report
{
    /**
     * Generate a new UID.
     * @return string
     */
    private function newUID()
    {
        $s = mb_strtoupper(md5(uniqid(rand(),true)));
        $uid_text =
            substr($s, 0, 8) . '-' .
            substr($s, 8, 4) . '-' .
            substr($s, 12, 4) . '-' .
            substr($s, 16, 4) . '-' .
            substr($s, 20);
        return $uid_text;
    }

    /**
     * Generate report.
     * @param string $name Report name
     * @param array $params Report parameters
     * @param string $format Report format
     * @param string $filename Report filename
     * @param bool $download Download option
     * @param bool $download_is_attachment Download as attachment
     * @return array|null
     * @throws
     */
    public function & generate($name, $params, $format, $filename, $download, $download_is_attachment = true)
    {
        //show(array($name, $params, $format, $filename, $download, $download_is_attachment));
        $result = null;
        $input = base_path() . '/reports/' . $name . '.jasper';
        $output = public_path() . '/temp/' . $this->newUID();
        $db = Config::get('database')["connections"]["mysql"];

        $options = [
            'format' => [$format],
            'locale' => Config::get('app.locale'),
            'params' => $params,
            'db_connection' => [
                'driver' => $db["driver"],
                'username' => $db["username"],
                //'password' => $db["password"],
                'host' => $db["host"],
                'database' => $db["database"],
                'port' => $db["port"]
            ]
        ];
        /*show(array($input,
                $output,
                $options));
        */
        try {
            $jasper = new PHPJasper;

            $jasper->process(
                $input,
                $output,
                $options
            );
            //error_log('DEBUG JASPER OUTPUT => ' . $jasper->output());
            //show($jasper);
            $jasper->execute();            
            $output .= '.' . $format;

            if ($download) {
                $response = response(file_get_contents($output), 200);
                $response->header('Content-Type', mime_content_type($output));
                $response->header('Content-disposition', ($download_is_attachment ? 'attachment' : 'inline') . '; filename=' . $filename . '.' . $format);
                $response->send();
                unlink($output);

            } else {
                $result = [
                    'type' => mime_content_type($output),
                    'name' => $filename . '.' . $format,
                    'data' => base64_encode(file_get_contents($output)),
                ];
                unlink($output);
            }
            return $result;
        }
        catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * Download report.
     * @param string $name Report name
     * @param array $params Report parameters
     * @param string $format Report format
     * @param string $filename Report filename
     * @param bool $download_is_attachment  Download as attachment
     * @throws \yii\web\HttpException
     */
    public function download($name, $params, $format, $filename, $download_is_attachment = true)
    {
        $this->generate($name, $params, $format, $filename, true, $download_is_attachment);
    }

    /**
     * Download as data.
     * @param string $name Report name
     * @param array $params Report parameters
     * @param string $format Report format
     * @param string $filename Report filename
     * @return array|null Report data
     * @throws \yii\web\HttpException
     */
    public function & asData($name, $params, $format, $filename)
    {
        return $this->generate($name, $params, $format, $filename, false);
    }
}

class ReportesJasperController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    } 
    /**
     * @var array Report data.
     */
    private $reportData;

    /**
     * Render report in a view.
     * @return string
     */
    public function report()
    {
        if ($this->reportData) {
            $report_data = &$this->reportData;
            return '<script>saveAs(new Blob([base64toBlob("' . $report_data['data'] . '", "' . $report_data['type'] . '")], {type: "' . $report_data['type'] . '"}), "' . $report_data['name'] . '");</script>';
        }
    }

    /**
     * Generate report.
     * @param string $name Report name
     * @param array $params Report parameters
     * @param string $format Report format
     * @param string $filename Report filename
     * @param bool $download Download option
     * @param bool $download_is_attachment Download as attachment
     * @throws \yii\web\HttpException
     */
    public function generateReport($name, $params, $format, $filename, $download, $download_is_attachment = true)
    {
        $report = new Report();
        $this->reportData = &$report->generate(
            $name,
            $params,
            $format,
            $filename,
            $download,
            $download_is_attachment
        );
    }

    public function reporteActividades()
    {
        $id = intval(Input::get("id"));
        if (($id = intval($id)) > 0) {
            $this->generateReport(
                'ReporteActividades',
                [
                    'ID_DIRECCION' => $id
                ],
                'pdf',
                'Reporte de actividades',
                true,
                false
            );
        }
    }
}
   