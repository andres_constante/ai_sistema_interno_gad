<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\UsuarioModuloModel;
use App\ModulosModel;
use App\UsuariosModel;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use Auth;
use DB;

class UsuarioModuloControler extends Controller
{
    var $configuraciongeneral = array ("Usuario Modulo", "usuariomodulo", "index",6=>"null");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[ 
        {"Tipo":"select","Descripcion":"Menu Modulo","Nombre":"id_modulo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI"  },
        {"Tipo":"select-multiple","Descripcion":"Usuario","Nombre":"id_usuario","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }
                            ]';

    var $validarjs =array(
            "id_usuario"=>"id_usuario: {
                            required: true
                        }",
            "id_modulo"=>"id_modulo: {
                            required: true
                        }"
        );
        public function __construct() {
            $this->middleware('auth');
        } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objetos=json_decode($this->objetos);
        $objetos[0]->Nombre="modulo";
        $objetos[1]->Nombre="usuario";
        $tabla=UsuarioModuloModel::
            join("ad_modulos as mo","mo.id","=","ad_usuario_modulo.id_modulo")    
            ->join("users as u","u.id","=","ad_usuario_modulo.id_usuario") 
            ->select("ad_usuario_modulo.id","u.name as usuario","mo.nombre as modulo")
        ->orderby("u.name")->select("ad_usuario_modulo.id","u.name as usuario","mo.nombre as modulo")->get();


        return view('vistas.index',[
            "objetos"=>$objetos,
            "tabla"=>$tabla,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "delete"=>"si",
            "create"=>'si'
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->configuraciongeneral[2]="crear";
        $objetos=json_decode($this->objetos);
        $modulo=ModulosModel::where("estado","ACT")->lists("nombre","id")->all();
        //$usuario=UsuariosModel::where("estado","ACT")->orderby("name")->lists("name","id")->all();
        $usuario=UsuariosModel::select("users.id",DB::raw("concat(users.name,IFNULL((select concat(' - ',direccion) from tmae_direcciones where id=users.id_direccion),'')) as name"))
            ->where("users.estado","ACT")
            ->orderby("users.name")->lists("name","id")->all();
        //show($usuario);
        $objetos[0]->Valor=$modulo;
        $objetos[1]->Valor=$usuario;
        return view('vistas.create',[
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->configuraciongeneral[2]="editar";
        $objetos=json_decode($this->objetos);
        $modulo=ModulosModel::where("estado","ACT")->lists("nombre","id")->all();
        $usuario=UsuariosModel::where("estado","ACT")->orderby("name")->lists("name","id")->all();
        $objetos[0]->Valor=$modulo;
        $objetos[1]->Valor=$usuario;
        $tabla=UsuarioModuloModel::find($id);
        return view('vistas.create',[
            "tabla"=>$tabla,
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }
public function guardar($id)
    {         
           $input=Input::all();

            $ruta=$this->configuraciongeneral[1];

            $msg="Registro Actualizado Exitosamente...!";
            $msgauditoria="Edición Variable de Configuración";
            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, UsuarioModuloModel::rules($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                 $sel=Input::get("id_usuario");
                 foreach ($sel as $key => $value) {
                     # code...
                    $guardar=UsuarioModuloModel::where("id_usuario",$value)->where("id_modulo",Input::get("id_modulo"))->first();
                    if(!$guardar)
                        $guardar= new UsuarioModuloModel;
                    $guardar->id_modulo=Input::get("id_modulo");
                    $guardar->id_usuario=$value;
                    $guardar->save();
                 }
                 
                 //Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->nombre));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla=UsuarioModuloModel::find($id);        
            //->update(array('estado' => 'INACTIVO'));
        $tabla->delete();        
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
