<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\MenuModel;
use App\PerfilPermisosModel;
use App\PerfilModel;
use App\UsuariosModel;
use App\AsignarPermisosModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;

class AsignarPermisosPerfilController extends Controller
{
    var $configuraciongeneral = array ("Asignar Permisos Perfil", "perfilpermisosasignar", "crear","permisostipousu");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[ 
        {"Tipo":"select2","Descripcion":"Perfil","Nombre":"perfil","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }
                  ]'; 
    public function __construct() {
        $this->middleware('auth');
    } 
    public function guardar($id)
        {            
            $input=Input::all();
            //show($input);

            $ruta=$this->configuraciongeneral[1];
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new PerfilPermisosModel;
                 $msg="Registro Creado Exitosamente...!";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= PerfilPermisosModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
            }
            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, PerfilPermisosModel::rules());
            
            if ($validator->fails()) {
                //die($ruta);
              if(!Input::has("idmenumain") && Input::has("name"))
              { 
                $del=PerfilPermisosModel::where('idperfil', '=', Input::get('perfil'))->delete(); 
                Session::flash('message', "Todos los permisos fueron eliminados para el perfil.");
                return Redirect::to($this->configuraciongeneral[1]);
              }else{
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
              }
            }else {
                //die($ruta);
                        $sw=0;
            $permimenu = Input::get('idmenumain');
                        $tipo=Input::get('perfil');
                        /*echo "<pre>";
                        echo "Usuario: $usuario <br>";
                        print_r($permimenu);
                        die();*/
                        //DB::table('menutipousuario')->where('idtipousuario', '=', $tipo)->delete();
                        $del=PerfilPermisosModel::where('idperfil', '=', $tipo)->delete();
                        //print_r($permimenu);
//                        if(is_array($permimenu))
  //                      {                        
                            foreach ($permimenu as $valor) {
                                echo "Menu:  $valor <br>";
                                $permisos = new PerfilPermisosModel;
                                $permisos->idperfil  = $tipo;
                                $permisos->idmenu     = $valor;
                                /*$verpa= DB::table('menutipousuario')->where(function($query)use($valor,$tipo){
                                    $query->where("idtipousuario",$tipo)->where("idmenu",$valor);*/
                                $verpa= PerfilPermisosModel::where(function($query)use($valor,$tipo){
                                    $query->where("idperfil",$tipo)->where("idmenu",$valor);

                                })->first();
                                if(!$verpa)
                                {
                                    $permisos->save();
                                }
                                $sw=1;
                                /*Ver padre del hijo*/
                                //$verpa= DB::table('menu')->where("id",$valor)->first();
                                $verpa=MenuModel::where("id",$valor)->first();
                                $idpa=intval($verpa->idmain);
                                $verpa= PerfilPermisosModel::where(function($query)use($idpa,$tipo){
                                    $query->where("idperfil",$tipo)->where("idmenu",$idpa);
                                })->first();
                                
                                if(!$verpa && $idpa!=0)
                                {
                                    $permisos = new PerfilPermisosModel;
                                    $permisos->idperfil  = $tipo;
                                    $permisos->idmenu     = $idpa;
                                    
                                    $permisos->save();
                                }
                                  
                            }
                            //Actualizar por permisos asignados a usuarios con este perfil
                            if($sw==1)
                            {
                                //Permisos por Perfil
                                $perfil= Input::get('perfil');
                                if($perfil)
                                {                                       
                                    AsignarPermisosModel::where('idusuario', function($query) use($perfil)
                                        {
                                            $query->select('id')
                                                ->from(with(new UsuariosModel)->getTable())
                                                ->where('id_perfil',$perfil);
                                        })->delete();
                                        $user= UsuariosModel::where('id_perfil',$perfil)->get();
                                        foreach ($user as $key => $value) {
                                            $permiper= PerfilPermisosModel::where("idperfil",$perfil)
                                                ->select(array('idmenu',DB::raw($value->id. " as idusuario")));
                                            $bindings = $permiper->getBindings();
                                            $insert = 'INSERT into ad_menuusuario (idmenu,idusuario) '
                                                    . $permiper->toSql();
                                            DB::insert($insert, $bindings);
                                        }
                                }
                                Auditoria("Asignación de Permisos a Perfil. ID Perfil $perfil");
                            }
                Auditoria("Asignación de Permisos por Perfil"." - ID: ". $tipo);   
                 //MisFunciones::auditoria("Usuarios Nuevo: ",Input::get('name'));
                // redirect
             Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
            }             
        }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $main=PerfilModel::orderby("perfil")->lists("perfil","id")->all();
        $objetos=json_decode($this->objetos);
        $objetos[0]->Valor= $this->escoja + $main;
        //show($objetos);        
        //$objetos=array_values($objetos);                
        $tabla=array();//->paginate(500);
        return view('permisosmenu.createindex',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,                
                //"checkmenu"=>"si",
                "validarjs"=>array(),
                "create"=>'si',
                "delete"=>'si'
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $idbus=Input::get("id");
        //$idbus=Tipo_Usuario::where("id",$idbus)->lists("id")->all();
        //show($idbus);
        $main=PerfilModel::orderby("perfil")->lists("perfil","id")->all();
        $objetos=json_decode($this->objetos);
        $objetos[0]->Valor= $this->escoja + $main;
        $objetos[0]->ValorAnterior= $idbus;

        //show($objetos[0]);
        //$objetos=array_values($objetos);                
        $tabla=array();//->paginate(500);
        return view('permisosmenu.createindex',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "checkmenu"=>"si",
                "validarjs"=>array(),
                "menutodo"=>"si",
                "idbus"=>$idbus
                ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
