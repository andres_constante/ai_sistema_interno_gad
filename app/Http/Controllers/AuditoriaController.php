<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\AuditoriaModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
class AuditoriaController extends Controller
{
    var $configuraciongeneral = array ("Auditoría del sistema", "auditoria", "index",6=>'auditoriaajax');
    var $objetos = '[ {"Tipo":"select","Descripcion":"Usuario","Nombre":"idusuario","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
                  {"Tipo":"text","Descripcion":"Acción","Nombre":"accion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"URL","Nombre":"urlmenu","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"IP","Nombre":"ip","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"PC","Nombre":"nompc","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"Fecha","Nombre":"created_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
                  ]';
//https://jqueryvalidation.org/validate/
    var $validarjs =array(
            "campo"=>"campo: {
                            required: true
                        }",
            "valor"=>"valor: {
                            required: true
                        }"
        );
    public function __construct() {
        $this->middleware('auth');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $tabla= AuditoriaModel::join("users as b","ad_auditoria.idusuario","=","b.id")
        //             ->select("ad_auditoria.*","b.name")
        //             ->orderby("ad_auditoria.created_at")
        //             ->get();
        $objetos=json_decode($this->objetos);
        $objetos[0]->Nombre='name';
        //show($objetos);
        $tabla=[];
        return view('vistas.index',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "create"=>'si',
                "delete"=>'si'
                ]);
    }
    public function auditoriaajax(Request $request)
    {
      $columns = array( 
                            0 =>'id', 
                            1 =>'name',
                            2=> 'accion',
                            3=> 'urlmenu',
                            4=> 'ip',
                            5=> 'nompc',
                            6=> 'created_at',
                            7=> 'acciones',
                        );
  
        $totalData = AuditoriaModel::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = AuditoriaModel::join("users as b","ad_auditoria.idusuario","=","b.id")
                        ->select("ad_auditoria.*","b.name")          
                        ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $posts =  AuditoriaModel::join("users as b","ad_auditoria.idusuario","=","b.id")
                            ->select("ad_auditoria.*","b.name")
                            ->where('id','LIKE',"%{$search}%")
                            ->orWhere('url', 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("name"), 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("accion"), 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = AuditoriaModel::join("users as b","ad_auditoria.idusuario","=","b.id")
                                ->select("ad_auditoria.*","b.name")
                            ->where('id','LIKE',"%{$search}%")
                             ->orWhere('url', 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("name"), 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("accion"), 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            //show($posts);
            foreach ($posts as $post)
            {
                $aciones='';

                $nestedData['id'] = $post->id;
                $nestedData['name'] = $post->name;
                $nestedData['accion'] = $post->accion;
                $nestedData['urlmenu'] = $post->urlmenu;
                $nestedData['ip'] = $post->ip;
                $nestedData['nompc'] = $post->nompc;
                $nestedData['created_at'] = $post->created_at->format('Y-m-d');
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
              
            }
        }
        //show($data);
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
        
        return response()->json($json_data); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
