<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\UsuariosModel;
use App\PerfilModel;
use DB;
use Auth;
use App\AsignarPermisosModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use App\UsuarioModuloModel;
use App\ModulosModel;
use Illuminate\Support\Facades\Crypt;
class UsuariosController extends Controller
{
    var $configuraciongeneral = array ("Usuarios del Sistema", "usuarios", "index",6=>'usuariosajax'); 
    
    
    var $objetos = '[ {"Tipo":"text","Descripcion":"Cédula","Nombre":"cedula","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
                    {"Tipo":"text","Descripcion":"Nombres","Nombre":"name","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"Teléfono","Nombre":"telefono","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"Email","Nombre":"email","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"datetext","Descripcion":"Fecha de Nacimiento","Nombre":"fecha_nacimiento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"password","Descripcion":"Contraseña","Nombre":"password","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"password","Descripcion":"Confirmar Contraseña","Nombre":"password_confirmation","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"select","Descripcion":"Perfil","Nombre":"id_perfil","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"select","Descripcion":"Dirección / Área","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"text","Descripcion":"Cargo","Nombre":"cargo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"select-multiple","Descripcion":"Módulo","Nombre":"id_modulo","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null"},
                  {"Tipo":"select","Descripcion":"Reset","Nombre":"nuevo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
                  ]';
    var $escoja=array(null=>"Escoja opción...") ;
    //var $index
//https://jqueryvalidation.org/validate/
    var $validarjs =array(
            "cedula"=>"cedula: {
                            required: true
                        }",
            "name"=>"name: {
                            required: true
                        }",
            "telefono"=>"telefono: {
                            required: true
                        }",
            "email"=>"email: {
                            required: true
                        }",
            "fecha_nacimiento"=>"fecha_nacimiento: {
                            required: true
                        }",
            "password"=>"password: {
                            required: true
                        }",
            "password_confirmation"=>"password_confirmation: {
                            required: true
                        }"
 
        );
    public function __construct() {
        $this->middleware('auth');
    } 
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        //
        $objetos=json_decode($this->objetos);        
        $objetos[7]->Nombre="perfil";
        $objetos[8]->Nombre="direccion";
        unset($objetos[5]);

        unset($objetos[6]);
        unset($objetos[10]);
        //             ->select("users.*","c.tipo_usuario",
        //                     DB::raw("(select perfil from ad_perfil where id=users.id_perfil) as perfil"),
        //                     DB::raw("(select direccion from tmae_direcciones where id=users.id_direccion) as direccion"))
        //             ->where("users.estado","ACT")
        //             ->get();
        $tabla=[];
        return view('vistas.index',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "delete"=>"si",
                "create"=>"si"
                ]);
    }
    public function usuariosajax(Request $request)
    {
      $columns = array( 
                            0 =>'id', 
                            1 =>'cedula',
                            2=> 'name',
                            3=> 'telefono',
                            4=> 'email',
                            5=> 'fecha_nacimiento',
                            6=> 'perfil',
                            7=> 'direccion',
                            8=> 'cargo',
                            9=> 'nuevo',
                            10=> 'acciones',
                        );
  
        $totalData = UsuariosModel::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = UsuariosModel::
                            select('users.*','dir.direccion','ad.nombre_perfil as perfil') 
                            ->join("ad_perfil as ad","users.id_perfil","=","ad.id")
                            ->join('tmae_direcciones as dir','dir.id','=','users.id_direccion')             
                        ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $posts =  UsuariosModel::
                            select('users.*','dir.direccion','ad.nombre_perfil as perfil') 
                            ->join("ad_perfil as ad","users.id_perfil","=","ad.id")
                            ->join('tmae_direcciones as dir','dir.id','=','users.id_direccion')
                            ->where('users.id','LIKE',"%{$search}%")
                            ->orWhere('cedula', 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("name"), 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("dir.direccion"), 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = UsuariosModel::
                             select('users.*','dir.direccion','ad.nombre_perfil as perfil') 
                            ->join("ad_perfil as ad","users.id_perfil","=","ad.id")
                            ->join('tmae_direcciones as dir','dir.id','=','users.id_direccion')
                            ->where('users.id','LIKE',"%{$search}%")
                             ->orWhere('cedula', 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("name"), 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("dir.direccion"), 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $aciones=link_to_route('usuarios.show','', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                link_to_route('usuarios.edit','', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.Crypt::encrypt($post->id).')"><i class="fa fa-trash"></i></a> ';

                $nestedData['id'] = $post->id;
                $nestedData['cedula'] = $post->cedula;
                $nestedData['name'] = $post->name;
                $nestedData['telefono'] = $post->telefono;
                $nestedData['email'] = $post->email;
                $nestedData['fecha_nacimiento'] = $post->fecha_nacimiento;
                $nestedData['perfil'] = $post->perfil;
                $nestedData['direccion'] = $post->direccion;
                $nestedData['cargo'] = $post->cargo;
                $nestedData['nuevo'] = $post->nuevo;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
                
            }
        }
        //show($data);
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
        
        return response()->json($json_data); 
    }


     /* Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $id=Crypt::decrypt($id);
        $objetos=json_decode($this->objetos);        
        $objetos[7]->Nombre="perfil";
        $objetos[8]->Nombre="direccion";
        unset($objetos[4]);
        unset($objetos[5]);
        unset($objetos[10]);
        unset($objetos[11]);
        $objetos=array_values($objetos);
        //show($objetos);
        $tabla = UsuariosModel::
        join("ad_perfil as ad","users.id_perfil","=","ad.id")
        ->select("users.*","ad.nombre_perfil",
        DB::raw("ad.nombre_perfil as perfil"),
      DB::raw("(select direccion from tmae_direcciones where id=users.id_direccion) as direccion"))
        ->where("users.id",$id)
        ->first();
        return view('vistas.show',[
            "objetos"=>$objetos,
            "tabla"=>$tabla,
            "configuraciongeneral"=>$this->configuraciongeneral
            ]);
        }

        
        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
           
            $this->configuraciongeneral[2]="crear";            
            $direccion=direccionesModel::where("estado","ACT")->lists("direccion","id")->all();
            $perfil=PerfilModel::lists("nombre_perfil","id")->all();
            $modulo=ModulosModel::where("estado","ACT")->lists("nombre","id")->all();
            $objetos=json_decode($this->objetos);
                    
            $objetos[7]->Valor=$this->escoja + $perfil;
            $objetos[8]->Valor=$this->escoja + $direccion;
            $objetos[10]->Valor=$modulo;
            $objetos[11]->Valor=[0=>'NO',1=>'SI'];
            return view('vistas.create',[
                    "objetos"=>$objetos,
                    "configuraciongeneral"=>$this->configuraciongeneral,
                    "validarjs"=>$this->validarjs
                    ]);
                }


        public function guardar($id)
        {         
            $input=Input::all();
            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new UsuariosModel;
                $msg="Registro Creado Exitosamente...!";
                $msgauditoria="Registro Variable de Configuración";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= UsuariosModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición Variable de Configuración";
            }
            
            $input=Input::all();
            $arrapas=array();
            $arrayval=UsuariosModel::rules($id);
            $validator = Validator::make($input, $arrayval);
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
            }else {
                unset($input["password_confirmation"]);
                //show($input);
                foreach($input as $key => $value)
                {
                    
                    if($key != "_method" && $key != "_token" & $key != "id_modulo")
                    {
                        if($key=="password")
                        {
                            if(trim(Input::get("password"))!="")
                            $guardar->$key = bcrypt($value);
                        }else{
                            $guardar->$key = $value;
                        }
                    }                        
                 }
                 //$guardar->nuevo=0;
                 $guardar->save();
                 if($id==0)
                    $id=$guardar->id;
                 /*Agregra Modulo*/
                 if(Input::has("id_modulo"))
                 {
                    $modulo=Input::get("id_modulo");
                    foreach ($modulo as $key => $value) {
                          # code...
                        $guardar=UsuarioModuloModel::where("id_modulo",$value)->where("id_usuario",$id)->first();
                        if(!$guardar)
                            $guardar= new UsuarioModuloModel;
                        //show($guardar);
                        $guardar->id_modulo=$value;
                        $guardar->id_usuario=$id;
                        $guardar->save();
                      }  
                 }                 
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get("cedula"));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $id=Crypt::decrypt($id);
        $this->configuraciongeneral[2]="editar";        
        $perfil=PerfilModel::lists("nombre_perfil","id")->all();
        $direccion=direccionesModel::where("estado","ACT")->lists("direccion","id")->all();
        $objetos=json_decode($this->objetos);        
        $objetos[7]->Valor=$this->escoja + $perfil;
        $objetos[8]->Valor=$this->escoja + $direccion;
       
        $objetos[11]->Valor=[0=>'NO',1=>'SI'];
        $objetos[11]->ValorAnterior=Auth::user()->nuevo;
        
        $tabla=UsuariosModel::find($id);        
        $validarjs=$this->validarjs;
        unset($validarjs["password"]);
        unset($validarjs["password_confirmation"]);
        $modulo=ModulosModel::where("estado","ACT")->lists("nombre","id")->all();
        $modulosasi=UsuarioModuloModel::join("ad_modulos as a","a.id","=","ad_usuario_modulo.id_modulo")
        ->where("ad_usuario_modulo.id_usuario",$id)
        ->select("a.nombre","a.id")
        ->lists("id","id")
        ->all();
        $objetos[10]->Valor=$modulo;
        $objetos[10]->ValorAnterior=$modulosasi;
        //show($validarjs);
        return view('vistas.create',[
                "tabla"=>$tabla,
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$validarjs
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $id=Crypt::decrypt($id);
        $tabla=UsuariosModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }

    public function getUsers(){
        $cadena=Input::get("cadena");
        //show($cadena);
        /*$valores = UsuariosModel::join("ad_tipousuario as c","users.id_tipo_usuario","=","c.id")
                    ->select("users.*","c.tipo_usuario",
                            DB::raw("(select perfil from ad_perfil where id=users.id_perfil) as perfil"),
                            DB::raw("(select direccion from tmae_direcciones where id=users.id_direccion) as direccion"))
                    ->where([["users.estado","ACT"],['users.name', 'like', '%'.$cadena.'%']])
                    ->get();*/
        $valores= UsuariosModel::join("ad_perfil as a","a.id","=","users.id_perfil")
        ->select("users.*","a.nombre_perfil as perfil",                            
            DB::raw("(select direccion from tmae_direcciones where id=users.id_direccion) as direccion"))
        ->where([["users.estado","ACT"],['users.name', 'like', '%'.$cadena.'%']])
            ->get();
        return response()->json($valores);
    }

    
}
