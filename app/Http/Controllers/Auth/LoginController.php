<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected function authenticated(Request $request, $user)
    {
    	$fecha=new DateTime();
        $user->update([
            'last_login_at' => $fecha,
            'last_login_ip' => $request->getClientIp()
        ]);
    }
}