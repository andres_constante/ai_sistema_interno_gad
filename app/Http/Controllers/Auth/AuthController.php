<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Session;
use Auth;
use Illuminate\Http\Request;
use App\ModulosModel;
use App\UsuarioModuloModel;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'moduleschoice';
    protected $loginPath = 'auth/login';
    protected $redirectAfterLogout = 'auth/login';
    //La siguiente funcion fue agregada para Obtener el último Inicio de Sesión
    protected function authenticated(Request $request, $user)
    {        
        $user->update([
            'last_login_at' => date("Y-m-d")." ".date("H:i:s"),
            'last_login_ip' => $request->getClientIp()
        ]);
        if($user->nuevo==1)
            return redirect()->intended("cambiardatos/".$user->id."/edit");
        return redirect()->intended($this->redirectPath());
    }
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'id_perfil'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        //$escoja=array(null=>"Escoja opción...") ;

        $tabla=User::create([            
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'cedula' => $data['cedula'],
                    'id_perfil'=>2
                ]);
        if($tabla)
        {
            $guardar= new UsuarioModuloModel;
            $guardar->id_modulo=$data["id_modulo"];
            $guardar->id_usuario=$tabla->id;
            $guardar->save();
        }
        return $tabla;

    }
    public function postRegister(Request $request)
    {       
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
        );
        }
        $user = $this->create($request->all());
       Auth::logout();       
       Session::flash('message','Cuenta creada exitosamente. Ingrese por favor...');
       return redirect('auth/login');
    }
}
