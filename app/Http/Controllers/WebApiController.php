<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use Modules\Coordinacioncronograma\Entities\CoorContratistaModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use App\User;
use App\UsuariosModel;
use Modules\Comunicacionalcaldia\Entities\cabComunicacionModel;
use Modules\Coordinacioncronograma\Entities\CoorTipoProyectoModel;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaCabModel;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaCabDetaModel;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\PerfilModel;
use Modules\Coordinacioncronograma\Entities\CoorProyectoEstrategicoModel;
use Illuminate\Support\Facades\Redirect;
use Modules\TramitesAlcaldia\Http\Controllers\TramAlcaldiaChartCabController;
use URL;
use Mail;
use Illuminate\Support\Str;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaDelModel;
use App\UsuarioModuloModel;
/*
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers:*');
//header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE"); 
*/
class WebApiController extends Controller
{
    //

    protected $ChartAlcaldia; 
    public function __construct(TramAlcaldiaChartCabController $ChartAlcaldia) {  
        $this->middleware('cors');      
         $this->ChartAlcaldia = $ChartAlcaldia;
    } 
    //$res=$this->TramiteDigital->liquidacioncertificadocatastro($idtramite,$idsecu);
    
    function initwebapp()
    {    	    	
    	if(Auth::attempt(['cedula' => request('cedula'), 'password' => request('password')])){ 
            $log=UsuariosModel::where("cedula",request('cedula'))->first();
            $perfil=PerfilModel::find($log->id_perfil);
            if($perfil)
            {
                if($perfil->tipo!=4 && $perfil->tipo!=1 && $perfil->tipo!=5)
                    return response()->json(['error'=>'Unauthorized'], 401);         
            }
            //
            $token = Str::random(60);
            $log->api_token=hash('sha256', $token);
            $log->save();
            //*UsuarioModuloModel*//
            $module=UsuarioModuloModel::join("ad_modulos as a","a.id","=","ad_usuario_modulo.id_modulo")
            ->where("ad_usuario_modulo.id_usuario",$log->id)
            ->where("a.estado","ACT")
            ->select("a.*")
            ->get();
            $rutas=array();
            foreach ($module as $key => $value) {
                # code...
                $rutas[]=$value->ruta;
            }
            //$rutas = substr ($rutas, 0, strlen($rutas) - 1);
            //show($module->toarray());
            $user=$log->toarray();
            $user["modulos"]=$rutas;
            return $user;
        } 
        else{ 
            return response()->json(['error'=>'Unauthorized'], 401); 
        }        	
    }
    function getobras()
    {        
        $obras=CoorObrasModel::join("coor_tmae_contratista as a","a.id","=","coor_tmov_obras_listado.id_contratista")
        ->join("coor_tmae_tipo_obra as b","b.id","=","coor_tmov_obras_listado.id_tipo_obra")
        ->join("parroquia as c","c.id","=","coor_tmov_obras_listado.id_parroquia")
        ->select("coor_tmov_obras_listado.*","a.nombres as contratista","b.tipo as tipoobra","c.parroquia",DB::raw("'#00000' as color"))
        ->where(function($query){
            $query->where("coor_tmov_obras_listado.estado","ACT");
            if(Input::has("estado"))
                $query->where("coor_tmov_obras_listado.estado_obra",Input::get("estado"));
                    if(Input::has("parroquia"))
                            $query->where("c.id",Input::get("parroquia"));
                    if(Input::has("estado"))                        
                            $query->where("coor_tmov_obras_listado.estado_obra",Input::get("estado"));
        })  
        ->where("coor_tmov_obras_listado.id_tipo_obra","<>",6)
        ->whereIn("coor_tmov_obras_listado.estado_obra",array("EJECUCION","POR_EJECUTAR"))          
        ->orderby("c.parroquia")//->orderby("id","desc")
        ->get();         
        $obras=colorobjestado($obras,"estado_obra");
        return $obras;
    }
    function getobrasid($id)
    {
    	$obras=CoorObrasModel::join("coor_tmae_contratista as a","a.id","=","coor_tmov_obras_listado.id_contratista")
        ->join("coor_tmae_tipo_obra as b","b.id","=","coor_tmov_obras_listado.id_tipo_obra")
        ->join("parroquia as c","c.id","=","coor_tmov_obras_listado.id_parroquia")
        ->select("coor_tmov_obras_listado.*","a.nombres as contratista","b.tipo as tipoobra","c.parroquia",DB::raw("'#00000' as color"))
        ->where("coor_tmov_obras_listado.estado","ACT")
		->where("coor_tmov_obras_listado.id",$id)
		->first();
        $obras=colorobjestado($obras,"estado_obra",1);
		return $obras;
    }
    function getobrastotal()
    {        
        $obras=CoorObrasModel::select("estado_obra",DB::raw("count(*) as total"),DB::raw("'#00000' as color"),DB::raw("sum(monto) as monto"))
        ->groupby("estado_obra")
        ->where("coor_tmov_obras_listado.estado","ACT")
        ->whereIn("estado_obra",array("EJECUCION","POR_EJECUTAR"))  
        ->where("coor_tmov_obras_listado.id_tipo_obra","<>",6)      
        ->orderby("id","desc")->get();//->paginate(500); 
        $obras=colorobjestado($obras,"estado_obra");
        return $obras;        
    }
    function getobrasparroquias()
    {
        $obras=CoorObrasModel::join("coor_tmae_contratista as a","a.id","=","coor_tmov_obras_listado.id_contratista")
        ->join("coor_tmae_tipo_obra as b","b.id","=","coor_tmov_obras_listado.id_tipo_obra")
        ->join("parroquia as c","c.id","=","coor_tmov_obras_listado.id_parroquia")
        ->select("c.id","c.parroquia",DB::raw("count(*) as total"))
        ->where(function($query){
            $query->where("coor_tmov_obras_listado.estado","ACT");
        })
        ->whereIn("coor_tmov_obras_listado.estado_obra",array("EJECUCION","POR_EJECUTAR")) 
        ->where("coor_tmov_obras_listado.id_tipo_obra","<>",6)               
        ->groupby("c.id","c.parroquia")
        ->orderby(DB::raw("count(*)"),"desc")->get();//->paginate(500);  
        return $obras;
    }

    function getobrasparroquiasid($id)
    {
        $obras=CoorObrasModel::join("coor_tmae_contratista as a","a.id","=","coor_tmov_obras_listado.id_contratista")
        ->join("coor_tmae_tipo_obra as b","b.id","=","coor_tmov_obras_listado.id_tipo_obra")
        ->join("parroquia as c","c.id","=","coor_tmov_obras_listado.id_parroquia")
        ->select("coor_tmov_obras_listado.*","a.nombres as contratista","b.tipo as tipoobra","c.parroquia",DB::raw("'#00000' as color"))
        ->where(function($query)use($id){
            $query->where("coor_tmov_obras_listado.estado","ACT")
            ->where("c.id",$id);
        })
        ->whereIn("coor_tmov_obras_listado.estado_obra",array("EJECUCION","POR_EJECUTAR")) 
        ->where("coor_tmov_obras_listado.id_tipo_obra","<>",6)               
        //->orderby("id","desc")->get();//->paginate(500); 
        ->orderby("coor_tmov_obras_listado.estado_obra")
        ->get();
         $obras=colorobjestado($obras,"estado_obra");
        return $obras;
    }
    //ACTIVIDADES
    function getactividades()
    {
    	$actividades = CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*","a.direccion",DB::raw("'#00000' as color"))
            ->where("coor_tmov_cronograma_cab.estado","ACT")             
            ->whereIn("coor_tmov_cronograma_cab.estado_actividad",["EJECUCION","POR_EJECUTAR"])     
        ->orderby("coor_tmov_cronograma_cab.fecha_fin")->get();
        $actividades=colorobjestado($actividades,"estado_actividad");
    	return $actividades;
    }
    function getactividadesid($id)
    {
    	$actividades = CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*","a.direccion",DB::raw("'#00000' as color"))
            ->where("coor_tmov_cronograma_cab.estado","ACT")            
            ->where("coor_tmov_cronograma_cab.id",$id)
            ->first();
        $actividades=colorobjestado($actividades,"estado_actividad",1);
    	return $actividades;
    }
    function getactividadestotal()
    {
        $actividades = CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.estado_actividad",DB::raw("count(*) as total"),DB::raw("'#00000' as color"))
            ->groupby("coor_tmov_cronograma_cab.estado_actividad")
            ->where("coor_tmov_cronograma_cab.estado","ACT")             
            ->whereIn("coor_tmov_cronograma_cab.estado_actividad",array("EJECUCION","POR_EJECUTAR"))          
        ->orderby("coor_tmov_cronograma_cab.fecha_inicio")->get();
        $actividades=colorobjestado($actividades,"estado_actividad");
        return $actividades;
    }
    function getactividadesdireccion()
    {
        $actividades = CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select("a.id","a.direccion",DB::raw("count(*) as total"))
            ->groupby("a.id","a.direccion")
            //->where([["coor_tmov_cronograma_cab.estado","ACT"],["coor_tmov_cronograma_cab.estado_actividad","<>", "EJECUTADO"]])
            ->where("coor_tmov_cronograma_cab.estado","ACT")
            ->whereIn("coor_tmov_cronograma_cab.estado_actividad",array("EJECUCION","POR_EJECUTAR"))
            
        ->get();
        //show("aqui");
        return $actividades;
    }
    function getactividadesdireccionid($id)
    {
            $actividades = CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*","a.direccion",DB::raw("'#00000' as color"))
            //->where([["coor_tmov_cronograma_cab.estado","ACT"],["coor_tmov_cronograma_cab.estado_actividad","<>", "EJECUTADO"]])                    
            ->where("coor_tmov_cronograma_cab.estado","ACT")
            ->whereIn("coor_tmov_cronograma_cab.estado_actividad",array("EJECUCION","POR_EJECUTAR"))
            ->where("a.id",$id)
            ->orderby("coor_tmov_cronograma_cab.fecha_fin")->get();
        $actividades=colorobjestado($actividades,"estado_actividad");
        return $actividades;
    }
    //ACTIVIDES DE LA CIUDAD
    function getcomunicacionactitotal()
    {
        $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                ->select("com_tmov_comunicacion_cab.estado_actividad", 
                    DB::raw("count(*) as total"),DB::raw("'#00000' as color"))
                    ->where("com_tmov_comunicacion_cab.estado", "ACT")
                    ->groupby("com_tmov_comunicacion_cab.estado_actividad")
                    ->orderby("com_tmov_comunicacion_cab.fecha_fin")
                ->get();
        $tabla=colorobjestado($tabla,"estado_actividad");
        return $tabla;
    }
    function getcomunicacionactilista()
    {
            $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                ->select("com_tmov_comunicacion_cab.*", "ac.nombre",
                    DB::raw("(select direccion from tmae_direcciones where id=com_tmov_comunicacion_cab.id_direccion) as direccion"),
                    DB::raw("'#00000' as color"))
                    ->where("com_tmov_comunicacion_cab.estado", "ACT")
                    ->orderby("com_tmov_comunicacion_cab.fecha_fin")
                ->get(); 
            $tabla=colorobjestado($tabla,"estado_actividad");           
        return $tabla;
    } 
    function getcomunicacionactidireccion()
    {
            $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                ->join("tmae_direcciones as di","di.id","=","com_tmov_comunicacion_cab.id_direccion")
                ->select("di.id","di.direccion",DB::raw("count(*) as total"))
                    ->where("com_tmov_comunicacion_cab.estado", "ACT")
                    ->groupby("di.direccion")
                    ->orderby("com_tmov_comunicacion_cab.fecha_fin")
                ->get();            
        return $tabla;
    } 
    function getcomunicacionactidireccionid($id)
    {
            $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                ->join("tmae_direcciones as di","di.id","=","com_tmov_comunicacion_cab.id_direccion")
                ->select("com_tmov_comunicacion_cab.*","ac.nombre as actividad","di.direccion",DB::raw("'#00000' as color"))
                    ->where("com_tmov_comunicacion_cab.estado", "ACT")
                    ->where("di.id",$id)                    
                    ->orderby("com_tmov_comunicacion_cab.fecha_fin")
                ->get();  
            $tabla=colorobjestado($tabla,"estado_actividad");
        return $tabla;
    } 
    function getcomunicacionactiid($id)
    {
        $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                ->select("com_tmov_comunicacion_cab.*", "ac.nombre",
                    DB::raw("(select direccion from tmae_direcciones where id=com_tmov_comunicacion_cab.id_direccion) as direccion")
                ,DB::raw("'#00000' as color"))
                    ->where("com_tmov_comunicacion_cab.estado", "ACT")
                    ->where("com_tmov_comunicacion_cab.id",$id)
                    ->orderby("com_tmov_comunicacion_cab.fecha_fin")
                ->first();   
        $tabla=colorobjestado($tabla,"estado_actividad",1);
        return $tabla;
    }

    /*Obtener el total por tipos*/
    function getcomponentes()
    {
        $tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as ac", "coor_tmov_proyecto_estrategico.id_tipo_proyecto", "=", "ac.id")
        ->select("coor_tmov_proyecto_estrategico.id_tipo_proyecto", "ac.tipo",DB::raw("count(*) as total"))
        ->where("coor_tmov_proyecto_estrategico.estado","ACT")
        ->groupby("ac.tipo")
        ->orderby("coor_tmov_proyecto_estrategico.id","desc")->get();
        return $tabla;

    }

    /** Obtener todos los componentes de id */
    function getcomponentesbyid($id)
    {
        $tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as ac", "coor_tmov_proyecto_estrategico.id_tipo_proyecto", "=", "ac.id")
        ->select("coor_tmov_proyecto_estrategico.*", "ac.tipo")
        ->where("coor_tmov_proyecto_estrategico.estado","ACT")
        ->where("coor_tmov_proyecto_estrategico.id_tipo_proyecto",$id)
        ->orderby("coor_tmov_proyecto_estrategico.id","desc")->get();
    
        return $tabla;

    }

    function getsumacomponentestotal()
    {        
        $obras=CoorProyectoEstrategicoModel::select("coor_tmov_proyecto_estrategico.id_tipo_proyecto",DB::raw("count(*) as total"),DB::raw("sum(monto) as monto"))
        ->groupby("id_tipo_proyecto")
        ->where("coor_tmov_proyecto_estrategico.estado","ACT")
        // ->whereIn("estado_obra",array("EJECUCION","POR_EJECUTAR"))  
        // ->where("coor_tmov_obras_listado.id_tipo_obra","<>",6)      
        ->orderby("id","desc")->get();//->paginate(500); 
        // $obras=colorobjestado($obras,"estado_obra");
        return $obras;        
    }


    

    /*Para obtener los detalles*/
    function getproyectosbyid($id)
    {
        $tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as ac", "coor_tmov_proyecto_estrategico.id_tipo_proyecto", "=", "ac.id")
        ->select("coor_tmov_proyecto_estrategico.*", "ac.tipo")
        ->where("coor_tmov_proyecto_estrategico.estado","ACT")
        ->where("coor_tmov_proyecto_estrategico.id",$id)
        ->orderby("coor_tmov_proyecto_estrategico.id","desc")->get();
    
        return $tabla;

    }

    /** Total de Proyectos */
    function getproyectototal()
    {
        $tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as ac", "coor_tmov_proyecto_estrategico.id_tipo_proyecto", "=", "ac.id")
        ->select("coor_tmov_proyecto_estrategico.estado_proyecto", "ac.tipo",
        DB::raw("count(*) as total"),DB::raw("'#00000' as color"))
        ->where("coor_tmov_proyecto_estrategico.estado","ACT")
        ->groupby("coor_tmov_proyecto_estrategico.estado_proyecto")
        ->orderby("coor_tmov_proyecto_estrategico.id","desc")->get();
        $tabla=colorobjestadoproyecto($tabla,"estado_proyecto");
        return $tabla;
    }


    /** Listado de peticiones */
    function getpeticionestotalgrafico()
    {
        return $this->ChartAlcaldia->getValoresCharts("PENDIENTE");        
    }

    function getpeticiones()
    {        
        if(Input::has("director"))
        {
            $tabla=UsuariosModel::join("tmae_direcciones as a","a.id","=","users.id_direccion")
            ->select("users.id","a.direccion","users.name","users.cargo")
            ->where("users.id_perfil",3)
            ->where("users.cargo","like","%DIRECTOR%")
            ->where("users.name","not like","%prueba%")
            ->where("users.name","not like","%VELASQUEZ REYES FRANCISCO GABRIEL%")
            ->where("users.name","not like","%Ericka Macias%")
            ->orWhere('users.id', 45)->orWhere('users.id', 88)->orWhere('users.id', 121)
            ->orWhere('users.id', 175)->orWhere( 'users.id',1019)
              ->orWhere('users.id',1469)->orWhere('users.id',1680)->orWhere('users.id',1668)

              ->orWhere('users.id',1682)->orWhere('users.id',1683)->orWhere('users.id',1684)
              ->orWhere('users.id',1687)->orWhere('users.id',1688)->orWhere('users.id',1689)

             ->orWhere('users.id',1633)->orWhere('users.id',1648)
             ->orWhere('users.id',1630)->orWhere('users.id',1649)->orWhere('users.id',1676)
             ->orWhere('users.id',1651)->orWhere('users.id',1634)
             ->orWhere('users.id',1641)->orWhere('users.id',1646)->orWhere('users.id',1182)
             ->orderby("cargo","asc");            
            $id=intval(Input::get("director"));
            if($id!=0)
                $tabla=$tabla->select("users.id","a.direccion","users.name","users.cargo","users.email","users.cedula","users.telefono")->where("users.id",$id)->first();
            else
                $tabla=$tabla->get();
        }else{
            $tabla=TramAlcaldiaCabModel::where("estado","ACT")
            ->select("*",DB::raw(" DATE_FORMAT(created_at,'%Y-%m-%d') as created_at_n"),DB::raw(" DATE_FORMAT(updated_at,'%Y-%m-%d') as updated_at_n"),DB::raw("case when archivo <>'' then concat('".URL::to("/")."/archivos_sistema/',archivo) else '' end as archivo"))
            ->where(function($query){
                if(Input::has("prioridad"))
                    $query->where("prioridad",Input::get("prioridad"));            
            })  
            ->where("disposicion","PENDIENTE")      
            ->orderby("updated_at")
            ->orderby("prioridad")
            ->orderby("disposicion");
            if(Input::has("idtramite"))
                $tabla=$tabla->where("id",Input::get("idtramite"))->first();
            else
                $tabla=$tabla->get();
        }
        return $tabla;
    }



/**
 * obtener el total de tramites
 */
    function getTramitestotal()
    {
        $tabla = TramAlcaldiaCabModel::select(
        DB::raw("count(*) as valor"))
        ->where("tram_peticiones_cab.estado","ACT")
        ->where("tram_peticiones_cab.disposicion","=","PENDIENTE")
        ->groupby("tram_peticiones_cab.disposicion")
        ->orderby("tram_peticiones_cab.id","desc")->get();

        $tablaTotal = TramAlcaldiaCabModel::select(
        DB::raw("count(*) as valor"))
        ->where("tram_peticiones_cab.estado","ACT")
        ->where("tram_peticiones_cab.disposicion","not like","%PENDIENTE%")
        ->where("tram_peticiones_cab.estado_proceso","=",0)
        ->orderby("tram_peticiones_cab.id","desc")->get();

        $tablaEstado = TramAlcaldiaCabModel::select(
        DB::raw("count(*) as valor"))
        ->where("tram_peticiones_cab.estado","ACT")
        ->where("tram_peticiones_cab.estado_proceso","=",1)
        ->orderby("tram_peticiones_cab.id","desc")->get();

        $datos[]=["valor"=> $tabla[0]->valor];
        $datos[]=["valor"=> $tablaTotal[0]->valor];
        $datos[]=["valor"=>$tablaEstado[0]->valor];
        return $datos;
    }
        

    public function EnviarEmail($id,$email,$nombres,$asunto="Trámite Despachado"){
       

        $tabla=TramAlcaldiaCabModel::where([["estado","ACT"],["id",$id]])->first();
            $tabladel=TramAlcaldiaDelModel::
            join("users as us","us.id","=","tram_peticion_del.id_usuario")
            ->where([["tram_peticion_del.estado","ACT"],["id_tram_cab",$id]])->first();
            

       
        Mail::send(
        'vistas.reporteshtml.reportemail',
        ["tabla"=>$tabla,"tabladel"=>$tabladel],
        function($m) use ($email,$nombres,$asunto){
        $m->from("alcaldia@manta.gob.ec","GAD Municipal Manta");
        $m->to($email,$nombres)->subject($asunto);
        });
        return "";
       
    }
    public function apruebatramite(Request $request)
    {
        if($request->_token)
        {
            $token=UsuariosModel::where("api_token",$request->_token)->first();
            if(!$token)
                return response()->json(['error'=>'Unauthorized'], 401); 
            $id=intval($request->id);
            $observacion=trim($request->observacion);
            $disposicion=trim($request->disposicion);
            $ip=trim($request->ip);
            $pc=trim($request->pc);
            $delegados=$request->delegados;
            $tabla=TramAlcaldiaCabModel::find($id);
            if(!$tabla)
                return response()->json(['error'=>'No existe el trámite.'], 401);
            DB::beginTransaction();
            try{ 
                $tabla->observacion=$observacion;
                $tabla->disposicion=$disposicion;
                $tabla->id_usuario= $token->id;
                $tabla->fecha_respuesta=date("Y-m-d");
                $tabla->ip=$ip;
                $tabla->pc=$pc;
                $tabla->save();
                //Delegados
                if($delegados!="")
                {
                    $dele=json_decode($delegados,true);
                    foreach ($dele as $key => $value) {
                        # code...
                        $savedele=TramAlcaldiaDelModel::where("id_tram_cab",$id)->where("id_usuario",$value["id"])->first();
                        if(!$savedele)
                            $savedele= new TramAlcaldiaDelModel;
                        $savedele->id_tram_cab=$id;
                        $savedele->id_usuario=$value["id"];
                        $savedele->save();
                    }                    
                }
                //Historial
                $histoact=TramAlcaldiaCabDetaModel::where('id_tram_cab',$id)->update(['estado'=>'INA']);         
                $tabla=TramAlcaldiaCabModel::select(DB::raw("numtramite,remitente,asunto,peticion,prioridad,recomendacion,disposicion,observacion,id_usuario,ip,pc,id,archivo,fecha_ingreso,fecha_respuesta,'$delegados' as delegados,'".date('Y-m-d H:i:s')."' as created_at,'".date('Y-m-d H:i:s')."' as updated_at"))
                ->where("id",$id);                
                $bindings = $tabla->getBindings();
                $insertQuery = "INSERT into tram_peticiones_cab_deta(numtramite,remitente,asunto,peticion,prioridad,recomendacion,disposicion,observacion,id_usuario,ip,pc,id_tram_cab,archivo,fecha_ingreso,fecha_respuesta,delegados_json,created_at,updated_at)"
                    . $tabla->toSql();
                DB::insert($insertQuery, $bindings);

                //return $insertQuery;
                //return response()->json($tabla->get());
                //return response()->json(json_decode($delegados));
            DB::commit();
            
            
            $this->EnviarEmail($id,'cgpg94@gmail.com',"Gonzalo");

            //DB::rollback();
            return response()->json(["success","Grabado Exitosamente"]);
            }//Try Transaction
            catch(\Exception $e){
                // ROLLBACK
                DB::rollback();
                $mensaje=$e->getMessage();                
                return response()->json(['error'=>$mensaje], 401);    
            }
        }
        else 
            return response()->json(['error'=>'Unauthorized'], 401); 
    }


}
