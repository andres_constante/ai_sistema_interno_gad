<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\MenuModulosModel;
use App\ModulosModel;
use App\MenuModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
class MenuModulosController extends Controller
{
    var $configuraciongeneral = array ("Asignar menú a módulos", "menumodulos", "index");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[         
        {"Tipo":"select","Descripcion":"Nombre del Menú","Nombre":"id_menu","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }, 
        {"Tipo":"text","Descripcion":"Ruta / URL","Nombre":"ruta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" },
        {"Tipo":"text","Descripcion":"Icono","Nombre":"icono","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "NO" },
        {"Tipo":"select","Descripcion":"Nivel","Nombre":"nivel","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" },
        {"Tipo":"select-ajax","Descripcion":"ID Padre","Nombre":"idmain","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "NO" },
        {"Tipo":"text","Descripcion":"Orden","Nombre":"orden","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" },
        {"Tipo":"select","Descripcion":"Visible","Nombre":"visible","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" },
        {"Tipo":"text","Descripcion":"Adicional","Nombre":"adicional","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "NO"  }
        ]'; 
//https://jqueryvalidation.org/validate/
    var $validarjs =array(
            "id_modulo"=>"id_modulo: {
                            required: true
                        }",
            "id_menu"=>"id_menu: {
                            required: true
                        }"
        );
    public function __construct() {
        $this->middleware('auth');
    }
    public function getmenumodulostree()
    {
        //return Input::all();
        $id=Input::get("id");        
        $menutodo=intval(Input::get("menutodo"));
        $modulo=$id;
        $submit=0;
        //return $id;
        $tipobus=Input::get("tipobus");
        $ruta=Input::get("ruta");
        $idperfil=Input::get("perfil");
        if($menutodo==1)
        {
            $idmainper=$id;
            $tabla=MenuModulosModel::
            join("menu as b","b.id","=","ad_menu_modulo.id_menu")            
            ->select("ad_menu_modulo.*","b.menu","b.adicional as adimenu")
            ->orderby("ad_menu_modulo.orden","asc")
            ->where("ad_menu_modulo.id_modulo",$id)
            ->get();
            //return $tabla;
             $submit=1;
        }
         else
         {  $idmainper=0;
            if($tipobus=="modulo")
            {
              $tabla=MenuModulosModel::
                join("menu as b","b.id","=","ad_menu_modulo.id_menu")            
                ->select("ad_menu_modulo.*","b.menu","b.adicional as adimenu")
                ->orderby("ad_menu_modulo.orden","asc")
                ->where("ad_menu_modulo.id_modulo",$id)
                ->get();
            }else{
              $tabla=MenuModulosModel::
                join("menu as b","b.id","=","ad_menu_modulo.id_menu")  
                ->join("ad_menu_perfil as c","c.id_menu_modulo","=","ad_menu_modulo.id")          
                ->select("ad_menu_modulo.*","b.menu","b.adicional as adimenu")
                ->orderby("ad_menu_modulo.orden","asc")
                ->where("ad_menu_modulo.id_modulo",$id)
                ->where("c.id_perfil",Input::get("perfil"))
                ->get();
              
            }           
        }
        //show($tabla);
        //show($tabla->toarray());
            /*echo "<pre>";
            print_r($tabla);
            die();*/
            //var_dump($menupermiso);
            // show the view and pass the nerd to it
            return view('permisosmenu.mainshow',[
                "total"=>$tabla->count(),
                "tabla"=>$tabla,
                "idmainper"=>$idmainper,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "submit"=>$submit,
                "idsi"=>$id,
                "ruta"=>$ruta,
                "create"=>'si',
                "delete"=>'si',
                "idperfil"=>$idperfil
                ]);
    }
    public function getmenumodulos()
    {
        $id=Input::get("idmodulo");
        $tabla=MenuModulosModel::
            join("menu as b","b.id","=","ad_menu_modulo.id_menu")
            ->join("ad_modulos as m","m.id","=","ad_menu_modulo.id_modulo")
            ->select("m.nombre as modulo","ad_menu_modulo.*","b.menu","b.adicional as adimenu")
            ->orderby("ad_menu_modulo.id")
            ->orderby("ad_menu_modulo.orden","desc")
            ->where("ad_menu_modulo.id",$id)
            ->get();
        return $tabla;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tabla=array();
        $id=0;
        if(Input::has("mod"))
        {
            $id=Input::get("mod");
        }
        $menumdulo=array();
        if($id!=0)
        {
          $tabla=MenuModulosModel::
            join("menu as b","b.id","=","ad_menu_modulo.id_menu")
            ->join("ad_modulos as m","m.id","=","ad_menu_modulo.id_modulo")
            ->select("m.nombre","ad_menu_modulo.*","b.menu","b.adicional as adimenu")
            ->where("m.id",$id)
            ->orderby("ad_menu_modulo.id")
            ->orderby("ad_menu_modulo.orden","desc")->get();
          $menumdulo=MenuModulosModel::
            join("menu as b","b.id","=","ad_menu_modulo.id_menu")
            ->select("ad_menu_modulo.id","b.menu")
            ->where("ad_menu_modulo.id_modulo",$id)
            ->orderby("ad_menu_modulo.id_modulo")        
            ->orderby("ad_menu_modulo.orden")
            ->lists("menu","id")
            ->all(); 
        } 
        $main=MenuModel::lists("menu","id")->all();
        $modulos=ModulosModel::lists("nombre","id")->all();
        //show($mod);
        $objetos=json_decode($this->objetos);
        $modulos= $this->escoja +$modulos;
        $objetos[3]->Valor=array(1=>"1",2=>"2");
         $objetos[0]->Valor= $this->escoja + $main;
        $objetos[4]->Valor= $this->escoja + $menumdulo;
        
        $objetos[6]->Valor=array("SI"=>"SI","NO"=>"NO");
        //show($objetos);        
        $objetos=array_values($objetos);

        //$tabla=MenuModel::orderby("orden","desc")->get();//->paginate(500);        
        /////////////// 
        //show($tabla);       
      return view('permisosmenu.menumoduloindex',[
                "modulos"=>$modulos,
                "modid"=>$id,
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "delete"=>"si",
                "urlmenu"=>"permisostipousu",
                "create"=>'si',
                "delete"=>'si'
                ]);        
    }
/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $modulo=ModulosModel::lists("nombre","id")->all();
        $menu=MenuModel::where("nivel",1)->orderby("orden")->lists("menu","id")->all();
        //show($menu);
        $menudeta=array();
        $objetos=json_decode($this->objetos);
        $objetos[0]->Valor=$this->escoja + $modulo;
        $objetos[1]->Valor=$this->escoja + $menu;
        $objetos[2]->Valor=$this->escoja + $menudeta;
        //show($objetos);
        $this->configuraciongeneral[2]="crear";
        return view('vistas.create',[
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }
    public function guardar($id)
    {         

  }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //       
        //show(Input::all()); 
        //return $this->guardar(0);
        $datosenvio= Input::get("datosenvio");
        $modulo=$datosenvio[count($datosenvio)-1];
        //return $modulo;
        //return $datosenvio;
        $id = trim($datosenvio[0]);

         //return $datosenvio;

        // Validacion
         $objetos=json_decode($this->objetos);
         $objetos=array_values($objetos);
         //return $objetos;
        foreach($objetos as $keycam => $valuecam)
        {                
          if ($datosenvio[$keycam + 1] == "-" || $datosenvio[$keycam + 1] == "" )
           {
               if ($valuecam->Requerido == "SI")
                 {
                   return "Por Favor Ingrese correctamente en el campo " . $valuecam->Nombre;
                 }                
           }; 
           if($valuecam->Nombre=="id_menu" && $id=="-")
            {
                //$verificarexisteid = MenuModel::where("menu",trim($datosenvio[$keycam + 1]))->first();
                $verificarexisteid=MenuModulosModel::where("id_menu",intval($datosenvio[$keycam + 1]))
                ->where("id_modulo",$modulo)
                ->first();
                if($verificarexisteid)
                    return "El menú '".trim($datosenvio[$keycam + 1])."' ya existe.";
            }       
        } 
        //
         $verificarexisteid = MenuModulosModel::find($id);
         if (!$verificarexisteid)
         {
            $guardar= new MenuModulosModel;

         }else {
            $guardar= MenuModulosModel::find($id);
         }   

         foreach($objetos as $keycam => $valuecam)
                 {
                    $dato=trim($datosenvio[$keycam + 1]);
                    if($dato=="-")
                        $dato="";
                    $nombre=$valuecam->Nombre;
                    $guardar->$nombre= $dato;
                 } 
                 $guardar->id_modulo=$modulo;
                 $guardar->save();
                 
            return $guardar->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return "Opciones";  
        $tabla=ModulosModel::join("ad_menu_modulo as a","a.id_modulo","=","ad_modulos.id")
            ->join("menu as b","b.id","=","a.id_menu")
            ->select(DB::raw("concat(ad_modulos.id,b.orden) as id"),"ad_modulos.nombre","b.menu")
            ->where("ad_modulos.estado","'ACT'")
            ->orderby("ad_modulos.id")
            ->orderby("b.orden","desc")
            ->where(DB::raw("id"),"like",$id)
            ->first();
        show($tabla);
        $objetos=json_decode($this->objetos);
        $objetos[0]->Nombre="nombre";
        $objetos[1]->Nombre="menu";
        unset($objetos[2]);

        return view('vistas.show',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return $this->create();        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla=ModulosModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
