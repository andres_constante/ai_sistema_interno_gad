<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\NotificacionesModel;
use Session;
use Auth;
use DB;




class NotificacionesController extends Controller
{
    var $configuraciongeneral = array ("Prueba De Notificaciones", "notificaciones", "index");
    var $objetos = '[
		{"Tipo":"text","Descripcion":"Título","Nombre":"titulo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"textarea","Descripcion":"Descripción","Nombre":"descripcion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
		]';
    
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        //show($this->objetos);

        $objetos=json_decode($this->objetos);
        return view('vistas.notificacion',[
            "objetos"=>$objetos,
            "configuraciongeneral"=>$this->configuraciongeneral,
            ]);
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required|max:255',
            'descripcion' => 'required',
        ]);


        $guardar= new NotificacionesModel;
        $guardar->token=$request->_token;
        $guardar->titulo=$request->titulo;
        $guardar->descripcion=$request->descripcion;
        $guardar->save();

        
        $this->notificacion($guardar->token, $guardar->titulo,$guardar->descripcion);
      
        Session::put('success','Notifiación Enviada Con Exito!!');

        $objetos=json_decode($this->objetos);
        return view('vistas.notificacion',[
            "objetos"=>$objetos,
            "configuraciongeneral"=>$this->configuraciongeneral,
            ]);
    }




    public function notificacion($to, $title,$body)
    {

   
        $content      = array(
            "en" => $title
        );
        $hashes_array = array();
        array_push($hashes_array, array(
            "id" => "like-button",
            "text" => $body,
            // "icon" => "http://i.imgur.com/N8SN8ZS.png",
            // "url" => "http://sistemasti.manta.gob.ec"
        ));

        $fields = array(
            'app_id' => "92c8acfb-c9ad-406c-abfa-0e60da2baea2",
            'include_player_ids' => array(
                $to
            ),
            'data' => array(
                "foo" => "bar"
            ),
            'contents' => $content,
            'web_buttons' => $hashes_array
        );
        
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic YzFhYzQ4MTctZWNmYi00ZGIyLTg0MmYtOTg3ZDY2ZTY3NGRi'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;
      

    }

}
