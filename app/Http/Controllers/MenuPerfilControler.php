<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\MenuPerfilModel;
use App\ModulosModel;
use App\PerfilModel;
use App\MenuModulosModel;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
class MenuPerfilControler extends Controller
{
    var $configuraciongeneral = array ("Menú Perfil", "menuperfil", "crear","getmenumodulostree");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[         
        {"Tipo":"select2","Descripcion":"Módulo","Nombre":"id_modulo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI"  },
        {"Tipo":"select2","Descripcion":"Perfil","Nombre":"id_perfil","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }
                            ]';

    var $validarjs =array(
            "id_perfil"=>"id_perfil: {
                            required: true
                        }",
            "id_modulo"=>"id_modulo: {
                            required: true
                        }"
        );

        public function __construct() {
            $this->middleware('auth');
        } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
        $modulo=ModulosModel::orderby("nombre")->lists("nombre","id")->all();
        $main=PerfilModel::orderby("nombre_perfil")->lists("nombre_perfil","id")->all();
        $objetos=json_decode($this->objetos);
        $objetos[1]->Valor= $this->escoja + $main;
        $objetos[0]->Valor= $this->escoja + $modulo;
        //show($objetos);        
        //$objetos=array_values($objetos);                
        $tabla=array();//->paginate(500);
        return view('permisosmenu.createindex',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,                
                //"checkmenu"=>"si",
                "validarjs"=>array(),
                "create"=>'si',
                "delete"=>'si'
                ]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $idbus=intval(Input::get("id"));
        $modulo=ModulosModel::orderby("nombre")->lists("nombre","id")->all();
        $main=PerfilModel::orderby("nombre_perfil")->lists("nombre_perfil","id")->all();
        $objetos=json_decode($this->objetos);
        $objetos[1]->Valor= $this->escoja + $main;
        $objetos[0]->Valor= $this->escoja + $modulo;
        $objetos[0]->ValorAnterior= $idbus;
        //show($objetos);        
        //$objetos=array_values($objetos);                
        $tabla=array();//->paginate(500);
        return view('permisosmenu.createindex',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "checkmenu"=>"si",
                "validarjs"=>array(),
                "menutodo"=>"si",
                "idbus"=>$idbus
                ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//return Input::all();
            $ruta=$this->configuraciongeneral[1];
                $ruta.="/create";
                $guardar= new MenuPerfilModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro Variable de Configuración";
            
            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, MenuPerfilModel::rules(0));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                $msg="No ha escogido menú de Opciones.";
                if(Input::has("idmenumain"))
                {
                    $menuper=Input::get("idmenumain");
                    $del=MenuPerfilModel::join("ad_menu_modulo as a","a.id","=","ad_menu_perfil.id_menu_modulo")
                    ->where("ad_menu_perfil.id_perfil",Input::get("id_perfil"))
                    ->where("a.id_modulo",Input::get("id_modulo"))
                    ->delete();
                    $sw=array();
                    foreach ($menuper as $key => $value) {
                        # code...
                        $guardar= new MenuPerfilModel;
                        $guardar->id_perfil=Input::get("id_perfil");
                        $guardar->id_menu_modulo=$value;
                        $guardar->save();
                        $msg="Registro Creado Exitosamente...!";
                        $sw[]=$guardar->id;
                    }
                    if(count($sw))
                    {
                        $ver=MenuModulosModel::join("ad_menu_perfil as a","a.id_menu_modulo","=","ad_menu_modulo.id")
                        ->wherein("a.id",$sw)
                        ->select("ad_menu_modulo.id","ad_menu_modulo.idmain",DB::raw("count(*) as total"))
                        ->groupby("ad_menu_modulo.id","ad_menu_modulo.idmain")
                        ->get();
                        //show($ver);
                        foreach ($ver as $key => $value) {
                            # code...
                            $guardar=MenuPerfilModel::where("id_perfil",Input::get("id_perfil"))->where("id_menu_modulo",$value->idmain)->first();
                            if(!$guardar)
                                $guardar=new MenuPerfilModel;
                            $guardar->id_perfil=Input::get("id_perfil");
                            $guardar->id_menu_modulo=$value->idmain;
                            $guardar->save();
                        }
                    }

                }

                 //$guardar->save();
                 //Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->nombre));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
