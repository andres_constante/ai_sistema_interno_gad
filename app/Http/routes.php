    <?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
/*WebAPI*/
//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Headers:*');
/*REPORTES*/
Route::post('reportescoordinacionactividades', "ReportesJasperController@reporteActividades");
Route::get('reportescoordinacionactividades', 'ReportController@reportescoordinacionactividades');
/*===========================*/
Route::group(['middleware' => 'cors'], function () {
    Route::get("initwebapp","WebApiController@initwebapp");
    Route::get("getobras","WebApiController@getobras");
    Route::get("getobras/{id}","WebApiController@getobrasid");
    Route::get("getobrastotal","WebApiController@getobrastotal");
    Route::get("getobrasparroquias","WebApiController@getobrasparroquias");
    Route::get("getobrasparroquias/{id}","WebApiController@getobrasparroquiasid");
    ////
    Route::get("getactividades","WebApiController@getactividades");
    Route::get("getactividades/{id}","WebApiController@getactividadesid");
    Route::get("getactividadestotal","WebApiController@getactividadestotal");
    Route::get("getactividadesdireccion","WebApiController@getactividadesdireccion");
    Route::get("getactividadesdireccion/{id}","WebApiController@getactividadesdireccionid");
    ///////
    Route::get("getcomunicacionactitotal","WebApiController@getcomunicacionactitotal");
    Route::get("getcomunicacionactilista","WebApiController@getcomunicacionactilista");
    Route::get("getcomunicacionactidireccion","WebApiController@getcomunicacionactidireccion");
    Route::get("getcomunicacionactidireccion/{id}","WebApiController@getcomunicacionactidireccionid");
    Route::get("getcomunicacionactilista/{id}","WebApiController@getcomunicacionactiid");

    // proyectos webApi
    Route::get("getcomponentes","WebApiController@getcomponentes");
    Route::get("getcomponentes","WebApiController@getcomponentes");
    Route::get("getcomponentesbyid/{id}","WebApiController@getcomponentesbyid");
    Route::get("getproyectosbyid/{id}","WebApiController@getproyectosbyid");
    Route::get("getproyectototal","WebApiController@getproyectototal");
    Route::get("getsumacomponentestotal","WebApiController@getsumacomponentestotal");


    // tramites alcaldia
    Route::get("getpeticionestotalgrafico","WebApiController@getpeticionestotalgrafico");
    Route::get("getpeticiones","WebApiController@getpeticiones");
    Route::get("getpeticiones/{id}","WebApiController@getpeticionesby");
    Route::get("getTramitestotal","WebApiController@getTramitestotal");
    /*Route::middleware('auth:api')->get('apruebatramite', function(Request $request) {
        return "Hola";
    });*/
    Route::post("apruebatramite","WebApiController@apruebatramite");
});//CORS
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////
////////////////////////////////////

/**/
Route::get("getmenumodulostree","MenuModulosController@getmenumodulostree");
Route::get("getmenumodulos","MenuModulosController@getmenumodulos");
Route::resource("menumodulos","MenuModulosController");
Route::resource("modulos","ModulosController");

////////////////////////////////////
Route::get("moduleschoice","ModulosController@moduleschoice");

Route::get('layout', function () {
    return view('home');
});
Route::get('/', ['middleware' => 'auth', function() {
    // Only authenticated users may enter...        
    $id_tipo_pefil=App\UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
    if(Auth::user()->id==1)
        return view('home');
    if(Session::has("escoger") && $id_tipo_pefil->tipo!=1)
    {        
    	return Redirect::to("moduleschoice");
    }
    else
    {
        $tabla=App\MenuModulosModel::join("ad_modulos as m","m.id","=","ad_menu_modulo.id_modulo")
        ->join("menu as me","me.id","=","ad_menu_modulo.id_menu")
        ->join("ad_menu_perfil as mp","mp.id_menu_modulo","=","ad_menu_modulo.id")
        ->join("ad_usuario_modulo as um","um.id_modulo","=","m.id")
        ->select("me.menu","ad_menu_modulo.adicional","ad_menu_modulo.ruta",DB::raw("count(*) as total"),DB::raw("max(um.id_usuario) as usu"))
        ->groupby("me.menu","ad_menu_modulo.adicional","ad_menu_modulo.ruta")
        ->where("um.id_usuario",Auth::user()->id)
        ->where("ad_menu_modulo.nivel","<>",1)->get();
        $totalmod=$tabla->count();
        //show($tabla);
        if($totalmod==1)
        {
            foreach ($tabla as $key => $value) {
                # code...
                return Redirect::to($value->ruta);
                break;
            }            
        }else{
            return Redirect::to("moduleschoice");
        }
    }
    return view('home');
}]);
//Route::get('home', 'HomeController@index');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);


// Password reset link request routes...
Route::get('password/email', ['as' => 'password/email', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('password/email', ['as' => 'password/postEmail', 'uses' => 'Auth\PasswordController@postEmail']);
 
// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', ['as' => 'password/postReset', 'uses' =>  'Auth\PasswordController@postReset']);

//Menu
Route::resource('menu','MenuController');
//Usuarios
Route::resource('usuarios','UsuariosController');
Route::get('getUsers', 'UsuariosController@getUsers');
//Editar Datos del Usuario
Route::resource('cambiardatos','CambiaDatosController');
//Tipo de Usuario
Route::resource('tipousuario','TipousuarioController');
//Perfil de Usuario
Route::resource('perfil','PerfilController');

//menu perfil
Route::resource('menuperfil','MenuPerfilControler');
//usuariomudulo
Route::resource('usuariomodulo','UsuarioModuloControler');
//Permisos de Usuario
//Route::resource('permisosusuario','PermisosUsuariosController');
//Route::resource('permisosasignar','PermisosAsignarController');
//Permisos Perfiles
//Route::resource('perfilpermisos','PerfilPermisosController');
Route::resource('perfilpermisosasignar', 'AsignarPermisosPerfilController');
//Auditoría
Route::resource('auditoria','AuditoriaController');
//Variables de Configuracion
Route::resource('configsystem','SysConfigController');
//menu notificaciones
Route::resource('notificaciones','NotificacionesController');


//Archivos
Route::resource("subirarchivos","ArchivosController");
// Route::post('archivos-gruardar', 'ArchivosController@store');
// Route::post('archivos-borrar', 'ArchivosController@destroy');
// Route::get('archivos-mostrar', 'ArchivosController@index');

//AJAX INDEX
Route::get('usuariosajax', 'UsuariosController@usuariosajax');
Route::get('auditoriaajax', 'AuditoriaController@auditoriaajax');
Route::get('modulosajax', 'ModulosController@modulosajax');
Route::get('perfilesajax', 'PerfilController@perfilesajax');






//
Route::get("permisostipousu","AsignarPermisosController@permisostipousu");
Route::resource("asignarpermisos","AsignarPermisosController");
//Route::get('');
//Pruebas
Route::get('prueba',function(){
	echo fechas(3,'2015-02-01 13:25:25')."<br>";
	echo validarCI('1309366514')."<br>";
	echo validarCI('1309366515')."<br>";
        echo limitarPalabras("Hola mundo como estas", 2);        
});


Route::get('registroappgo',function(){
	return  Redirect::to("http://ciudadanodigital.manta.gob.ec/registro");      
});
