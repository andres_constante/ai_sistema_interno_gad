<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModulosModel extends Model
{
    //
    protected $table = 'ad_modulos';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'nombre'=>'required|unique:ad_modulos'. ($id ? ",id,$id" : ''),
                'detalle'=>"required",
                'detalle'=>"required"
            ], $merge);
        } 
}
