/*AGRUPADO DIRECCION*/
SELECT 
	a.direccion,	
	max(a.alias) as alias,
	AVG(b.avance) AS avance,	
	AVG(DATEDIFF(b.fecha_fin, b.fecha_inicio) +1) AS dias_plan,
CASE WHEN AVG(DATEDIFF(CURDATE(), b.fecha_inicio) +1) < 0 THEN 0 ELSE AVG(DATEDIFF(CURDATE(), b.fecha_inicio) + 1) END AS dias_trans,
CASE WHEN AVG(DATEDIFF(b.fecha_fin, b.fecha_inicio)) = 0 THEN 0
  ELSE
  CASE WHEN AVG(DATEDIFF(CURDATE(), b.fecha_inicio) +1 )< 0 THEN 0 ELSE AVG((((DATEDIFF(CURDATE(), b.fecha_inicio) + 1)/(DATEDIFF(b.fecha_fin, b.fecha_inicio)+1))*100)) END END AS Meta_Planificada,
CASE WHEN AVG((DATEDIFF(CURDATE(), b.fecha_inicio)/DATEDIFF(b.fecha_fin, b.fecha_inicio))) = 0 THEN 0
  ELSE
  CASE WHEN AVG((DATEDIFF(CURDATE(), b.fecha_inicio) +1)) < 0 THEN 0 ELSE AVG((((avance/100)/((DATEDIFF(CURDATE(), b.fecha_inicio)+1)/(DATEDIFF(b.fecha_fin, b.fecha_inicio)+1))) * 100))  END END AS Rendimiento
FROM coor_tmov_cronograma_cab  AS b 
	INNER JOIN tmae_direcciones AS a ON 
	 b.id_direccion = a.id 
	INNER JOIN users ON 
	 b.id_usuario = users.id 
	 WHERE b.estado='ACT' 
AND CAST(a.id AS CHAR) LIKE CASE WHEN $P{IDDIRECCION} ='0' THEN '%' ELSE  $P{IDDIRECCION}  END
	AND	b.prioridad LIKE case when $P{PRIORIDAD}='0' then '%' else  $P{PRIORIDAD} end 
	AND b.estado_actividad LIKE case when $P{ESTADO}='0' then '%' else  $P{ESTADO} end
	AND DATE_FORMAT(b.updated_at,'%Y-%m-%d') BETWEEN $P{FECHAACTUDESDE} AND $P{FECHAACTUHASTA}
	AND b.fecha_inicio >= $P{FECHAINICIO} 
	AND b.fecha_fin <=$P{FECHAFINAL} 
	GROUP BY a.direccion

	 ORDER BY 
	 Rendimiento desc
/*lISTADO aCTIVIDAD*/
SELECT b.id,
	a.direccion,
	b.estado_actividad,
	b.actividad,
	b.fecha_inicio,
	b.fecha_fin,
	users.name,
	b.updated_at,
	b.avance,
	b.prioridad,
	b.estado,
	a.id ,
	DATEDIFF(b.fecha_fin, b.fecha_inicio) +1 AS dias_plan,
CASE WHEN DATEDIFF(CURDATE(), b.fecha_inicio) +1 < 0 THEN 0 ELSE DATEDIFF(CURDATE(), b.fecha_inicio) + 1 END AS dias_trans,
CASE WHEN DATEDIFF(b.fecha_fin, b.fecha_inicio) = 0 THEN 0
  ELSE
  CASE WHEN DATEDIFF(CURDATE(), b.fecha_inicio) +1 < 0 THEN 0 ELSE (((DATEDIFF(CURDATE(), b.fecha_inicio) + 1)/(DATEDIFF(b.fecha_fin, b.fecha_inicio)+1))*100) END END AS Meta_Planificada,
CASE WHEN (DATEDIFF(CURDATE(), b.fecha_inicio)/DATEDIFF(b.fecha_fin, b.fecha_inicio)) = 0 THEN 0
  ELSE
  CASE WHEN (DATEDIFF(CURDATE(), b.fecha_inicio) +1) < 0 THEN 0 ELSE (((avance/100)/((DATEDIFF(CURDATE(), b.fecha_inicio)+1)/(DATEDIFF(b.fecha_fin, b.fecha_inicio)+1))) * 100)  END END AS Rendimiento
FROM coor_tmov_cronograma_cab  AS b 
	INNER JOIN tmae_direcciones AS a ON 
	 b.id_direccion = a.id 
	INNER JOIN users ON 
	 b.id_usuario = users.id 
	 WHERE b.estado='ACT' 
	  AND CAST(a.id AS CHAR) LIKE case when $P{IDDIRECCION}='0' then '%' else  $P{IDDIRECCION} end AND
		 b.prioridad LIKE case when $P{PRIORIDAD}='0' then '%' else  $P{PRIORIDAD} end AND
		 b.estado_actividad LIKE case when $P{ESTADO}='0' then '%' else  $P{ESTADO} end
	AND DATE_FORMAT(b.updated_at,'%Y-%m-%d') BETWEEN $P{FECHAACTUDESDE} AND $P{FECHAACTUHASTA}
	AND b.fecha_inicio >= $P{FECHAINICIO} 
	AND b.fecha_fin <=$P{FECHAFINAL}
	 ORDER BY a.direccion,
	 b.prioridad,
	 Rendimiento desc,
	 b.fecha_inicio