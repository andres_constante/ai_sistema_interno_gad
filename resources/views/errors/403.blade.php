@extends ('layout')
@section ('titulo')  Página no Autorizada @stop
@section ('contenido')
<h1> 
    Página no Autorizada
</h1>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        @if (Session::has('msgacceso'))
                        <script>    
                    $(document).ready(function() {
                        //toastr.succes("{{ Session::get('message') }}");
                        toastr["success"]("{{ Session::get('msgacceso') }}");
                        //$.notify("{{ Session::get('message') }}","success");
                    });
                    </script>    
                    <div class="alert alert-info"><i>{{ Session::get('msgacceso') }}</i></div>
                        @endif                          

                        
                    </div>
                    <div class="ibox-content">
                        <div class="text-center animated fadeInDown">
        <i class="fa fa-lock fa-5x"></i>
        <h1>403</h1>
        <div class="error-desc">
            La página a la que intentas acceder no está disponible en este momento.<br><br>
            <i>{{ Request::url() }}</i>
        </div>
    </div>    
                </div> <!-- ibox-content -->
             </div> <!-- ibox float-e-margins -->
@stop
