<?php
if(!isset($nobloqueo))
   Autorizar(Request::path());
?>

@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
 <script>
        $(document).ready(function() {

            $(".chosen-select").chosen(
  {
    no_results_text: "No existe coincidencia con lo que busca...",
    placeholder_text_single: "Seleccione...",
    placeholder_text_multiple: "Seleccione..."
});

        $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.50), innerHeight:screen.height -(screen.height * 0.55)}); 
        
        if("{{$configuraciongeneral[1]}}"=="coordinacioncronograma/coordinaciongraficos"){
            var chart = c3.generate({
            bindto:'#chart1',
            data: {
                columns: [
                    
                ],
                type : 'donut',
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "OBRAS"
            }
        });
        var chart2 = c3.generate({
            bindto:'#chart2',
            data: {
                columns: [
                    
                ],
                type : 'donut',
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "ACTIVIDADES"
            }
        });
        var chart3 = c3.generate({
            bindto:'#chart3',
            
            data: {
                columns: [
                    
                ],
                type : 'bar',
                
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "ACTIVIDADES POR DIRECCIONES"
            }
        });


        var chart4 = c3.generate({
            bindto:'#chart4',
            
            data: {
                columns: [
                    
                ],
                type : 'donut',
                
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "ACTIVIDADES"
            }
        });






        setTimeout(function () {
                //chart 1
                var used1 = []; 
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("coordinacioncronograma/getValoresCharts") }}',
                    data: {'tipo':'1'},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['estado_obra'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        var remove = used1.filter(function(v){ return nkeys.indexOf(v) < 0;});
                        used1 = nkeys;

                        // Load the new data
                        chart.load({columns: x, unload: remove});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 

                //chart2
                var used2 = []; 
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("coordinacioncronograma/getValoresCharts") }}',
                    data: {'tipo':'2'},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['estado_actividad'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        var remove = used2.filter(function(v){ return nkeys.indexOf(v) < 0;});
                        used2 = nkeys;

                        // Load the new data
                        chart2.load({columns: x, unload: remove});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 

                //CHART3
                var used3 = []; 
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("coordinacioncronograma/getValoresCharts") }}',
                    data: {'tipo':'3'},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['direccion'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        var remove = used3.filter(function(v){ return nkeys.indexOf(v) < 0;});
                        used3 = nkeys;

                        // Load the new data
                        chart3.load({columns: x, unload: remove});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 
            
        }, 10);


        $("#direccion").change(function(){               
                var used = []; 
                var direccion=0;

                direccion= $("#direccion").val();
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("coordinacioncronograma/getValoresCharts") }}',
                    data: {'tipo':'4','direccion':direccion},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['estado_actividad'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        
                        used = nkeys;

                       
                            chart4.load({columns: x, unload: true});
                         
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 
            });

        }else if("{{$configuraciongeneral[1]}}"=="comunicacionalcaldia/comunicaciongraficos"){
            
        var chart2 = c3.generate({
            bindto:'#chart2',
            data: {
                columns: [
                    
                ],
                type : 'donut',
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "ACTIVIDADES"
            }
        });
        var chart3 = c3.generate({
            bindto:'#chart3',
            
            data: {
                columns: [
                    
                ],
                type : 'bar',
                
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "ACTIVIDADES POR DIRECCIONES"
            }
        });


        var chart4 = c3.generate({
            bindto:'#chart4',
            
            data: {
                columns: [
                    
                ],
                type : 'donut',
                
                onclick: function (d, i) { console.log("onclick", d, i); },
                onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            },
            transition: {
                duration: 3000
            },
            donut: {
                title: "ACTIVIDADES"
            }
        });






        setTimeout(function () {
                //chart2
                var used2 = []; 
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("comunicacionalcaldia/getValoresCharts") }}',
                    data: {'tipo':'2'},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['estado_actividad'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        var remove = used2.filter(function(v){ return nkeys.indexOf(v) < 0;});
                        used2 = nkeys;

                        // Load the new data
                        chart2.load({columns: x, unload: remove});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 

                //CHART3
                var used3 = []; 
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("comunicacionalcaldia/getValoresCharts") }}',
                    data: {'tipo':'3'},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['direccion'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                        var remove = used3.filter(function(v){ return nkeys.indexOf(v) < 0;});
                        used3 = nkeys;

                        // Load the new data
                        chart3.load({columns: x, unload: remove});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 
            
        }, 10);


        $("#direccion").change(function(){               
                var used = []; 
                var direccion=0;

                direccion= $("#direccion").val();
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("comunicacionalcaldia/getValoresCharts") }}',
                    data: {'tipo':'4','direccion':direccion},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       var x=[];
                       for (i = 0; i < datos.length; i++) {
                        x.push([datos[i]['estado_actividad'],datos[i]['valor']]);
                        }
                         // Create an array of old keys
                        var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                       
                        used = nkeys;
                        chart4.load({columns: x, unload: true});
                        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 
            });
        }else if("{{$configuraciongeneral[1]}}"=="coordinacioncronograma/coordinaciongraficosobras"){
            
            var chart2 = c3.generate({
                bindto:'#chart2',
                data: {
                    columns: [
                        
                    ],
                    type : 'donut',
                    onclick: function (d, i) { console.log("onclick", d, i); },
                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                },
                transition: {
                duration: 3000
            },
                donut: {
                    title: "OBRAS"
                }
            });
            var chart3 = c3.generate({
                bindto:'#chart3',
                
                data: {
                    columns: [
                        
                    ],
                    type : 'bar',
                    
                    onclick: function (d, i) { console.log("onclick", d, i); },
                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                },
                transition: {
                duration: 3000
            },
                donut: {
                    title: "OBRAS POR PARROQUIAS"
                }
            });
            var chart4 = c3.generate({
                bindto:'#chart4',
                
                data: {
                    columns: [
                        
                    ],
                    type : 'donut',
                    
                    onclick: function (d, i) { console.log("onclick", d, i); },
                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                },
                transition: {
                duration: 3000
            },
                donut: {
                    title: "OBRAS POR PARROQUIA"
                }
            });      
    
    
    
    
            setTimeout(function () {
                    //chart2
                    var used2 = []; 
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("coordinacioncronograma/getValoresChartsObras") }}',
                        data: {'tipo':'2'},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           var x=[];
                           for (i = 0; i < datos.length; i++) {
                            x.push([datos[i]['estado_obra'],datos[i]['valor']]);
                            }
                             // Create an array of old keys
                            var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                            var remove = used2.filter(function(v){ return nkeys.indexOf(v) < 0;});
                            used2 = nkeys;
    
                            // Load the new data
                            chart2.load({columns: x, unload: remove});
    
            
                    },
                        statusCode: {
                            404: function() {
                           alert("<div class='alert alert-danger'>No existe URL</div>");
                        }	
                        }	  
                    }); 
    
                    //CHART3
                    var used3 = []; 
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("coordinacioncronograma/getValoresChartsObras") }}',
                        data: {'tipo':'3'},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           var x=[];
                           for (i = 0; i < datos.length; i++) {
                            x.push([datos[i]['parroquia'],datos[i]['valor']]);
                            }
                             // Create an array of old keys
                            var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                            var remove = used3.filter(function(v){ return nkeys.indexOf(v) < 0;});
                            used3 = nkeys;
    
                            // Load the new data
                            chart3.load({columns: x, unload: remove});
    
            
                    },
                        statusCode: {
                            404: function() {
                           alert("<div class='alert alert-danger'>No existe URL</div>");
                        }	
                        }	  
                    }); 
                
            }, 10);
    
    
            $("#direccion").change(function(){               
                    var used = []; 
                    var parroquia=0;
    
                    parroquia= $("#direccion").val();
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("coordinacioncronograma/getValoresChartsObras") }}',
                        data: {'tipo':'4','direccion':parroquia},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           var x=[];
                           for (i = 0; i < datos.length; i++) {
                            x.push([datos[i]['estado_obra'],datos[i]['valor']]);
                            }
                             // Create an array of old keys
                            var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                           
                            used = nkeys;
                            chart4.load({columns: x, unload: true});
                            
                    },
                        statusCode: {
                            404: function() {
                           alert("<div class='alert alert-danger'>No existe URL</div>");
                        }	
                        }	  
                    }); 
                });
            }else if("{{$configuraciongeneral[1]}}"=="tramitesalcaldia/tramitesgraficos"){
                var chart2 = c3.generate({
                bindto:'#chart2',
                data: {
                    columns: [
                        
                    ],
                    type : 'donut',
                    onclick: function (d, i) { console.log("onclick", d, i); },
                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                },
                transition: {
                duration: 3000
            },
                donut: {
                    title: "POR DISPOSICIÓN"
                }
            });
            var chart4 = c3.generate({
                bindto:'#chart4',
                
                data: {
                    columns: [
                        
                    ],
                    type : 'donut',
                    
                    onclick: function (d, i) { console.log("onclick", d, i); },
                    onmouseover: function (d, i) { console.log("onmouseover", d, i); },
                    onmouseout: function (d, i) { console.log("onmouseout", d, i); }
                },
                transition: {
                duration: 3000
            },
                donut: {
                    title: "POR PRIORIDAD"
                }
            });

            // var chart3 = c3.generate({
            //     bindto:'#chart3',
                
            //     data: {
            //         columns: [
                        
            //         ],
            //         type : 'area-spline',
                    
            //         onclick: function (d, i) { console.log("onclick", d, i); },
            //         onmouseover: function (d, i) { console.log("onmouseover", d, i); },
            //         onmouseout: function (d, i) { console.log("onmouseout", d, i); }
            //     },
            //     transition: {
            //     duration: 3000
            // },
            //     donut: {
            //         title: "POR SOLICITUD"
            //     }
            // });
           
            
            setTimeout(function () {
                    //chart2
                    var used2 = []; 
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("tramitesalcaldia/getValoresCharts") }}',
                        data: {'tipo':'2'},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           var x=[];
                           for (i = 0; i < datos.length; i++) {
                            x.push([datos[i]['disposicion'],datos[i]['valor']]);
                            }
                             // Create an array of old keys
                            var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                            var remove = used2.filter(function(v){ return nkeys.indexOf(v) < 0;});
                            used2 = nkeys;
    
                            // Load the new data
                            chart2.load({columns: x, unload: remove});
    
            
                    },
                        statusCode: {
                            404: function() {
                           alert("<div class='alert alert-danger'>No existe URL</div>");
                        }	
                        }	  
                    }); 
    
                    //CHART3
                    var used3 = []; 
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("tramitesalcaldia/getValoresCharts") }}',
                        data: {'tipo':'3'},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           var x=[];
                           for (i = 0; i < datos.length; i++) {
                            x.push([datos[i]['prioridad'],datos[i]['valor']]);
                            }
                             // Create an array of old keys
                            var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                            var remove = used3.filter(function(v){ return nkeys.indexOf(v) < 0;});
                            used3 = nkeys;
    
                            // Load the new data
                            chart4.load({columns: x, unload: remove});
    
            
                    },
                        statusCode: {
                            404: function() {
                           alert("<div class='alert alert-danger'>No existe URL</div>");
                        }	
                        }	  
                    }); 
                //     $.ajax({
                //     type: "GET",
                //     url: '{{ URL::to("tramitesalcaldia/getValoresCharts") }}',
                //     data: {'tipo':'4'},
                //     error: function(objeto, quepaso, otroobj){
                //         alert(quepaso);
                //     },				
                //     success: function(datos){
                //        var x=[];
                //        for (i = 0; i < datos.length; i++) {
                //         x.push([datos[i]['created_at'],datos[i]['valor']]);
                //         }
                //          // Create an array of old keys
                //         var nkeys = x.reduce(function(p, c){ p.push(c[0]); return p; }, []);
                       
                //         used = nkeys;
                //         chart3.load({columns: x, unload: true});
                        
                // },
                //     statusCode: {
                //         404: function() {
                //        alert("<div class='alert alert-danger'>No existe URL</div>");
                //     }	
                //     }	  
                // }); 
                
            }, 10);
        
            }
                
    });  
    





</script>
@stop
@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .c3-chart-arcs-title {
  fill:#0e9aef     ;
  font-size: 18%;
  font-weight: bold;
}
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

@stop
@section ('contenido')
<h1 style="background-color: #FFFFFF"> {{ $configuraciongeneral[0] }}</h1>
                <div class="ibox float-e-margins">
                    <div class="ibox-title"> 
    @if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
     <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif                    

                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                               
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">   
                    <div class="row">
                    @if($configuraciongeneral[1]=="coordinacioncronograma/coordinaciongraficos")
                    @foreach($objetos as $key => $value)

                    @if($value->Nombre=='chart2'|| $value->Nombre=='chart4')
                    <div class="col-lg-6">
                            <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                            @if($value->Nombre=='chart4')
                                <div>
                                <select  class="form-control chosen-select" name="direccion" id="direccion">
                                <option value="">Seleccione Direccion</option>
                                @foreach($direcciones as $direccion)
                                    <option value="{{$direccion->id}}">{{$direccion->direccion}}</option>
                                @endforeach
                                </select>
                                
                                
                                </div>
                            @endif

                                <div>
                                    <div id="{{ $value->Nombre }}"></div>
                                </div>
                            </div>
                            </div>
                            
                        
                    </div>
                    @elseif($value->Nombre=='chart3')
                    <div class="col-lg-12">
                    
                            <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                           
                                
                                <div>
                                    <div id="{{ $value->Nombre }}"></div>
                                </div>
                            </div>
                            </div>
                            
                        
                    </div>
                    @endif


                    @endforeach
                @elseif($configuraciongeneral[1]=="comunicacionalcaldia/comunicaciongraficos")
                @foreach($objetos as $key => $value)

                    @if($value->Nombre=='chart2'|| $value->Nombre=='chart4')
                    <div class="col-lg-6">
                            <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                            @if($value->Nombre=='chart4')
                                <div>
                                <select  class="form-control" name="direccion" id="direccion">
                                <option value="">Seleccione una Direccíon</option>
                                @foreach($direcciones as $direccion)
                                    <option value="{{$direccion->id}}">{{$direccion->direccion}}</option>
                                @endforeach
                                </select>
                                
                                
                                </div>
                            @endif

                                <div>
                                    <div id="{{ $value->Nombre }}"></div>
                                </div>
                            </div>
                            </div>
                            
                        
                    </div>
                    @elseif($value->Nombre=='chart3')
                    <div class="col-lg-12">
                    
                            <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                           
                                
                                <div>
                                    <div id="{{ $value->Nombre }}"></div>
                                </div>
                            </div>
                            </div>
                            
                        
                    </div>
                    @endif


                    @endforeach

                    @elseif($configuraciongeneral[1]=="coordinacioncronograma/coordinaciongraficosobras")
                @foreach($objetos as $key => $value)

                    @if($value->Nombre=='chart2'|| $value->Nombre=='chart4')
                    <div class="col-lg-6">
                            <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                            @if($value->Nombre=='chart4')
                                <div>
                                <select  class="form-control" name="direccion" id="direccion">
                                <option value="">Seleccione una Parroquia</option>
                                @foreach($direcciones as $direccion)
                                    <option value="{{$direccion->id}}">{{$direccion->parroquia}}</option>
                                @endforeach
                                </select>
                                
                                
                                </div>
                            @endif

                                <div>
                                    <div id="{{ $value->Nombre }}"></div>
                                </div>
                            </div>
                            </div>
                            
                        
                    </div>
                    @elseif($value->Nombre=='chart3')
                    <div class="col-lg-12">
                    
                            <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>{{ $value->Descripcion }}</h5>
                            </div>
                            <div class="ibox-content">
                           
                                
                                <div>
                                    <div id="{{ $value->Nombre }}"></div>
                                </div>
                            </div>
                            </div>
                            
                        
                    </div>
                    @endif


                    @endforeach
                    @elseif($configuraciongeneral[1]=="tramitesalcaldia/tramitesgraficos")
                    @foreach($objetos as $key => $value)
    
                        @if($value->Nombre=='chart2'|| $value->Nombre=='chart4')
                        <div class="col-lg-6">
                                <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>{{ $value->Descripcion }}</h5>
                                </div>
                                <div class="ibox-content">

                                    <div>
                                        <div id="{{ $value->Nombre }}"></div>
                                    </div>
                                </div>
                                </div>
                                
                            
                        </div>
                        @elseif($value->Nombre=='chart3')
                        <div class="col-lg-12">
                        
                                <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>{{ $value->Descripcion }}</h5>
                                </div>
                                <div class="ibox-content">
                               
                                    
                                    <div>
                                        <div id="{{ $value->Nombre }}"></div>
                                    </div>
                                </div>
                                </div>
                                
                            
                        </div>
                        @endif
    
    
                        @endforeach
    
                @endif

            </div>
        </div>
                      </div>

                        
                </div> <!-- ibox-content -->
</div> <!-- ibox float-e-margins -->
@stop
