<?php
if(!isset($nobloqueo))
   Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
 <script>
    setTimeout(function(){
        parent.$.fn.colorbox.close();
    },2000)
    
        $(document).ready(function() {
          
            if ('{{$configuraciongeneral[6]}}'=='null') {
            
               
                $('#tbbuzonmain').dataTable({
                    responsive : true,
                    language: {
                        "emptyTable":     "No hay datos disponibles en la tabla",
                        "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                        "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                        "infoFiltered":   "(filtered from _MAX_ total entries)",
                        "infoPostFix":    "",
                        "thousands":      ",",
                        "lengthMenu":     "Mostrar _MENU_ entradas",
                        "loadingRecords": "Cargando...",
                        "processing":     "Procesando...",
                        "search":         "Buscar:",
                        "zeroRecords":    "No se encontraron registros coincidentes",
                        "paginate": {
                            "first":      "Primero",
                            "last":       "Último",
                            "next":       "Siguiente",
                            "previous":   "Atrás"
                        },
                        "aria": {
                            "sortAscending":  ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        }
                    },
                    "dom": 'T<"clear">lfrtip',
                    
                    "tableTools": {
                        "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                    },
                        "order": ([[ 0, 'desc' ]])
                        //"order": ([[ 2, 'asc' ], [ 1, 'asc' ]])
                        //../js/plugins/dataTables/swf/copy_csv_xls_pdf.swf
                    
                });
                
            }else{
                
                URL="{{ URL::to("$configuraciongeneral[6]") }}";

            $('#tbbuzonmain').dataTable({
                responsive : true,
                processing: true,
                serverSide: true,
                lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],		
                order: ([ 0, 'desc' ]),	
                dom: 'flrBtip',//Bflipt	
                buttons: [ 'print','copy', 'excel', 'pdf'],	
                ajax: {
                    url: URL,   
                    dataType: "json",	
                    type: "GET",	
                    data:{ _token: "{{csrf_token()}}"}
                },
                
                columns:[
                    { "data": 'id' },
                    @foreach($objetos as $key => $value)
                            { "data":'{{$value->Nombre}}' },
                    @endforeach
                    
                    { "data": 'acciones' },
                ],
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'T<"clear">lfrtip',
                
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                    "order": ([[ 0, 'desc' ]])
                    //"order": ([[ 2, 'asc' ], [ 1, 'asc' ]])
                    //../js/plugins/dataTables/swf/copy_csv_xls_pdf.swf
                  
            });

            }
                        
        $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.10), innerHeight:screen.height -(screen.height * 0.30)}); 
        });  

        function mostrar()
    {
            var cadena= $("#cadena").val();
                $('#tbbuzonmain').DataTable({
                destroy: true,
                responsive : true,
                ajax: {
                url: '{{ URL::to("getUsers") }}'+'?cadena='+cadena,   
                dataSrc:"",
                },
                columns: [
                        { data: 'id'},
                        { data: 'cedula'},
                        { data: 'name'},
                        { data: 'telefono'},
                        { data: 'email'},
                        { data: 'fecha_nacimiento'}, 
                        { data: 'perfil'},
                        { data: 'direccion'},
                        { data: 'cargo'},
        
                    {data: "id" , render : function ( data, type, row, meta ) {
              return type === 'display'  ?
              '<a href="{{ URL::to($configuraciongeneral[1]) }} /'+data+'" class="divpopup" target="_blank""><i class="fa fa-newspaper-o"></i></a><a href="{{ URL::to($configuraciongeneral[1]) }}/'+data+'/edit"><i class="fa fa-pencil-square-o"></i></a>@if(isset($delete))<a href="javascript::" onclick="eliminar('+data+')"><i class="fa fa-trash"></i> </a>@endif':
                data;
            }}],
                    
                    language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'T<"clear">lfrtip',
                
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                    "order": ([[ 0, 'desc' ]])
                    //"order": ([[ 2, 'asc' ], [ 1, 'asc' ]])
                    //../js/plugins/dataTables/swf/copy_csv_xls_pdf.swf
                  
            });  


    }









@if(isset($delete))
@if($delete=='si')
    function eliminar($id)
    {
            var r= confirm("Seguro de eliminar este registro?");
            if(r==true)                        
                $('#frmElimina'+$id).submit();
            else
                return false;
    }
@endif
@endif
</script>
@stop
@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

@stop
@section ('contenido')
<h1 style="background-color: #FFFFFF"> {{ $configuraciongeneral[0] }}</h1>
                <div class="ibox float-e-margins">
                    <div class="ibox-title"> 
    @if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
     <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif                          
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                @if($create=='si')
                                <li><a href="{{ URL::to($configuraciongeneral[1]) }}">Listar Todos</a>
                                </li>
                               
                                <li><a id="btnnuevo" href="{{ URL::to($configuraciongeneral[1]."/create") }}">Nuevo</a>
                                </li>
                                @endif
                               
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                     @if (isset($configuraciongeneral[3]))   
                      {{Form::open(array('method' => 'PUT','action' => $configuraciongeneral[3],'role'=>'form' ,'class'=>'form-inline','id'=>'formu')) }}
                           <div class="row"> 
                               <div class="col-md-12">
                                    <div class="col-md-6">        
                                        {{ Form::label($configuraciongeneral[1].'bus', 'Filtrar Busqueda por:',array('class'=>'control-label')) }}          
                                        {{ Form::select($configuraciongeneral[1].'bus',[null=>'Escoja'] + $listabusqueda ,$valorcmb,array('class' => 'chosen-select','style' => 'width:90%')) }}  <br> <br> 
                                    </div>
                                    <div class="col-md-3">
                                        {{ Form::submit('Buscar', array('class' => 'btn btn-primary')) }}
                                    </div>    
                             </div>
                          </div>
                        {{Form::hidden('ruta',URL::to('pensionrecaudacion') ,array('id'=>'ruta'))}}
                        {{ Form::close() }} 
                     @endif   
                      <div class="">
                      @if($configuraciongeneral[1]=='usuarios')
                            
                            <label for="cadena">    Nombre</label>
                            <input id="cadena" class="cadena" type="text">
                            <a onclick="mostrar()" class="btn btn-primary">Mostrar</a>
                        @endif
                        @if($create=='si')
                       <a  href="{{ URL::to($configuraciongeneral[1]) }}" class="btn btn-primary ">Todos</a>
                       
                        <a  href="{{ URL::to($configuraciongeneral[1]."/create") }}" class="btn btn-default ">Nuevo</a>
                       @endif
                      </div>
                
                <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display" >
                    <thead>
                        <tr>           
                            @if (!isset($verid))
                            <th>ID</th>
                        @endif 
                        
                        
                           @foreach($objetos as $key => $value)
                           <th>{{ $value->Descripcion }}</th>
                           @endforeach
                            <th>Acción</th>
                    </tr>
                    </thead>

                    <tfoot>
                        <tr>            
                            @if (!isset($verid))
                            <th>ID</th>
                            @endif 
                           @foreach($objetos as $key => $value)
                           <th>{{ $value->Descripcion }}</th>
                           @endforeach
                            <th>Acción</th>
                    </tr>
                    </tfoot> 
                    <tbody>
                    
                    @foreach($tabla as $key => $value)
                    <tr>
                        @if (!isset($verid))
                        <td>{{ $value->id }} </td> 
                        @endif 
                        
                        @foreach($objetos as $keycam => $valuecam)                        
                               <td>
                               <?php
                               if($valuecam->Tipo=="file")
                               {     $cam=$valuecam->Nombre;                           
                                    if($value->$cam!="")
                                        $cadena="echo '<a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$value->".$valuecam->Nombre.").' class=\"btn btn-success dropdown-toggle divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i></a>';";
                                    else
                                        $cadena="echo '';";
                               }else
                               {
                                $cadena="echo trim(\$value->".$valuecam->Nombre.");";
                               }

                                ?>
                                    @if($valuecam->Nombre=="valor_predefinido")
                                        <textarea style="margin: 0px; width: 343px; height: 185px;" disabled>
                                            <?php eval($cadena);?>
                                         </textarea>
                                    @else
                                        <?php eval($cadena); ?>
                                    @endif
                               </td>
                        @endforeach 
                        <td>
                        <!--      Cuando es vista de muchos botones -->
                        @if (isset($botonruta))
                                        <div class="btn-group">
                                                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Editar <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                @foreach($botonruta as $keyboton => $botonesvalue)
                                                    <?php 
                                                        eval("\$cade=\$value->".$botonvariable[$keyboton].";");
                                                        $rutaboton = str_replace("@variable",$cade, $botonesvalue);
                                                    //$rutaboton = str_replace("@variable", $value->$botonvariable[$keyboton], $botonesvalue);
                                                    ?>
                                                        <li><a href="{{ URL::to($rutaboton)}}" target="_blank" class="font-bold">{{ $botoncaption[$keyboton] }} </a></li>
                                                        <li class="divider"></li>
                                                @endforeach   
                                                </ul>
                                        </div>
                                    @else
                                        <a href="{{ URL::to($configuraciongeneral[1]."/".$value->id) }}" class="divpopup" target="_blank"">
                                            <i class="fa fa-newspaper-o" ></i></a>&nbsp;&nbsp;
                                            @if(!isset($edit))
                                            <a href="{{ URL::to($configuraciongeneral[1]."/".$value->id."/edit") }}"><i class="fa fa-pencil-square-o"></i></a>
                                            @endif&nbsp;
                                            @if(isset($editfinalizartramite))
                                            <a href="{{ URL::to("tramitesalcaldia/finalizartramite?id=".$value->id) }}"><i class="fa fa-pencil-square-o"></i></a>
                                            @endif&nbsp;
                                           
                                        @if(isset($delete))
                                        @if($delete=='si')
                                            <a href="javascript::" onclick="eliminar({{ $value->id }})"><i class="fa fa-trash"></i> </a>                                            
                                        @endif
                                        @endif

                                        @if(isset($imprimir))
                                     
                                            <a href="vertramite/{{ $value->id }}" class="divpopup" target="_blank""><i class="fa fa-print"></i> </a>                                            
                                     
                                        @endif
                                    @endif
                        </td>
                    </tr>
                      @endforeach
                     </tbody>
                    </tfoot> 
                           
                </table>
                        {{-- $tabla->links() --}}
                </div> <!-- ibox-content -->
</div> <!-- ibox float-e-margins -->
@if(isset($delete))
@if($delete=='si'   )
    <div style="display: none;">
    @foreach($tabla as $key => $value)
        {!! Form::open(['route' => [str_replace("/",".",$configuraciongeneral[1]).'.destroy', $value->id], 'method' => 'delete','id'=>'frmElimina'.$value->id,'class' => 'pull-right']) !!}
            {!! Form::submit('Eliminar', array('class' => 'btn btn-small btn-warning')) !!}
        {!! Form::close() !!}
    @endforeach
    </div>
@endif
@endif
@stop




