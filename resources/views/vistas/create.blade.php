<?php
if(!isset($nobloqueo))
    Autorizar(Request::path());
?>
@extends(Input::has("menu") ? 'layout_basic_no_head':'layout'  )
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
<style type="text/css">
  #contenidodiv {
    height: 150px;
    overflow: auto;
}
</style>

@include("vistas.includes.mainjs")
<script type="text/javascript">

$(document).ready(function() {
//Evento change al escoger en Select
$("#btnmas").click(function(){
    $('#contenidodiv').css("overflow","hidden").css("height","auto");
    $(this).hide();
});
Dropzone.options.myDropzone = {
            autoProcessQueue: false,
            uploadMultiple: true,
            maxFilezise: 5,
            maxFiles: 5,
            acceptedFiles:".jpg,.png,.pdf",
            addRemoveLinks: true,
            
            init: function() {
                var submitBtn = document.querySelector("#submit");
                myDropzone = this;
                
                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });
                this.on("addedfile", function(file) {
                  toastr["success"]("Se Agregó un Nuevo Archivo al Gestor");
                  
                });
                
                this.on("complete", function(file) {
                    myDropzone.removeFile(file);
                    
                  
                    
                });
                
                this.on("success", function() {
                  myDropzone.processQueue.bind(myDropzone);
                   //$("#ModalArchivos").hide();
                   toastr["success"]("Archivos Agregados Correctamente");
                   location.reload();
                  
                });
                  
            }
};

// document.getElementById('lightBoxGallery').onclick = function (event) {
//     event = event || window.event;
//     var target = event.target || event.srcElement,
//         link = target.src ? target.parentNode : target,
//         options = {index: link, event: event},
//         links = this.getElementsByTagName('a');
//     blueimp.Gallery(links, options);
// };


@if (isset($edit))
  $('#idusuario_chosen').hide();
  //alert("ss");
@endif
 $("#id_menu").change(function(){
    var id=this.value;
    var ruta= '<?php echo URL::to('getmenumodulos'); ?>?idmenu='+id; 
    $.get(ruta, function(data) {
      console.log(data);
      $("#id_menu_hijo").empty();  
      /*$("#id_menu_hijo").append("<option value>Escoja...</option>");*/
      $.each(data, function(key, element) {
        $("#id_menu_hijo").append("<option value='" + key + "'>" + element + "</option>");
      });
      $('#id_menu_hijo').trigger("chosen:updated");
    });
 });

@if($configuraciongeneral[2]=="crear"){

  //$("#estado_obra").hide();
  //$("label[for='estado_obra']").hide();
  

}

@endif
$(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.10), innerHeight:screen.height -(screen.height * 0.30)}); 
});


</script>
@include("vistas.includes.jsfunciones")
@stop
@section ('contenido')
@if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
    $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.50), innerHeight:screen.height -(screen.height * 0.55)}); 
        }); 
});
</script>    
     <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif   
<h1> 
    @if($configuraciongeneral[2]=="crear")
        Nuevo Registro - {!!  $configuraciongeneral[0]  !!}
    @else
        Editar Registro - {!!  $configuraciongeneral[0]  !!}
    @endif
</h1>

<div class="ibox float-e-margins">
     <div class="ibox-title">
        <div class="">
       
        @if(!isset($variableControl))
       
        <a  href="{{ URL::to($configuraciongeneral[1]) }}" class="btn btn-primary ">Todos</a>
            <a  href="{{ URL::to($configuraciongeneral[1]."/create") }}" class="btn btn-default ">Nuevo</a>
                          
          
        @endif
      </div>

     
<!--Enviar errores de validacion en caso de que los haya-->
    @if($errors->all())
        <script>    
        $(document).ready(function() {
        //    $.notify("Error al guardar!","error");
        toastr["error"]($("#diverror").html());    
        });
        </script>
        <div class="alert alert-danger" style="text-align: left;" id='diverror'>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <h4>Error!</h4>
          <div id="diverror">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
        </div> 
        </div>
    @endif  
    @if(isset($alerta) && $alerta!="")
        <div class="alert alert-danger" style="text-align: left;" id='diverror'>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4>Error!</h4>
            <ul><li>{!!  $alerta  !!}</li></ul>
        </div>
    @endif
 </div>

<div class="ibox-content">
    
    
    @if($configuraciongeneral[2]=="crear")
     <!--{!  Form::open(array('url' => $configuraciongeneral[1],'role'=>'form' , 'id'=>'form1','class'=>'form-horizontal'))  !!} -->
     {!!  Form::open(array('url' => $configuraciongeneral[1],'role'=>'form' , 'id'=>'form1','class'=>'form-horizontal', 'files' => true))  !!}
    @else
      {!!  Form::model($tabla, array('route' => array(str_replace("/",".",$configuraciongeneral[1]).'.update', $tabla->id), 'method' => 'PUT','class'=>'form-horizontal', 'id'=>'form1', 'files' => true ))  !!}
    @endif
    

         @foreach($objetos as $i => $campos)
                <div class="form-group">
                    {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                  <div class="col-lg-8"> 
                      @if($campos->Tipo=="date")
                          <!-- {! Form::input('date',$campos->Nombre,$campos->Valor, ['class' => 'form-control', 'placeholder' => 'Date']) !!}   -->
                        {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datefecha','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                      @elseif($campos->Tipo=="datetext")
                        <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          
                          {!! Form::text($campos->Nombre, null , array(
                            'data-placement' =>'top',
                            'data-toogle'=>'tooltip',
                            'class' => 'form-control datefecha',
                            'placeholder'=>$campos->Descripcion,
                            $campos->Nombre,
                            'readonly'=>'readonly'
                            )) !!}
                      </div>
                      @elseif($campos->Tipo=="select")
                         {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                      @elseif($campos->Tipo=="select2")
                         {!! Form::select($campos->Nombre,$campos->Valor, $campos->ValorAnterior, array('class' => "form-control ".$campos->Clase)) !!}   
                      @elseif($campos->Tipo=="select-multiple")
                         {!! Form::select($campos->Nombre."[]",$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre))!!}
                      @elseif($campos->Tipo=="file")                         
                          {!! Form::file($campos->Nombre, array('class' => 'form-control')) !!}
                        @if(isset($tabla))
                        <br>
                          <?php
                            $cadena="if(\$tabla->".$campos->Nombre."!='') echo '<a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$tabla->".$campos->Nombre.").' class=\"btn btn-success divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i> Ver Archivo</a>';";
                            eval($cadena);
                          ?>  
                        @endif
                      @elseif($campos->Tipo=="textdisabled")
                         {!!   Form::text($campos,Input::old($campos), array('class' => 'form-control','placeholder'=>'Ingrese '.$camposcaption[$i],'readonly')) !!}
                      @elseif($campos->Tipo=="textarea")
                        @if (isset($verObservaciones) &&  $campos->Clase=="mostrarobservaciones" )
                          <div id="contenidodiv">
                            @foreach ($timelineActividades as $item)
                              <p style="text-align: justify;"> <strong> {{$item->updated_at}} </strong> - {{$item->observacion }}</p>
                            @endforeach
                          </div>
                          <a class="btn btn-xs btn-primary" href="javascript:" id="btnmas"><i class="fa fa-search-plus"></i> Mostrar Todo </a>
                          <script type="text/javascript">

                          $(document).ready(function() {
                          //Evento change al escoger en Select
                          $('#contenidodiv').scrollTop($('#contenidodiv')[0].scrollHeight);
                        }
                      </script>
                        @endif
                      {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}


                      @elseif($campos->Tipo=="textmayus")
                          {!! Form::text($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion,'onkeyup'=>'aMays(event, this)')) !!}
                          
                      @elseif($campos->Tipo=="password")
                         {!!   Form::password($campos->Nombre,array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Nombre)) !!}
                         @elseif($campos->Tipo=="textdisabled2")
                         {!!   Form::text($campos->Nombre,Input::old($campos->Valor), array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}
                      @elseif($campos->Tipo=="selectdisabled")
                         {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase, 'readonly')) !!}
                      @elseif($campos->Tipo=="select-multiple-disabled")
                         {!! Form::select($campos->Nombre."[]",$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre,'readonly'))!!}          
                      @elseif($campos->Tipo=="textarea-disabled")
                          {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}                         
                      @else
                          {!! Form::text($campos->Nombre, $campos->ValorAnterior!="Null" ? $campos->ValorAnterior : Input::old($campos->Valor) , array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                      @endif
                  </div>
                </div>
        
        @endforeach
        
        @if(isset($popup)) 
          <input type="hidden" value="popup" name="txtpopup" />
        <!--else -->
          <!--input type="hidden" value="" name="txtpopup"/-->
        @endif
       
        
        {!!  Form::submit("Guardar", array('class' => 'btn btn-primary', 'style' => 'float:right' ))  !!}
        {!!  Form::close()  !!}
        @if($configuraciongeneral[2]=="editar" && isset($configuraciongeneral[3]) && isset($configuraciongeneral[4]))  

        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#ModalArchivos">
            <i class="fa fa-cloud-upload" style="font-size: 1.5em;">   </i> Subir Archivos</button><br><br>

        <div id="ModalArchivos" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
    
                      <div class="ibox-content row">
                                <div class="col-sm-12 offset-sm-1">
                                    <h1 class="page-heading"> <i class="fa fa-cloud-upload" style="font-size: 1em;"> Gestor Archivos  </i><span id="counter"></span></h1>
                          
                                  {!! Form::open(['route'=> 'subirarchivos.store', 'method' => 'POST', 'files'=>'true', 'id' => 'my-dropzone' , 'class' => 'dropzone']) !!}
                                  <div class="dz-message" style="height:200px;">
                                      Arrastre los Archivos Aquí!!
                                  </div>
                                  <input type="text" hidden name="id_referencia" value="{{$tabla->id}}">
                                <input type="text" hidden name="tipo" value="{{$configuraciongeneral[3]}}">
                                  <div class="dropzone-previews"></div>
                                  <button type="submit" class="btn btn-success" id="submit">Save</button>
                                  {!! Form::close() !!}
                                </div>
                      </div>  
                        
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <h2>Imágenes</h2>
                        <div class="lightBoxGallery" id="lightBoxGallery">
                          @foreach ($dicecionesImagenes as $item)
                          <a href="{{asset('archivos_sistema/'.$item->ruta)}}" title="{{$item->nombre}}" data-gallery=""><img width="200" height="100" src="{{asset('archivos_sistema/'.$item->ruta)}}"></a>           
                          @endforeach
                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            
        </div>
        
            <div class="row">
                <div class="col-lg-12">
               

                    <div class="ibox-content">

                        <h2>Archivos</h2>
                        <div>
                          @foreach ($dicecionesDocumentos as $item)
                        
                          
                          
                  
                        <a href="{{asset('archivos_sistema/'.$item->ruta)}}" type="button" class="btn btn-success dropdown-toggle divpopup" target="_blank"><i class="fa fa-file-pdf-o" style="font-size: 2em;"></i> </a> {{$item->nombre}}
                            
                          
                          @endforeach
                           
                            
                            
                        </div>

                </div>
          

            </div>
        </div>
       @endif
        
       
        @if (isset($timeline_obras))
        <h1>Historial</h1>        
        <div hidden>{{$contador=0}}</div> 
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
            <div class="vertical-timeline-block">
                @foreach ($timeline_obras as $item)
                <?php
                $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                $color_aleatoreo = $arraycolores[array_rand($arraycolores )];
              ?>
                <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                        {{-- <i class="fa fa-briefcase"></i> --}}
                    </div>
                  <div class="vertical-timeline-content">
                      
                      @if ($contador==0)
                      <h2><i class="fa fa-save"></i>  Inicial</h2>
                      
                      @elseif($contador>=1)
                      <h2><i class="fa fa-pencil"></i> Actualización</h2>
                      @endif
                     <div hidden> {{$contador+=1}} </div>

                  <p>
                    <pre style="text-align: left; white-space: pre-line;">
                        <strong>Nombre de Obra: </strong>{{$item->nombre_obra}}
                        <strong>Parroquia: </strong> {{$item->parroquia}}
                        <strong>Calle: </strong>  {{$item->calle}}
                         <strong>Tipo de obra: </strong> {{$item->tipo}} 
                            <strong>Monto: </strong> <span class="label label-primary" style="font-size: 1em;">$ {{ number_format ($item->monto,2) }}</span>                                
                                    <strong>Contratista: </strong>  {{$item->contratista}}
                                        <strong>Fecha de inicio: </strong>  {{$item->fecha_inicio}}
                                            <strong>Fecha de fin: </strong>  {{$item->fecha_final}}
                                            <strong>Estado de la obra: </strong>  <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->estado_obra)  }};">{{$item->estado_obra}}</span>
                                            <br>
                                            <i><strong>Modificado: </strong>  {{$item->name}}</i>
                                              
                                              
                                                    
                      </pre> 
                      </p>
                      <div class="ibox-content">                                              
                          <h2>Avance Actual: {{round($item->avance,0)}} %</h2>
                          <div class="progress progress-striped active m-b-sm">
                            <div style="width: {{$item->avance}}%;" class="progress-bar"></div>
                        </div>
                      </div>
                      <span class="vertical-date">
                          {{fechas(2,$item->updated_at)}} <br/>
                          <small>{{fechas(1000,$item->updated_at)}} </small>
                      </span>
                  </div>
                </div>
                @endforeach
               
            </div>

           
        </div>

        @endif
        @if (isset($timelineActividades))
    <h1>Historial</h1>
        <div hidden>{{$contador=0}}</div> 
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
            <div class="vertical-timeline-block">
               
                @foreach ($timelineActividades as $item)
                <?php
                $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                $color_aleatoreo = $arraycolores[array_rand($arraycolores )];
                
              ?>
                <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                        {{-- <i class="fa fa-briefcase"></i> --}}
                    </div>
                  <div class="vertical-timeline-content">
                      
                      @if ($contador==0)
                      <h2><i class="fa fa-save"></i>  Inicial</h2>
                      
                      @elseif($contador>=1)
                      <h2><i class="fa fa-pencil"></i> Actualización</h2>
                      @endif
                     <div hidden> {{$contador+=1}} </div>
                  <p>
                    <pre style="text-align: left; white-space: pre-line;">
                        <strong>Actividad: </strong>{{$item->actividad}}                        
                        <strong>Dirección a cargo: </strong>  {{$item->direccion}}
                                        <strong>Fecha de inicio: </strong>  {{$item->fecha_inicio}}
                                            <strong>Fecha de fin: </strong>  {{$item->fecha_fin}}                       
                        <strong>Prioridad: </strong>  {{$item->prioridad}}
                        <strong>Estado de la actividad: </strong> <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->estado_actividad)  }};">{{$item->estado_actividad}}</span>
                        <strong>Observación: </strong>  {{$item->observacion}}
                        <br>
                        <strong>Modificado por: </strong>  {{$item->name}}
                        
            </pre>
            <strong><i class="fa fa-users"></i> Responsables</strong>
            @if (isset($timelineActividadesResp))
            <pre style="text-align: left; white-space: pre-line;">
              @foreach ($timelineActividadesResp as  $i)
              <strong>Nombre: </strong>{{$i->name}}
              @endforeach

            </pre>
            @endif
                    </p>
                    <div class="ibox-content">                                              
                        <h2>Avance Actual: {{round($item->avance,0)}} %</h2>
                        <div class="progress progress-striped active m-b-sm">
                          <div style="width: {{$item->avance}}%;" class="progress-bar"></div>
                      </div>
                    </div>
                      <span class="vertical-date">
                          {{fechas(2,$item->updated_at)}} <br/>
                          <small>{{fechas(1000,$item->updated_at)}} </small>
                      </span>
                  </div>
                </div>
                @endforeach
               
            </div>

           
        </div>

        @endif

        @if(isset($timelineProyecto))
        
        <h1>Historial</h1>
            <div hidden>{{$contador=0}}</div> 
            <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
                <div class="vertical-timeline-block">
                   
                    @foreach ($timelineProyecto as $item)
                    <?php
                    $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                    $color_aleatoreo = $arraycolores[array_rand($arraycolores )];
                    
                  ?>
                    <div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                            {{-- <i class="fa fa-briefcase"></i> --}}
                        </div>
                      <div class="vertical-timeline-content">
                          
                          @if ($contador==0)
                          <h2><i class="fa fa-save"></i>  Inicial</h2>
                          
                          @elseif($contador>=1)
                          <h2><i class="fa fa-pencil"></i> Actualización</h2>
                          @endif
                         <div hidden> {{$contador+=1}} </div>
                      <p>
                        <pre style="text-align: left; white-space: pre-line;">
                            <strong>Nombre: </strong>{{$item->nombre}}                        
                            <strong>Monto: </strong>  {{$item->monto}}
                            <strong>Linea Base: </strong>  {{$item->linea_base}}
                            <strong>Meta: </strong>  {{$item->meta}}
                            <strong>Descripción: </strong>  {{$item->descripcion}}
                            <strong>Estado de la actividad: </strong> <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->estado_proyecto)  }};">{{$item->estado_proyecto}}</span>                      </p>
                          <strong>Observación: </strong>  {{$item->observacion}}
                          </pre>
                          <!-- <div class="ibox-content">                                              
                              <h2>Meta Actual: {{round($item->meta,0)}} %</h2>
                              <div class="progress progress-striped active m-b-sm">
                                <div style="width: {{$item->meta}}%;" class="progress-bar"></div>
                            </div>
                          </div> -->


                          <span class="vertical-date">
                              {{fechas(2,$item->updated_at)}} <br/>
                              <small>{{fechas(1000,$item->updated_at)}} </small>
                          </span>
                      </div>
                    </div>
                    @endforeach
                   
                </div>
    
               
            </div>
    
            @endif




  </div> 
  
</div> <!-- ibox float-e-margins -->

<script>
</script>    

@stop

