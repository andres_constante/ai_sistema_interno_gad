<?php
if(!isset($nobloqueo)) 
    //Autorizar(Request::path());
?>
@extends ('layout_basic_no_head')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('contenido')
<div class="container">
<!-- /.panel-heading -->
<h1>{{ $configuraciongeneral[0] }}</h1>
<hr />
 @if(!isset($detalle))
 <p>
  @foreach($objetos as $key => $valuecam)    
    <?php
    if(!isset($validararry))
      $cadena="echo \$tabla->".$valuecam->Nombre.";";
  	else
  	{
  		if(!in_array("div-".$valuecam->Nombre,$validararry))
  			$cadena="echo \$tabla->".$valuecam->Nombre.";";
  		else{  			
  			continue;
  		}
  	}
  		echo '<strong> '. $valuecam->Descripcion .':</strong>';
      	eval($cadena); 
    ?>
    <br>
@endforeach
</p>
@else
	<table class="table table-striped table-bordered table-hover">
	@foreach($objetos as $key => $value)
		<th>{{ $value->Descripcion }}</th>
	@endforeach
	@foreach($tabla as $key => $value)
		<tr>
		@foreach($objetos as $keycam => $valuecam)
			<td>
			<?php 
				$cadena="echo \$value->".$valuecam->Nombre.";";
					eval($cadena);                                   
				//echo $cadena;
				?>
			</td>
        @endforeach
        </tr>
    @endforeach
    </table>
@endif

@if($configuraciongeneral[1]=='coordinacioncronograma/cronogramacompromisos')
		<div class="col-lg-12">
                    
                            <div class="ibox float-e-margins">
                            <div class="ibox-title">
                              
							<strong>Responsables</strong>
							</div>

							<table id="tbbuzonmain" class="table table-striped table-bordered table-hover display" >
                    <thead>
                        <tr>           
                           <th>Nombre</th>
                        	<th>Correo</th>
                    </tr>
                    </thead>

                    
					<tbody>
                    @foreach($tabla2 as $key => $value)
                    <tr>
                         <td>{{ $value->name }} </td> 
						 <td>{{ $value->email }} </td>
                        
                    </tr>
                      @endforeach
                     </tbody>
                           
                </table>
				{{-- $tabla2->links() --}}   
                    </div>
@endif
@if($configuraciongeneral[1]=='tramitesalcaldia/tramites')
@if (isset($tabla2))
<div class="col-lg-12">
                    
    <div class="ibox float-e-margins">
    <div class="ibox-title">
      
<strong>Delegado</strong>
</div>

<table id="tbbuzonmain" class="table table-striped table-bordered table-hover display" >
<thead>
<tr>           
   <th>Nombre</th>
  <th>Correo</th>
</tr>
</thead>


<tbody>
@foreach($tabla2 as $key => $value)
<tr>
 <td>{{ $value->name }} </td> 
<td>{{ $value->email }} </td>

</tr>
@endforeach
</tbody>
   
</table>
{{-- $tabla2->links() --}}   
</div>
@endif
		
@endif

@if (isset($timeline_obras))
		<h1>Historial</h1>
        <div hidden>{{$contador=0}}</div> 
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
            <div class="vertical-timeline-block">
               
                @foreach ($timeline_obras as $item)
                <?php
                $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                $color_aleatoreo = $arraycolores[array_rand($arraycolores )];
                
              ?>
                <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                        {{-- <i class="fa fa-briefcase"></i> --}}
                    </div>
                  <div class="vertical-timeline-content">
                      
                        @if ($contador==0)
                        <h2><i class="fa fa-save"></i>  Inicial</h2>
                        
                        @elseif($contador>=1)
                        <h2><i class="fa fa-pencil"></i> Actualización</h2>
                        @endif
                       <div hidden> {{$contador+=1}} </div>

                  <p>
                        <pre style="text-align: left; white-space: pre-line;">
                        <strong>Nombre de Obra: </strong>{{$item->nombre_obra}}
                        <strong>Parroquia: </strong> {{$item->parroquia}}
                        <strong>Calle: </strong>  {{$item->calle}}
                         <strong>Tipo de obra: </strong> {{$item->tipo}} 
                            <strong>Monto: </strong> <span class="label label-primary" style="font-size: 1em;">$ {{ number_format ($item->monto,2) }}</span>                                
                                    <strong>Contratista: </strong>  {{$item->contratista}}
                                        <strong>Fecha de inicio: </strong>  {{$item->fecha_inicio}}
                                            <strong>Fecha de fin: </strong>  {{$item->fecha_final}}
                                            <strong>Estado de la obra: </strong>  <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->estado_obra)  }};">{{$item->estado_obra}}</span>
                                            <br>
                                            <i><strong>Modificado: </strong>  {{$item->name}}</i>
                                              
                                              
                                                    
                      </pre>
                      </p>
                      <div class="ibox-content">                                              
                            <h2>Avance Actual: {{round($item->avance,0)}} %</h2>
                            <div class="progress progress-striped active m-b-sm">
                              <div style="width: {{$item->avance}}%;" class="progress-bar"></div>
                          </div>
                        </div>
                      <span class="vertical-date">
                          {{fechas(2,$item->updated_at)}} <br/>
                          <small>{{fechas(1000,$item->updated_at)}} </small>
                      </span>
                  </div>
                </div>
                @endforeach
               
            </div>

           
        </div>

        @endif



        @if (isset($timelineTramite))
        <h1>Historial</h1>
            <div hidden>{{$contador=0}}</div> 
            <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
                <div class="vertical-timeline-block">
                   
                    @foreach ($timelineTramite as $item)
                    <?php
                    $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                    $color_aleatoreo = $arraycolores[array_rand($arraycolores )];
                    
                  ?>
                    <div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                            {{-- <i class="fa fa-briefcase"></i> --}}
                        </div>
                      <div class="vertical-timeline-content">
                          
                            @if ($contador==0)
                            <h2><i class="fa fa-save"></i>  Inicial</h2>
                            
                            @elseif($contador>=1)
                            <h2><i class="fa fa-pencil"></i> Actualización</h2>
                            @endif
                           <div hidden> {{$contador+=1}} </div>
    
                      <p>
                        <pre style="text-align: left; white-space: pre-line;">
                            <strong>Fecha de registro: </strong>{{$item->created_at	}}       
                            <strong>Numero de trámite: </strong>{{$item->numtramite}}                        
                            <strong>Remitente: </strong>  {{$item->remitente}}
                                            <strong>Asunto: </strong>  {{$item->asunto}}
                                                <strong>Petición: </strong>  {{$item->peticion}}                       
                            <strong>Prioridad: </strong>  {{$item->prioridad	}}
                            <strong>Recomendacion: </strong>  {{$item->recomendacion	}}
                            <strong>Disposicion	: </strong> <span class="label" style="font-size: 1em;background-color: ">{{$item->disposicion}}</span>
                            <strong>Observacion: </strong>  {{$item->observacion}}
                            <strong>Fecha de Repuesta: </strong>  {{$item->fecha}}
                            <br>
                            <strong>Modificado Por: </strong>  {{$item->name}}
                            
                </pre>
               
                @if (isset($timelineTramiteResp))
                <strong><i class="fa fa-users"></i> Delegados</strong>
                <pre style="text-align: left; white-space: pre-line;">
                  @foreach ($timelineTramiteResp as  $i)
                  <strong>Nombre: </strong>{{$i->name}}
                  @endforeach
    
                </pre>
                @endif
                        </p>
                    </div>
                    @endforeach
                   
                </div>
    
               
            </div>
    
            @endif



		@if (isset($timelineActividades))
		<h1>Historial</h1>
        <div hidden>{{$contador=0}}</div> 
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
            <div class="vertical-timeline-block">
               
                @foreach ($timelineActividades as $item)
                <?php
                $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                $color_aleatoreo = $arraycolores[array_rand($arraycolores )];
                
              ?>
                <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                        {{-- <i class="fa fa-briefcase"></i> --}}
                    </div>
                  <div class="vertical-timeline-content">
                      
                        @if ($contador==0)
                        <h2><i class="fa fa-save"></i>  Inicial</h2>
                        
                        @elseif($contador>=1)
                        <h2><i class="fa fa-pencil"></i> Actualización</h2>
                        @endif
                       <div hidden> {{$contador+=1}} </div>

                  <p>
                    <pre style="text-align: left; white-space: pre-line;">
                        <strong>Actividad: </strong>{{$item->actividad}}                        
                        <strong>Dirección a cargo: </strong>  {{$item->direccion}}
                                        <strong>Fecha de inicio: </strong>  {{$item->fecha_inicio}}
                                            <strong>Fecha de fin: </strong>  {{$item->fecha_fin}}                       
                        <strong>Prioridad: </strong>  {{$item->prioridad}}
                        <strong>Estado de la actividad: </strong> <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->estado_actividad)  }};">{{$item->estado_actividad}}</span>
                        <strong>Observación: </strong>  {{$item->observacion}}
                        <br>
                        <strong>Modificado por: </strong>  {{$item->name}}
                        
            </pre>
					  <strong><i class="fa fa-users"></i> Responsables</strong>
					  @if (isset($timelineActividadesResp))
					  <pre style="text-align: left; white-space: pre-line;">
						  @foreach ($timelineActividadesResp as  $i)
						  <strong>Nombre: </strong>{{$i->name}}
						  @endforeach

					  </pre>
					  @endif
                    </p>
                    <div class="ibox-content">                                              
                            <h2>Avance Actual: {{round($item->avance,0)}} %</h2>
                            <div class="progress progress-striped active m-b-sm">
                              <div style="width: {{$item->avance}}%;" class="progress-bar"></div>
                          </div>
                        </div>
                      <span class="vertical-date">
                          {{fechas(2,$item->updated_at)}} <br/>
                          <small>{{fechas(1000,$item->updated_at)}} </small>
                      </span>
                  </div>
                </div>
                @endforeach
               
            </div>

           
        </div>

        @endif



		@if (isset($avancesTimeline))
		<h1>Historial</h1>
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
            <div class="vertical-timeline-block">
               
                @foreach ($avancesTimeline as $item)
                <?php
                $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                $color_aleatoreo = $arraycolores[array_rand($arraycolores )];
                
              ?>
                <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                        {{-- <i class="fa fa-briefcase"></i> --}}
                    </div>
                  <div class="vertical-timeline-content">
                      
                        
                        <h2><i class="fa fa-pencil"></i> Actualización</h2>
                        

                  <p>
                    <pre style="text-align: left; white-space: pre-line;">
                        <strong>Parroquia: </strong>{{$item->parroquia}}
                        <strong>Barrio:</strong> {{$item->barrio}}
                        <strong>Modificado por el usuario:</strong>  {{$item->name}}
                        <span class="vertical-date">
                            {{fechas(2,$item->updated_at)}} <br/>
                            <small>{{fechas(1000,$item->updated_at)}} </small>
                        </span>                      
                                                
                      </pre>
                    </p>
                    <div class="ibox-content">                                              
                            <h2>Avance Actual: {{round($item->avance,0)}} %</h2>
                            <div class="progress progress-striped active m-b-sm">
                              <div style="width: {{round($item->avance,0)}}%;" class="progress-bar"></div>
                          </div>
                        </div>
                    
                  </div>
                </div>
                @endforeach
               
            </div>

           
        </div>

        @endif


        @if(isset($timelineProyecto))
        
        <h1>Historial</h1>
            <div hidden>{{$contador=0}}</div> 
            <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
                <div class="vertical-timeline-block">
                   
                    @foreach ($timelineProyecto as $item)
                    <?php
                    $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                    $color_aleatoreo = $arraycolores[array_rand($arraycolores )];
                    
                  ?>
                    <div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                            {{-- <i class="fa fa-briefcase"></i> --}}
                        </div>
                      <div class="vertical-timeline-content">
                          
                          @if ($contador==0)
                          <h2><i class="fa fa-save"></i>  Inicial</h2>
                          
                          @elseif($contador>=1)
                          <h2><i class="fa fa-pencil"></i> Actualización</h2>
                          @endif
                         <div hidden> {{$contador+=1}} </div>
                      <p>
                        <pre style="text-align: left; white-space: pre-line;">
                            <strong>Nombre: </strong>{{$item->nombre}}                        
                            <strong>Monto: </strong>  {{$item->monto}}
                            <strong>Linea Base: </strong>  {{$item->linea_base}}
                            <strong>Meta al: </strong>  {{$item->meta}} <strong>% </strong>
                            <strong>Descripción: </strong>  {{$item->descripcion}}
                            <strong>Estado de la actividad: </strong> <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->estado_proyecto)  }};">{{$item->estado_proyecto}}</span>                      </p>
                          <strong>Observación: </strong>  {{$item->observacion}}
                          <span class="vertical-date">
                              {{fechas(2,$item->updated_at)}} <br/>
                              <small>{{fechas(1000,$item->updated_at)}} </small>
                          </span>
                      </div>
                    </div>
                    @endforeach
                   
                </div>
    
               
            </div>
    
            @endif

            
@if($configuraciongeneral[1]=='comunicacionalcaldia/comunicacion')
		
@endif




@stop
