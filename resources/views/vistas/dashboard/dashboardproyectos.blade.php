<?php
if(!isset($nobloqueo))
   Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
 <script>
        $(document).ready(function() {
           
            $(".chosen-select").chosen(
                        {
                            no_results_text: "No existe coincidencia con lo que busca...",
                            placeholder_text_single: "Seleccione...",
                            placeholder_text_multiple: "Seleccione..."
                        });
        var chart3 = c3.generate({
            bindto:'#lineChart',
            zoom: {
            rescale: true
            },
            data: {
                columns: [
                    
                ],
                
                type : 'area-spline',
                //  labels: true

            },
            transition: {
                duration: 1000
            },
                legend: {
                        show: false
                    },
                    size: {
                       // height: 600
                    },
                    
                axis: {
                        rotated: false,
		                x : {
                           
                        type: 'category',
                        categories:  ['EJECUCION', 'EJECUTADO', 'EJECUTADO VENCIDO', 'POR EJECUTAR', 'SUSPENDIDO']
                },
               
				tick: {
                        x:{
                                    
                                    multiline:false,
                                  
                                    culling: {
                                        max: 1
                                    },
                            },
                            label : {
                                text: 'Estados',
                                position: 'center-bottom',
                                
                            }                 },
                        y: {
                            label : {
                                text: 'Cantidad',
                                position: 'outer-middle',
                            },
                          
                        }
                    },
                    count:2,
                    

                    
                    
                    
            });
            var chart2 = c3.generate({
            bindto:'#lineChart2',
            zoom: {
            rescale: true
            },
            data: {
                columns: [            
                ],
                type : 'bar',
                labels: true
            },
            transition: {
                duration: 1000
            },
                legend: {
                        show: false
                    },
                    size: {
                        //height: 600
                    },
                    
                axis: {
                        rotated: true,
		                x : {
                            label: 'Direciones',
                            type: 'indexed',

                        },

                        y: {
                            label : {
                                text: 'Porcentaje',
                                position: 'outer-middle',
                            },
                          
                        }
                    },
                   
                    tooltip: {
                            format: {
                                title: function (x, index) { return 'Avance de actividades en EJECUCIÓN por dirección'; }
                            }
                    }
                    

                    
                    
                    
            });

            var chart4 = c3.generate({
            bindto:'#lineChart3',
            zoom: {
            rescale: true
            },
            data: {
                columns: [
                    
                ],
                
                type : 'bar',
              
                labels: true

            },
            transition: {
                duration: 1000
            },
                legend: {
                        //show: false
                    },
                    size: {
                       // height: 600
                    },
                    
                axis: {
                        rotated: true,
                            x : {
                                show: false,
                            type: 'category',
                            categories:  [0,1,2, 3,4,5,6]
                    },
                       
                y : {
                        type: 'category',
                        categories:  [0,1,2, 3,4,5,6]
                },
				tick: {
                        x:{
                                    
                                    multiline:false,
                                    culling: {
                                        max: 1
                                    },
                            },
                            label : {
                                text: 'Estados',
                                position: 'center-bottom',
                                
                            }                 },
                        y: {
                            label : {
                                text: 'CANTIDAD DE EVENTOS',
                                position: 'outer-middle',
                            },
                          
                        }
                    },
                   //  count:2,
                    tooltip: {
                            format: {
                                title: function (x, index) { return 'EVENTOS REALIZADOS HOY POR DIRECCIÓN'; }
                            }
                    }
                    
                    

                    
                    
                    
            });

                
                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("comunicacionalcaldia/getChartLine") }}',
                    data: {'tipo':4},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       console.log( datos)
                        chart4.load({columns: datos, unload: true});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                });

                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("comunicacionalcaldia/getChartLine") }}',
                    data: {'tipo':1},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                       console.log( datos)
                        chart3.load({columns: datos, unload: true});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                });

                $.ajax({
                    type: "GET",
                    url: '{{ URL::to("comunicacionalcaldia/getChartLine") }}',
                    data: {'tipo':3},
                    error: function(objeto, quepaso, otroobj){
                        alert(quepaso);
                    },				
                    success: function(datos){
                        console.log(datos);
                        chart2.load({columns: datos, unload: true});

        
                },
                    statusCode: {
                        404: function() {
                       alert("<div class='alert alert-danger'>No existe URL</div>");
                    }	
                    }	  
                }); 

                $("#direccion").change(function(){ 
                    direccion= $("#direccion").val();
                    $.ajax({
                        type: "GET",
                        url: '{{ URL::to("comunicacionalcaldia/getChartLine") }}',
                        data: {'id':direccion,'tipo':2},
                        error: function(objeto, quepaso, otroobj){
                            alert(quepaso);
                        },				
                        success: function(datos){
                           
                            chart3.load({columns: datos, unload: true});
                            
                    },
                        statusCode: {
                            404: function() {
                        alert("No existe URL");
                        }	
                        }	  
                    }); 
                });
       
    });
</script>
@stop
@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
   
 
</style>

@stop
@section ('contenido')
{{-- <h1 style="background-color: #FFFFFF"> {{ $configuraciongeneral[0] }}</h1> --}}
                <div class="ibox float-e-margins">
                    <div class="ibox-title"> 
    @if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
     <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif                          


                <div class="wrapper wrapper-content">
                        <div class="row">
                                <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-title">
                                                <span class="label label-primary pull-right">HOY</span>
                                                <h3>PROYECTOS <span class="badge" style="font-size: 1em;">{!! $total->total !!}</span></h3>
                                            </div>
                                            <div class="ibox-content">
                            
                                                <div class="row">
                                                        @foreach ($iboxProyectos as $item)
                                                        <div class="col-lg-4">
                                                                <div class="ibox float-e-margins">
                                                                    <div class="ibox-title">
                                                                    <span class="label pull-right" style="background-color:{{$item->color}};color:#FFFFFF;">     </span>
                                                                        <h5>{{$item->estado_proyecto}}</h5>
                                                                    </div>
                                                                    <div class="ibox-content">
                                                                        <h1 class="no-margins">{{$item->total}}</h1>
                                                                        <div class="stat-percent font-bold " style="color:{{$item->color}}">{{$item->porcentaje}}% <i class="fa fa-bolt"></i></div>
                                                                        <small>{{$item->estado_proyecto}} </small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach     
                                                </div>
                            
                            
                                            </div>
                                        </div>
                                    </div>
                            
                        </div>
                        <div>
                                <div class="wrapper wrapper-content animated fadeInRight" >
                                      
                                        <div class="row">
                                                <div class="ibox float-e-margins">
                                                    <div class="ibox-title">
                                                        <h5>Movimientos</h5>
                                                        <div class="ibox-tools">
                                                            {{-- <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                                <i class="fa fa-wrench"></i>
                                                            </a>
                                                            <ul class="dropdown-menu dropdown-user">
                                                                <li><a href="#">Config option 1</a>
                                                                </li>
                                                                <li><a href="#">Config option 2</a>
                                                                </li>
                                                            </ul> --}}
                                                            <a class="close-link">
                                                                <i class="fa fa-times"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content inspinia-timeline">
                                                        @foreach ($proyectosActualizados as $item)
                                                        <div class="timeline-item">
                                                                <div class="row">
                                                                    <div class="col-xs-3 date">
                                                                        <i class="fa fa-briefcase"></i>
                                                                        {{ fechas(2,$item->updated_at) }}
                                                                        <br/>
                                                                        <small class="text-navy">{{Carbon\Carbon::parse($item->updated_at)->diffForHumans()}}</small>
                                                                    </div>
                                                                    <div class="col-xs-7 content ">
                                                                        <p class="m-b-xs"><strong>Proyecto</strong></p>{{$item->nombre}} <br>
                                                                        <strong>Monto: </strong>{{$item->monto}}<br>
                                                                        <strong>Estado:</strong> {{$item->estado_proyecto}}<br>
                                                                        <strong>Descripción:</strong> {{$item->descripcion}}<br>
                                                                        <strong>Obeservación:</strong> {{$item->observacion}}<br>
                                                                        <strong>Modificado por el usuario:</strong>  {{$item->name}}
                                                                        <br>
                                                                        <br>

                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        

                    
                                                    </div>
                                                </div>
                                            </div>
                    
                                            </div>
                                            

                        </div>
                      
                         
                      
                     
                                
                        
                </div>
@stop
