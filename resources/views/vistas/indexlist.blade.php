<?php
if(!isset($nobloqueo))
    Autorizar(Request::path());
?>

@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
 <script>
        $(document).ready(function() {
            $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: false, //True para mostrar Numero de semanas
                autoclose: true,
                format: 'yyyy-mm-dd',
                language: 'es'
            });
            $('#desde').datepicker("setDate", new Date('01-01-2018')); 
            $('#hasta').datepicker("setDate", new Date()); 
function cargafiltros(URL,tipo_filtro)
{
  $.ajax({
                    type: "GET",
                    url: URL,
                    data: {'tipo_filtro':tipo_filtro},
                    error: function(objeto, quepaso, otroobj){
                        $("#flitros").html(quepaso);
                    },              
                    success: function(datos){
                        $("#flitros").html(datos);
                        $(".chosen-select").chosen(
                        {
                            no_results_text: "No existe coincidencia con lo que busca...",
                            placeholder_text_single: "Seleccione...",
                            placeholder_text_multiple: "Seleccione..."
                        });
                },
                    statusCode: {
                        404: function() {
                        $("#flitros").html("No hubo respuesta");
                    }   
                    }     
                });   
}
            $("#tipofiltro").change(function(){
                var tipo_filtro= $("#tipofiltro").val();
                
                if("{{$configuraciongeneral[1]}}"=="comunicacionalcaldia/comunicacionlista"){
                        return cargafiltros('{{ URL::to("comunicacionalcaldia/getValoresFiltro") }}',tipo_filtro);
                }else if("{{$configuraciongeneral[1]}}"=="coordinacioncronograma/coordinacionlista"){
                    return cargafiltros('{{ URL::to("coordinacioncronograma/getValoresFiltro") }}',tipo_filtro);                    
                }else if("{{$configuraciongeneral[1]}}"=="coordinacioncronograma/coordinacionlistaobras"){
                    return cargafiltros('{{ URL::to("coordinacioncronograma/getValoresFiltroObras") }}',tipo_filtro);
                }else if("{{$configuraciongeneral[1]}}"=="tramitesalcaldia/tramiteslista"){
                    return cargafiltros('{{ URL::to("tramitesalcaldia/getValoresFiltro") }}',tipo_filtro);
                }                
            });
        

            $('#tbbuzonmain').dataTable({
                responsive : true,
                destroy: true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'T<"clear">lfrtip',
                
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                    "order": ([[ 0, 'desc' ]])
                   
                     
                  
            });            
        $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.50), innerHeight:screen.height -(screen.height * 0.55)}); 
        });  
@if(isset($delete))
    function eliminar($id,$tipo)
    {
            var r= confirm("Seguro de eliminar este registro?");
            if(r==true){
               
               if($tipo==1){
                $.ajax({
                    type: "POST",
                    url: '{{ URL::to("coordinacioncronograma/cronogramacompromisos") }}/'+$id,
                    data: {_method: 'delete', _token :'{{ csrf_token() }}',vista:'no'},
                    success: function(data){
                        //location.reload();  
                        toastr["success"](data);
                       return mostrar();
                        
                    }
                }); 
                    
               }else{

                $.ajax({
                    type: "POST",
                    url: '{{ URL::to("coordinacioncronograma/listobras") }}/'+$id,
                    data: {_method: 'delete', _token :'{{ csrf_token() }}',vista:'no'},
                    success: function(data){
                        //location.reload();  
                        toastr["success"](data);
                       return mostrar();
                        
                    }
                }); 
               }
            }    
    
                
    }
@endif



function popup(obj)
{
    //console.log(obj);
    $(obj).colorbox({fixed:true,iframe:true, innerWidth:screen.width -(screen.width * 0.20), innerHeight:screen.height -(screen.height * 0.25)}); 
}
function mostrar()
    {
            var tipo_filtro= $("#tipofiltro").val();
            var valor_filtro= $("#valorfiltro").val();
            var desde= $("#desde").val();
            var hasta= $("#hasta").val();


            if("{{$configuraciongeneral[1]}}"=="comunicacionalcaldia/comunicacionlista"){
                $('#tbbuzonmain').DataTable({
                destroy: true,
                responsive : true,
                ajax: {
                url: '{{ URL::to("comunicacionalcaldia/getComunicacionesFiltro") }}'+'?tipo_filtro='+tipo_filtro+ '&valor_filtro='+valor_filtro+'&desde='+desde+'&hasta='+hasta,   
                dataSrc:"",
                },
                columns: [
                        { data: 'id'},
                        { data: 'tipo_actividad'},
                        { data: 'nombre_actividad'},
                        { data: 'direccion'},
                        { data: 'zona'},
                        { data: 'beneficiarios'},
                        { data: 'observacion'},
                        { data: 'fecha_inicio'},
                        { data: 'fecha_fin'},
                        { data: 'dias_restantes'},
                        { data: 'kilometros'},
        
                    {data: "id" , render : function ( data, type, row, meta ) {
              return type === 'display'  ?
              "<a  id='mostrar' href='comunicacion/"+data+"' target='_blank' > <i class='fa fa-check-square'  aria-hidden='true'> Ver </i></a>" :
                data;
            }}],
                    
                    language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'T<"clear">lfrtip',
                
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                    "order": ([[ 0, 'desc' ]])
                  
            });  
                }else if("{{$configuraciongeneral[1]}}"=="coordinacioncronograma/coordinacionlista"){
                    $('#tbbuzonmain').DataTable({
                destroy: true,
                responsive : true,
                ajax: {
                url: '{{ URL::to("coordinacioncronograma/getComunicacionesFiltro") }}'+'?tipo_filtro='+tipo_filtro+ '&valor_filtro='+valor_filtro+'&desde='+desde+'&hasta='+hasta,  
                dataSrc:"",
                },
                columns: [
                        { data: 'id'},
                        { data: 'estado_actividad'},
                        { data: 'actividad'},
                        { data: 'fecha_inicio'},
                        { data: 'fecha_fin'},
                        { data: 'direccion'},
                        { data: 'avance'},
                        { data: 'prioridad'},
                        { data: 'updated_at'},
                        
                    {data: "id" , render : function ( data, type, row, meta ) {
              return type === 'display'  ?
              '<a  id="mostrar" href="cronogramacompromisos/'+data+'"  class="divpopup" target="_blank" onclick="popup(this)"> <i class="fa fa-check-square"  aria-hidden="true"> Ver </i></a> &nbsp;&nbsp;  <a href="cronogramacompromisos/'+data+'/edit?menu=no" target="_blank" onclick="popup(this)"><i class="fa fa-pencil-square-o"></i></a>  &nbsp;&nbsp;@if(isset($delete))<a href="javascript::" onclick="eliminar('+data+',1)"><i class="fa fa-trash"></i> </a>@endif':
                data;
            }}],
                    
                    language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'T<"clear">lfrtip',
                
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                    "order": ([[ 0, 'desc' ]])
                   
                     
                  
            }); 
                     
                }else if("{{$configuraciongeneral[1]}}"=="coordinacioncronograma/coordinacionlistaobras"){
                   
                    $('#tbbuzonmain').DataTable({
                destroy: true,
                responsive : true,
                ajax: {
                url: '{{ URL::to("coordinacioncronograma/getComunicacionesFiltroObras") }}'+'?tipo_filtro='+tipo_filtro+ '&valor_filtro='+valor_filtro+'&desde='+desde+'&hasta='+hasta, 
                dataSrc:"",
                },
                columns: [
                        { data: 'id'},
                        { data: 'parroquia'},
                        { data: 'tipoobra'},
                        { data: 'nombre_obra'},
                        { data: 'calle'},
                        { data: 'numero_proceso'},
                        { data: 'monto'},
                        { data: 'estado_obra'},
                        { data: 'contratista'},
                        { data: 'avance'},
                        { data: 'fecha_inicio'},
                        { data: 'fecha_final'},
                        { data: 'updated_at'},
        
                    {data: "id" , render : function ( data, type, row, meta ) {
              return type === 'display'  ?
              '<a  id="mostrar" href="listobras/'+data+'"  class="divpopup" target="_blank"> <i class="fa fa-check-square"  aria-hidden="true"> Ver </i></a>&nbsp;&nbsp; @if(isset($delete))<a href="javascript::" onclick="eliminar('+data+',2)"><i class="fa fa-trash"></i> </a>@endif':
                data;
            }}],
                    
                    language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'T<"clear">lfrtip',
                
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                    "order": ([[ 0, 'desc' ]])
                  
            }); 
                     
                }else if("{{$configuraciongeneral[1]}}"=="tramitesalcaldia/tramiteslista"){
                   
                   $('#tbbuzonmain').DataTable({
               destroy: true,
               responsive : true,
               ajax: {
               url: '{{ URL::to("tramitesalcaldia/getTramitesFiltro") }}'+'?tipo_filtro='+tipo_filtro+ '&valor_filtro='+valor_filtro+'&desde='+desde+'&hasta='+hasta,    
               dataSrc:"",
               },
               columns: [
                       { data: 'id'},
                       { data: 'numtramite'},
                       { data: 'remitente'},
                       { data: 'asunto'},
                     
                       { data: 'peticion'},
                       { data: 'prioridad'},
                       { data: 'recomendacion'},
                       { data: 'disposicion'},
                       { data: 'observacion'},
                       { data: 'fecha_ingreso'},
                       { data: 'fecha_respuesta'},
                       { data: 'archivo'},
       
                   {data: "id" , render : function ( data, type, row, meta ) {
             return type === 'display'  ?
             '<a  id="mostrar" href="tramites/'+data+'"  class="divpopup" target="_blank"> <i class="fa fa-check-square"  aria-hidden="true"> Ver </i></a>&nbsp;&nbsp; @if(isset($delete))<a href="javascript::" onclick="eliminar('+data+',3)"><i class="fa fa-trash"></i> </a>@endif':
               data;
           }}],
                   
                   language: {
                   "emptyTable":     "No hay datos disponibles en la tabla",
                   "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                   "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                   "infoFiltered":   "(filtered from _MAX_ total entries)",
                   "infoPostFix":    "",
                   "thousands":      ",",
                   "lengthMenu":     "Mostrar _MENU_ entradas",
                   "loadingRecords": "Cargando...",
                   "processing":     "Procesando...",
                   "search":         "Buscar:",
                   "zeroRecords":    "No se encontraron registros coincidentes",
                   "paginate": {
                       "first":      "Primero",
                       "last":       "Último",
                       "next":       "Siguiente",
                       "previous":   "Atrás"
                   },
                   "aria": {
                       "sortAscending":  ": activate to sort column ascending",
                       "sortDescending": ": activate to sort column descending"
                   }
               },
               "dom": 'T<"clear">lfrtip',
               
               "tableTools": {
                   "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
               },
                   "order": ([[ 0, 'desc' ]])
                  
                    
                 
           }); 
                    
               }
                      
}





</script>
@stop
@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

@stop
@section ('contenido')


<div  style="float: right;">
        <h1 class="col-lg-7"> {{ $configuraciongeneral[0] }}</h1>
        <br>
        <div class="col-lg-2">
                <div class="input-group date">
                        <span class="input-group-addon">Desde <i class="fa fa-calendar"></i></span>
                            <input data-placement="top" data-toogle="tooltip" class="form-control datefecha" placeholder="Desde" desde="" readonly="readonly" name="desde" type="text" id="desde">
                     </div>
        </div>
        <div class="col-lg-2">
                <div class="input-group date">
                        <span class="input-group-addon">Hasta  <i class="fa fa-calendar"></i></span>
                            <input data-placement="top" data-toogle="tooltip" class="form-control datefecha" placeholder="Hasta" hasta="" readonly="readonly" name="hasta" type="text" id="hasta">
                     </div>
        </div>
      </div>

      
           
           
<div class="ibox float-e-margins">
    
                    <div class="ibox-title"> 
    @if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
     <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif    
                    
    <div>
            
                
        
                    <div class="ibox-content">   
                      
                      {!!  Form::open(array('url' => $configuraciongeneral[1],'role'=>'form' , 'id'=>'form1','class'=>'form-horizontal'))  !!}
                      <div class="form-group">
    
                        @foreach($objetos as $i => $campos)
                     
                      @if($campos->Tipo=="select")

                      <label class="col-lg-1 control-label">{{$campos->Descripcion}}</label>
                        <div class="col-lg-4">    
                        <select class="form-control" name="{{$campos->Nombre}}" id="{{$campos->Nombre}}">
                        
                                @for($e=0; $e<=(sizeof($campos->Valor)-1); $e++)
                                
                                <option value="{{$campos->Valor[$e]}}">{{$campos->Valor[$e]}}</option>
                                @endfor
                        </select>
                        </div>
                        
                        @endif

                      
                      @endforeach
                      
                      <div  id="flitros">
                      
                      </div>
                        <a onclick="mostrar()" class="btn btn-primary">Mostrar</a>

                    
                      {!!  Form::close()  !!}
                      </div>
                      
                
                <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display" >
                    <thead>
                        <tr>           
                           <th>ID</th>
                           @foreach($objetosTable as $key => $value)
                           <th>{{ $value->Descripcion }}</th>
                           @endforeach
                           <th>ACCION</th>
                    </tr>
                    </thead>

                    <tfoot>
                        <tr>            
                          <th>ID</th>
                           @foreach($objetosTable as $key => $value)
                           <th>{{ $value->Descripcion }}</th>
                           @endforeach
                           <th>ACCION</th>
                            
                    </tr>
                    </tfoot>                    
                    
                           
                </table>
                        
                </div> <!-- ibox-content -->
</div> <!-- ibox float-e-margins -->

@stop
