<?php
if (!isset($nobloqueo)) {
    Autorizar(Request::path());
}

?>
@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
<script type="text/javascript">
  var jsonobject=JSON.parse('{"0":"campo"}');
  var _idT=-1;
  var _idB=-1;
  var tablaBenediciarios = [];
</script>
@include("vistas.includes.mainjs")
@include("vistas.includes.modalschange")
<script type="text/javascript">

function cargarEventos(){
  document.getElementById("guardarnuevo").addEventListener("click", mostrar, false);
}
$(document).ready(function(){


  @if($save=='no')
   $("#div-percar").hide();
    $("#div-estado_actividad").hide();
  @endif
  
  @if(isset($btnguardar) && $btnguardar==1)
    $("input").prop("disabled", true);
    $("select").prop("disabled", true);
    $("button").prop("disabled", true);
    $("textarea").prop("disabled", true);
    $(".btn-success").prop("disabled", true);
  @endif
  $('#guardarnuevoBeneficiarios').click(function(){
    let cuerpoTabla = document.getElementById("tabla-body-beneficiarios");
    let avanceBeneficiarios = "";

    let _parroqui = $("#parroquia_id option:selected").html();
    let _parroquia_id = $("#parroquia_id").val(); //$("#parroquia_id option:selected").val();
    let _calles = document.getElementById("calles").value;
    let _id_barrio = document.getElementById("id_barrio").value;
    let _barri = $("#id_barrio option:selected").html();
    let _idusuario = "<?php echo Auth::user()->id ?>";
    let _avance = document.getElementById("avance").value
    //console.log("Parro :"+_parroquia_id);

    if (_parroquia_id == 0) {
      alert("Debe seleccionar una parroquia");
      $("#parroquia_id").focus();
      return;
    }
    
    if (_calles == "") {
      alert("Debe Ingresar una dirección");
      $("#calles").focus();
      // toastr["success"]("{{ Session::get('message') }}");
      return;
    }
    
    if (_id_barrio == 0) {
      alert("Debe seleccionar un barrio");
      $("#id_barrio").focus();
      // toastr["success"]("{{ Session::get('message') }}");
      return;
    }
    
    if (_avance == "") {
      alert("Debe Ingresar un avance");
      $("#avance").focus();
    return;
  }


  let ingresoBeneficiarios = {
    parroquia_id: _parroquia_id,
    parroqui: _parroqui,
    calles: _calles,
    id_barrio: _id_barrio,
    barri: _barri,
    avance: _avance
  };

  tablaBenediciarios.push(ingresoBeneficiarios);

  $("#txtavancebapa").val(JSON.stringify(tablaBenediciarios));

  for (let i = 0; i < tablaBenediciarios.length; i++) {
    avanceBeneficiarios +=
    "<tr class='seleted' id='s"+i+
    "'><td class='hidden'>"+tablaBenediciarios[i].parroquia_id+
    "</td><td>"+tablaBenediciarios[i].parroqui+"</td><td>"+tablaBenediciarios[i].calles+
    "</td><td>"+tablaBenediciarios[i].barri+"</td><td>"+tablaBenediciarios[i].avance+
    "</td><td>"+
    @if(isset($tabla))
      "<a id='"+i+"' name='edit' class='btn btn-success' onclick='editarTablaBeneficiarios(this.id)'>Editar <i class='fa fa-pencil-square-o'></a></td></tr>";
      @else
      "<a id='"+i+"' class='btn btn-danger' onclick=' eliminar(this.id) '>Borrar <i class='fa fa-trash'></a></td></tr>";
      @endif
      "</td></tr>";

  }

  cuerpoTabla.innerHTML = avanceBeneficiarios;
  $("#modalBeneficiarios").modal("hide");

  document.getElementById("parroquia_id").value = "";
  // document.getElementById("calles").value = "";
  document.getElementById("id_barrio").value = "";
  document.getElementById("avance").value = "";





    });//Fin Guardar Beneficiarios
});//Fin Document REady
// Presupuesto
function editarTabla(id){
  //console.log("Pruedfsdfsdba: "+id);
  _idT=id;
  $("#btnEditar").show();
   $("#guardarnuevo").hide();
  $("#modalNuevo").modal();

}

function editarTablaBeneficiarios(id){
      console.log("Beneficiarios : "+id);
      _idB=id;
       $("#guardarEditarBeneficiarios").show();
       $("#guardarnuevoBeneficiarios").hide();
      $("#modalBeneficiarios").modal();
}



$('#first :selected').text();



$('#monto_inicial').keyup(function(event) {

// skip for arrow keys
console.log("Hola");
if(event.which >= 37 && event.which <= 40){
  event.preventDefault();
}

$(this).val(function(index, value) {
  return value
    .replace(/\D/g, "")
    .replace(/([0-9])([0-9]{2})$/, '$1.$2')  
    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",")
  ;
});
});
  
// ===============================
// Aqui se crea en primera instancia las tablas temporales dinamicas en la vista
// ===============================
var tabla = [];
function mostrar(){    

  let cuerpoTabla = document.getElementById("tabla-body");
  var presupuestoLLeno = ""; // variable global para recoger la tabla de presupuesto


  let parroquia = $("#id_parroquia option:selected").html();    
  let _id_parroquia = $("#id_parroquia option:selected").val();
  let _monto_inicial = document.getElementById("monto_inicial").value;
  let _monto_final = document.getElementById("monto_final").value;
  let _idusuario = "<?php echo Auth::user()->id ?>";
  let _observacion = document.getElementById("observacionpresu").value;

  // $("#monto_inicial").onkeypress(this,cpf);
  // document.getElementById("monto_inicial").addEventListener("keypress", alert("hola"));
  if (_id_parroquia == 0) {
  alert("Debe seleccionar una parroquia");
  $("#id_parroquia").focus();
  return;
  }

  if (_monto_inicial == "") {
   alert("Debe ingresar un valor para monto inicial");
   $("#monto_inicial").focus();
    return;
  }

  if(_monto_final == ""){
    document.getElementById("monto_final").value = 0;
  }

  let nuevoPresupuesto = {
    parroquia: parroquia,
    id_parroquia: _id_parroquia,
    monto_inicial: _monto_inicial,
    monto_final: _monto_final,
    observacion: _observacion
  };
  console.log(_idT);
  //var isEdicion = tabla;
  for (let i = 0; i < tabla.length; i++) {
    if(tabla[i].id_parroquia == nuevoPresupuesto.id_parroquia){
      alert("La parroquia ingresada ya se encuentra en registro");
      return;
    }

  }

  tabla.push(nuevoPresupuesto);

  $("#txtpresupuesto").val(JSON.stringify(tabla));
  for (let i = 0; i < tabla.length; i++) {

      presupuestoLLeno +=
      "<tr class='seleted' id='"+i+"'><td>"+tabla[i].parroquia+"</td><td>"+tabla[i].monto_inicial+
      "</td><td>"+tabla[i].monto_final+"</td><td>"+tabla[i].observacion+"</td><td>"+
      @if(isset($tabla))
      "<a id='"+i+"' class='btn btn-success' onclick=' editarTabla(this.id) '>Editar <i class='fa fa-pencil-square-o'></a></td></tr>";
      @else
      "<a id='"+i+"' class='btn btn-danger' onclick=' eliminar(this.id) '>Borrar <i class='fa fa-trash'></a></td></tr>";
      @endif
      "</td></tr>";

  }
  cuerpoTabla.innerHTML = presupuestoLLeno;
//  console.log(tabla);
//  limpiar();
  $("#modalNuevo").modal("hide");
}
function mostrarEditarBeneficiarios() {
  //console.log("hola");
  let cuerpoTabla = document.getElementById("tabla-body-beneficiarios");
  let avanceBeneficiarios = "";
  
  let _idpabaavance = $("#idpabaavance").val();
  let _parroqui = $("#parroquia_id option:selected").html();
  let _parroquia_id = $("#parroquia_id").val(); //$("#parroquia_id option:selected").val();
  let _calles = document.getElementById("calles").value;
  let _id_barrio = document.getElementById("id_barrio").value;
  let _barri = $("#id_barrio option:selected").html();
  let _idusuario = "<?php echo Auth::user()->id ?>";
  let _avance = document.getElementById("avance").value
  //console.log("Parro :"+_parroquia_id);
  
  if (_parroquia_id == 0) {
    alert("Debe seleccionar una parroquia");
    $("#parroquia_id").focus();
    return;
  }
  
  if (_id_barrio == 0) {
    alert("Debe seleccionar una parroquia");
    $("#id_barrio").focus();
    return;
  }
  if (_avance == "" ) {
    alert("Debe actualizar el porcentaje de avance");
    $("#avance").focus();
    return;
  }
  
  if (_avance < 0 || _avance > 100) {
    alert("Se debe ingresar un valor válido");
    $("#avance").focus();
    return;
  }
  
  
  let ingresoBeneficiarios = {
    idpabaavance: _idpabaavance,
    parroquia_id: _parroquia_id,
    parroqui: _parroqui,
    calles: _calles,
    id_barrio: _id_barrio,
    barri: _barri,
    avance: _avance
  };
  
  for (let i = 0; i < tablaBenediciarios.length; i++) {
    if(tablaBenediciarios[i].parroquia_id == _parroquia_id){
      console.log(tabla[_idpabaavance]);
      // alert("La parroquia ingresada ya se encuentra en registro");
      tablaBenediciarios[_idpabaavance].parroqui = _parroqui;
      tablaBenediciarios[_idpabaavance].parroquia_id = _parroquia_id;
      tablaBenediciarios[_idpabaavance].calles = _calles;
      tablaBenediciarios[_idpabaavance].id_barrio = _id_barrio;
      tablaBenediciarios[_idpabaavance].barri = _barri;
      //console.log("Barrio"+tablaBenediciarios[_idpabaavance].barri);
      tablaBenediciarios[_idpabaavance].avance = _avance;
    }
    //console.log("hola:" +1 +" "+tablaBenediciarios[1]);
  }
  
  $("#txtavancebapa").val(JSON.stringify(tablaBenediciarios));
  
  for (let i = 0; i < tablaBenediciarios.length; i++) {
    avanceBeneficiarios +=
    "<tr class='seleted' id='s"+i+
    "'><td class='hidden'>"+tablaBenediciarios[i].parroquia_id+
    "</td><td>"+tablaBenediciarios[i].parroqui+"</td><td>"+tablaBenediciarios[i].calles+
    "</td><td>"+tablaBenediciarios[i].barri+"</td><td>"+tablaBenediciarios[i].avance+
    "</td><td>"+
    @if(isset($tabla))
    "<a id='"+i+"' name='edit' class='btn btn-success' onclick=' editarTablaBeneficiarios(this.id) '>Editar <i class='fa fa-pencil-square-o'></a></td></tr>";
    @else
    "<a id='"+i+"' class='btn btn-danger' onclick=' eliminar(this.id) '>Borrar <i class='fa fa-trash'></a></td></tr>";
    @endif
    "</td></tr>";
    
  }
  cuerpoTabla.innerHTML = avanceBeneficiarios;
  $("#modalBeneficiarios").modal("hide");
  console.log(tablaBenediciarios);
  
  
}

// esto es para editar
function mostrarEditar(){
  let cuerpoTabla = document.getElementById("tabla-body");
  var presupuestoLLeno = ""; // variable global para recoger la tabla de presupuesto
  let _parroquia = $("#id_parroquia option:selected").html();
  let _id_parroquia = $("#id_parroquia option:selected").val();
  let _monto_inicial = document.getElementById("monto_inicial").value;
  let _monto_final = document.getElementById("monto_final").value;
  let _idusuario = "<?php echo Auth::user()->id ?>";
  let _observacion = document.getElementById("observacionpresu").value;
  let _idpapresu = $("#idpapresu").val();
  
  
  if (_id_parroquia == 0) {
  alert("Debe seleccionar una parroquia");
  $("#id_parroquia").focus();
  return;
}

if (_monto_inicial == "") {
  alert("Debe ingresar un valor para monto incial");
  $("#monto_final").focus();
  return;
}

if(_monto_final == ""){
  document.getElementById("monto_final").value = 0;
}






  let nuevoPresupuesto = {
    parroquia: _parroquia,
    id_parroquia: _id_parroquia,
    monto_inicial: _monto_inicial,
    monto_final: _monto_final,
    observacion: _observacion,
    idpapresu: _idpapresu
  };

  console.log("Hola "+_idT);
  //var isEdicion = tabla;
  for (let i = 0; i < tabla.length; i++) {
    if(tabla[i].id_parroquia == _id_parroquia){
      //console.log(tabla[i]);
      // alert("La parroquia ingresada ya se encuentra en registro");
      tabla[_idpapresu].parroquia = _parroquia;
      tabla[_idpapresu].id_parroquia = _id_parroquia;
      tabla[_idpapresu].monto_inicial = _monto_inicial;
      tabla[_idpapresu].monto_final = _monto_final;
      tabla[_idpapresu].observacion = _observacion;
    }

  }
  $("#txtpresupuesto").val(JSON.stringify(tabla));
  for (let i = 0; i < tabla.length; i++) {

      presupuestoLLeno +=
      "<tr class='seleted' id='"+i+"'><td>"+tabla[i].parroquia+"</td><td>"+tabla[i].monto_inicial+
      "</td><td>"+tabla[i].monto_final+"</td><td>"+tabla[i].observacion+"</td><td>"+
      @if(isset($tabla))
      "<a id='"+i+"' class='btn btn-success' onclick=' editarTabla(this.id) '>Editar <i class='fa fa-pencil-square-o'></a></td></tr>";
      @else
      "<a id='"+i+"' class='btn btn-danger' onclick=' eliminar(this.id) '>Borrar <i class='fa fa-trash'></a></td></tr>";
      @endif
      "</td></tr>";

  }
  cuerpoTabla.innerHTML = presupuestoLLeno;
  $("#modalNuevo").modal("hide");

}


function limpiar(){
  document.getElementById("id_parroquia").value = "";
  document.getElementById("monto_inicial").val = "";
  document.getElementById("monto_final").val = "";
  document.getElementById("observacionpresu").val = "";
}

// ===============================
// js para editar los campos
// ===============================
$(document).ready(function(){
  var ms = "{{ $configuraciongeneral[2] }}";
  $("label[for='id']").hide();
  $("#id").hide();
  $("td[class='colOculta']").hide();

  //$("#txtavancebapa").val(JSON.stringify(tablaBenediciarios));
  //Cuando es Editar
    var rellenoCbx;
  @if(isset($tabla))
  if(ms == "editar"){
    var id= {{ $tabla->id }};
    // ===============================
    // Cargar los datos de la tabla dinamica de presupuesto
    // ===============================
    ruta= '<?php echo URL::to('comunicacionalcaldia/getPresupuesto'); ?>?id='+id;
    $.get(ruta, function(data) {
      let cuerpoTabla = document.getElementById("tabla-body");
      let presupuestoLLeno = "";
      var datos = data;

      for (let i = 0; i < data.length; i++) {

        let ingresoTemporal = {
        parroquia: data[i].parroquia,
        id_parroquia: data[i].id_parroquia,
        monto_inicial: data[i].monto_inicial,
        monto_final: data[i].monto_final,
        observacion: data[i].observacion
      }
      tabla.push(ingresoTemporal);
    }

      for (let i = 0; i < data.length; i++) {

        if(data[i].observacion == null){
          data[i].observacion = "No hay observaciones";
        }
        presupuestoLLeno +=
        "<tr class='seleted' id='"+i+"'><td>"+data[i].parroquia+"</td><td>"+data[i].monto_inicial+
        "</td><td>"+data[i].monto_final+"</td><td>"+data[i].observacion+"</td><td>"+
        "<a id='"+i+"' name='edit' class='btn btn-success edit' onclick=' editarTabla(this.id) '>Editar <i class='fa fa-pencil-square-o'></a></td></tr>";
      var rellenoCbx = data[i].id_parroquia;

      }
      $("#txtpresupuesto").val(JSON.stringify(data));
      cuerpoTabla.innerHTML = presupuestoLLeno;
    });
    // ===============================
    // Cargar los datos de la tabla dinamica de beneficiarios
    // ===============================

    ruta= '<?php echo URL::to('comunicacionalcaldia/getBeneficiarios'); ?>?id='+id;
    $.get(ruta, function(data_bene) {
      let cuerpoTabla = document.getElementById("tabla-body-beneficiarios");
      let avanceBeneficiarios = "";
      let datos = data_bene;

      for (let i = 0; i < data_bene.length; i++) {

        let ingresoTemporal = {
          parroquia_id: data_bene[i].parroquia_id,
          parroqui: data_bene[i].parroquia,
          calles: data_bene[i].calles,
          id_barrio: data_bene[i].id_barrio,
          barri: data_bene[i].barrioName,
          avance: data_bene[i].avance
        }
        tablaBenediciarios.push(ingresoTemporal);
      }

      for (let i = 0; i < data_bene.length; i++) {

        avanceBeneficiarios +=
        "<tr class='seleted' id='"+i+"'><td class='hidden'>"+data_bene[i].parroquia_id+"</td><td>"+data_bene[i].parroquia+"</td><td>"+data_bene[i].calles+
        "</td><td>"+data_bene[i].barrioName+"</td><td>"+data_bene[i].avance+"</td><td><a id='"+i+"' class='btn btn-success' onclick='editarTablaBeneficiarios(this.id)'>Editar <i class='fa fa-pencil-square-o'></a></td></tr>";

      }
      //console.log(data_bene);
      $("#txtavancebapa").val(JSON.stringify(tablaBenediciarios));
      cuerpoTabla.innerHTML = avanceBeneficiarios;
    });
  }
  //Finaliza Editar
  @endif
});//Fin Document ready

// ===============================
// Valida la consistencia de parroquia exist 1 vez
// ===============================
function validar(tabla, propiedad, obje){
  for (let i = 0; i < tabla.length; i++) {
    if(tabla[i].propiedad == obje.propiedad){
      return true;
    } else {
      alert("La parroquia ingresada ya se encuentra en registro");
      return false;
    }

  }
}

// ===============================
// elimina de la tabla temporal de beneficiarios
// ===============================
function eliminarBeneficiarios(id_fila){
    for (var i = 0; i < tablaBenediciarios.length; i++) {
      if (i == id_fila) {
        tablaBenediciarios.splice(i, 1);
        break;
      }
    }

  $ ('#s'+id_fila).remove();
}


function eliminar(id_fila){

  for (var i = 0; i < tabla.length; i++) {
  if (i == id_fila) {
    tabla.splice(i, 1);
    $("#txtpresupuesto").val(JSON.stringify(tabla));
    break;
  }
}

  $('#'+id_fila).remove();
}
</script>
@stop
@section ('contenido')
@if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
     <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif   
@include("vistas.objetosgenerate")
<h1>
    @if($configuraciongeneral[2]=="crear")
        Nuevo Registro - {!!  $configuraciongeneral[0]  !!}
    @else
        Editar Registro - {!!  $configuraciongeneral[0]  !!}
    @endif
</h1>

<div class="ibox float-e-margins">
     <div class="ibox-title">
      <div class="">
        <a  href="{{ URL::to($configuraciongeneral[1]) }}" class="btn btn-primary ">Todos</a>
        @if($create=='si')
        <a  href="{{ URL::to($configuraciongeneral[1]."/create") }}" class="btn btn-default ">Nuevo</a>
        @endif
      </div>
<!--Enviar errores de validacion en caso de que los haya-->
    @if($errors->all())
        <script>
        $(document).ready(function() {
        //    $.notify("Error al guardar!","error");
          toastr["error"]($("#diverror").html());
          $("#id_tipo_actividad").trigger("change");
        });
        </script>
        <div class="alert alert-danger" style="text-align: left;" id='diverror'>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <h4>Error!</h4>
          <div id="diverror">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
        </div>
        </div>
    @endif
    @if(isset($alerta) && $alerta!="")
        <div class="alert alert-danger" style="text-align: left;" id='diverror'>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4>Error!</h4>
            <ul><li>{!!  $alerta  !!}</li></ul>
        </div>
    @endif
 </div>

<div class="ibox-content">
    @if($configuraciongeneral[2]=="crear")
     {!!  Form::open(array('url' => $configuraciongeneral[1],'role'=>'form' , 'id'=>'form1','class'=>'form-horizontal'))  !!}
    @else
      {!!  Form::model($tabla, array('route' => array(str_replace("/",".",$configuraciongeneral[1]).'.update', $tabla->id), 'method' => 'PUT','class'=>'form-horizontal', 'id'=>'form1' ))  !!}
    @endif
    <div id="{{ $wizid[0] }}">
    <h1>{{ $wiz[0] }}</h1>
    <hr>
         {{ generaobjetos($objetos) }}
      </div>
        @if(isset($popup))
          <input type="hidden" value="popup" name="txtpopup" />
        <!--else -->
          <!--input type="hidden" value="" name="txtpopup"/-->
        @endif
        <!-- -->
        <div id="{{ $wizid[1] }}">
        <h1>{{ $wiz[1] }}</h1>
        <hr>
        {{ generaobjetos($objetosCabComPersonas) }}
        </div>
        <!-- -->
        <div id="{{ $wizid[2] }}">
        <h1>{{ $wiz[2] }}</h1>
        <hr>
        {!! Form::hidden("txtpresupuesto","",array("id"=>"txtpresupuesto")) !!}
        <div class="modal fade" id="modalNuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLong">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar Presupuesto:</h4>
              </div>
              <div class="modal-body">
                  <div class="row">
                  {{ generaobjetos($objetosCabPresupuesto) }}

                  </div>
              </div>
              <div class="modal-footer">

                <button type="button" class="btn btn-primary" id="guardarnuevo" onclick="mostrar();">
                Agregar
                </button>

                <button id="btnEditar" type="button" class="btn btn-primary" id="guardarEditar" onclick="mostrarEditar();">
                Editar
                </button>
              </div>
            </div>
          </div>
        </div>


        <div class="row">
        <div class="col-sm-12">
          <div class="table-responsive">
          <table class="table table-hover table-condensed table-bordered">
          <caption>
            @if($create=='si')
            <a class="btn btn-primary" id="addNuevo" data-toggle="modal" data-target="#modalNuevo">
              Agregar nuevo
              <span class="glyphicon glyphicon-plus"></span>
            </a>
            @endif
          </caption>
          <thead>
            <tr>
              <td>Parroquia</td>
              <td>Monto inicial</td>
              <td>Monto final</td>
              <td>Observación</td>
              <td class="">Eliminar</td>
            </tr>
            </thead>
            <tbody id="tabla-body">
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td class="">
                <!-- <a class="btn btn-danger glyphicon glyphicon-remove"></a> -->
              </td>

            </tr>
            </tbody>
		</table>
  </div>
</div>
</div>
</div>

        <!-- -->
      <div id="{{ $wizid[3] }}">
        <h1>{{ $wiz[3] }}</h1>
        <hr>
        {!! Form::hidden("txtavancebapa","",array("id"=>"txtavancebapa")) !!}
        <div class="modal fade" id="modalBeneficiarios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLong">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Agregar un nuevo detalle:</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                <div class="row">
                  {{ generaobjetos($objetosCominicacionParroquia) }}
                </div>
              <div class="row">
                {{ generaobjetos($objetosCominicacionBarrio) }}
              </div>
                </div>
            </div>
            <div class="modal-footer">
              <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" id="guardarnuevoBeneficiarios">
              Agregar
              </button> -->

              <!-- onclick="mostrar();" -->
              <button type="button" class="btn btn-primary" id="guardarnuevoBeneficiarios">
                Agregar
                </button>

                <button type="button" class="btn btn-primary" id="guardarEditarBeneficiarios" onclick="mostrarEditarBeneficiarios();">
                Editar
                </button>


            </div>
          </div>
        </div>
      </div>
        <div class="row">
        <div class="col-sm-12">
          <div class="table-responsive">
          <table class="table table-hover table-condensed table-bordered">
          <caption>
            @if($create=='si')
            <a class="btn btn-primary" id="addNuevoBeneficiarios" data-toggle="modal" data-target="#modalBeneficiarios">
              Agregar nuevo
              <span class="glyphicon glyphicon-plus"></span>
            </a>
            @endif
          </caption>
          <thead>
            <tr>
              <td>Parroquia</td>
              <td>Dirección</td>
              <td>Barrio</td>
              <td>Avance (%)</td>
              <td class="">Editar</td>
            </tr>
            </thead>
            <tbody id="tabla-body-beneficiarios">
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td class="">
                <!-- <a class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion">

                </a> -->
                <!-- <a class="btn btn-danger glyphicon glyphicon-remove"></a> -->
              </td>

            </tr>
            </tbody>
    </table>
  </div><!-- Responsive-->
    </div>

  </div>
</div>
        {!!  Form::submit("Guardar", array('class' => 'btn btn-primary', 'style' => 'float:right' ))  !!}

        {!!  Form::close()  !!}


  </div>

</div> <!-- ibox float-e-margins -->

<script>
</script>

@stop

