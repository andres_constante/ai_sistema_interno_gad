<?php
if(!isset($nobloqueo))
 //   Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop


@section ('contenido')
@if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
     <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif   

<div class="ibox float-e-margins">
     <div class="ibox-title">

     
<!--Enviar errores de validacion en caso de que los haya-->
    @if($errors->all())
        <script>    
        $(document).ready(function() {
        //    $.notify("Error al guardar!","error");
        toastr["error"]($("#diverror").html());    
        });
        </script>
        <div class="alert alert-danger" style="text-align: left;" id='diverror'>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <h4>Error!</h4>
          <div id="diverror">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
        </div> 
        </div>
    @endif  
    @if(isset($alerta) && $alerta!="")
        <div class="alert alert-danger" style="text-align: left;" id='diverror'>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4>Error!</h4>
            <ul><li>{!!  $alerta  !!}</li></ul>
        </div>
    @endif
 </div>
 <script src="https://www.gstatic.com/firebasejs/4.6.2/firebase.js"></script>
 <script>
  // Initialize Firebase
  // TODO: Replace with your project's customized code snippet
  var config = {
    apiKey: "AIzaSyCtUlHLhGozDFfAD6galdl9Dr7ALd1q_Is",
    authDomain: "api-8433993749479886012-293657.firebaseapp.com",
    databaseURL: "https://api-8433993749479886012-293657.firebaseio.com",
    storageBucket: "api-8433993749479886012-293657.appspot.com",
    messagingSenderId: "492178290066",
  };
  firebase.initializeApp(config);
</script> 
<div class="ibox-content"> 
    {!!  Form::open(array('url' => $configuraciongeneral[1],'role'=>'form' , 'id'=>'form1','class'=>'form-horizontal', 'files' => true))  !!}
         @foreach($objetos as $i => $campos)
                <div class="form-group">
                    {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                  <div class="col-lg-8"> 
                  
                    @if($campos->Tipo=="textarea")
                          {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                  @else
                        {!! Form::text($campos->Nombre, $campos->ValorAnterior!="Null" ? $campos->ValorAnterior : Input::old($campos->Valor) , array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                  @endif
                  </div>
                </div>
        
        @endforeach
        {!!  Form::submit("Guardar", array('class' => 'btn btn-primary', 'style' => 'float:right' ))  !!}
        {!!  Form::close()  !!}
        

  </div> 
  
</div> <!-- ibox float-e-margins -->

<script>
</script>    

@stop

