
<html>

<head>
	<title></title>


	<style >
		body {
			font-family: "Tahoma", "Geneva", sans-serif;
			font-weight: normal;
			font-size: 11pt;
			color: black;
			margin: 0px 0px 0px 0px;

			padding: 0px;
		}
		@page{
			margin: 0px 30px 0px 30px;
}
th {
	text-align: center;
	font-size: 9px;	
	
}
	td {
	font-size: 12px;
	text-align: justify;
	font-weight: normal;
	height: 13px;
	padding: 15px;
	
}
table {
  border-collapse: collapse;

  
}

table, th, td {
  border: 1px solid gray;
 
  
}

.thazul{
	background-color: #5DADE2;
}
</style>
<script>
window.print();
// window.close();

	
	setTimeout(function(){
		window.close()
}, 2000);
</script>
</head>
<body>

	<br>

	@foreach($tabla as $item)
	<h5 align="right"><img  align="left" width="200" src="{{ asset('img/mantafirmes.png') }}" alt=""> TRÁMITE  {{$item->numtramite}} &nbsp;&nbsp; FECHA DE RESPUESTA: {{$item->fecha_respuesta}} </h5>
	<br>
	<Table class="table" >
			
		<tr scope="col">
		<td width="400px"><span align="right"><strong>DETALLE DEL TRÁMITE  <br></strong>{{$item->peticion}} </span></td>
		<td width="400px"><span align="right"><strong>&nbsp;OBSERVACIONES GENERALES SR. ALACALDE</strong><br>{{$item->observacion}}</span> <br> <br>
		@if(isset($tabladel))
			<strong> Delegados </strong> <br>
			@foreach ($tabladel as $i)
					
			- {{$i->name}} <br>
			
		@endforeach

		@endif
		
		</td>
	
		</tr>
			
		<tr>
		<td width="400px"><span align="left"><strong> OBSERVACIONES COORD. DE DESPACHO: </strong> <br>{{$item->recomendacion}} </span></td>
		<td width="100px" style="text-align:left;"><span><strong>&nbsp;ESTADO: </strong> {{$item->disposicion}}  </span></td>	

		</tr> 
	</Table>
	
    @endforeach
</body>
</html>