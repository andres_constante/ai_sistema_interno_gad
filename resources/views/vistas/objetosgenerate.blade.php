<?php
function generaobjetos($objetos)
{
?>
@foreach($objetos as $i => $campos)
                <div class="form-group" id="div-{!! $campos->Nombre !!}">
                    @if($campos->Tipo!="hidden")
                      {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                    @endif
                  <div class="col-lg-8"> 
                      @if($campos->Tipo=="date")
                          <!-- {! Form::input('date',$campos->Nombre,$campos->Valor, ['class' => 'form-control', 'placeholder' => 'Date']) !!}   -->
                        {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datefecha','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                      @elseif($campos->Tipo=="datetext")
                        <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          
                          {!! Form::text($campos->Nombre, null , array(
                            'data-placement' =>'top',
                            'data-toogle'=>'tooltip',
                            'class' => 'form-control datefecha',
                            'placeholder'=>$campos->Descripcion,
                            $campos->Nombre,
                            'readonly'=>'readonly'
                            )) !!}
                      </div>
                      @elseif($campos->Tipo=="select")
                         {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                      @elseif($campos->Tipo=="select2")
                         {!! Form::select($campos->Nombre,$campos->Valor, $campos->ValorAnterior, array('class' => "form-control ".$campos->Clase)) !!}   
                      @elseif($campos->Tipo=="select-multiple")
                         {!! Form::select($campos->Nombre."[]",$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre))!!}
                      @elseif($campos->Tipo=="file") 
                          {!! Form::file($campos, array('class' => 'form-control')) !!}
                      @elseif($campos->Tipo=="textdisabled")
                         {!!   Form::text($campos,Input::old($campos), array('class' => 'form-control','placeholder'=>'Ingrese '.$camposcaption[$i],'readonly')) !!}
                      @elseif($campos->Tipo=="textarea")
                          {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                            
                      @elseif($campos->Tipo=="password")
                         {!!   Form::password($campos->Nombre,array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Nombre)) !!}
                      @elseif($campos->Tipo=="hidden")
                        {!! Form::hidden($campos->Nombre,$campos->Valor ,array("id"=>$campos->Nombre)) !!}
                      @elseif($campos->Tipo=="textdisabled2")
                         {!!   Form::text($campos->Nombre,Input::old($campos->Valor), array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}
                      @elseif($campos->Tipo=="selectdisabled")
                         {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase, 'readonly')) !!}
                      @elseif($campos->Tipo=="select-multiple-disabled")
                         {!! Form::select($campos->Nombre."[]",$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre,'readonly'))!!}          
                      @elseif($campos->Tipo=="textarea-disabled")
                          {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}      
                      @else
                          {!! Form::text($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                      @endif
                  </div>
                </div>
        
        @endforeach
<?php
}
?>