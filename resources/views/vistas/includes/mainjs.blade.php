<script type="text/javascript" src="{{asset('js/jquery.validate.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() {

  $('.selective-normal').selectize();



	$('.solonumeros').on('input', function () {
              this.value = this.value.replace(/[^0-9.]/g,'');
              
            });
  $('input.moneda').keyup(function(event) {

     // skip for arrow keys
  if(event.which >= 37 && event.which <= 40){
    event.preventDefault();
  }

  $(this).val(function(index, value) {
    return value
      .replace(/\D/g, "")
      .replace(/([0-9])([0-9]{2})$/, '$1.$2')  
      .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",")
    ;
  });

});

$('.numeros-letras-guion').on('input', function () { 
              //alert("ss");
              this.value = this.value.replace(/[^0-9a-zA-Z-]/g,'');
            });

$(".mayuscula").on('keyup', function(e) {
    $(this).val($(this).val().toUpperCase());
});
/*
$(document).on('keyup', function(e) {
    $("#asunto").val($("#asunto").val().toUpperCase());
    $("#remitente").val($("#remitente").val().toUpperCase());
    $("#numtramite").val($("#numtramite").val().toUpperCase());
})
*/ 

var texto = $(".porcentaje").text();
console.log(texto);
  $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: false, //True para mostrar Numero de semanas
                autoclose: true,
                format: 'yyyy-mm-dd',
                language: 'es'
            });
    @if(isset($validarjs))
    //Validacion
    jQuery.extend(jQuery.validator.messages, {
      required: "Este campo es obligatorio.",
      remote: "Por favor, rellena este campo.",
      email: "Por favor, escribe una dirección de correo válida",
      url: "Por favor, escribe una URL válida.",
      date: "Por favor, escribe una fecha válida.",
      dateISO: "Por favor, escribe una fecha (ISO) válida.",
      number: "Por favor, escribe un número entero válido.",
      digits: "Por favor, escribe sólo dígitos.",
      creditcard: "Por favor, escribe un número de tarjeta válido.",
      equalTo: "Por favor, escribe el mismo valor de nuevo.",
      accept: "Por favor, escribe un valor con una extensión aceptada.",
      maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
      minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
      rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
      range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
      max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
      min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
    /*$("form").submit(function(event){
        res=confirm("Seguro de Continuar?")
        if(!res)
          return false;
        //$(this).submit();
        event.preventDefault();
        $(this).unbind('submit').submit();
    });*/
      $('form').validate({
          rules: {
            @foreach($validarjs as $key)
              {!! $key !!},
            @endforeach
            /*
              clave: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              },
              valor: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              }
            */
          },
          highlight: function(element) {
              $(element).closest('.form-group').addClass('has-error');
          },
          unhighlight: function(element) {
              $(element).closest('.form-group').removeClass('has-error');
          },
          errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function(error, element) {
              if(element.parent('.input-group').length) {
                  error.insertAfter(element.parent());
              } else {
                  error.insertAfter(element);
              }
          },
          submitHandler : function(form) {
          //do something here
              res=confirm("Seguro de Continuar?")
              if(!res)
                  return false;
              form.submit();
              /*var frm = $('#form1');
              frm.submit(function (ev) {
                ev.preventDefault();
                res=confirm("Seguro de Continuar?")
                if(!res)
                  return false;
                //$(this).submit();
                frm.unbind('submit').submit();
                //alert("Hola");
                //form.submit();
            }); */
        }
      });

@endif
$(".chosen-select").chosen(
  {
    no_results_text: "No existe coincidencia con lo que busca...",
    placeholder_text_single: "Seleccione...",
    placeholder_text_multiple: "Seleccione..."
});

  //Fin Validacion

 $('.selective-tags').selectize({
          plugins: ['remove_button'],
          persist: false,
          create: true,
          render: {
            item: function(data, escape) {
              return '<div>"' + escape(data.text) + '"</div>';
            }
          },
          onDelete: function(values) {
            //return confirm(values.length > 1 ? 'Esta seguro de borrar la selección ' + values.length : '');
            return confirm('Esta seguro de borrar la selección?');
          }
        });
   //Inicio de PAgina ocultar objetos todos
    let ruta= '<?php echo URL::to('comunicacionalcaldia/getacticampos'); ?>?id=0';
    $.get(ruta, function(data) {
      console.log(data);
      $.each(data, function(arrayID,value) {
            //console.log(value);
            $("#"+value).hide();
      });
      jsonobject=data;
      @if(isset($tabla))
        $("#id_tipo_actividad").trigger("change");
      @endif
    });
})
</script>