<script type="text/javascript">
// ===============================
// Filtros para selccion de barrios y parroquias
// ===============================
$(document).ready(function(){
  //Evento change al escoger en Select
 $("#zona").change(function(e, callback){
    var id=this.value;
    var ruta= '<?php echo URL::to('comunicacionalcaldia/getparroquia'); ?>?zona='+id;
    $.get(ruta, function(data) {
      $("#id_parroquia").empty();
      $("#id_parroquia").append("<option value>Seleccione...</option>");
      $.each(data, function(key, element) {
        $("#id_parroquia").append("<option value='" + key + "'>" + element + "</option>");
      });
      // $('.id_parroquia').trigger("chosen:updated");
      if (typeof callback === "function")
          callback();
    });
 });
  $("#id_direccion").change(function(){
    var id=this.value;
    var ruta= '<?php echo URL::to('comunicacionalcaldia/getusuariosdireccion'); ?>?id='+id;
    $.get(ruta, function(data) {
      $("#idusuario").empty();            
      $.each(data, function(key, element) {
        $("#idusuario").append("<option value='" + key + "'>" + element + "</option>");        
      });
       $('#idusuario').trigger("chosen:updated");
       

    });
  });


 //Evento change al escoger en Select en barrio
 $("#parroquia_id").change(function(e, callback){
  //alert("BArrio");
    var id=this.value;
    var ruta= '<?php echo URL::to('comunicacionalcaldia/getBarrio'); ?>?parroquia_id='+id;
    $.get(ruta, function(data) {
      $("#id_barrio").empty();
      $("#id_barrio").append("<option value>Seleccione...</option>");
      $.each(data, function(key, element) {
        $("#id_barrio").append("<option value='" + key + "'>" + element + "</option>");
      });
      $('#id_barrio').trigger("chosen:updated");
      if (typeof callback === "function")
          callback();
    });

 });
 //Evento Change al escoger Actividad Campos ocultos
//id_tipo_actividad
$("#id_tipo_actividad").change(function(){
  //console.log(jsonobject);
    var id=this.value;
    var ruta= '<?php echo URL::to('comunicacionalcaldia/getacticampos'); ?>?id='+id;
    $.get(ruta, function(data) {
      console.log(data);
      $.each(jsonobject, function(arrayID,value) {
            ///console.log(value);
            $("#"+value).show();
      });
      $.each(data, function(arrayID,value) {
            //console.log(value);
            $("#"+value).hide();
      });
      jsonobject=data;
    });
 });
  $("#monto_inicial").focusout().keyup(function(){
    //$("#monto_final").val($(this).val());
    $("#monto_final").val(0);
  });
  ////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////
        $('#modalNuevo').on('show.bs.modal', function (e) {
          // do something...
          console.log("Trigger Zona");
          $("#btnEditar").hide();
          $("#guardarnuevo").hide();
           $("#zona").trigger("change",function(){
              console.log("Valor _idT: " +_idT);
              if(_idT<0)
              {
                $("#guardarnuevo").show();
                return false;
              }
              $("#btnEditar").show();
              for (var i = 0; i < tabla.length; i++) {
                if (i == _idT) {
                let parrosel=parseInt(tabla[i].id_parroquia);
                  $("#id_parroquia").val(parrosel).prop("selected",true);
                  document.getElementById("monto_inicial").value = tabla[i].monto_inicial;
                  document.getElementById("monto_final").value = tabla[i].monto_final;
                  document.getElementById("observacionpresu").value = tabla[i].observacion;
                  $("#idpapresu").val(_idT);
                }
              }
                _idT = -1;
           });
        });
          $('#modalNuevo').on('hidden.bs.modal', function(){
            $(this).find('select').val(0).prop("selected",true);
            $(this).find('input').val("");
            $(this).find('textarea').val("");
          });

          $('#modalBeneficiarios').on('hidden.bs.modal', function(){
            $(this).find('select').val(0).prop("selected",true);
            $(this).find('input').val("");
            $(this).find('textarea').val("");
          });
          
        $('#modalBeneficiarios').on('show.bs.modal', function (e) {
          //$("#zona").trigger("change");      
          $("#guardarEditarBeneficiarios").hide();
          $("#guardarnuevoBeneficiarios").hide();    
            var id=$("#txtpresupuesto").val();
            var ruta= '<?php echo URL::to('comunicacionalcaldia/getparroquiafiltro'); ?>?zona='+id;
            $.get(ruta, function(data) {
              $("#parroquia_id").empty();
              $("#parroquia_id").append("<option value>Seleccione...</option>");
              $.each(data, function(key, element) {
                $("#parroquia_id").append("<option value='" + key + "'>" + element + "</option>");
              });
                // $('.id_parroquia').trigger("chosen:updated");
                //console.log("Valor _idT Desde beneficiario : " +_idB);
              //console.log(tablaBenediciarios);
              if(_idB<0){
                $("#guardarnuevoBeneficiarios").show();                    
                return false;
              }
              $("#guardarEditarBeneficiarios").show();
              for (var i = 0; i < tablaBenediciarios.length; i++) {
                if (i == _idB) {

                  let parrosel=parseInt(tablaBenediciarios[i].parroquia_id);
                  $("#parroquia_id").val(parrosel).prop("selected",true);
                  let barriosel=parseInt(tablaBenediciarios[i].id_barrio); 
                  $("#parroquia_id").trigger("change",function(){
                      $("#id_barrio").val(barriosel).prop("selected",true);  
                  });
                  $("#idpabaavance").val(_idB);
                  document.getElementById("calles").value = tablaBenediciarios[i].calles;
                  // document.getElementById("avance").value = tablaBenediciarios[i].avance;

                  let avancesel=parseInt(tablaBenediciarios[i].avance);
                  $("#avance").val(avancesel).prop("selected",true);  
                                  
                  
                  // console.log(tablaBenediciarios[i].id_barrio);
                }
              }
                _idB = -1;
            });                    
        });
});
</script>