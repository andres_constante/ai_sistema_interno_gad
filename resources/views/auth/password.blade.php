@extends('auth')
@section('titulo')
    {{ trans('formauth.email.title') }}
@endsection
@section('cabecera')
    {{ trans('formauth.email.titlesub') }}    
@endsection
@section('errores')
     @if (Session::has('message'))
                        <div class="alert alert-warning">{{ Session::get('message') }}</div>
                    @endif 
                    @if($errors->all())
                                <div class="alert alert-danger" style="text-align: left;" id='diverror'>
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                      <h4>Error!</h4>
                                      {{ HTML::ul($errors->all()) }}
                                </div>
                    @endif 
                   {{-- $error --}}
                   {{-- $status --}}
@endsection
@section('formulario')       
@if (Session::has('status'))
  <div class="alert alert-success">{{ Session::get('status') }}</div>
@endif                       
    {{ Form::open(array('url' => 'password/email','role'=>'form' ,'class'=>'form-horizontal')) }}                                
                <div class="form-group">
                    <!--input type="password" class="form-control" placeholder="Contraseña" required=""-->
                     {{ Form::text('email', Input::old('email'), array('class' => 'form-control','placeholder'=>trans('formauth.label.email'),'required'=>'', 'autofocus'=>'')) }}
                </div>                
                <button type="submit" class="btn btn-primary block full-width m-b">{{ trans('formauth.email.submit') }}</button>
                <a href="{{ URL::to('auth/login') }}"><small class="Opass">{{ trans('formauth.login.inicio') }}</small></a><br>
                <!--p class="text-muted text-center"><small>No tiene una cuenta?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Crear una cuenta</a-->
            <!--/form-->
                 {{ Form::close() }}
@endsection