@extends('auth')
@section('titulo')
    {{ trans('formauth.login.title') }}
@endsection
@section('cabecera')
    {{ trans('formauth.login.titlesub') }}    
@endsection
@section('errores')
     @if($errors->all())
                                <script>    
                            $(document).ready(function() {
                            //    $.notify("Error al guardar!","error");
                            toastr["error"]("{{ HTML::ul($errors->all()) }}");    
                            });
                            </script>
                                <div class="alert alert-danger" style="text-align: left;" id='diverror'>
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                      <h4>Error!</h4>
                                      {{ HTML::ul($errors->all()) }}
                                </div>
                            @endif              
                                @if (Session::has('message'))
                        <div class="alert alert-warning">{{ Session::get('message') }}</div>
    @endif 
@endsection
@section('formulario')
       
            <!--form class="m-t" role="form" action="index.html"-->
                {{ Form::open(array('url' => 'auth/login','class'=>'form-signin','role'=>'form')) }}        
                <div class="form-group">
                    <!--input type="username" class="form-control" placeholder="Usuario" required=""-->
                    {{ Form::text('cedula', Input::old('cedula'), array('class' => 'form-control','placeholder'=>trans('formauth.label.cedula'),'required'=>'', 'autofocus'=>'')) }}
                </div>
                <div class="form-group">
                    <!--input type="password" class="form-control" placeholder="Contraseña" required=""-->
                     {{ Form::password('password',array('class' => 'form-control','placeholder'=>trans('formauth.label.password'),'required'=>'')) }}
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">{{ trans('formauth.login.submit') }}</button>

                <a href="{{ URL::to('auth/register') }}"><small class="Opass">{{ trans('formauth.login.registro') }}</small></a><br>
                <a href="{{ URL::to('password/email') }}"><small class="Opass">{{ trans('formauth.login.olvido') }}</small></a>
                <!--p class="text-muted text-center"><small>No tiene una cuenta?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Crear una cuenta</a-->
            <!--/form-->
            {{ Form::close() }}

@endsection