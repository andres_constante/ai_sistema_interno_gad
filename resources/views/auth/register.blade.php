<?php
$modulo=App\ModulosModel::where("estado","ACT")->where("nombre","<>","GENERAL")->lists("nombre","id")->all();
?>
@extends('auth')
@section('titulo')
    {{ trans('formauth.registro.title') }}
@endsection
@section('cabecera')
    {{ trans('formauth.registro.titlesub') }}    
@endsection
@section('errores')
     @if (Session::has('message'))
                        <div class="alert alert-warning">{{ Session::get('message') }}</div>
                    @endif 
                    @if($errors->all())
                                <div class="alert alert-danger" style="text-align: left;" id='diverror'>
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                      <h4>Error!</h4>
                                      {{ HTML::ul($errors->all()) }}
                                </div>
                    @endif 
                   {{-- $error --}}
                   {{-- $status --}}
@endsection
@section('formulario')                              
    {{ Form::open(array('url' => 'auth/register','role'=>'form' ,'class'=>'form-horizontal')) }}                
                <div class="form-group">
                    <!--input type="username" class="form-control" placeholder="Usuario" required=""-->
                    {{ Form::text('name', Input::old('name'), array('class' => 'form-control','placeholder'=>trans('formauth.label.name'),'required'=>'', 'autofocus'=>'')) }}
                </div>
                <div class="form-group">
                    <!--input type="password" class="form-control" placeholder="Contraseña" required=""-->
                     {{ Form::text('email', Input::old('email'), array('class' => 'form-control','placeholder'=>trans('formauth.label.email'),'required'=>'', 'autofocus'=>'')) }}
                </div>
                <div class="form-group">
                    <!--input type="password" class="form-control" placeholder="Contraseña" required=""-->
                     {{ Form::text('cedula', Input::old('cedula'), array('class' => 'form-control','placeholder'=>trans('formauth.label.cedula'),'required'=>'', 'autofocus'=>'')) }}
                    </div>
                <div class="form-group">
                    <!--input type="password" class="form-control" placeholder="Contraseña" required=""-->
                     {{ Form::select('id_modulo', $modulo ,Input::old('id_modulo'), array('class' => 'form-control','required'=>'', 'autofocus'=>'')) }}
                    </div>
                <div class="form-group">                    
                     {{ Form::password('password',array('class' => 'form-control','placeholder'=>trans('formauth.label.password'),'required'=>'')) }}
                </div>
                 <div class="form-group">                    
                     {{ Form::password('password_confirmation',array('class' => 'form-control','placeholder'=>trans('formauth.label.password_confirmation'),'required'=>'')) }}
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">{{ trans('formauth.registro.submit') }}</button>
                <a href="{{ URL::to('auth/login') }}"><small class="Opass">{{ trans('formauth.login.inicio') }}</small></a><br>
                <!--p class="text-muted text-center"><small>No tiene una cuenta?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Crear una cuenta</a-->
            <!--/form-->
                 {{ Form::close() }}
@endsection