	<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Reseteo de Contraseña</h2>

		<div>
			Para resetear la contraseña complete el siguiente formulario: 
                        <a href="{{ URL::to('password/reset', $token) }}" target="_blank">Formulario de Reseteo de Password</a>.<br/>			
		</div>
                <br><br>
	</body>
</html>
