<?php
if(!isset($nobloqueo))
    Autorizar(Request::path());
?>
@extends(Input::has("menu") ? 'layout_basic_no_head':'layout'  )
@section ('titulo') {{ $general_config[0] }} @stop
@section ('scripts')
@include("vistas.includes.mainjs")
<script type="text/javascript">
  var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
function pdfiframe(URL)
{  
  var form = $("#form1");  
  var data = JSON.stringify( $(form).serializeArray() ); 
  var paramadi= $("#paramadi").val();
  //console.log(URL);
  //console.log(params);
  //console.log(data);
  URL=URL+"?token={{csrf_token()}}&data="+data+"&namedata={{ Crypt::encrypt($general_config[2])}}&paramadi="+paramadi;
  //console.log(URL);
  //window.location=URL;
  if(isMobile.any()) {
    //window.location=URL;
    window.open("http://docs.google.com/gview?url="+URL,"_self");

    return;
    }
  $.colorbox({href:URL, iframe:true, innerWidth:screen.width -(screen.width * 0.10), innerHeight:screen.height -(screen.height * 0.30),
    onLoad:function(){
      $('#cboxContent').block({ message: '<h1>Por favor espere...</h1><br><div class="sk-spinner sk-spinner-three-bounce"><div class="sk-bounce1"></div><div class="sk-bounce2"></div><div class="sk-bounce3"></div></div>', centerY: 0,centerX: 0 }); 
    },
    onComplete:function(){
      $("#cboxLoadedContent .cboxIframe").load(function(){
        $('#cboxContent').unblock(); 
      });
    }    
  });
}
$(document).ready(function() {

});

</script>
<!--include("vistas.includes.jsfunciones") -->
@stop
@section ('contenido')
<h1> 
  {!! $general_config[0] !!}  
</h1>
<div class="ibox-content">
    {!!  Form::open(['url' => $general_config[1], 'role' => 'form', 'id' => 'form1', 'class' => 'form-horizontal', 'files' => true])  !!}
         @foreach($objects as $i => $campos)
                <div class="form-group">
                    {!! Form::label($campos->Nombre, $campos->Descripcion.':', array("class"=>"col-lg-3 control-label")) !!}
                  <div class="col-lg-8"> 
                      @if($campos->Tipo=="date")
                          <!-- {! Form::input('date',$campos->Nombre,$campos->Valor, ['class' => 'form-control', 'placeholder' => 'Date']) !!}   -->
                        {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datefecha','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                      @elseif($campos->Tipo=="datetext")
                        <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          
                          {!! Form::text($campos->Nombre, $campos->Valor , array(
                            'data-placement' =>'top',
                            'data-toogle'=>'tooltip',
                            'class' => 'form-control datefecha',
                            'placeholder'=>$campos->Descripcion,
                            $campos->Nombre,
                            'readonly'=>'readonly'
                            )) !!}
                      </div>
                      @elseif($campos->Tipo=="select")
                         {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                      @elseif($campos->Tipo=="select2")
                         {!! Form::select($campos->Nombre,$campos->Valor, $campos->ValorAnterior, array('class' => "form-control ".$campos->Clase)) !!}   
                      @elseif($campos->Tipo=="select-multiple")
                         {!! Form::select($campos->Nombre."[]",$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre))!!}
                      @elseif($campos->Tipo=="file")                         
                          {!! Form::file($campos->Nombre, array('class' => 'form-control')) !!}
                        @if(isset($tabla))
                        <br>
                          <?php
                            $cadena="if(\$tabla->".$campos->Nombre."!='') echo '<a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$tabla->".$campos->Nombre.").' class=\"btn btn-success divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i> Ver Archivo</a>';";
                            eval($cadena);
                          ?>  
                        @endif
                      @elseif($campos->Tipo=="textdisabled")
                         {!!   Form::text($campos,Input::old($campos), array('class' => 'form-control','placeholder'=>'Ingrese '.$camposcaption[$i],'readonly')) !!}
                      @elseif($campos->Tipo=="textarea")
                          {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                      @elseif($campos->Tipo=="textmayus")
                          {!! Form::text($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion,'onkeyup'=>'aMays(event, this)')) !!}
                          
                      @elseif($campos->Tipo=="password")
                         {!!   Form::password($campos->Nombre,array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Nombre)) !!}
                         @elseif($campos->Tipo=="textdisabled2")
                         {!!   Form::text($campos->Nombre,Input::old($campos->Valor), array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}
                      @elseif($campos->Tipo=="selectdisabled")
                         {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase, 'readonly')) !!}
                      @elseif($campos->Tipo=="select-multiple-disabled")
                         {!! Form::select($campos->Nombre."[]",$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre,'readonly'))!!}          
                      @elseif($campos->Tipo=="textarea-disabled")
                          {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}                         
                      @else
                          {!! Form::text($campos->Nombre, $campos->ValorAnterior!="Null" ? $campos->ValorAnterior : Input::old($campos->Valor) , array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                      @endif
                  </div>
                </div>

        @endforeach
        <div class="form-group">
          <span class="col-lg-3 control-label"></span>
          <div class="col-lg-8"> 
          <button type="button" class="bnt btn-success form-control" name="btnPDF" onclick="return pdfiframe('{{ URL::to($general_config[1]) }}','{{ $general_config[2] }}')" style="float:right; color:#ffff; background-color: #1485c9;"><i class="fa fa-file-pdf-o"></i>&nbsp&nbsp&nbsp&nbspGenerar PDF</button>
          <input type="hidden" id="paramadi" value="{{ $paramadi }}">
        </div>
        </div>
        {!!  Form::close()  !!}
        

  </div>   
@stop

