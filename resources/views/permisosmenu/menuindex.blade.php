<?php
if(!isset($nobloqueo))
    Autorizar(Request::path());
?>
@extends('layout')
@section ('titulo')  {{$configuraciongeneral[0]}} @stop
@section ('scripts')
 {!! HTML::script('js/jquery.blockUI.js') !!}
 {!! HTML::script('gridedit/mindmup-editabletable.js') !!}
 {!! HTML::script('gridedit/numeric-input-example.js') !!}
  <style>
    .jstree-open > .jstree-anchor > .fa-folder:before {
        content: "\f07c";
    }

    .jstree-default .jstree-icon.none {
        width: 0;
    }
</style>
 <script>

        $(document).ready(function() {
          //Cargar Menu
//$('#mainTable').editableTableWidget({editor: $('<textarea>')}).find('td:first').focus();
$('#mainTable').editableTableWidget().find('td:first').focus();

          //Fin Carga Menu
            $('#mainTable').dataTable({
              "pageLength": 50,
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'T<"clear">lfrtip',
                
                "tableTools": {
                    "sSwfPath": "{{ asset('admin/plantilla/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf') }}"
                },
                    "order": ([[ 0, 'desc' ]])
                    //"order": ([[ 2, 'asc' ], [ 1, 'asc' ]])
                    //../js/plugins/dataTables/swf/copy_csv_xls_pdf.swf
                  
            });            
        $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.50), innerHeight:screen.height -(screen.height * 0.55)}); 
        });  
@if(isset($delete))
    function eliminar($id)
    {
            var r= confirm("Seguro de eliminar este registro?");
            if(r==true)                        
                $('#frmElimina'+$id).submit();
            else
                return false;
    }
@endif
//Menu de Opciones
function bloquearpantalla(sw){
    if(sw==1)
    {
        //$(".ibox-content").block({
          $.blockUI({
                 message: '<strong>Generando. Por favor espere :)</strong>',
                 centerY: 0, 
                css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#fff', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#000',
                top: '30px'
            } });
    }else{
        //setTimeout($.unblockUI, 2000); 
        //setTimeout('$(".ibox-content").unblock()', 2000); 
        $.unblockUI();
        //$(".ibox-content").unblock();
        //$.unblockUI();
    }
}



$(document).on('click','.botonactualizar', function(){      
      actualizardatos($(this).attr('id'));     
 });  

function actualizardatos(id)
{
    //alert(id);
//return false;
   bloquearpantalla(1);  

   var datosenvio = [];
   var filasi=$('#tr'+id);

   $('#tr'+id).find('td').each(function(i, tdrecor){
       
        console.log(tdrecor);
         var relnom = $(tdrecor).attr("rel");
        //Si es texto normal 
        if (relnom == "texto")
        { 
           //alert(query);  
            var query = $(tdrecor).html();

           datosenvio [i] = $.trim(query);
        }
        //Si es select
        else if (relnom == "seleccion") {
            $(tdrecor).find('select').each(function(is, selectrecor){
                var query2 = $(selectrecor).val();
                //alert(query2);  
                 datosenvio [i] = query2;
             });   
        }
       
    });

    // alert (datosenvio);
    console.log(datosenvio);

   $.ajax({
            type : "POST",
            url : '<?php echo URL::to($configuraciongeneral[1]) ; ?>',
            data : {datosenvio:datosenvio, _token:'{{ csrf_token() }}' },//la última id  {info:info}
            success : function(data) 
            {                
              console.log(data);
              //$("#mostrarlibretas").html(data);
               bloquearpantalla(0);

              if($.isNumeric(data) == true)
              {
                  toastr["success"]("Registro guardado con Exito"); 
                  bloquearpantalla(0);
                  $("#trnuevoguardar td:eq(0)").html(data);
                  $("#trnuevoguardar").attr("id","tr" + data);
                  $("#nuevoguardar").attr("id",data);
                  $("#nuevoguardar").hide();
                  $('#tr'+data).css('background','#79ffd1');
                  filasi.css('background','#79ffd1');

              } else {
                  toastr["error"](data); 
                  bloquearpantalla(0);                 
              }
              
            },
            error: function()
            {
                alert("Se produjo un error. Intentelo mas tarde...");
                bloquearpantalla(0);

            }
    }); 

}


$(document).on('click','#nuevo', function(){      
      agregarrow();
 });  



function agregarrow()
{
    $('#mainTable tr:first').after( '<tr style="background: lightgoldenrodyellow;" id = "trnuevoguardar"> <td tabindex=1 class="edit-disabled" rel = "texto"> - </td>' + 

        '<?php
        foreach($objetos as $key => $value)
        {   
              
            if ($value->Tipo=="select" || $value->Tipo=="select-ajax")
            {

              
              echo '<td  tabindex=1 rel = "seleccion" class="edit-disabled" rel = "seleccion">' . Form::select("Escoja" ,$value->Valor,Input::old("Escoja"), 
                                array("class" => $value->Clase)) .'</td>';  


            } else {

               echo '<td tabindex=1 rel = "texto"> - </td>';

            }
            
        }
        ?>'  + '  <td class="edit-disabled" style="font-size: 22px;" tabindex=1> <a class="botonactualizar" id = "nuevoguardar"> <i class="fa fa-save"></i></a> </td> </tr>'

        );
    refrescarchosen();
                   
}


function refrescarchosen()
{ 
     var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"60%"}
    }


     for (var selector in config) {
      $(selector).chosen(config[selector]);
    } 
}


$(document).on('click','.botonchosen', function(){   
  
      cargardatosajax($(this).attr('rel'), $(this).attr('name') );   
}
);


function cargardatosajax(id, funcionaj) {

       var ruta= '<?php echo URL::to('/') . '/'; ?>' + funcionaj; 
      $.get(ruta, function(data) {
               $('#'+id).empty();  
               $.each(data, function(key, element) {
                $('#'+id).append("<option value='" + key + "'>" + element + "</option>");
              });

            $('#'+id).trigger("chosen:updated");
        });
}

</script>
@stop
@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
</style>

@stop
@section ('contenido')
<h1 style="background-color: #FFFFFF"> {{ $configuraciongeneral[0] }}</h1>
                <div class="ibox float-e-margins">
                    <div class="ibox-title"> 
    @if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
	   <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif                          
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="{{ URL::to($configuraciongeneral[1]) }}">Listar Todos</a>
                                </li>
                               
                                <li><a id="nuevo" href="javascript:">Nuevo</a>
                                </li>
                               
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                     @if (isset($configuraciongeneral[3]))   
                      {{Form::open(array('method' => 'PUT','action' => $configuraciongeneral[3],'role'=>'form' ,'class'=>'form-inline','id'=>'formu')) }}
                           <div class="row"> 
                               <div class="col-md-12">
                                    <div class="col-md-6">        
                                        {{ Form::label($configuraciongeneral[1].'bus', 'Filtrar Busqueda por:',array('class'=>'control-label')) }}          
                                        {{ Form::select($configuraciongeneral[1].'bus',[null=>'Escoja'] + $listabusqueda ,$valorcmb,array('class' => 'chosen-select','style' => 'width:90%')) }}  <br> <br> 
                                    </div>
                                    <div class="col-md-3">
                                        {{ Form::submit('Buscar', array('class' => 'btn btn-primary')) }}
                                    </div>    
                             </div>
                          </div>
                        {{Form::hidden('ruta',URL::to('pensionrecaudacion') ,array('id'=>'ruta'))}}
                        {{ Form::close() }} 
                     @endif   
                      <div class="">
                       <a  href="{{ URL::to($configuraciongeneral[1]) }}" class="btn btn-primary ">Todos</a>
                       
                        <a  id="nuevo" href="javascript:" class="btn btn-default ">Nuevo</a>
                                                                       </div>
                
                <table id="mainTable" class="table table-striped table-bordered table-hover display" >
                    <thead>
                        <tr>           
                           <th>ID</th>
                           @foreach($objetos as $key => $value)
                           <th>{{ $value->Descripcion }}</th>
                           @endforeach
                            <th>Acción</th>
                    </tr>
                    </thead>

                    <tfoot>
                        <tr>            
                          <th>ID</th>
                           @foreach($objetos as $key => $value)
                           <th>{{ $value->Descripcion }}</th>
                           @endforeach
                            <th>Acción</th>
                    </tr>
                    </tfoot>                    
                    <tbody>
                    @foreach($tabla as $key => $value)
            	      <tr id="tr{{ $value->id }}">
                         <td class="edit-disabled" rel="texto">{{ $value->id }}</td>	
                        @foreach($objetos as $keycam => $valuecam)
                            <?php $tipo='rel="texto"'; ?>
                            @if($valuecam->Tipo=='text')
                                <?php $tipo='rel = "texto"'; ?>
                            @elseif($valuecam->Tipo=='select-ajax' || $valuecam->Tipo=='select')
                                <?php $tipo='class="edit-disabled" rel="seleccion"'; ?>
                            @endif
                                <?php
                                    $cadena="echo trim(\$value->".$valuecam->Nombre.");";                                    
                                ?>
                                @if($valuecam->Nombre=="valor_predefinido")
                                        <td {!! $tipo !!}><textarea style="margin: 0px; width: 343px; height: 185px;" disabled>
                                            <?php eval($cadena); ?>
                                        </textarea>
                                        </td>
                                    @else
                                      @if($valuecam->Tipo=="select" || $valuecam->Tipo=="select-ajax") 
                                      <?php $men=$valuecam->Nombre; ?>
                                          <td  tabindex=1 rel = "seleccion" class="edit-disabled" rel = "seleccion"> {!! Form::select("Escoja" ,$valuecam->Valor,$value->$men, 
                                array("class" => $valuecam->Clase,"style"=>"width: 100px;")) !!}</td>  

                                      @else
                                        <td {!! $tipo !!}><?php eval($cadena); ?></td>
                                      @endif

                                    @endif
                        @endforeach
                        <td class="edit-disabled">   
                    <a class="botonactualizar" style="font-size: 22px;" id = "{{$value->id}}"> <i class="fa fa-save"></i></a>
                  </td>
                        
            	      </tr>
                      @endforeach
                     </tbody>
                           
                </table>
                        {{-- $tabla->links() --}}
                </div> <!-- ibox-content -->
</div> <!-- ibox float-e-margins -->
@if(isset($delete))
    <div style="display: none;">
    @foreach($tabla as $key => $value)
        {!! Form::open(['route' => [$configuraciongeneral[1].'.destroy', $value->id], 'method' => 'delete','id'=>'frmElimina'.$value->id,'class' => 'pull-right']) !!}
            {!! Form::submit('Eliminar', array('class' => 'btn btn-small btn-warning')) !!}
        {!! Form::close() !!}
    @endforeach
    </div>
@endif
<div id="divresul"></div>
@stop
