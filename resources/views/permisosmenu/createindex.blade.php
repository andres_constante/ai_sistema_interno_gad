<?php
if(!isset($nobloqueo))
    Autorizar(Request::path());
?>
@extends('layout')
@section ('titulo')  {{$configuraciongeneral[0]}} @stop
@section('estilos')
<style>
    .jstree-open > .jstree-anchor > .fa-folder:before {
        content: "\f07c";
    }

    .jstree-default .jstree-icon.none {
        width: 0;
    }
</style>
@stop
@section ('scripts')
 <script>
$(document).ready(function() {

 function cargapermisosp(sel,tipobus)
 {
  var div = "#divresul";
  var url='{!! URL::to($configuraciongeneral[3]) !!}';
  var ruta='{!! URL::to($configuraciongeneral[1]) !!}';
  //var sel = $(this);
  //alert(sel.val());
  //alert(url);
  $(div).html("Espere...");
  $.ajax({
      type: "GET",
      url: url,
      data: { id: $("#id_modulo").val(),menutodo: @if(isset($menutodo)) 1 @else 0 @endif, tipobus: tipobus, ruta: ruta, perfil: $("#id_perfil").val() },
      error: function(objeto, quepaso, otroobj){
        //alert(quepaso);
        $(div).html(quepaso);
      },        
      success: function(datos){
          //alert(datos);
        $(div).html(datos);
        @if(!isset($checkmenu))
              $('#jstree').jstree({
                'state': {
                                        'opened': true
                                        },
                                    'core' : {
                                        'check_callback' : true
                                    }                                    
                                }) ;
        @else
          $('#jstree').on('changed.jstree', function (e, data) {
                                    var i, j, r = [];
                                    for(i = 0, j = data.selected.length; i < j; i++) {
                                        r.push(data.instance.get_node(data.selected[i]).text);
                                        //r.push(data.instance.get_node(data.selected[i]).text.)
                                    }
                                    //$('#event_result').html('Selected: ' + r.join(', '));
                                    var valor = r.join(', ');
                                    valor= valor.replace(/idmenu/g,'idmenumain[]');
                                    //valor= valor.replace(/hidden/g,'text');
                                    $('#divselect').html(valor);
                                    //            alert (valor);
                                }).jstree({
                                    'state': {
                                        'opened': true
                                        },
                                    'core' : {
                                        'check_callback' : true
                                    },
                                    "checkbox" : {
                                        "keep_selected_style" : false
                                    },
                                    "plugins" : [ "checkbox" ]                                      
                                }) ;
        @endif

      },
        statusCode: {
          404: function() {
              //alert('No Existe URL');
          $(div).html(":(");
        } 
      }   
  });
 }
$("#name").change(function()
{
    return cargapermisosp($(this),"usuario");
});//select Change
$("#id_modulo").change(function()
{
    $("#id_perfil").val(0);    
    return cargapermisosp($(this),"modulo");
});//select Change
$("#id_perfil").change(function()
{
    return cargapermisosp($(this),"perfil");
});//select Change
  @if(isset($idbus))
    $("#id_modulo").trigger("change");
  @endif
  $('#tbbuzonmain').dataTable({
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                /*"dom": 'T<"clear">lfrtip',
                
                "tableTools": {
                    "sSwfPath": "{ asset('admin/plantilla/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf') }"
                },*/
                    "order": ([[ 0, 'desc' ]])
                    //"order": ([[ 2, 'asc' ], [ 1, 'asc' ]])
                    //../js/plugins/dataTables/swf/copy_csv_xls_pdf.swf
                  
            });
  $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: false, //True para mostrar Numero de semanas
                autoclose: true,
                format: 'yyyy-mm-dd',
                language: 'es'
            });
    @if(isset($validarjs) || count($validarjs))
    //Validacion
    jQuery.extend(jQuery.validator.messages, {
      required: "Este campo es obligatorio.",
      remote: "Por favor, rellena este campo.",
      email: "Por favor, escribe una dirección de correo válida",
      url: "Por favor, escribe una URL válida.",
      date: "Por favor, escribe una fecha válida.",
      dateISO: "Por favor, escribe una fecha (ISO) válida.",
      number: "Por favor, escribe un número entero válido.",
      digits: "Por favor, escribe sólo dígitos.",
      creditcard: "Por favor, escribe un número de tarjeta válido.",
      equalTo: "Por favor, escribe el mismo valor de nuevo.",
      accept: "Por favor, escribe un valor con una extensión aceptada.",
      maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
      minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
      rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
      range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
      max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
      min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
      $('form').validate({       
          rules: {
            @foreach($validarjs as $key)
              {!! $key !!},
            @endforeach
            /*
              clave: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              },
              valor: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              }
            */
          },
          highlight: function(element) {
              $(element).closest('.form-group').addClass('has-error');
          },
          unhighlight: function(element) {
              $(element).closest('.form-group').removeClass('has-error');
          },
          errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function(error, element) {
              if(element.parent('.input-group').length) {
                  error.insertAfter(element.parent());
              } else {
                  error.insertAfter(element);
              }
          }
      });
@endif
});
</script>
@stop
@section ('contenido')

<h1> 
    @if($configuraciongeneral[2]=="crear")
       {!!  $configuraciongeneral[0]  !!}
    @else
       {!!  $configuraciongeneral[0]  !!}
    @endif
</h1>
<div class="ibox float-e-margins">
     
  <div class="">
                       <a  href="{{ URL::to($configuraciongeneral[1]) }}" class="btn btn-primary ">Actualizar</a>
                                                                       </div>

<div class="ibox-content">
 <!-- AQUI CONSTRUCCION DE FORMULARIO DE ENVIO DE INFORMACION    -->

    @if($configuraciongeneral[2]=="crear")
     {!!  Form::open(array('url' => $configuraciongeneral[1],'role'=>'form' , 'id'=>'form1','class'=>'form-horizontal', 'files' => true))  !!}
    @else
      {!!  Form::model($tabla, array('route' => array($configuraciongeneral[1].'.update', $tabla->id), 'method' => 'PUT','class'=>'form-horizontal', 'id'=>'form1' ))  !!}
    @endif
    
       @if(isset($objetos)) 
         @foreach($objetos as $i => $campos)               
                  @if($campos->Tipo=="htmltabla") 
                    <?php continue; ?>
                  @endif

                  @if($campos->Tipo=="datetext")                    
                      <div class="form-group" id="divobj-{{ $campos->Nombre }}">
                           {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                       <div class="col-lg-8"> 
                        
                        <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          
                          {!! Form::text($campos->Nombre, null , array(
                            'data-placement' =>'top',
                            'data-toogle'=>'tooltip',
                            'class' => 'form-control datefecha',
                            'placeholder'=>$campos->Descripcion,
                            $campos->Nombre,
                            'readonly'=>'readonly'
                            )) !!}
                      </div>
                       </div>   
                     </div>   

                  @elseif($campos->Tipo=="date")

                   <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                     <div class="col-lg-8"> 

                      {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datefecha','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                      
                     </div>   
                   </div>  

                   @elseif($campos->Tipo=="timefecha")

                     <div class="form-group">
                      {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                     <div class="col-lg-8"> 
                        <div class="input-group bootstrap-timepicker timepicker">
                         {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control timefecha input-small', $campos->Nombre]) !!}
                         <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                        </div> 
                      </div>
                    </div>  

                   @elseif($campos->Tipo=="select")

                     <div class="form-group">
                      {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                     <div class="col-lg-8"> 
                       {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : isset($listaanterior[$i]) ? $listaanterior[$i]:Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                      </div>
                    </div>    

                   @elseif($campos->Tipo=="select2")

                    <div class="form-group">
                       {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                      <div class="col-lg-8"> 
                       {!! Form::select($campos->Nombre,[null=>"Escoja..."]+(array)$campos->Valor, $campos->ValorAnterior, array('class' => "form-control ".$campos->Clase)) !!}   
                      </div> 
                    </div>   

                   @elseif($campos->Tipo=="select-multiple")

                    <div class="form-group">
                       {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                    </div>    

                     <div class="col-lg-8"> 
                       {!! Form::select($campos,$lista[$i], Input::old($campos) ? Input::old($campos) : isset($listaanterior[$i]) ? $listaanterior[$i]:Input::old($campos), array('class' => "form-control ".$clase[$i])) !!}
                     </div>

                   @elseif($campos->Tipo=="file") 

                      <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                       <div class="col-lg-8"> 
                          {!! Form::file($campos->Nombre, array('class' => 'form-control')) !!}
                       </div>  
                      </div>   

                   @elseif($campos->Tipo=="textdisabled")

                   <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                       <div class="col-lg-8"> 
                          {!! Form::text($campos->Nombre, $campos->Valor , array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Descripcion ,'readonly')) !!}
                      </div>  
                    </div>    

                   @elseif($campos->Tipo=="textarea")

                      <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}

                         <div class="col-lg-8"> 
                            {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                        </div>  
                      </div>     

                   @elseif($campos->Tipo=="password")

                      <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}

                       <div class="col-lg-8"> 
                         {!!   Form::password($campos,array('class' => 'form-control','placeholder'=>'Ingrese '.$camposcaption[$i])) !!}
                       </div> 

                      </div>  

                   @elseif($campos->Tipo=="Boton")

                      <div class="form-group">
                       <div class="col-lg-11"> 
                        {!!  Form::submit($campos->Descripcion, array('class' => 'btn btn-primary', 'style' => 'float:right' ))  !!}
                       </div>  
                      </div> 

                   @elseif($campos->Tipo=="FormHiddenJson")

                     <div class="form-group">
                       <div class="col-lg-12"> 
                        {!! Form::hidden($campos->Nombre,json_encode($campos->Valor) ) !!}
                       </div> 
                     </div>  

                   @elseif($campos->Tipo=="FormHidden")
                        {!! Form::hidden($campos->Nombre,$campos->Valor ,array("id"=>$campos->Nombre)) !!}

                   @elseif($campos->Tipo=="filehtml")

                     <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                       <div class="col-lg-8"> 
                        <a href="{{URL::to('local/public'.$campos->Valor)}}" target="_blank">  Ver Archivo </a>
                        {!! Form::hidden($campos->Nombre,$campos->Valor ) !!}  
                       </div>  
                      </div>    

                   @elseif($campos->Tipo=="html")

                    <div class="form-group" id="divobj-{{ $campos->Nombre }}">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                      <div class="col-lg-8" id="div-{{ $campos->Nombre }}"> 
                        <?php echo $campos->Valor; ?>  
                      </div>  
                    </div>  

                   @elseif($campos->Tipo=="html2")

                    <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                      <div class="col-lg-8"> 
                        <?php echo $campos->Valor; ?>  
                      </div>  
                    </div>  

                   @elseif($campos->Tipo=="htmlplantilla")
                    
                    <div class="form-group">
                      <div class="col-lg-12"> 
                        <?php echo $campos->Valor; ?>  
                      </div> 
                    </div>


                @elseif($campos->Tipo=="link")
                
                   <div class="form-group">
                     {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                   <div class="col-lg-8"> 
                    <a href="{{URL::to($campos->Valor)}}" target="_blank" >  {{$campos->Descripcion}} </a>
                   </div>  
                  </div>    

                 @elseif($campos->Tipo=="botonjs")
                  <div class="form-group">
                   <div class="col-lg-11"> 
                  <a id="{{$campos->Nombre}}" class=" btn btn-primary pull-right "> {{$campos->Descripcion}} </a>
                    <a> </a>
                   </div>  
                  </div> 

                  @elseif($campos->Tipo=="funcionjs")
                  
                     <?php echo $campos->Valor; ?>  


                  @elseif($campos->Tipo=="htmljs")
                   <div class="col-lg-11"> 
                     <?php echo $campos->Valor; ?>     
                    </div>  
                  @elseif($campos->Tipo=="checkbox")

                     <div class="form-group">
                         
                       <div class="col-lg-11"> 
                           {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-9 control-label")) !!}
                           {!! Form::checkbox($campos->Nombre,null,null, array('id'=>$campos->Nombre)) !!}    
                      </div>  
                    </div>  
                @elseif($campos->Tipo=="divresul")
                     <div class="form-group">
                         
                       <div class="col-lg-11" id="{{ $campos->Nombre }}"> 
                           
                      </div>  
                    </div>  
                  @elseif($campos->Tipo=="botontabla" || $campos->Tipo=="botontablapopup")


                  @else
                    <div class="form-group">
                         {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                       <div class="col-lg-8"> 
                          {!! Form::text($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                      </div>  
                    </div>    
                  @endif
                      
        @endforeach
      @endif  
      <div id="divselect" style="display:none">
    
</div>
<div id="divresul"></div>

        {!!  Form::close()  !!}
<!--Enviar errores de validacion en caso de que los haya-->
    @if($errors->all())
        <script>    
        $(document).ready(function() {
        //    $.notify("Error al guardar!","error");
        toastr["error"]($("#diverror").html());    
        });
        </script>
        <div class="alert alert-danger" style="text-align: left; width:75%;display: table; margin: 0 auto;" id='diverror'>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <h4>Aviso!</h4>
          <div id="diverror">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
        </div> 
        </div>
    @endif  
    @if (Session::has('message'))    
        <div class="alert alert-info" style="text-align: left; width:75%;display: table; margin: 0 auto;" id='diverror'>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4>Mensaje !</h4>
            <ul><li>{!!  Session::get('message')  !!}</li></ul>
        </div>
    @endif    
        
  </div> 
    <!-- Estado del Tramite -->
  @if(isset($estadotram))
    @if($estadotram!='ACTIVO')
    <div class="alert alert-danger">
      <i>Este trámite aún está en <strong>Modo prueba</strong>. Disculpe las molestias. :)</i>
    </div>
    @endif
  @endif
<!-- Si tiene trámites pendientes -->
@if(isset($totaltramites))
    @if($totaltramites==0)
      <div class="jumbotron">
        <h2>Usted no tiene solicitudes pendientes.</h2>
      </div>
    @endif
@endif
<!-- -->
 @if (isset($tablatramites))
      <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display" >
          

<?php $ii=0; #AGCM-AGOSTO 
//show($tablatramites->toarray());
DB::enableQueryLog();
?>  
          @foreach($tablatramites as $key => $value)

             <?php 
             //Esta consulta ubicarla en campo extras
             $usuario = UsuarioModel::find(Auth::id());
             if(!isset($estadomistramites))
             {
              $estadomistramites=$value->id_estado;
             }
             $camposextras = CamposExtras::join("tiposolicitudescamposextras as b","b.id_campos_extras","=","camposextras.id")
                            ->join("estadostiposolicitudcamposextras as c","c.id_tiposolicitudescamposextras","=","b.id")
                            ->join("tusuariotsolcitudescextras as d","d.id_estados_tipo_socilitud_cextras","=","c.id") // Marcelo
                            ->where("b.id_tipo_solicitudes","=",$value->id_tipo_solicitudes)
                            ->where("c.id_estado","=",$estadomistramites) //Estado inicial
                            ->where("d.id_tipo_usuario","=",$usuario->tipo_usuario_id)  // Marcelo
                            ->select("camposextras.*","c.tipo","c.valor","d.id as idtusuariotsolcitudescextras")
                            ->orderby("camposextras.orden")
                            ->get();     
             // show($value->id);
                            //show($camposextras->toarray(),0);                            
              $jsoncamposextras = generarcamposextras($camposextras, $value->id);
              $camposextrasentabla =  '['. $jsoncamposextras . ' ]'; 
              $camposextrasentabla = json_decode($camposextrasentabla);
              //show($camposextrasentabla);
             ?>
          @if($ii==0)
           <thead>
              <tr>           
                 @foreach($objetostramites as $keyot => $valueot)
                 <th>{{ $valueot->Descripcion }}</th>
                 @endforeach                  
                  @if(isset($camposextrasentabla)) 
                    @foreach($camposextrasentabla as $i => $campos)
                      @if($campos->Tipo=="htmltabla") 
                        <th>{{ $campos->Descripcion }}</th>
                      @endif
                  @endforeach 
                @endif
               <th>Acción</th>
             </tr>
          </thead>
                    
          <tbody>
          <?php $ii=1; #AGCM-AGOSTO 
          ?>
          @endif
          <tr>
              @foreach($objetostramites as $keycam => $valuecam)
                 <td><?php 
                      $cadena="echo \$value->".$valuecam->Nombre.";";
                      eval($cadena);                                   
                      ?>
                 </td>
              @endforeach
              @if(isset($camposextrasentabla)) 
                    @foreach($camposextrasentabla as $i => $campos)
                      @if($campos->Tipo=="htmltabla") 
                        <td>{{ $campos->Valor }}</td>
                      @endif
                  @endforeach 
                @endif
             <td>   

               @if(isset($camposextrasentabla)) 
                @foreach($camposextrasentabla as $i => $campos)
                    @if($campos->Tipo=="botontabla") 
                      <a href="{{URL::to($campos->Valor .'/'.$value->id)}}" class="btn btn-primary ">Atender</a>
                    @elseif($campos->Tipo=="botontablapopup")
                      <a href="{{URL::to($campos->Valor .'/'.$value->id)}}" class="popupboton btn btn-primary ">Ver Detalle</a>                     
                    @endif
                @endforeach  
               @endif

            </td>
          </tr>
            @endforeach
         </tbody>                           
     </table>
     {{-- show(DB::getQueryLog(),0) --}}
 
 @endif 
  
</div> <!-- ibox float-e-margins -->


<div id="divtimeline"></div>
<script>

$(".popupboton").colorbox({fixed:true,iframe:true, innerWidth:screen.width -(screen.width * 0.4), innerHeight:screen.height -(screen.height * 0.3)}); 
$(document).ready(function() {
  //alert("holaa");
  $("#name").trigger("change");
  $("#perfil").trigger("change");
});
</script>    

@stop

