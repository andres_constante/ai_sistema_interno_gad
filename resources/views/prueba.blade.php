@extends('layouts.backendInicio')

@section('ruta')
Gertor CMS FIXSYSTEM
@stop

@section('titulo')
  INICIO
@stop

@section('contenido')
@include('alerts.success')
<?php 
      $acep = Auth::user();
 ?>
 <?php 
    use App\Estado_TramiteModel;
    use App\cabeceraModel;


    $datas =Estado_TramiteModel::all();
    $dato_imp_titu_cab = cabeceraModel::all();

    $tram_reali = count($datas) + count($dato_imp_titu_cab);

    $datas2 = Estado_TramiteModel::where("estado","=","finalizado")->get();
    $tram_fin = count($datas2) + count($dato_imp_titu_cab);
  ?>
<style> 
#foto_modificar
{
position: absolute;
opacity: 0;
}


#foto_modificar:hover {
    position: absolute;
    opacity: 10;
}

</style>
<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
     
        <div class="col-lg-9"> 
     <img src="{{asset('/local/public/tramites-en-linea.png') }}" alt="" class="col-lg-12">
     <!--div class="col-lg-6 col-md-6">
                  <div class="contact-box">
                      <a href="{{ URL::asset('consultadeuda') }}">
                      <h3 style="text-align: center;"><strong>Consulta deudas</strong></h3>
                      <div class="col-sm-4">
                          <div class="text-center">
                              <i class="fa fa fa-search fa-5x"></i>                              
                          </div>
                      </div>
                      <div class="col-sm-8">                          
                          <p><i class="fa fa-check"></i> Consulte valores adeudados por impuesto predial.</p>                         
                      </div>
                      <div class="clearfix"></div>
                      </a>
                  </div>
              </div>
              <div class="col-lg-6 col-md-6">
                  <div class="contact-box">
                      <a href="{{ URL::asset('tramitedigital/10/1') }}">
                      <h3 style="text-align: center;"><strong>Certificado Catastral</strong></h3>
                      <div class="col-sm-4">
                          <div class="text-center">
                              <i class="fa fa-list-alt fa-5x"></i>                              
                          </div>
                      </div>
                      <div class="col-sm-8">
                          <p><i class="fa fa-check"></i>Revise las solicitudes de certificados catastrales.</p>
                      </div>
                      <div class="clearfix"></div>
                          </a>
                  </div>
              </div-->
     @foreach($iconos as $key => $value)
      @if($value->nivel==1)
        <div class="col-lg-12 wrapper border-bottom">
              <h2>{{ $value->menu }}</h2>        
          </div>      
      @else
     <div class="col-lg-6 col-md-6">
                  <div class="contact-box">
                      <a href="{{ URL::asset($value->ruta) }}">
                      <h3 style="text-align: center;"><strong>{{ $value->menu }}</strong></h3>
                      <div class="col-sm-4">
                          <div class="text-center">
                              <i class="{{ $value->icono }} fa-5x"></i>                              
                          </div>
                      </div>
                      <div class="col-sm-8">
                          <p><i class="fa fa-check"></i>{{ $value->detalle }}</p>
                      </div>
                      <div class="clearfix"></div>
                          </a>
                  </div>
              </div>
      @endif
     @endforeach                
        </div>
        <div class="col-lg-3">
                        <div class="widget-head-color-box navy-bg p-lg text-center">
                            <div class="m-b-md">
                            <h2 class="font-bold no-margins">
                                {{$acep->nombres}}
                            </h2>
                                <small>{{$acep->apellidos}}</small>
                            </div>
                            <p style="text-align: center;">
                             <?php   
                            $img_dir=asset('local/public/images')."/".$acep->path;
                            $img_act=asset('local/public/images')."/actualizarf.png";
                             ?>                            
                            @if(empty($acep->path))
                              <img class="sobre" src="{{$img_act}}"  style="width: 25%;" onclick="Actualizar_foto_pop();" />
                              <img class="sobre" src="{{$img_act}}"  id="foto_modificar" onclick="Actualizar_foto_pop();" />
                              <img style="width: 60%;" src="avatar.jpg"  class=" img-circle  circle-border m-b-md" alt="profile">                              
                            @else   
                            <div> 
                             <img class="sobre" src="{{$img_act}}"  style="width: 25%;" onclick="Actualizar_foto_pop();" />
                             <img class="sobre" src="{{$img_act}}"   id="foto_modificar" onclick="Actualizar_foto_pop();" />

                             <img src="{{$img_dir}}"  style ="width: 112px; height: 112px"  class=" img-circle  circle-border m-b-md" alt="profile" id="f_foto">

                            </div>
                            @endif
                            </p>
                            <div>
                                <span>Email: {{$acep->email}}</span> <br>
                                <span>Teléfono:  {{$acep->telefono}}</span> 
                              
                            </div>
                            <br>
                             <div>
                               
                              <span ><a class="btn btn-default btn-xs" style="font-size: 10px;"   href="{{ URL::to('cambiardatos/'.Auth::user()->id.'/edit') }}">Editar Datos</a> </span>  
                              <span hidden><a class="btn btn-default btn-xs" style="font-size: 10px;"   href="javascript:">Cambiar Contraseña</a> </span> 
                            </div>
                        </div>
                        <div class="widget-text-box">
                            <img src="logos.jpg" alt="" style="width: 100%;">
                        </div>
        </div>
     

        

        </div>


        </div>


<!-- Table de Mis propiedades -->

<!-- Obtener fecha actual -->

<?php 
  date_default_timezone_set('America/Guayaquil');
  
  function nombremes($mes){
     setlocale(LC_TIME, 'spanish');  
     
     $nombremes=strftime("%B",mktime(0, 0, 0, $mes, 1, 2000)); 
     return $nombremes;
  }

    $fecha = date('d-m-Y');
    $fechats = strtotime($fecha);
    $day = null; 
    switch (date('w', $fechats)){ 
        case 0: $day = "Domingo"; break; 
        case 1: $day = "Lunes"; break; 
        case 2: $day = "Martes"; break; 
        case 3: $day = "Miércoles"; break; 
        case 4: $day = "Jueves"; break; 
        case 5: $day = "Viernes"; break; 
        case 6: $day = "Sábado"; break; 
    }
 ?>

<!--POP-UP DE ACEPTAR TERMINOS Y CONDICIONES-->

<div class="modal fade" id="pop_up" data-backdrop="static" data-keyboard="false" style="font-size: 16px; font-family: 'Times New Roman'">

  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="padding: 10px;">
      <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
        <h2 class="modal-title"><center>Términos y condiciones de uso de cuenta</center></h2>
      </div>
      <div class="modal-body" data-spy="scroll" data-target=".bs-docs-sidebar" style="overflow-y:auto; width: default; height: 300px; text-align: justify; border-top: 1px solid #B4B5B5; border-bottom: 1px solid #B4B5B5;">
        <br>
        <h3 style="text-align: center;">GOBIERNO AUTÓNOMO DESCENTRALIZADO MUNICIPALIDAD DEL CANTÓN MANTA</h3><br>
        <h4 style="text-align: center;"><i>ACUERDO DE RESPONSABILIDAD POR EL USO DE MEDIOS O SERVICIOS ELECTRÓNICOS.</i></h4>
        <p><br><b>MANTA, {{$day}} {{date('d')}} de {{nombremes(date('n'))}} del {{date('Y')}}</b>
        <br>Estimado/a <b>{{$acep->nombres}} {{$acep->apellidos}}</b>, con cédula <b>{{$acep->cedula_ruc}}</b>, </p>
        <p>Acepte este acuerdo de licencia:</p>
        <p>El Gobierno Autónomo Descentralizado Municipal de Manta y el usuario solicitante del servicio, celebran el presente acuerdo de uso de medios electrónicos que la Municipalidad ofrece a través de Internet, el cual se regirá por las siguientes responsabilidades y condiciones:  
        </p>
        <br>
        <h5 style="text-decoration-line: underline;">
        RESPONSABILIDADES DE LOS USUARIOS Y CONDICIONES DE USO DE MEDIOS ELECTRÓNICOS</h5>

        <p>•  El usuario asume la responsabilidad total del uso, tanto de la clave de usuario, así como de la veracidad de la información ingresada y la utilización de los servicios que la Municipalidad ponga a su disposición a través de medios electrónicos. </p>
        <p>
          • Todas las transacciones realizadas a través de medios electrónicos se garantizan mediante la clave de usuario del contribuyente, la cual tiene una equivalencia de su firma autógrafa, y de ella se derivarán todas las responsabilidades de carácter civil y tributario según lo señala la “Ley de Comercio Electrónico, Firmas Electrónicas y Mensajes de Datos”, y en base al principio de libertad tecnológica estipulado en el mismo cuerpo legal, las partes acuerdan que la clave proporcionada por el Gobierno Autónomo Descentralizado Municipal de Manta al usuario, surtirá los mismos efectos que una firma electrónica, por lo que, tanto su funcionamiento como su aplicación se entenderán como una completa equivalencia funcional, técnica y jurídica. 
        </p>
        <p>
          • Tendrán la consideración de usuarios del servicio las personas naturales o jurídicas que suscriban el correspondiente acuerdo,  previa acreditación de su identidad en el caso de personas naturales a través de la Cédula/R.U.C. y de la representación que ostenten en el de personas jurídicas a través de la Cédula/R.U.C. del representante, R.U.C. de la persona jurídica y documento que acredite la representación que invoca, sea cual fuere el domicilio de su residencia o la razón comercial de la persona jurídica. 
        </p>
        <p>
          • La solicitud y firma del presente acuerdo se formalizará en las oficinas de la Municipalidad de Manta.  
        </p>
        <p>
          • El usuario se compromete a no facilitar la identificación de usuario y la clave de acceso a terceros; en caso de incumplimiento de este compromiso, el usuario asumirá las posibles responsabilidades y perjuicios que se deriven de sus actuaciones. 
        </p>
        <p>
          • El usuario autoriza a la Municipalidad a no atender las peticiones cursadas, cualquiera que sea su naturaleza, cuando tenga dudas razonables sobre la identidad de la persona que las emite. 
        </p>
        <p>
          • La Municipalidad de Manta se reserva el derecho de verificar la autenticidad de la información y documentos ingresados, así como las acciones legales a que hubiere lugar, según las leyes vigentes en caso de adulteración de dichos documentos o falsedad de la información ingresada.  
        </p>
        <p>
        •  El usuario declara la dirección de correo electrónico registrada en su cuenta como el medio autorizado para recibir comunicaciones, requerimientos de información y notificaciones administrativas derivadas del presente acuerdo. 
        </p>
        <p>
          • El usuario se compromete expresamente a abstenerse de llevar a cabo cualquiera de las actividades siguientes: 
        </p>

        <ol>
          <li>Destruir, alterar, inutilizar o de cualquier otra forma dañar los datos o documentos electrónicos de la Municipalidad, de otras administraciones públicas cuyos contenidos estén incluidos en el Portal o de terceros. </li>
          <li>Obstaculizar el acceso de otros usuarios o de las administraciones públicas al portal y a sus servicios mediante el consumo masivo de los recursos informáticos, a través de los cuales la Municipalidad presta sus servicios, así como realizar actuaciones susceptibles de estropear, interrumpir o generar errores en estos sistemas. </li>
          <li>Utilizar el sistema para intentar acceder a áreas restringidas de los sistemas informáticos de la Municipalidad  o de terceros.</li>
          <li>Intentar aumentar el nivel de privilegios de un usuario en el sistema.</li>
          <li>Introducir, por cualquier medio, programas, virus, macros, applets, controles ActiveX o cualquier otro dispositivo lógico o secuencia de caracteres que causen o sean susceptibles de causar cualquier tipo de alteración en los sistemas informáticos de la Municipalidad, de otros usuarios, de las administraciones públicas o de terceros.</li>
          <li>Suplantar la identidad de otro usuario, de las administraciones públicas o de un tercero. </li>
          <li>Incorporar, poner a disposición o permitir el acceso a productos, elementos, mensajes o servicios delictivos, violentos, pornográficos, ofensivos o, en general, contrarios a la ley, a la moral o al orden público. </li>
          <li>Vulnerar los derechos de propiedad intelectual del Gobierno Autónomo Descentralizado Municipal de Manta, de los autores de los contenidos o de terceras personas. </li>
          <li>Violar la confidencialidad de la información o los secretos empresariales de terceros. </li>
        </ol>
         <br>
        <h5 style="text-decoration-line: underline;">
        RESTRICCIÓN Y RESPONSABILIDAD DEL GOBIERNO AUTÓNOMO DESCENTRALIZADO MUNICIPAL DEL CANTÓN MANTA</h5>
        <br>
        <p>•  La entidad no será responsable por las pérdidas o daños sufridos por el usuario por causa de terceros o fallas tecnológicas bajo responsabilidad del mismo o de terceros.  </p>
        <p>•  La entidad no tiene responsabilidad por la exactitud, veracidad, contenido o por cualquier error en la información proporcionada por el usuario, sea que se trate de errores humanos o tecnológicos.</p>
        <br>
         <h5 style="text-decoration-line: underline;">
        ACEPTACION</h5>
        <p>El acceso y utilización del Portal Web atribuye la condición de usuario del mismo, y presupone la previa lectura y aceptación del presente acuerdo. Con el acceso y utilización del Portal Web se entenderá que el usuario acepta los términos por su propia iniciativa, de forma expresa, plena, sin reservas, y se somete voluntariamente a lo aquí estipulado.  </p>
        <p>El usuario acepta la validez de este acuerdo, de la clave de usuario que se le proporciona, así como de la información que envíe a la Administración haciendo uso de los sistemas o medios electrónicos que el Gobierno Autónomo Descentralizado Municipal de Manta ponga a su disposición.  </p>
        <br>
                 <h5 style="text-decoration-line: underline;">
        DURACIÓN/RESICIÓN</h5>
        <p>•  La duración de este acuerdo es por tiempo indefinido, siempre y cuando el usuario lo considere así, la Municipalidad se reserva la potestad de rescindir este acuerdo o de rehacer uno nuevo cuando el usuario no cumpla las cláusulas que en él se especifican. </p>
        <p>•  Tanto el usuario como la Municipalidad podrán rescindir el acuerdo, por mutuo entendimiento o de manera unilateral por cualquiera de las dos partes, mediante comunicación escrita.  </p>
        <p>•  Todas las operaciones ordenadas con anterioridad a la rescisión, tendrán valor legal y vigencia hasta la total finalización de los trámites de éstas. </p>
        <p>•  La Municipalidad podrá interrumpir la prestación del servicio temporal o definitivamente, en caso de producirse modificaciones relevantes en la situación de los ciudadanos o titulares del servicio, o si observa incumplimiento de las normas de uso del mismo. </p>
        <p>•  La Municipalidad se reserva el derecho de modificar las condiciones del servicio, el periodo, y los procedimientos técnicos de identificación y validación de los datos transmitidos, de acuerdo con sus necesidades. </p>
        <p>•  La no aceptación de estas modificaciones por parte de los usuarios del servicio supondrá la rescisión anticipada del servicio objeto de la presente normativa. </p>



<!--
        <p>
        De la responsabilidad que se desprende de este Acuerdo firmado y rubricado, según señala la "Ley de Comercio Electrónico, Firmas Electrónicas y Mensajes de Datos", la persona natural o jurídica de derecho privado o público acepta que el Usuario y Contraseña proporcionada por El GAD Manta, surtirá los mismos efectos que una firma electrónica y se entenderá como una completa equivalencia funcional, técnica y jurídica. En tal virtud, todas las transacciones realizadas por la persona natural o jurídica de derecho privado o público en la página web de el Gobierno Autónomo Descentralizado Municipal del Cantón Manta, garantizarán y legalizarán con el "Usuario y Contraseña registrada", para lo cual, además, señalo como correo electrónico Único y personal (no serán admitidos los dominios ".gob.ec" ni institucionales) para futuros avisos el siguiente: <b>{{$acep->mail}}</b>, dirección domiciliaria actual (MANTA/Centro//Centro/Sector), teléfono convencional 644245, teléfono celular <b>{{$acep->telefono}}</b>, teléfono de un contacto 900, referencia del contacto
        <b>{{$acep->nombres}} {{$acep->apellidos}}</b> y  nombre de usuario con número de Cédula <b>{{$acep->cedula_ruc}}</b></p> 

        <p>
        El GAD Manta tendrá derecho a negar, restringir o condicionar el acceso al usuario al Sistema de Trámites en Líneas, de manera total o parcial, a su entera discreción.</p>

        <p>
        En el caso de utilización de medios o servicios electrónicos para la presentación de la Declaración Patrimonial
        Jurada, por disposición del Art. 14 de la "Ley para la Presentación y Control de las Declaraciones Patrimoniales Juradas", acepto y reconozco que mi Declaración Patrimonial Jurada, generada electrónicamente en línea a través del Sistemas de Tramites en lineas del Gad Manta . Igualmente me comprometo a proporcionar al Gad Manta, cuando me solicite oficialmente para efectos de una acción de control, todos los sustentos que justifiquen la información declarada bajo juramento.</p>

        <p>
        El Gobierno Autónomo Descentralizado Municipal del Cantón Manta no será responsable por las pérdidas o daños sufridos en la información ingresada por el usuario ya sea por fallas tecnológicas causadas por el mismo o por terceros. El Gobierno Autónomo Descentralizado Municipal del Cantón Manta hace conocer que la información proporcionada es de exclusiva responsabilidad de la persona natural o jurídica de derecho privado o público que ingresa a la página web. </p>
        -->
      </div> 
      <div class="modal-body">
        <label for="#check">Acepto los términos de licencia</label>
        <input type="checkbox" name="" id="check">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSig2">Siguiente</button>
      </div>
    </div>
  </div> 
</div>

<!--POP-UP DE PREGUNTAS DE SEGURIDAD DE CONTRASEÑA-->

<div class="modal fade" id="pop_up2" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
        <h2 class="modal-title"><center>Pregunta de seguridad de cuenta</center></h2>
      </div>
      <div class="modal-body" data-spy="scroll" data-target=".bs-docs-sidebar" style="overflow-y:auto; width: default; height: 250px;">
        <div class="form-group has-feedback">
            <label for="pregunta">Elija una pregunta para asegurar su cuenta</label>
            <select name="pregunta1" id="pregunta" class="form-control">
              <option>¿Cuál es el nombre de su mascota?</option>
              <option>¿Cuál es el segundo nombre de su abuela materna?</option>
              <option>¿Cuál es su comida favorita?</option>
              <option>¿Cuál es el mejor amigo de su infancia?</option>
            </select>
        </div>

        <div class="form-group has-feedback">
            <label for="txtpregunta">Escriba su respuesta</label>
            <input class="form-control" type="text" name="txtpregunta" id="txtpregunta" placeholder="Ingrese su respuesta" onkeypress="return val(event)" style="text-transform:uppercase;" onkeyup="javascript:this.value=this.value.toUpperCase();" maxlength="15">
        </div>
        <div id="error_pregunta">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btnSig1">Siguiente</button>
      </div>
    </div>
  </div> 
</div>

<!--POP-UP DE QUE ESTA ACEPTADO COMPLETAMENTE-->

<div class="modal fade" id="pop_up3" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
        <h2 class="modal-title">Último paso</h2>
      </div>
      
         
      <div class="modal-body">
        <h1>Usted ha finalizado los requisitos de la cuenta</h1>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnfin">Finalizar</button>
      </div>
    </div>
  </div> 
</div>

  <!-- Pop up de Cambiar Foto -->
  <div class="modal fade" id="pop_up_cambio_foto" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header"> 
      <h2 class="modal-title"><center>Actualizar Foto</center></h2>
      </div>
      <div class="modal-body" data-spy="scroll" data-target=".bs-docs-sidebar" style="overflow-y:auto; width: default; height: 100px;">
 <div class="form-group">

        {!! Form::open(array('method' => 'PATCH', 'route' => array('act_foto', $acep->id),'files' => true)) !!} 
        <div class="form-group has-feedback">
                  <label class="control-label">Seleccione una Imagen para guadarla como su foto de perfil</label>

             <input  name="path" type="file" class="filestyle" data-input="true" data-buttonName="btn-primary" required data-buttonText="Buscar Foto" data-iconName="fa fa-picture-o" data-buttonBefore="true" data-placeholder="No ha seleccionado el documento aún" >
           
           
        </div>
    </div>        
      </div>
      <div class="modal-footer">
      <enter> 
            <button type="submit" class="btn btn-primary">Aceptar</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnSig2">Cancelar</button>         
        {!! Form::close() !!}
      </enter>       
      </div>
    </div>
  </div> 
</div>
@stop
@section('script')
<script type="text/javascript">
	$(document).ready(function() {

//GENERA LA TABLA CON MIS PROPIEDADES
$.ajax({
         type:"GET",
        // url:"http://localhost/WST/api/Propiedad/ObtenerPropiedad/{{$acep->cedula_ruc}}",
           url:"http://webserver.manta.gob.ec:8083/api/Propiedad/ObtenerPropiedad/{{$acep->cedula_ruc}}",
           dataType:"json",
         contentType:"text/plain"
      }).done(function(msg){
         arreglo2=msg;
         if (arreglo2.length==0) {
            var gene1="<br><div style='text-align: center;'>";
               gene1+="<b style='border-color: red; border-style: solid; border-width: 3px; padding: 10px; font-size: 15px;'>Usted no tiene propiedades registradas en el catastro municipal</b><br><br>";
            gene1+="</div>";
            $('#tpropiedades2').html(gene1);
         }else{
                gene="<table id='t_prop_d' class='table table-striped table-bordered table-hover display'>";
                gene+="<tr>";        
                gene+="<th  colspan='6' ><center><h1>MIS PROPIEDADES</h1> </center></th>";
                gene+="</tr>";
                gene+="<tr>";         
                gene+="<th><center>#</center></th>";
                gene+="<th><center>Clave Catastral</center></th>";
                gene+="<th><center>Dirección</center> </th>";
                gene+="<th><center>Valor Pendiente</center> </th>";
                gene+="</tr>";
         var numer=0;
         arreglo2.forEach(function(elemen){
            numer++;
              gene+="<tr align='center'>";              
              gene+='<td>'+numer+'</td>';
              gene+='<td>'+elemen.clave_catastral+'</td>';
              gene+='<td>'+elemen.direccion+'</td>';
              var cl_c=elemen.clave_catastral;
              var clave=cl_c.substring(0, 1)+cl_c.substring(2, 4)+cl_c.substring(5,7)+cl_c.substring(8,10)+cl_c.substring(11,14);
              console.log(clave);
              console.log(elemen.valor_pendiente);
               gene+='<td>'+elemen.valor_pendiente+'</td>';
              gene+="<td> <button class='btn btn-primary btn-xs'  onClick='return Ejemplo("+clave+")'>Ver Títulos de Créditos</button></td>"       
              });
         gene+='</table>';
         gene+='</div></div>';
         $('#tpropiedades2').html(gene);
         }
//hacer nuevamente la peticion 
      }).error(function(error){
         console.log('error');
         var gene0="<div style='text-align: center;'>";
               gene0+="<b style='padding: 10px; font-size: 15px;'>No se tiene acceso a la Base de Datos Municipal. Disculpe las molestias, inténtelo más tarde</b><br><br>";
            gene0+="</div>";
            $('#tpropiedades2').html(gene0);
            console.log('el error de la bd es '+error);
      });

      // TERMINO DE GENERAR LA TABLA
      

      var preguntag = "nada";
      var respuestag = "nada";
      var aceptadog = false;
      if ('{{$acep->tipo_usuario_id}}' == 2) {
        if ('{{$acep->aceptado}}' == false) {
          $('#pop_up2').modal('show');
          $('#btnSig2').attr('disabled', true);
        }
      }

      $( '#check' ).on( 'click', function() {
          if( $(this).is(':checked') ){
              $('#btnSig2').attr('disabled', false);
          } else {
              $('#btnSig2').attr('disabled', true);
          }
      });
      
      $('#btnSig1').click(function(){
        var expreg = new RegExp("^[A-Za-zÑñ]{2,50}$");;
        if ((expreg.test($('#txtpregunta').val())==true)) 
        {
          $('#pop_up2').modal('hide');
          $('#pop_up').modal('show');
          preguntag = $('#pregunta').val();
          respuestag = $('#txtpregunta').val();
        }else{
            var newHtml = "<p><span class='ui-icon ui-icon-alert'>";
            newHtml += "</span>";
            newHtml += " Por favor ingrese correctamente su respuesta";
            newHtml += "</p>";
            $('#error_pregunta').html(newHtml);
            $('#txtpregunta').css({"border":"1px solid red"});
        }
      });
      $('#btnSig2').click(function(){
        $('#pop_up').modal('hide');
        $('#pop_up3').modal('show');
      });
      $('#btnfin').click(function(){
          var indice = $('#pregunta').prop('selectedIndex');
          window.location.href = ("{{url('completar')}}"+"/{{$acep->id}}/"+indice+"/"+respuestag);        
      });
    });
    function val(e) {
      tecla = (document.all) ? e.keyCode : e.which;
      if (tecla==8) return true;
      var patron =/[A-Za-zÑñ]/;
      var te = String.fromCharCode(tecla);
      return patron.test(te);      
    }
    function Ejemplo(id)
      {
        window.location.href="verpago/"+id+"/1";
      }
function Actualizar_foto_pop()
{
  $('#pop_up_cambio_foto').modal('show');
}
</script>
@endsection

