@extends ('layout_basic')
@section ('titulo') {{ trans('html.main.sistema') }} @stop
@section ('scripts')
<<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
            $('.contact-box').each(function() {
                animationHover(this, 'pulse');
            });
        });
setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('{{ trans('html.main.sistema') }}', 'Bienvenido');

            }, 1300);
</script>
@stop
@section ('contenido')
	<h1 class="titulo">
		Escoja un módulo para empezar
	</h1>
@if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]();
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
	   <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<hr>
<div class="row">
<div class="col-lg-8"> 
        @foreach($tabla as $key => $value)
     <div class="col-lg-6 col-md-6">
                  <div class="contact-box">
                      <a href="{{ URL::asset($value->ruta) }}">
                      <h3 style="text-align: center;"><strong>{{ $value->nombre }}</strong></h3>
                      <div class="col-sm-4">
                          <div class="text-center">
                              <i class="fa fa-cube fa-5x"></i>                              
                          </div>
                      </div>
                      <div class="col-sm-8">
                          <p><i class="fa fa-check"></i>{{ $value->detalle }}</p>
                      </div>
                      <div class="clearfix"></div>
                          </a>
                  </div>
              </div>
     @endforeach 
</div>
</div>
<div class="row">
    <div class="col-lg-12"> 
<center>
<img src="{{asset('http://manta.gob.ec/images/manta-firmes.png') }}" alt="" class="img-responsive" style="width: 450px;">
</center>
</div>
</div>
@stop