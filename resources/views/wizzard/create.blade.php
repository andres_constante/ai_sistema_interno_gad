<?php
if(!isset($nobloqueo))
    Autorizar(Request::path());
?>
@extends ('layout')
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
<script type="text/javascript" src="{{asset('js/jquery.validate.min.js')}}"></script>
<script>
.pic-container {
  width: 1350px;
  margin: 0 auto;
  white-space: nowrap;
}

.pic-row {
  /* As wide as it needs to be */
  width: 1350px;
  height: 400px;
  overflow: auto;
}

.pic-row a {
  clear: left;
  display: block;
}
</script>
<script>
$(document).ready(function() {
  
    
   
    @if(isset($validarjs))
    //Validacion
    jQuery.extend(jQuery.validator.messages, {
      required: "Este campo es obligatorio.",
      remote: "Por favor, rellena este campo.",
      email: "Por favor, escribe una dirección de correo válida",
      url: "Por favor, escribe una URL válida.",
      date: "Por favor, escribe una fecha válida.",
      dateISO: "Por favor, escribe una fecha (ISO) válida.",
      number: "Por favor, escribe un número entero válido.",
      digits: "Por favor, escribe sólo dígitos.", 
      creditcard: "Por favor, escribe un número de tarjeta válido.",
      equalTo: "Por favor, escribe el mismo valor de nuevo.",
      accept: "Por favor, escribe un valor con una extensión aceptada.",
      maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
      minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
      rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
      range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
      max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
      min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
      $('form').validate({       
          rules: {
            @foreach($validarjs as $key)
              {!! $key !!},
            @endforeach
            /*
              clave: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              },
              valor: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              }
            */
          },
          highlight: function(element) {
              $(element).closest('.form-group').addClass('has-error');
          },
          unhighlight: function(element) {
              $(element).closest('.form-group').removeClass('has-error');
          },
          errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function(error, element) {
              if(element.parent('.input-group').length) {
                  error.insertAfter(element.parent());
              } else {
                  error.insertAfter(element);
              }
          }
    @if(isset($popup))  
          ,
          submitHandler : function(form) {
          //do something here
              var frm = $('#form1');
              frm.submit(function (ev) {
                ev.preventDefault(); 
                //$("#divconsulta").html($("#gif_cargando").html());
                //$("#gif_cargando")
                $.ajax({
                    type: frm.attr('method'),
                    url: frm.attr('action'),
                    data: frm.serialize(),
                    statusCode: {
                        404: function() {
                        toastr["error"]("Se produjo un error al consultar los datos. 1 :(");
                        //bloquearpantalla(0);                                
                        },                            
                        401: function() {
                        toastr["error"]("Se produjo un error al consultar los datos. 2 :(");
                        //bloquearpantalla(0);
                        }
                        //$("#divconsulta").html("Se produjo un error al consultar los datos. 4 :( ");
                    },
                    error: function(objeto, opciones, quepaso){
                        //setTimeout(function(){ajaxres.abort();}, 100);
                        //bloquearpantalla(0);
                        //$(tr).attr( "class","danger" );
                        //$("#divconsulta").html(quepaso);
                        toastr["error"]("Se produjo un error al consultar los datos. 2 :(");
                    },
                    success: function (data) {       
                      console.log(data);
                      toastr[data[0]](data[1]);
                      parent.jQuery.colorbox.close();
                      /*
                      $("#divconsulta").html(""); 
                      $("#tipobuscar").val("");
                      if($.isArray(data)){
                        //console.log("Es array");
                        toastr["error"](data[1]);

                        $("#divconsulta").html(data[2]);
                      }
                      else{
                        var tipo = $('input:radio[name=radio]:checked').val();             
                        if(tipo==2) {
                          $("#divpropiedades").html(data);
                          $("#divconsulta").text("");
                        }
                        else{
                            $("#divconsulta").html(data);                      
                        }
                    }
                    */
                }
            });
            //bloquearpantalla(0);
            }); 
        }
        @endif
      });
@endif
  //Fin Validacion
 $('.selective-normal').selectize();
 $('.selective-tags').selectize({
          plugins: ['remove_button'],
          persist: false,
          create: true,
          render: {
            item: function(data, escape) {
              return '<div>"' + escape(data.text) + '"</div>';
            }
          },
          onDelete: function(values) {
            //return confirm(values.length > 1 ? 'Esta seguro de borrar la selección ' + values.length : '');
            return confirm('Esta seguro de borrar la selección?');
          }
        });
  // var ruta= '<?php echo URL::to('cargartipousuario'); ?>/'; 
  // $.get(ruta, function(data) {
  //              $("#asignarinspectort").empty();  
  //              $("#asignarinspectort").append("<option value>Escoja...</option>");
  //              $.each(data, function(key, element) {
  //               $("#asignarinspectort").append("<option value='" + key + "'>" + element + "</option>");
  //             });

  //           //$('#'+id).trigger("chosen:updated");
  //       });

});


 $(document).ready(function(){
            $("#wizard").steps();
            $("#form1").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
                    $(".chosen-select").chosen();
            $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: false, //True para mostrar Numero de semanas
                autoclose: true,
                format: 'yyyy-mm-dd',
                language: 'es'
            });
            
            $('.solonumeros').on('input', function () { 
              this.value = this.value.replace(/[^0-9.]/g,'');
            });
          
       });
</script>
@stop
@section ('contenido')
<h1> 
    @if($configuraciongeneral[2]=="crear")
        Nuevo Registro - {!!  $configuraciongeneral[0]  !!}
    @else
        Editar Registro - {!!  $configuraciongeneral[0]  !!}
    @endif
</h1>

<div class="ibox float-e-margins">
     <div class="ibox-title">
        <div class="">
          <a  href="{{ URL::to($configuraciongeneral[1]) }}" class="btn btn-primary ">Todos</a>
          <a  href="{{ URL::to($configuraciongeneral[1]."/create") }}" class="btn btn-default ">Nuevo</a>
                        
        </div>
<!--Enviar errores de validacion en caso de que los haya-->
    @if($errors->all())
        <script>   
        $(document).ready(function() {
        //    $.notify("Error al guardar!","error");
        toastr["error"]($("#diverror").html());    
        });
        </script>
          <div class="alert alert-danger" style="text-align: left;" id='diverror'>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4>Error!</h4>
              <div id="diverror">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
            </div> 
          </div>
    @endif  
    @if(isset($alerta) && $alerta!="")
        <div class="alert alert-danger" style="text-align: left;" id='diverror'>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4>Error!</h4>
            <ul><li>{!!  $alerta  !!}</li></ul>
        </div>
    @endif
      </div>
 

      <div class="ibox-content">

                          
                        <div class="ibox-content">
                            @if($configuraciongeneral[2]=="crear")
                            {!!  Form::open(array('url' => $configuraciongeneral[1],'role'=>'form' , 'id'=>'form1','class'=>'wizard-big'))  !!}
                            @else
                              {!!  Form::model($tabla, array('route' => array(str_replace("/",".",$configuraciongeneral[1]).'.update', $tabla->id), 'method' => 'PUT','class'=>'wizard-big', 'id'=>'form1' ))  !!}
                            @endif

                            <!-- <form id="form" action="#" class="wizard-big"> -->
                              <h1 class="paso1">Información Concreta</h1>
                              <fieldset>
                                <h2>Registro de datos</h2>
                                <div class="row">
                                      @foreach($objetos as $i => $campos)                                      
                                            <div class="form-horizontal">
                                              {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                                              <div class="col-lg-3"> 
                                                  @if($campos->Tipo=="date")
                                                      <!-- {! Form::input('date',$campos->Nombre,$campos->Valor, ['class' => 'form-control', 'placeholder' => 'Date']) !!}   -->
                                                    {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datepicker','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                                                  @elseif($campos->Tipo=="datetext")
                                                    <div class="input-group date">
                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                      
                                                      {!! Form::text($campos->Nombre, null , array(
                                                        'data-placement' =>'top',
                                                        'data-toogle'=>'tooltip',
                                                        'class' => 'form-control datepicker',
                                                        'placeholder'=>$campos->Descripcion,
                                                        $campos->Nombre,
                                                        'readonly'=>'readonly'
                                                        )) !!}
                                                  </div>
                                                  @elseif($campos->Tipo=="select")
                                                    {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                                                  @elseif($campos->Tipo=="select2")
                                                    {!! Form::select($campos->Nombre,$campos->Valor, $campos->ValorAnterior, array('class' => "form-control ".$campos->Clase)) !!}   
                                                    @elseif($campos->Tipo=="select-multiple")
                                                    {!! Form::select($campos->Nombre."[]",$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre))!!}
                                                    @elseif($campos->Tipo=="file") 
                                                    {!! Form::file($campos, array('class' => 'form-control')) !!}
                                                    @elseif($campos->Tipo=="textdisabled")
                                                    {!!   Form::text($campos,Input::old($campos), array('class' => 'form-control','placeholder'=>'Ingrese '.$camposcaption[$i],'readonly')) !!}
                                                    @elseif($campos->Tipo=="textarea")
                                                    <div class="row">
                                                      <div class="col-lg-12"> 
                                                        {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                                                      </div>
                                                    </div>
                                                    @elseif($campos->Tipo=="password")
                                                    {!!   Form::password($campos->Nombre,array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Nombre)) !!}
                                                    @else
                                                    {!! Form::text($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                                                    @endif
                                                  </div>
                                                </div>
                                                @endforeach
                                                
                                                
                                                <div class="col-lg-4">
                                                  <div class="text-center">
                                                    <div style="margin-top: 20px">
                                                        <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                      
                                              </fieldset>
                                              <h1>Personas a Cargo /  Fiscalizadores</h1>
                                              <fieldset>
                                                <h2>Personal</h2>
                                                <div class="row">
                                                @foreach($objetosCabComPersonas as $i => $campos)
                                            <div class="form-group">
                                              {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                                              <div class="col-lg-3"> 
                                                  @if($campos->Tipo=="date")
                                                      <!-- {! Form::input('date',$campos->Nombre,$campos->Valor, ['class' => 'form-control', 'placeholder' => 'Date']) !!}   -->
                                                    {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datepicker','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                                                  @elseif($campos->Tipo=="datetext")
                                                    <div class="input-group date">
                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                      
                                                      {!! Form::text($campos->Nombre, null , array(
                                                        'data-placement' =>'top',
                                                        'data-toogle'=>'tooltip',
                                                        'class' => 'form-control datepicker',
                                                        'placeholder'=>$campos->Descripcion,
                                                        $campos->Nombre,
                                                        'readonly'=>'readonly'
                                                        )) !!}
                                                  </div>
                                                  @elseif($campos->Tipo=="select")
                                                    {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                                                  @elseif($campos->Tipo=="select2")
                                                    {!! Form::select($campos->Nombre,$campos->Valor, $campos->ValorAnterior, array('class' => "form-control ".$campos->Clase)) !!}   
                                                    @elseif($campos->Tipo=="select-multiple")
                                                    {!! Form::select($campos->Nombre."[]",$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre))!!}
                                                    @elseif($campos->Tipo=="file") 
                                                    {!! Form::file($campos, array('class' => 'form-control')) !!}
                                                    @elseif($campos->Tipo=="textdisabled")
                                                    {!!   Form::text($campos,Input::old($campos), array('class' => 'form-control','placeholder'=>'Ingrese '.$camposcaption[$i],'readonly')) !!}
                                                    @elseif($campos->Tipo=="textarea")
                                                    {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                                                    
                                                    @elseif($campos->Tipo=="password")
                                                    {!!   Form::password($campos->Nombre,array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Nombre)) !!}
                                                    @else
                                                    {!! Form::text($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                                                    @endif
                                                  </div>
                                                </div>
                                                @endforeach
                                                
                                              </div>
                                            </fieldset>
                                            
                                            <h1>Parroquia / Barrios</h1>
                                            <fieldset>
                                              <!-- <div class="text-center" style="margin-top: 120px"> -->
                                                  <h2>Lugar</h2>
                                                  <div class="row">
                                                @foreach($objetosCabPresupuesto as $i => $campos)
                                            <div class="form-group">
                                              {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                                              <div class="col-lg-3"> 
                                                  @if($campos->Tipo=="date")
                                                      <!-- {! Form::input('date',$campos->Nombre,$campos->Valor, ['class' => 'form-control', 'placeholder' => 'Date']) !!}   -->
                                                    {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datepicker','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                                                  @elseif($campos->Tipo=="datetext")
                                                    <div class="input-group date">
                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                      
                                                      {!! Form::text($campos->Nombre, null , array(
                                                        'data-placement' =>'top',
                                                        'data-toogle'=>'tooltip',
                                                        'class' => 'form-control datepicker',
                                                        'placeholder'=>$campos->Descripcion,
                                                        $campos->Nombre,
                                                        'readonly'=>'readonly'
                                                        )) !!}
                                                  </div>
                                                  @elseif($campos->Tipo=="select")
                                                    {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                                                  @elseif($campos->Tipo=="select2")
                                                    {!! Form::select($campos->Nombre,$campos->Valor, $campos->ValorAnterior, array('class' => "form-control ".$campos->Clase)) !!}   
                                                    @elseif($campos->Tipo=="select-multiple")
                                                    {!! Form::select($campos->Nombre."[]",$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre))!!}
                                                    @elseif($campos->Tipo=="file") 
                                                    {!! Form::file($campos, array('class' => 'form-control')) !!}
                                                    @elseif($campos->Tipo=="textdisabled")
                                                    {!!   Form::text($campos,Input::old($campos), array('class' => 'form-control','placeholder'=>'Ingrese '.$camposcaption[$i],'readonly')) !!}
                                                    @elseif($campos->Tipo=="textarea")
                                                    {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                                                    
                                                    @elseif($campos->Tipo=="password")
                                                    {!!   Form::password($campos->Nombre,array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Nombre)) !!}
                                                    @else
                                                    {!! Form::text($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                                                    @endif
                                                  </div>
                                                </div>
                                                @endforeach
                                                
                                              </div>
                                                  <!-- </div> -->
                                            </fieldset>

                                            <h1>Parroquia / Barrios</h1>
                                            <fieldset>
                                              <!-- <div class="text-center" style="margin-top: 120px"> -->
                                                  <h2>Lugar</h2>
                                                  <div class="row">
                                                @foreach($objetosCominicacionParroquia as $i => $campos)
                                            <div class="form-group">
                                              {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                                              <div class="col-lg-3"> 
                                                  @if($campos->Tipo=="date")
                                                      <!-- {! Form::input('date',$campos->Nombre,$campos->Valor, ['class' => 'form-control', 'placeholder' => 'Date']) !!}   -->
                                                    {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datepicker','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                                                  @elseif($campos->Tipo=="datetext")
                                                    <div class="input-group date">
                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                      
                                                      {!! Form::text($campos->Nombre, null , array(
                                                        'data-placement' =>'top',
                                                        'data-toogle'=>'tooltip',
                                                        'class' => 'form-control datepicker',
                                                        'placeholder'=>$campos->Descripcion,
                                                        $campos->Nombre,
                                                        'readonly'=>'readonly'
                                                        )) !!}
                                                  </div>
                                                  @elseif($campos->Tipo=="select")
                                                    {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                                                  @elseif($campos->Tipo=="select2")
                                                    {!! Form::select($campos->Nombre,$campos->Valor, $campos->ValorAnterior, array('class' => "form-control ".$campos->Clase)) !!}   
                                                    @elseif($campos->Tipo=="select-multiple")
                                                    {!! Form::select($campos->Nombre."[]",$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre))!!}
                                                    @elseif($campos->Tipo=="file") 
                                                    {!! Form::file($campos, array('class' => 'form-control')) !!}
                                                    @elseif($campos->Tipo=="textdisabled")
                                                    {!!   Form::text($campos,Input::old($campos), array('class' => 'form-control','placeholder'=>'Ingrese '.$camposcaption[$i],'readonly')) !!}
                                                    @elseif($campos->Tipo=="textarea")
                                                    {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                                                    
                                                    @elseif($campos->Tipo=="password")
                                                    {!!   Form::password($campos->Nombre,array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Nombre)) !!}
                                                    @else
                                                    {!! Form::text($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                                                    @endif
                                                  </div>
                                                </div>
                                                @endforeach
                                                
                                              </div>
                                                  <!-- </div> -->
                                            </fieldset>

                                            <h1>Parroquia / Barrios</h1>
                                            <fieldset>
                                              <!-- <div class="text-center" style="margin-top: 120px"> -->
                                                  <h2>Lugar</h2>
                                                  <div class="row">
                                                @foreach($objetosCominicacionBarrio as $i => $campos)
                                            <div class="form-group">
                                              {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-3 control-label")) !!}
                                              <div class="col-lg-3"> 
                                                  @if($campos->Tipo=="date")
                                                      <!-- {! Form::input('date',$campos->Nombre,$campos->Valor, ['class' => 'form-control', 'placeholder' => 'Date']) !!}   -->
                                                    {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control datepicker','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                                                  @elseif($campos->Tipo=="datetext")
                                                    <div class="input-group date">
                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                      
                                                      {!! Form::text($campos->Nombre, null , array(
                                                        'data-placement' =>'top',
                                                        'data-toogle'=>'tooltip',
                                                        'class' => 'form-control datepicker',
                                                        'placeholder'=>$campos->Descripcion,
                                                        $campos->Nombre,
                                                        'readonly'=>'readonly'
                                                        )) !!}
                                                  </div>
                                                  @elseif($campos->Tipo=="select")
                                                    {!! Form::select($campos->Nombre,$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase)) !!}
                                                  @elseif($campos->Tipo=="select2")
                                                    {!! Form::select($campos->Nombre,$campos->Valor, $campos->ValorAnterior, array('class' => "form-control ".$campos->Clase)) !!}   
                                                    @elseif($campos->Tipo=="select-multiple")
                                                    {!! Form::select($campos->Nombre."[]",$campos->Valor, Input::old($campos->Nombre) ? Input::old($campos->Nombre) : ($campos->ValorAnterior !="Null") ? $campos->ValorAnterior : Input::old($campos->Nombre), array('class' => "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre))!!}
                                                    @elseif($campos->Tipo=="file") 
                                                    {!! Form::file($campos, array('class' => 'form-control')) !!}
                                                    @elseif($campos->Tipo=="textdisabled")
                                                    {!!   Form::text($campos,Input::old($campos), array('class' => 'form-control','placeholder'=>'Ingrese '.$camposcaption[$i],'readonly')) !!}
                                                    @elseif($campos->Tipo=="textarea")
                                                    {!! Form::textarea($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase  ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                                                    
                                                    @elseif($campos->Tipo=="password")
                                                    {!!   Form::password($campos->Nombre,array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Nombre)) !!}
                                                    @else
                                                    {!! Form::text($campos->Nombre, Input::old($campos->Valor) , array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                                                    @endif
                                                  </div>
                                                </div>
                                                @endforeach
                                                
                                              </div>
                                                  <!-- </div> -->
                                            </fieldset>

                                                
                                                <h1>Finalizar</h1>
                                                <fieldset>
                                                  <h2>Terms and Conditions</h2>
                                                  <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required"> <label for="acceptTerms">I agree with the Terms and Conditions.</label>
                                                  @if(isset($popup)) 
                                                    <input type="hidden" value="popup" name="txtpopup" />
                                                  <!--else -->
                                                    <!--input type="hidden" value="" name="txtpopup"/-->
                                                  @endif
                                                  {!!  Form::submit("Guardar", array('class' => 'btn btn-primary', 'style' => 'float:right' ))  !!}
                                                  
                                                  {!!  Form::close()  !!}
                                                </fieldset>
                                                <!-- </form> -->
                                                
                                                
                        
                                                </div>
<script>


</script>

@stop

