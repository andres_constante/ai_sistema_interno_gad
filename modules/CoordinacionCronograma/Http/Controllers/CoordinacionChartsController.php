<?php namespace Modules\Coordinacioncronograma\Http\Controllers;


use Pingpong\Modules\Routing\Controller;
use App\ModulosModel;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Route;
use Auth;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use DB;
use App\UsuariosModel;



class CoordinacionChartsController extends Controller {
	var $configuraciongeneral = array ("MONITOREO Y CONTROL - ACTIVIDADES", "coordinacioncronograma/coordinaciongraficos", "index");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[
        {"Tipo":"chart","Descripcion":"GRÁFICO DE ACTIVIDADES","Nombre":"chart2","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"chart2","Descripcion":"GRÁFICO DE ACTIVIDADES POR DIRECCIÓN","Nombre":"chart4","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"chart","Descripcion":"GRÁFICO DE ACTIVIDADES POR DIRECCIONES","Nombre":"chart3","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';

    
		public function __construct() {
			$this->middleware('auth');
		}

	public function index()
	{
        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
        $id_usuario=Auth::user()->id;
        if($id_tipo_pefil->tipo ==2 ||$id_tipo_pefil->tipo==3 ){
            $direcciones=direccionesModel::
            join("users as us","us.id_direccion","=","tmae_direcciones.id")
            ->select('tmae_direcciones.id','direccion')->where([['tmae_direcciones.estado','=','ACT'],["us.id",$id_usuario]])->orderby("direccion","asc")->get();
           
        }else{

            $direcciones=direccionesModel::select('id','direccion')->where('estado','=','ACT')->orderby("direccion","asc")->get();
            //show($direcciones);
        }

        
		$objetos = json_decode($this->objetos);
		$objetos=array_values($objetos);
		
        //show($objetos);
       
            
        return view('vistas.indexchart',[
            "objetos"=>$objetos,
            "direcciones"=>$direcciones,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "delete"=>"si",
            "create"=>"si"
            ]);
    }
    


    
  public function getValoresCharts(){
    $tipo_chart=Input::get("tipo");
    $direccion=Input::get("direccion");
    $valores;
    $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
    $id_usuario=Auth::user()->id;

    switch($tipo_chart){
        case '1':

       
            $valores=CoorObrasModel::select(DB::raw('count(estado_obra) as valor, estado_obra'))
            ->where("coor_tmov_obras_listado.estado","ACT")->groupby("estado_obra")->get();
            return $valores;
       
        break;
        case '2':
        switch($id_tipo_pefil->tipo)
        {
            case 1:
            $valores=CoorCronogramaCabModel::select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
		    ->where("coor_tmov_cronograma_cab.estado","ACT")->groupby("estado_actividad")->get();
            return $valores;
            break;
            case 2:
          
            $valores = CoorCronogramaCabModel::
            join("coor_tmov_cropnograma_resposables as cr","cr.id_crono_cab","=","coor_tmov_cronograma_cab.id")
            ->join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
            ->where([["coor_tmov_cronograma_cab.estado","ACT"],["cr.id_usuario",$id_usuario]])
            ->get();
            return $valores;
            
            break;
            case 3:
            $valores=CoorCronogramaCabModel::select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
            ->join("users as u","u.id_direccion","=","coor_tmov_cronograma_cab.id_direccion")
            ->where([["coor_tmov_cronograma_cab.estado","ACT"],["u.id",$id_usuario]])
            ->groupby("estado_actividad")->get();
            return $valores;
            return $valores;
            break;
            case 4:
            $valores=CoorCronogramaCabModel::select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
		    ->where("coor_tmov_cronograma_cab.estado","ACT")->groupby("estado_actividad")->get();
            return $valores;
            break;
            
        }
        break;
        case '3':
        switch($id_tipo_pefil->tipo)
        {
            case 1:
            $valores=CoorCronogramaCabModel::
            join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select(DB::raw('count(id_direccion) as valor, a.direccion'))
            ->where("coor_tmov_cronograma_cab.estado","ACT")->groupby("a.direccion")->get();
            return $valores;
            break;
            case 2:
            $valores = CoorCronogramaCabModel::
            join("coor_tmov_cropnograma_resposables as cr","cr.id_crono_cab","=","coor_tmov_cronograma_cab.id")
            ->join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select(DB::raw('count(coor_tmov_cronograma_cab.id) as valor, a.direccion'))
            ->where([["coor_tmov_cronograma_cab.estado","ACT"],["cr.id_usuario",$id_usuario]])
            ->groupby("a.direccion")
            ->get();
            return $valores;

            break;
            case 3:
            //show("estoy aqui");
            $valores=CoorCronogramaCabModel::
            join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->join("users as u","u.id_direccion","=","coor_tmov_cronograma_cab.id_direccion")
            ->select(DB::raw('count(*) as valor, a.direccion'))
		    ->where([["coor_tmov_cronograma_cab.estado","ACT"],["u.id",$id_usuario]])->groupby("a.direccion")->get();
            return $valores;
            break;
            case 4:
            $valores=CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select(DB::raw('count(id_direccion) as valor, a.direccion'))
            ->where("coor_tmov_cronograma_cab.estado","ACT")->groupby("a.direccion")->get();
            return $valores;
            break;
            
        }
        break;
        case '4':
        $valores=CoorCronogramaCabModel::select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
        ->where([["coor_tmov_cronograma_cab.estado","ACT"],["coor_tmov_cronograma_cab.id_direccion",$direccion]])->groupby("estado_actividad")->get();
        return $valores;
        break;

        return response()->json($valores);
    }




  }
	
}