<?php namespace Modules\Coordinacioncronograma\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Auth;
use Carbon\Carbon;
use App\UsuariosModel;
use DB;
class CoordinacionListController extends Controller {

	var $configuraciongeneral = array ("Coordinación Actividades - Búsqueda", "coordinacioncronograma/coordinacionlista", "index");
    var $escoja=array(0=>"TODOS");
	var $objetos = '[{"Tipo":"select","Descripcion":"Tipo de Filtro","Nombre":"tipofiltro","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]'; 	
		
	var $objetosTable = '[
		
				
		{"Tipo":"text","Descripcion":"Estado Actividad","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Avance","Nombre":"avance","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Prioridad","Nombre":"prioridad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Ultima Modificacion","Nombre":"updated_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }     
				
				]';
				
	public function __construct() {
				$this->middleware('auth');
	} 
	
	/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
	*/
	public function index()
	{

		$objetos = json_decode($this->objetos);
		//show($this->objetos);
		$objetos[0]->Nombre="tipofiltro";
		$tipoFiltro=array(1=>"DIRECCION",2=>"ESTADO",3=>"PRIORIDAD");
		
		$objetos[0]->Valor=$this->escoja + $tipoFiltro;
		$objetosTable = json_decode($this->objetosTable);
		$objetos=array_values($objetos);
		$delete='si';

		$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
		if($id_tipo_pefil->tipo==2||$id_tipo_pefil->tipo==3){
			$delete=null;
		}

        return view('vistas.indexlist',[
			"objetos"=>$objetos,
			"objetosTable"=>$objetosTable,
			"configuraciongeneral"=>$this->configuraciongeneral,
			"delete"=>$delete,
            "create"=>"si"
            ]);


	}
	public function getValoresFiltro(){
		$tipo_filtro=Input::get("tipo_filtro");
		$html = '';
		$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
		//($id_tipo_pefil->tipo);
		$id_usuario=Auth::user()->id;
		switch($tipo_filtro){
			
			case 'DIRECCION':
			if($id_tipo_pefil->tipo==2|| $id_tipo_pefil->tipo==3){
				$valores=direccionesModel::
				join("users as us","us.id_direccion","=","tmae_direcciones.id")
				->where([["tmae_direcciones.estado", "ACT"],["us.id",$id_usuario]])->select("direccion","tmae_direcciones.id")->orderby("direccion","asc")->get();
			}else{
				$valores=direccionesModel::where("tmae_direcciones.estado", "ACT");
				//if(Input::has("coor")){
					$valores=$valores->join("coor_tmov_cronograma_cab as a","a.id_direccion","=","tmae_direcciones.id")
					->select("tmae_direcciones.direccion","tmae_direcciones.id",DB::raw("count(*) as total"))
					->groupBy("tmae_direcciones.direccion","tmae_direcciones.id");
				// }else{
				// 	$valores=$valores->select("tmae_direcciones.direccion","tmae_direcciones.id");
				// }
				$valores=$valores->orderby("tmae_direcciones.direccion","asc")->get();
			}
		
			
			$html .= '<label class="col-lg-1 control-label">Dirección</label> <div class="col-lg-4">';
			$html .= '<select class="form-control chosen-select"  id="valorfiltro">';
			foreach ($valores as $item) {
				$html .= '<option value="'.$item->id.'">'.$item->direccion.'</option>';
			}
			$html .= '</select></div>';
			return $html;
			break;

			case 'ESTADO':
			$valores=explodewords(ConfigSystem("estadoobras"),"|");
			$html .= '<label class="col-lg-1 control-label">Estado</label> <div class="col-lg-4">';
			$html .= '<select class="form-control chosen-select" id="valorfiltro">';
			foreach ($valores as $item) {
				$html .= '<option value="'.$item.'">'.$item.'</option>';
			}
			$html .= '</select></div>';
			return $html;
			break;
			case 'PRIORIDAD':
			$valores=explodewords(ConfigSystem("prioridad"),"|");
			$html .= '<label class="col-lg-1 control-label">Estado</label> <div class="col-lg-4">';
			$html .= '<select class="form-control chosen-select" id="valorfiltro">';
			foreach ($valores as $item) {
				$html .= '<option value="'.$item.'">'.$item.'</option>';
			}
			$html .= '</select></div>';
			return $html;
			break;

			
		}
		return response()->json(['html' => $html]);
		
  }

  public function getComunicacionesFiltro(){
	$tipo_filtro=Input::get("tipo_filtro");
	$valor_filtro=Input::get("valor_filtro");
	$desde=Carbon::parse(Input::get("desde"));
	$hasta=Carbon::parse(Input::get("hasta"));

	$valores=[];
	$create='';
    $delete='';
   
    $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
	$id_usuario=Auth::user()->id;


	switch($tipo_filtro){

		
		case 'TODOS':
		switch($id_tipo_pefil->tipo){
			case 1:
			$valores=CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad",
			"coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where("coor_tmov_cronograma_cab.estado","ACT")
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
			->get();
					
			return $valores;
		
			break;
			case 2:
			$valores = CoorCronogramaCabModel::
            join("coor_tmov_cropnograma_resposables as cr","cr.id_crono_cab","=","coor_tmov_cronograma_cab.id")
            ->join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.actividad","coor_tmov_cronograma_cab.estado_actividad",
			"coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin","coor_tmov_cronograma_cab.avance","a.direccion","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado","ACT"],["cr.id_usuario",$id_usuario]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
            ->get();		
			return $valores;

			break;
			case 3:
			$valores = CoorCronogramaCabModel::
            join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->join("users as u","u.id_direccion","=","a.id")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad",
			"coor_tmov_cronograma_cab.actividad","coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin",
			"a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado","ACT"],["u.id",$id_usuario]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
			->get();
			
			return $valores;

			break;
			case 4:
			$valores=CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad",
			"coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where("coor_tmov_cronograma_cab.estado","ACT")
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
			->get();		
			return $valores;
			
			break;
		}

		

		break;

		case 'DIRECCION':

		switch($id_tipo_pefil->tipo){
			case 1:
			$valores=CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad",
			"coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado","ACT"],["coor_tmov_cronograma_cab.id_direccion",$valor_filtro]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
			->get();
			return $valores;
			break;

			case 2:
			$valores = CoorCronogramaCabModel::
            join("coor_tmov_cropnograma_resposables as cr","cr.id_crono_cab","=","coor_tmov_cronograma_cab.id")
            ->join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad",
			"coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado","ACT"],["cr.id_usuario",$id_usuario],["coor_tmov_cronograma_cab.id_direccion",$valor_filtro]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
            ->get();
			break;

			case 3:
			$valores = CoorCronogramaCabModel::
            join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->join("users as u","u.id_direccion","=","a.id")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad",
			"coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado","ACT"],["u.id",$id_usuario],["coor_tmov_cronograma_cab.id_direccion",$valor_filtro]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
            ->get();
			break;

			case 4:	
			$valores=CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad","coor_tmov_cronograma_cab.fecha_inicio",
			"coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado","ACT"],["coor_tmov_cronograma_cab.id_direccion",$valor_filtro]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
			->get();
			return $valores;

			break;

		}

		break;

		case 'ESTADO':
		switch($id_tipo_pefil->tipo){
			case 1:
			$valores=CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad",
			"coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado", "ACT"],["coor_tmov_cronograma_cab.estado_actividad",$valor_filtro]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
			->get();

			return $valores;

			break;
			case 2:
			$valores = CoorCronogramaCabModel::
            join("coor_tmov_cropnograma_resposables as cr","cr.id_crono_cab","=","coor_tmov_cronograma_cab.id")
            ->join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad",
			"coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado","ACT"],["cr.id_usuario",$id_usuario],["coor_tmov_cronograma_cab.estado_actividad",$valor_filtro]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
			->get();
			return $valores;
			break;
			case 3:
			$valores = CoorCronogramaCabModel::
            join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->join("users as u","u.id_direccion","=","a.id")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad","coor_tmov_cronograma_cab.fecha_inicio",
			"coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado","ACT"],["u.id",$id_usuario],["coor_tmov_cronograma_cab.estado_actividad",$valor_filtro]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
            ->get();
	return $valores;
			break;
			case 4:
			$valores=CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad",
			"coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado", "ACT"],["coor_tmov_cronograma_cab.estado_actividad",$valor_filtro]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
			->get();
			return $valores;
			break;
	
		}
		case 'PRIORIDAD':
		switch($id_tipo_pefil->tipo){
			case 1:
			$valores=CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad",
			"coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado", "ACT"],["coor_tmov_cronograma_cab.prioridad",$valor_filtro]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
			->get();

			return $valores;

			break;
			case 2:
			$valores = CoorCronogramaCabModel::
            join("coor_tmov_cropnograma_resposables as cr","cr.id_crono_cab","=","coor_tmov_cronograma_cab.id")
            ->join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad",
			"coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado","ACT"],["cr.id_usuario",$id_usuario],["coor_tmov_cronograma_cab.prioridad",$valor_filtro]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
			->get();
			return $valores;
			break;
			case 3:
			$valores = CoorCronogramaCabModel::
            join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->join("users as u","u.id_direccion","=","a.id")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad","coor_tmov_cronograma_cab.fecha_inicio",
			"coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado","ACT"],["u.id",$id_usuario],["coor_tmov_cronograma_cab.prioridad",$valor_filtro]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
            ->get();
	
			break;
			case 4:
			$valores=CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
			->select("coor_tmov_cronograma_cab.id","coor_tmov_cronograma_cab.estado_actividad","coor_tmov_cronograma_cab.actividad",
			"coor_tmov_cronograma_cab.fecha_inicio","coor_tmov_cronograma_cab.fecha_fin","a.direccion","coor_tmov_cronograma_cab.avance","coor_tmov_cronograma_cab.prioridad","coor_tmov_cronograma_cab.updated_at")
			->where([["coor_tmov_cronograma_cab.estado", "ACT"],["coor_tmov_cronograma_cab.prioridad",$valor_filtro]])
			->whereBetween(DB::raw("date_format(coor_tmov_cronograma_cab.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->orderBy("coor_tmov_cronograma_cab.updated_at","desc")
			->get();
			return $valores;
			break;
	
		}
		
		
	}
	return response()->json($valores);
}

	
}