<?php namespace Modules\Coordinacioncronograma\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use App\ModulosModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Route;
use Auth;
use App\UsuariosModel;
use Carbon\Carbon;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use Modules\Coordinacioncronograma\Entities\CoorTipoObraModel;
use Modules\Coordinacioncronograma\Entities\CoorContratistaModel;

use DB;

class CoordinacionListObrasController extends Controller {
	var $configuraciongeneral = array ("Coordinación Obras - Búsqueda", "coordinacioncronograma/coordinacionlistaobras", "index");
    var $escoja=array(0=>"TODOS");
	var $objetos = '[{"Tipo":"select","Descripcion":"Tipo de Filtro","Nombre":"tipofiltro","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]'; 	
		
	var $objetosTable = '[ 
    	{"Tipo":"text","Descripcion":"Parroquia","Nombre":"parroquia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Tipo Obra","Nombre":"tipo_obra","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Nombre Obra","Nombre":"nombre_obra","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"text","Descripcion":"Calle","Nombre":"calle","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Número de Proceso / Contrato","Nombre":"numero_proceso","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"text","Descripcion":"Monto","Nombre":"monto","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"text","Descripcion":"Estado Obra","Nombre":"estado_obra","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"text","Descripcion":"Contratista","Nombre":"contratista","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"text","Descripcion":"Avance %","Nombre":"avance","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"text","Descripcion":"Fecha Inicial","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Fecha Final","Nombre":"fecha_final","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Ultima Actualización","Nombre":"update_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                  ]';	
	public function __construct() {
				$this->middleware('auth');
	} 



	/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
	*/
	public function index()
	{

		$objetos = json_decode($this->objetos);
		//show($this->objetos);
		$objetos[0]->Nombre="tipofiltro";

		$tipoFiltro=array(1=>"PARROQUIA",2=>"TIPO DE OBRA", 3=>"ESTADO",4=>"CONTRATISTA");

		$objetos[0]->Valor=$this->escoja + $tipoFiltro;

		$objetosTable = json_decode($this->objetosTable);
		$objetos=array_values($objetos);
		//show($objetos);
		//return view('comunicacionalcaldia::index');
		$delete='si';

		$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
		if($id_tipo_pefil->tipo==2||$id_tipo_pefil->tipo==3){
			$delete=null;
		}

        return view('vistas.indexlist',[
			"objetos"=>$objetos,
			
			"objetosTable"=>$objetosTable,
			"configuraciongeneral"=>$this->configuraciongeneral,
			"delete"=>$delete,
            "create"=>"si"
            ]);


	}
	public function getValoresFiltroObras(){
		$tipo_filtro=Input::get("tipo_filtro");
		$html = '';
		switch($tipo_filtro){
			
			case 'PARROQUIA':
			$valores=ParroquiaModel::where("estado", "ACT")->select("parroquia","id","zona")->get();
			$html .= '<label class="col-lg-1 control-label">Parroquia</label> <div class="col-lg-4">';
			$html .= '<select class="form-control chosen-select" id="valorfiltro">';
			foreach ($valores as $item) {
				$html .= '<option value="'.$item->id.'">'.$item->parroquia.'</option>';
			}
			$html .= '</select></div>';
			return $html;
			break;

			case 'TIPO DE OBRA':
			$valores=CoorTipoObraModel::where("estado", "ACT")->select("tipo","id")->get();
			$html .= '<label class="col-lg-1 control-label">Parroquia</label> <div class="col-lg-4">';
			$html .= '<select class="form-control chosen-select" id="valorfiltro">';
			foreach ($valores as $item) {
				$html .= '<option value="'.$item->id.'">'.$item->tipo.'</option>';
			}
			$html .= '</select></div>';
			return $html;
			break;

			case 'ESTADO':
			$valores=explodewords(ConfigSystem("estadoobras"),"|");
			$html .= '<label class="col-lg-1 control-label">Estado</label> <div class="col-lg-4">';
			$html .= '<select class="form-control chosen-select" id="valorfiltro">';
			foreach ($valores as $item) {
				$html .= '<option value="'.$item.'">'.$item.'</option>';
			}
			$html .= '</select></div>';
			return $html;

			case 'CONTRATISTA':
			$valores=CoorContratistaModel::where("estado", "ACT")->select("nombres","id")->get();
			$html .= '<label class="col-lg-1 control-label">Estado</label> <div class="col-lg-4">';
			$html .= '<select class="form-control chosen-select" id="valorfiltro">';
			foreach ($valores as $item) {
				$html .= '<option value="'.$item->id.'">'.$item->nombres.'</option>';
			}
			$html .= '</select></div>';
			return $html;
			break;

			
		}
		return response()->json(['html' => $html]);
		
  }

  public function getComunicacionesFiltroObras(){
	$tipo_filtro=Input::get("tipo_filtro");
	$valor_filtro=Input::get("valor_filtro");
	
	$desde=Carbon::parse(Input::get("desde"));
	$hasta=Carbon::parse(Input::get("hasta"));
	$valores=[];
	
	switch($tipo_filtro){
		case 'TODOS':
		$valores=CoorObrasModel::join("coor_tmae_contratista as a","a.id","=","coor_tmov_obras_listado.id_contratista")
        ->join("coor_tmae_tipo_obra as b","b.id","=","coor_tmov_obras_listado.id_tipo_obra")
        ->join("parroquia as c","c.id","=","coor_tmov_obras_listado.id_parroquia")
		->select("coor_tmov_obras_listado.*","a.nombres as contratista","b.tipo as tipoobra","c.parroquia","coor_tmov_obras_listado.updated_at")
		->where("coor_tmov_obras_listado.estado","ACT")->orderby("updated_at","desc")
		->whereBetween(DB::raw("date_format(coor_tmov_obras_listado.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])

		->get();
		
		return $valores;

		break;

		case 'PARROQUIA':

		$valores=CoorObrasModel::join("coor_tmae_contratista as a","a.id","=","coor_tmov_obras_listado.id_contratista")
        ->join("coor_tmae_tipo_obra as b","b.id","=","coor_tmov_obras_listado.id_tipo_obra")
        ->join("parroquia as c","c.id","=","coor_tmov_obras_listado.id_parroquia")
		->select("coor_tmov_obras_listado.*","a.nombres as contratista","b.tipo as tipoobra","c.parroquia","coor_tmov_obras_listado.updated_at")
		->where([["coor_tmov_obras_listado.estado","ACT"],["coor_tmov_obras_listado.id_parroquia",$valor_filtro]])
		->whereBetween(DB::raw("date_format(coor_tmov_obras_listado.updated_at, '%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
		->orderby("updated_at","desc")
		->get();

		return $valores;
		break;

		case 'TIPO DE OBRA':
		
		$valores=CoorObrasModel::join("coor_tmae_contratista as a","a.id","=","coor_tmov_obras_listado.id_contratista")
        ->join("coor_tmae_tipo_obra as b","b.id","=","coor_tmov_obras_listado.id_tipo_obra")
        ->join("parroquia as c","c.id","=","coor_tmov_obras_listado.id_parroquia")
		->select("coor_tmov_obras_listado.*","a.nombres as contratista","b.tipo as tipoobra","c.parroquia")
		->where([["coor_tmov_obras_listado.estado","ACT"],["coor_tmov_obras_listado.id_tipo_obra",$valor_filtro]])
		->whereBetween(DB::raw("date_format(coor_tmov_obras_listado.updated_at, '%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
		->orderby("updated_at","desc")
		->get();
		return $valores;

		case 'ESTADO':
		
		$valores=CoorObrasModel::join("coor_tmae_contratista as a","a.id","=","coor_tmov_obras_listado.id_contratista")
        ->join("coor_tmae_tipo_obra as b","b.id","=","coor_tmov_obras_listado.id_tipo_obra")
        ->join("parroquia as c","c.id","=","coor_tmov_obras_listado.id_parroquia")
		->select("coor_tmov_obras_listado.*","a.nombres as contratista","b.tipo as tipoobra","c.parroquia")
		->where([["coor_tmov_obras_listado.estado","ACT"],["coor_tmov_obras_listado.estado_obra",$valor_filtro]])
		->whereBetween(DB::raw("date_format(coor_tmov_obras_listado.updated_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
		->orderby("updated_at","desc")->get();
		return $valores;

		case 'CONTRATISTA':
		
		$valores=CoorObrasModel::join("coor_tmae_contratista as a","a.id","=","coor_tmov_obras_listado.id_contratista")
        ->join("coor_tmae_tipo_obra as b","b.id","=","coor_tmov_obras_listado.id_tipo_obra")
        ->join("parroquia as c","c.id","=","coor_tmov_obras_listado.id_parroquia")
		->select("coor_tmov_obras_listado.*","a.nombres as contratista","b.tipo as tipoobra","c.parroquia")
		->where([["coor_tmov_obras_listado.estado","ACT"],["coor_tmov_obras_listado.id_contratista",$valor_filtro]])
		->whereBetween(DB::raw("date_format(coor_tmov_obras_listado.updated_at, '%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
		->orderby("updated_at","desc")->get();
		return $valores;
	}
	return response()->json($valores);
}
	
}