<?php namespace Modules\Coordinacioncronograma\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;    
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Modules\Coordinacioncronograma\Entities\CoorTipoProyectoModel;
use Modules\Coordinacioncronograma\Entities\CoorTipoProyectoDetalleModel;
use Modules\Coordinacioncronograma\Entities\CoorProyectoEstrategicoModel;
use App\ArchivosModel;
use App\UsuariosModel;
use Auth; 

class CoorProyectoEstrategicoController extends Controller {
	
	var $configuraciongeneral = array ("Proyectos", "coordinacioncronograma/proyecto", "index",6=>"null");
	var $escoja=array(null=>"Escoja opción...") ;
	// var $avance= array(0 => "0%" , 10 => "10%", 20 => "20%", 30 => "30%", 40 => "40%", 50 => "50%", 60 => "60%", 70 => "70%", 80 => "80%", 90 => "90%", 100 => "100%");
    var $objetos = '[
        {"Tipo":"select","Descripcion":"Tipo Componente","Nombre":"id_tipo_proyecto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Nombre","Nombre":"nombre","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Monto","Nombre":"monto","Clase":"moneda","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Estado del proyecto","Nombre":"estado_proyecto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Linea Base","Nombre":"linea_base","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Meta","Nombre":"meta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Descripción del proyecto","Nombre":"descripcion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" } ,
        {"Tipo":"textarea","Descripcion":"Observación","Nombre":"observacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';


	//https://jqueryvalidation.org/validate/
    var $validarjs =array(
		"id_tipo_proyecto"=>"id_tipo_proyecto: {
						required: true
					}",
		"nombre"=>"nombre: {
						required: true
					}",
		"estado_proyecto"=>"estado_proyecto: {
						required: true
					}"
					
	);


public function __construct() {
	$this->middleware('auth');
}

	public function index()
	{
		// $tabla=CoorProyectoEstrategicoModel::where("estado","ACT")->orderby("id","desc")->get();//->paginate(500);
		$objetos=json_decode($this->objetos);
        $objetos[0]->Nombre="tipo";
		$objetos=array_values($objetos);
		//$this->calcularMeta();
        //show($objetos);
		$tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as tp","coor_tmov_proyecto_estrategico.id_tipo_proyecto","=","tp.id")
				->select("coor_tmov_proyecto_estrategico.*","tp.tipo")
				->where("coor_tmov_proyecto_estrategico.estado","ACT")
				->get();
		return view('vistas.index',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "delete"=>"si",
                "create"=>"si"
                ]);
	}


	public function create()
    {
		//
		$this->configuraciongeneral[2]="crear";
		$seleccionar_tipo=CoorTipoProyectoModel::where("estado", "ACT")->lists("tipo","id")->all();
		$objetos=json_decode($this->objetos);
		$objetos[0]->Valor=$this->escoja + $seleccionar_tipo;
		$avance=explodewords(ConfigSystem("estadoproyecto"),"|");
		$objetos[3]->Valor=$this->escoja + $avance;
		return view('vistas.create',[
				"objetos"=>$objetos,
				"configuraciongeneral"=>$this->configuraciongeneral,
				"validarjs"=>$this->validarjs
		]);
	}

	public function store(Request $request)
    {
        //
        return $this->guardar(0);
	}

	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $objetos=json_decode($this->objetos);
        $objetos[0]->Nombre="tipo";
		$objetos=array_values($objetos);
		$timelineProyecto=$this->getTimeLineCab($id);
		
        //show($objetos);
        $tabla = CoorProyectoEstrategicoModel::join("coor_tmae_tipo_proyecto as tp","coor_tmov_proyecto_estrategico.id_tipo_proyecto","=","tp.id")
					->select("coor_tmov_proyecto_estrategico.*","tp.tipo")
					->where("coor_tmov_proyecto_estrategico.estado","ACT")
					->first();
		return view('vistas.show',[
				"timelineProyecto"=>$timelineProyecto,
				"objetos"=>$objetos,
				"tabla"=>$tabla,
				"configuraciongeneral"=>$this->configuraciongeneral
				]);
	}

	public function guardar($id)
    {         
           $input=Input::all();
		   //$this->calcularMeta();
		   //show($this->calcularMeta());
			$ruta=$this->configuraciongeneral[1];
			DB::beginTransaction();
            try{
				if($id==0)
				{
					$ruta.="/create";
					$guardar= new CoorProyectoEstrategicoModel;
					$msg="Registro Creado Exitosamente...!";
					$msgauditoria="Registro Variable de Configuración";
				}
				else{
					$ruta.="/$id/edit";
					$guardar= CoorProyectoEstrategicoModel::find($id);
					$msg="Registro Actualizado Exitosamente...!";
					$msgauditoria="Edición Variable de Configuración";
				}

				$input=Input::all();
				$arrapas=array();
				
				$validator = Validator::make($input, CoorProyectoEstrategicoModel::rules($id));
				
				if ($validator->fails()) {
					//die($ruta);
					return Redirect::to("$ruta")
						->withErrors($validator)
						->withInput();
				}else {
					foreach($input as $key => $value)
					{
					
						if($key != "_method" && $key != "_token")
						{
							if($key=="monto")
								$value=floatval(str_replace(",", "",$value));
							$guardar->$key = $value;
						}                        
					}

					// 
					
					//show($guardar);
					// if(Input::get("estado_proyecto") != "SUSPENDIDO")
					// 	$guardar->estado_proyecto=$this->estado($guardar->fecha_inicio,$guardar->fecha_fin);
					
					
					$guardar->id_usuario = Auth::user()->id;
					$guardar->ip=\Request::getClientIp();
					$guardar->pc=\Request::getHost();
					
					// show($guardar->monto);
					$guardar->save();

					$idcab=$guardar->id;

					$timeLine = new CoorTipoProyectoDetalleModel;
					$timeLine->id_tipo_proyecto=$guardar->id_tipo_proyecto;
					$timeLine->nombre=$guardar->nombre;
					$timeLine->monto=$guardar->monto;
					// if(Input::get("estado_proyecto") != "SUSPENDIDO")
					// 	$timeLine->estado_proyecto=$this->estado($guardar->fecha_inicio,$guardar->fecha_fin);
					
					$timeLine->estado_proyecto=$guardar->estado_proyecto;
					$timeLine->linea_base=$guardar->linea_base;
					$timeLine->meta=$guardar->meta;
					
					// show($timeLine);
					// DB::rollback();
					$timeLine->descripcion=$guardar->descripcion;
					$timeLine->observacion=$guardar->observacion;
					$timeLine->estado=$guardar->estado;
					$timeLine->id_usuario=$guardar->id_usuario;

					$timeLine->ip=\Request::getClientIp();
					$timeLine->pc=\Request::getHost();
					$timeLine->id_cab_pro=$idcab;


					$timeLine->save();


					Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->nombre));   
				}
			DB::commit();
			}
            catch(\Exception $e){
				DB::rollback();
                $mensaje=$e->getMessage();
                return redirect()->back()->withErrors([$mensaje])->withInput();    
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }

	public function edit($id)
    {
        //
		$this->configuraciongeneral[2]="editar";	
        $tabla = CoorProyectoEstrategicoModel::find($id);
		$objetos=json_decode($this->objetos);
		$seleccionar_tipo=CoorTipoProyectoModel::where("estado", "ACT")->lists("tipo","id")->all();
		$objetos[0]->Valor=$this->escoja + $seleccionar_tipo;

		$avance=explodewords(ConfigSystem("estadoproyecto"), "|");
		
		$objetos[3]->Valor=$this->escoja + $avance;
		$this->configuraciongeneral[3]="3";
        $this->configuraciongeneral[4]="si";


		//$timelineProyecto=$this->getTimeLineCab($id);
		//show($timelineProyecto);
        $dicecionesImagenes=ArchivosModel::where([["id_referencia",$tabla->id],["tipo_archivo","<>","pdf"]])->get();
        $dicecionesDocumentos=ArchivosModel::where([["id_referencia",$tabla->id],["tipo_archivo","=","pdf"]])->get();
        return view('vistas.create',[
				// "timelineProyecto"=>$timelineProyecto,
        		"dicecionesImagenes"=>$dicecionesImagenes,
            "dicecionesDocumentos"=>$dicecionesDocumentos,
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "tabla"=>$tabla,
                "validarjs"=>$this->validarjs
                ]);
	}
	
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
	}
	
	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla=CoorProyectoEstrategicoModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
	}
	
	public function getTimeLineCab($id){

		$tabla = CoorTipoProyectoDetalleModel::
					select("coor_tmov_proyecto_estrategico_deta.*", "p.id")
					->join("coor_tmov_proyecto_estrategico as p","coor_tmov_proyecto_estrategico_deta.id_cab_pro","=","p.id")
					// ->where("coor_tmov_proyecto_estrategico_deta.estado","ACT")
					->where("id_cab_pro",$id)
					->orderBy("coor_tmov_proyecto_estrategico_deta.updated_at","ASC")
					->get();
		return $tabla;
	}


	// public function estado($fi,$ff){
	// 	$fecha_actual = date("Y-m-d");
	// 	$fecha_act=Carbon::parse($fecha_actual);
	// 	$fecha_f=Carbon::parse($ff);
	// 	$fecha_ini=Carbon::parse($fi);

	// 	$diferencia = $fecha_act->diffInDays($fecha_f);
	// 	$dfinicial = $fecha_act->diffInDays($fecha_ini);

	// 	if($diferencia == 0){
	// 		return $estado = "EJECUTADO";
	// 	}

	// 	if($fecha_act >= $fecha_ini){
	// 		return $estado = "EJECUCION";
	// 	}

	// 	if ($fecha_act <= $fecha_ini) {
	// 		return $estado = "POR_EJECUTAR";
	// 	}
	// }
	
}

