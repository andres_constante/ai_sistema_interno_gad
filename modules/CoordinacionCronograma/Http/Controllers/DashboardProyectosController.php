<?php namespace Modules\Coordinacioncronograma\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\Coordinacioncronograma\Entities\CoorProyectoEstrategicoModel;
use Modules\Coordinacioncronograma\Entities\CoorProyectoEstrategicoDetaModel ;
use DB;
class DashboardProyectosController extends Controller {
	var $configuraciongeneral = array ("DASHBOARD PROYECTOS", "coordinacioncronograma/dashboardproyectos", "index");
	
	public function index()
	{
		$fechainicial=date("Y-m-d")." 00:00:00";
		//$fechainicial="2019-06-01 00:00:00";
		$fechafinal=date("Y-m-d")." 23:59:59";


        //show($modificacionesHoy);
		$datos=CoorProyectoEstrategicoModel::select(DB::raw("count(id) as total,estado_proyecto,'0' as color,0 as porcentaje"))
		->groupBy("estado_proyecto")
		->where("estado","ACT")
		->get();
		$total=CoorProyectoEstrategicoModel::select(DB::raw("count(id) as total"))
		->where("estado","ACT")
		->first();		
		$iboxProyectos=$this->getProyectos($datos,$total);

	
		$proyectosActualizados=$this->getTimeLineCab(0,1);


		return view('vistas.dashboard.dashboardproyectos',[
			"proyectosActualizados"=>$proyectosActualizados,
			"iboxProyectos"=>$iboxProyectos,
			"configuraciongeneral"=>$this->configuraciongeneral,
			"total"=>$total
			]);
	}


	public function getTimeLineCab($id,$tipo){
        $sw=0;
        switch($tipo){
            case 1:
            $tabla=CoorProyectoEstrategicoDetaModel::
            select("coor_tmov_proyecto_estrategico_deta.*","u.name")
            ->join("users as u","u.id","=","coor_tmov_proyecto_estrategico_deta.id_usuario")
			//->where('id_crono_cab',$id)
			->offset(1)
            ->limit(10)
			->orderBy("coor_tmov_proyecto_estrategico_deta.updated_at","DESC")
            ->get();
            return $tabla;
            break;
        }
	}
	
	public function getProyectos($datos,$total){

		$estadoproyecto=explode("|",ConfigSystem("estadoproyecto"));
		$estadoproyectocolores=explode("|",ConfigSystem("estadoproyectocolor"));
		foreach($datos as $item){
			switch($item->estado_proyecto){

				case $estadoproyecto[0]:
					$item->estado_proyecto='TERMINOS DE REFERENCIA';
					$item->color=$estadoproyectocolores[0];
					$item->porcentaje=round((($item->total/$total->total)*100),2);

				break;
				case $estadoproyecto[1]:
					$item->estado_proyecto='ANTEPROYECTO';
					$item->color=$estadoproyectocolores[1];
					$item->porcentaje=round((($item->total/$total->total)*100),2);
				break;
				case $estadoproyecto[2]:
					$item->estado_proyecto='ESTUDIO DEFINITIVO';
					$item->color=$estadoproyectocolores[2];
					$item->porcentaje=round((($item->total/$total->total)*100),2);

				break;

			}


		}
		return $datos;
		
	}



	// public function getChartLine(){
	// 	$id_direccion= Input::get('id');
	// 	$tipo= Input::get('tipo');
	// 	//show($tipo);
	// 	$datos=null;
	// 	if($tipo==1){
	// 		$direcciones=direccionesModel::select("id","direccion")->where("estado", "ACT")->get(); 
	// 		foreach($direcciones as $direccion){
	// 			$EJECUCION=cabComunicacionModel::select(DB::raw("count(id) as total"))
	// 			->where([["estado","ACT"],["estado_proyecto","EJECUCION"],["id_direccion",$direccion->id]])
	// 			->first();
				
	// 			$EJECUTADO=cabComunicacionModel::select(DB::raw("count(id) as total"))
	// 			->where([["estado","ACT"],["estado_proyecto","EJECUTADO"],["id_direccion",$direccion->id]])
	// 			->first();
	// 			$EJECUTADO_VENCIDO=cabComunicacionModel::select(DB::raw("count(id) as total"))
	// 			->where([["estado","ACT"],["estado_proyecto","EJECUTADO_VENCIDO"],["id_direccion",$direccion->id]])
	// 			->first();
	// 			$POR_EJECUTAR=cabComunicacionModel::select(DB::raw("count(id) as total"))
	// 			->where([["estado","ACT"],["estado_proyecto","POR_EJECUTAR"],["id_direccion",$direccion->id]])
	// 			->first();
	// 			$SUSPENDIDO=cabComunicacionModel::select(DB::raw("count(id) as total"))
	// 			->where([["estado","ACT"],["estado_proyecto","SUSPENDIDO"],["id_direccion",$direccion->id]])
	// 			->first();
				
	// 			$datos[]=[$direccion->direccion,$EJECUCION->total,$EJECUTADO->total,$EJECUTADO_VENCIDO->total,$POR_EJECUTAR->total,$SUSPENDIDO->total];
				
	// 		}
			
	// 	}else if($tipo==2){
	// 		$direcciones=direccionesModel::select("id","direccion")->where([["estado", "ACT"],["id",$id_direccion]])->get(); 
	// 		$EJECUCION=cabComunicacionModel::select(DB::raw("count(id) as total"))
	// 			->where([["estado","ACT"],["estado_proyecto","EJECUCION"],["id_direccion",$id_direccion]])
	// 			->first();

	// 			$EJECUTADO=cabComunicacionModel::select(DB::raw("count(id) as total"))
	// 			->where([["estado","ACT"],["estado_proyecto","EJECUTADO"],["id_direccion",$id_direccion]])
	// 			->first();
	// 			$EJECUTADO_VENCIDO=cabComunicacionModel::select(DB::raw("count(id) as total"))
	// 			->where([["estado","ACT"],["estado_proyecto","EJECUTADO_VENCIDO"],["id_direccion",$id_direccion]])
	// 			->first();
	// 			$POR_EJECUTAR=cabComunicacionModel::select(DB::raw("count(id) as total"))
	// 			->where([["estado","ACT"],["estado_proyecto","POR_EJECUTAR"],["id_direccion",$id_direccion]])
	// 			->first();
	// 			$SUSPENDIDO=cabComunicacionModel::select(DB::raw("count(id) as total"))
	// 			->where([["estado","ACT"],["estado_proyecto","SUSPENDIDO"],["id_direccion",$id_direccion]])
	// 			->first();
				
	// 			$datos[]=[$direcciones[0]->direccion,$EJECUCION->total,$EJECUTADO->total,$EJECUTADO_VENCIDO->total,$POR_EJECUTAR->total,$SUSPENDIDO->total];
	// 	}
	// 	else if($tipo==3){



	// 		$avances=MovComunicacionParroquiaModel::
	// 		join("com_tmov_comunicacion_cab as com_tmov_comunicacion_cab", "com_tmov_comunicacion_parroquia.id_com_cab", "=", "com_tmov_comunicacion_cab.id")
    //         ->select("com_tmov_comunicacion_cab.id_direccion")
    //         ->groupBy("com_tmov_comunicacion_cab.id_direccion")
	// 		->get();
			


	// 		foreach($avances as $item){
	// 			//show($item->id);
	// 			$valor=MovComunicacionParroquiaModel::select(DB::raw("dir.direccion, avg(com_tmov_comunicacion_parroquia.avance) as porcentaje"))
	// 			->join("com_tmov_comunicacion_cab as com_tmov_comunicacion_cab", "com_tmov_comunicacion_parroquia.id_com_cab", "=", "com_tmov_comunicacion_cab.id")
	// 			->join("tmae_direcciones as dir","dir.id","=","com_tmov_comunicacion_cab.id_direccion")
	// 			->where([["com_tmov_comunicacion_cab.estado","ACT"],["dir.id",$item->id]])
	// 			->first();
	// 			if($valor->porcentaje==0){
	// 				$datos[]=[$valor->direccion,null];
	// 			}else{
	// 				$datos[]=[$valor->direccion,round($valor->porcentaje,2)];
	// 			}				
				
				
	// 		}
			

	// 	}
	// 	else if($tipo==4){
	// 		$fechainicial=date("Y-m-d")." 00:00:00";
	// 	//$fechainicial="2019-06-01 00:00:00";
	// 		$fechafinal=date("Y-m-d")." 23:59:59";
	// 		$modificacionesHoy=CoorProyectoEstrategicoDetaController::
	// 			join("users as u","u.id","=","coor_tmov_proyecto_estrategico_deta.idusuario")
	// 			->join("ad_perfil as p","u.id_perfil","=","p.id")
	// 			->select("dir.direccion",DB::raw("count(*) as total"))
	// 			->whereBetween("com_tmov_comunicacion_cab.updated_at",[$fechainicial,$fechafinal])
	// 			->where("p.tipo","<>",3)
	// 			->where("p.tipo","<>",1)
	// 			->groupBy("dir.direccion")
	// 			->get();
			
			
			
	// 		foreach($modificacionesHoy as $item){

	// 			$datos[]=[$item["direccion"],$item["total"]];
	// 		}

		
	// 	}
	
	// 	return response()->json($datos);
		
	// }
	
}