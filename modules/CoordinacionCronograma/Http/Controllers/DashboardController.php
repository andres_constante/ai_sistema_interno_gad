<?php namespace Modules\Coordinacioncronograma\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaDetaModel;

use DB;
use App\UsuariosModel;
use Route;
use Auth;
use Illuminate\Support\Facades\Input;
class DashboardController extends Controller {
	var $configuraciongeneral = array ("DASHBOARD", "coordinacioncronograma/dashboard", "index");
	// var $ibox  = '[ 
	// 	{"Tipo":"ibox","Descripcion":"EJECUCION","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
	// 	{"Tipo":"ibox","Descripcion":"EJECUTADO","Nombre":"ejecutado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    // 	{"Tipo":"ibox","Descripcion":"EJECUTADO_VENCIDO","Nombre":"ejecutado_vencido","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    // 	{"Tipo":"ibox","Descripcion":"POR_EJECUTAR","Nombre":"telefono","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    // 	{"Tipo":"ibox","Descripcion":"SUSPENDIDO","Nombre":"correo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
	// 			  ]';
				  
	public function index()
	{
		$fechainicial=date("Y-m-d")." 00:00:00";
		//$fechainicial="2019-06-01 00:00:00";
		$fechafinal=date("Y-m-d")." 23:59:59";

		
		$modificacionesHoy=CoorCronogramaCabModel::
			join("tmae_direcciones as dir","dir.id","=","coor_tmov_cronograma_cab.id_direccion")
			->join("users as u","u.id","=","coor_tmov_cronograma_cab.id_usuario")
			->join("ad_perfil as p","u.id_perfil","=","p.id")
			->select("dir.direccion",DB::raw("count(*) as total"))
			->whereBetween("coor_tmov_cronograma_cab.updated_at",[$fechainicial,$fechafinal])
			->where("p.tipo","<>",3)
			->where("p.tipo","<>",1)
			->groupBy("dir.direccion")
			->get();
		

        //show($modificacionesHoy);
		$datos=CoorCronogramaCabModel::select(DB::raw("count(id) as total,estado_actividad,'0' as color,0 as porcentaje"))
		->groupBy("estado_actividad")
		->where("estado","ACT")
		->get();
		$total=CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
		->where("estado","ACT")
		->first();		
		$iboxActividades=$this->getActividades($datos,$total);
		$totalacti=$total;
		unset($datos);
		unset($total);
		

		$datos=CoorObrasModel::select(DB::raw("count(id) as total,estado_obra"))
		->groupBy("estado_obra")
		->get();
		$total=CoorObrasModel::select(DB::raw("count(id) as total"))
		->groupBy("estado_obra")
		->first();		
		$iboxObras=$this->getObras($datos,$total);

		//show($iboxObras);
		//
		
        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
        $id_usuario=Auth::user()->id;
        if($id_tipo_pefil->tipo ==2 ||$id_tipo_pefil->tipo==3 ){
            $direcciones=direccionesModel::
            join("users as us","us.id_direccion","=","tmae_direcciones.id")
			->select('tmae_direcciones.id','direccion')->where([['tmae_direcciones.estado','=','ACT'],["us.id",$id_usuario]])->orderby("direccion","asc")->get();
			$direcciones=CoorCronogramaCabModel::select(DB::raw("dir.id,direccion"))
			->join("tmae_direcciones as dir","dir.id","=","coor_tmov_cronograma_cab.id_direccion")
			->join("users as us","us.id_direccion","=","tmae_direcciones.id")
			->where([['tmae_direcciones.estado','=','ACT'],["us.id",$id_usuario]])
			->groupBy("coor_tmov_cronograma_cab.id_direccion")
			->orderby("direccion","asc")
			->get();
           
        }else{

            $direcciones=CoorCronogramaCabModel::select(DB::raw("dir.id,direccion"))
			->join("tmae_direcciones as dir","dir.id","=","coor_tmov_cronograma_cab.id_direccion")
			->where([["coor_tmov_cronograma_cab.estado","ACT"]])
			->groupBy("coor_tmov_cronograma_cab.id_direccion")
			->orderby("direccion","asc")
			->get();
            //show($direcciones);
		}
		
		$actividadesActualizadas=$this->getTimeLineCab(0,1);

		//show($actividadesActualizadas);

		return view('vistas.dashboard.dashboard',[
			"modificacionesHoy"=>$modificacionesHoy,
			"actividadesActualizadas"=>$actividadesActualizadas,
			"direcciones"=>$direcciones,
			"iboxActividades"=>$iboxActividades,
			"iboxObras"=>$iboxObras,
			"configuraciongeneral"=>$this->configuraciongeneral,
			"totalacti"=>$totalacti
			]);
	}



public function getTimeLineCab($id,$tipo){
        $sw=0;
        switch($tipo){
            case 1:
            $tabla=CoorCronogramaDetaModel::
            select("coor_tmov_cronograma_deta.*","u.name","dir.direccion")
            ->join("users as u","u.id","=","coor_tmov_cronograma_deta.id_usuario")
            ->join("tmae_direcciones as dir","dir.id","=","coor_tmov_cronograma_deta.id_direccion")
			//->where('id_crono_cab',$id)
			->offset(1)
            ->limit(5)
			->orderBy("coor_tmov_cronograma_deta.updated_at","DESC")
            ->get();
            return $tabla;
            break;
        }
    }


	public function getActividades($datos,$total){

		$estadoobras=explode("|",ConfigSystem("estadoobras"));
		$estadoobrascolores=explode("|",ConfigSystem("estadoobrascolor"));
		
	
		foreach($datos as $item){
			switch($item->estado_actividad){

				case $estadoobras[0]:
					$item->estado_actividad='EJECUTADO VENCIDO';
					$item->color=$estadoobrascolores[0];
					$item->porcentaje=round((($item->total/$total->total)*100),2);

				break;
				case $estadoobras[1]:
					$item->estado_actividad='EJECUTADO';
					$item->color=$estadoobrascolores[1];
					$item->porcentaje=round((($item->total/$total->total)*100),2);
				break;
				case $estadoobras[2]:
					$item->estado_actividad='EJECUCIÓN';
					$item->color=$estadoobrascolores[2];
					$item->porcentaje=round((($item->total/$total->total)*100),2);

				break;
				case $estadoobras[3]:
					$item->estado_actividad='POR EJECUTAR';
					$item->color=$estadoobrascolores[3];
					$item->porcentaje=round((($item->total/$total->total)*100),2);

				break;
				case $estadoobras[4]:
					$item->color=$estadoobrascolores[4];
					$item->porcentaje=round((($item->total/$total->total)*100),2);
				break;

			}


		}
		return $datos;
		
	}
	public function getObras($datos,$total){

		$estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
		$estadoobrascolores=explode("|",ConfigSystem("estadoobrascolor"));
		
	
		foreach($datos as $item){
			switch($item->estado_obra){
				case 'EJECUTADO_VENCIDO':
					$item->estado_obra='EJECUTADO VENCIDO';
					$item->color=$estadoobrascolores[0];
					$item->porcentaje=round((($item->total/$total->total)*100),2);

				break;
				case 'EJECUTADO':
					$item->estado_obra='EJECUTADO';
					$item->color=$estadoobrascolores[1];
					$item->porcentaje=round((($item->total/$total->total)*100),2);
				break;
				case 'EJECUCION':
					$item->estado_obra='EJECUCIÓN';
					$item->color=$estadoobrascolores[2];
					$item->porcentaje=round((($item->total/$total->total)*100),2);

				break;
				case 'POR_EJECUTAR':
					$item->estado_obra='POR EJECUTAR';
					$item->color=$estadoobrascolores[3];
					$item->porcentaje=round((($item->total/$total->total)*100),2);

				break;
				case 'SUSPENDIDO':
					$item->color=$estadoobrascolores[4];
					$item->porcentaje=round((($item->total/$total->total)*100),2);
				break;

			}


		}
		return $datos;
		
	}
	

	public function getChartLine(){
		$id_direccion= Input::get('id');
		$tipo= Input::get('tipo');
		//show($tipo);
		$datos=null;
		if($tipo==1){
			$direcciones=direccionesModel::select("id","direccion")->where("estado", "ACT")->get(); 
			foreach($direcciones as $direccion){
				$EJECUCION=CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","EJECUCION"],["id_direccion",$direccion->id]])
				->first();
				
				$EJECUTADO=CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","EJECUTADO"],["id_direccion",$direccion->id]])
				->first();
				$EJECUTADO_VENCIDO=CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","EJECUTADO_VENCIDO"],["id_direccion",$direccion->id]])
				->first();
				$POR_EJECUTAR=CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","POR_EJECUTAR"],["id_direccion",$direccion->id]])
				->first();
				$SUSPENDIDO=CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","SUSPENDIDO"],["id_direccion",$direccion->id]])
				->first();
				
				$datos[]=[$direccion->direccion,$EJECUCION->total,$EJECUTADO->total,$EJECUTADO_VENCIDO->total,$POR_EJECUTAR->total,$SUSPENDIDO->total];
				
			}
			
		}else if($tipo==2){
			$direcciones=direccionesModel::select("id","direccion")->where([["estado", "ACT"],["id",$id_direccion]])->get(); 
			$EJECUCION=CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","EJECUCION"],["id_direccion",$id_direccion]])
				->first();

				$EJECUTADO=CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","EJECUTADO"],["id_direccion",$id_direccion]])
				->first();
				$EJECUTADO_VENCIDO=CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","EJECUTADO_VENCIDO"],["id_direccion",$id_direccion]])
				->first();
				$POR_EJECUTAR=CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","POR_EJECUTAR"],["id_direccion",$id_direccion]])
				->first();
				$SUSPENDIDO=CoorCronogramaCabModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","SUSPENDIDO"],["id_direccion",$id_direccion]])
				->first();
				
				$datos[]=[$direcciones[0]->direccion,$EJECUCION->total,$EJECUTADO->total,$EJECUTADO_VENCIDO->total,$POR_EJECUTAR->total,$SUSPENDIDO->total];

		}else if($tipo==3){
			
			$avances=CoorCronogramaCabModel::select(DB::raw("dir.id"))
				->join("tmae_direcciones as dir","dir.id","=","coor_tmov_cronograma_cab.id_direccion")
				->where([["coor_tmov_cronograma_cab.estado","ACT"],["estado_actividad","EJECUCION"]])
				->groupBy("coor_tmov_cronograma_cab.id_direccion")
				->get();
			
			foreach($avances as $item){
				//show($item->id);
				$valor=CoorCronogramaCabModel::select(DB::raw("dir.direccion, avg(avance) as porcentaje"))
				->join("tmae_direcciones as dir","dir.id","=","coor_tmov_cronograma_cab.id_direccion")
				->where([["coor_tmov_cronograma_cab.estado","ACT"],["dir.id",$item->id]])
				->first();
				if($valor->porcentaje==0){
					$datos[]=[$valor->direccion,null];
				}else{
					$datos[]=[$valor->direccion,round($valor->porcentaje,2)];
				}				
				
				
			}
			

		}
		else if($tipo==4){
			$fechainicial=date("Y-m-d")." 00:00:00";
		//$fechainicial="2019-06-01 00:00:00";
		$fechafinal=date("Y-m-d")." 23:59:59";
			$modificacionesHoy=CoorCronogramaCabModel::
				join("tmae_direcciones as dir","dir.id","=","coor_tmov_cronograma_cab.id_direccion")
				->join("users as u","u.id","=","coor_tmov_cronograma_cab.id_usuario")
				->join("ad_perfil as p","u.id_perfil","=","p.id")
				->select("dir.direccion",DB::raw("count(*) as total"))
				->whereBetween("coor_tmov_cronograma_cab.updated_at",[$fechainicial,$fechafinal])
				->where("p.tipo","<>",3)
				->where("p.tipo","<>",1)
				->groupBy("dir.direccion")
				->get();
			
			
			
			foreach($modificacionesHoy as $item){

				$datos[]=[$item["direccion"],$item["total"]];
			}

		
		}
	
		return response()->json($datos);
		
	}
}