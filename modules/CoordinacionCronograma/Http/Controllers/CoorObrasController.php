<?php namespace Modules\Coordinacioncronograma\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\ArchivosModel;
use Modules\CoordinacionCronograma\Entities\CoorObrasDetaModel;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use Modules\Coordinacioncronograma\Entities\CoorContratistaModel;
use Modules\Coordinacioncronograma\Entities\CoorTipoObraModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;

use App\UsuariosModel;
class CoorObrasController extends Controller {
	var $configuraciongeneral = array ("Listado de Obras", "coordinacioncronograma/listobras", "index",6=>"null");
	var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[ 
    	{"Tipo":"select","Descripcion":"Parroquia","Nombre":"id_parroquia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Tipo Obra","Nombre":"id_tipo_obra","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Nombre Obra","Nombre":"nombre_obra","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"text","Descripcion":"Calle","Nombre":"calle","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Número de Proceso / Contrato","Nombre":"numero_proceso","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"text","Descripcion":"Monto","Nombre":"monto","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"select","Descripcion":"Estado Obra","Nombre":"estado_obra","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"select","Descripcion":"Contratista","Nombre":"id_contratista","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"select","Descripcion":"Avance %","Nombre":"avance","Clase":"porcentaje","Valor":"Null","ValorAnterior" :"Null" },
    	{"Tipo":"datetext","Descripcion":"Fecha Inicial","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha Final","Nombre":"fecha_final","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Ultima Modificación","Nombre":"updated_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
                  ]';
//https://jqueryvalidation.org/validate/
    var $validarjs =array(
        "id_parroquia"=>"id_parroquia: {
                            required: true
                        }",
            "id_tipo_obra"=>"id_tipo_obra: {
                            required: true
                        }",
            "nombre_obra"=>"nombre_obra: {
                            required: true
                        }",
            "calle"=>"calle: {
                            required: true
                        }",
            "monto"=>"monto: {
                            required: true
                        }",
            // "estado_obra"=>"estado_obra: {
            //                 required: true
            //             }",
            "id_contratista"=>"id_contratista: {
                            required: true
                        }",
            "avance"=>"avance: {
                            required: true
                        }",
            "fecha_inicio"=>"fecha_inicio: {
                            required: true
                        }",
            "fecha_final"=>"fecha_final: {
                            required: true
                        }"
        );
    public function __construct() {
        $this->middleware('auth');
    }
    public function traspasoobras()
    {        
        $tabla=CoorObrasModel::all();
        $pro=0;
        $nopro=0;
        foreach ($tabla as $key => $value) {
            # code...
            $ver=coorObrasDetaModel::where("id_cab_obra",$value->id)->first();
            if(!$ver)
            {
                $timeline=new coorObrasDetaModel;
                $timeline->id_cab_obra=$value->id;
                $timeline->id_tipo_obra=$value->id_tipo_obra;
                $timeline->calle=$value->calle;
                $timeline->nombre_obra=$value->nombre_obra;
                $timeline->numero_proceso=$value->numero_proceso;
                $timeline->monto=$value->monto;
                $timeline->estado_obra=$value->estado_obra;
                $timeline->id_contratista=$value->id_contratista;
                $timeline->avance=$value->avance;
                $timeline->fecha_inicio=$value->fecha_inicio;
                $timeline->fecha_final=$value->fecha_final;
                //$timeline->created_at=$value->created_at;
                //$timeline->updated_at=$value->updated_at;
                $timeline->id_usuario=$value->id_usuario;
                $timeline->id_parroquia=$value->id_parroquia;                
                $timeline->ip=$value->ip;
                $timeline->pc=$value->pc;
                $timeline->save();
                $pro++;
            }else
            $nopro++;
        }
        return array("PROCESADOS"=>$pro,"NO PROCESADOS"=>$nopro);
    }
	public function index()
	{
		$objetos=json_decode($this->objetos);
        $objetos[7]->Nombre="contratista";        
        $objetos[0]->Nombre="parroquia";
        $objetos[1]->Nombre="tipoobra";
		$tabla=CoorObrasModel::join("coor_tmae_contratista as a","a.id","=","coor_tmov_obras_listado.id_contratista")
        ->join("coor_tmae_tipo_obra as b","b.id","=","coor_tmov_obras_listado.id_tipo_obra")
        ->join("parroquia as c","c.id","=","coor_tmov_obras_listado.id_parroquia")
        ->select("coor_tmov_obras_listado.*", "a.nombres as contratista","b.tipo as tipoobra","c.parroquia")
        ->where("coor_tmov_obras_listado.estado","ACT")->orderby("id","desc")->get();//->paginate(500);   
        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
        $delete="si";
        $create="si";
        $edit=null;
        $id_usuario=Auth::user()->id;
        if($id_tipo_pefil->tipo==5){
            $create='no';
            $delete='no';
            $edit='no';
            
        }







    
        return view('vistas.index',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "delete"=>$delete,
                "create"=>$create,
                "edit"=>$edit

                ]);
		//return view('coordinacioncronograma::index');
	}
	public function create()
    {
        //
        $tipoobra=CoorTipoObraModel::where("estado","ACT")->lists("tipo","id")->all();        
        $Avance= explodewords(ConfigSystem("avance"),"|");
        $estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
        $contratista=CoorContratistaModel::lists("nombres","id")->all();   
        $seleccionar_parroquia=parroquiaModel::where("estado", "ACT")->lists("parroquia","id")->all();     
        $objetos=json_decode($this->objetos);        
        $objetos[0]->Valor=$this->escoja + $seleccionar_parroquia;
        $objetos[1]->Valor=$this->escoja + $tipoobra;
        $objetos[6]->Valor=$this->escoja + $estadoobras;
        $objetos[7]->Valor=$this->escoja + $contratista;        
        $objetos[8]->Valor=$this->escoja + $Avance;  
        unset($objetos[11]);      
        $this->configuraciongeneral[2]="crear";        
        return view('vistas.create',[
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }
	public function guardar($id)
    {         
           $input=Input::all();

            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new CoorObrasModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro Variable de Configuración";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= CoorObrasModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición Variable de Configuración";
            }

            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, CoorObrasModel::rules($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                 foreach($input as $key => $value)
                 {
                   
                    if($key != "_method" && $key != "_token")
                    {
                        $guardar->$key = $value;
                    }                        
                 }


                
                $guardar->save();

                $timeline= new CoorObrasDetaModel;
                //show($timeline);
                $timeline->id_tipo_obra=$guardar->id_tipo_obra;
                $timeline->calle=$guardar->calle;
                $timeline->nombre_obra=$guardar->nombre_obra;
                $timeline->numero_proceso=$guardar->numero_proceso;
                $timeline->monto=$guardar->monto;
                $timeline->estado_obra=$guardar->estado_obra;
                $timeline->id_contratista=$guardar->id_contratista;
                $timeline->avance=$guardar->avance;
                $timeline->fecha_inicio=$guardar->fecha_inicio;
                $timeline->fecha_final=$guardar->fecha_final;
                //$timeline->created_at=$guardar->created_at;
                //$timeline->updated_at=$guardar->updated_at;
                $timeline->id_usuario=Auth::user()->id;
                $timeline->id_parroquia=$guardar->id_parroquia;
                $timeline->id_cab_obra=$guardar->id;
                $timeline->ip=\Request::getClientIp();
                $timeline->pc=\Request::getHost();
                $timeline->save();


                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->nombre_obra));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
  	public function store(Request $request)
    {
        //        
        return $this->guardar(0);
    }
    public function show($id)
    {
        //
        $objetos=json_decode($this->objetos);
        $objetos[7]->Nombre="contratista";        
        //$objetos[8]->Nombre="avance";        
        $objetos[0]->Nombre="parroquia";
        $objetos[1]->Nombre="tipoobra";
        
        $tabla=CoorObrasModel::join("coor_tmae_contratista as a","a.id","=","coor_tmov_obras_listado.id_contratista")
        ->join("coor_tmae_tipo_obra as b","b.id","=","coor_tmov_obras_listado.id_tipo_obra")
        ->join("parroquia as c","c.id","=","coor_tmov_obras_listado.id_parroquia")
        ->select("coor_tmov_obras_listado.*","a.nombres as contratista","b.tipo as tipoobra","c.parroquia")
        ->where("coor_tmov_obras_listado.estado","ACT")
        ->where("coor_tmov_obras_listado.id",$id)
        ->first();//->paginate(500);

        $timeline_obras=$this->getTimeLine($id);
        unset($objetos[11]);      
        return view('vistas.show',[
                "timeline_obras"=>$timeline_obras,
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }
    public function edit($id)
    {
        //
        $tabla = CoorObrasModel::find($id);
        $tipoobra=CoorTipoObraModel::where("estado","ACT")->lists("tipo","id")->all();        
        $estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
        $Avance= explodewords(ConfigSystem("avance"),"|");
        $contratista=CoorContratistaModel::lists("nombres","id")->all();   
        $seleccionar_parroquia=parroquiaModel::where("estado", "ACT")->lists("parroquia","id")->all();     
        $objetos=json_decode($this->objetos);        
        $objetos[0]->Valor=$this->escoja + $seleccionar_parroquia;
        $objetos[1]->Valor=$this->escoja + $tipoobra;
        $objetos[6]->Valor=$this->escoja + $estadoobras;
        $objetos[7]->Valor=$this->escoja + $contratista;  
        $objetos[8]->Valor=$this->escoja + $Avance;  
        $this->configuraciongeneral[2]="editar";

        $timeline_obras=$this->getTimeLine($id);
        unset($objetos[11]);      
        //show($timeline);

        $dicecionesImagenes=ArchivosModel::where([["id_referencia",$tabla->id],["tipo_archivo","<>","pdf"]])->get();
        $dicecionesDocumentos=ArchivosModel::where([["id_referencia",$tabla->id],["tipo_archivo","=","pdf"]])->get();

        //show($dicecionesImagenes);
        $this->configuraciongeneral[3]="1";
        $this->configuraciongeneral[4]="si";
        return view('vistas.create',[
            "dicecionesImagenes"=>$dicecionesImagenes,
            "dicecionesDocumentos"=>$dicecionesDocumentos,
            "timeline_obras"=>$timeline_obras,
        		"tabla"=>$tabla,
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //    
        DB::beginTransaction();
        try{
            $tabla=CoorObrasModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
          
        DB::commit();
        Session::flash('message', 'El registro se eliminó Exitosamente!');
        if(Input::has('vista')){
            return 'El registro se eliminó Exitosamente!';
        }
            return Redirect::to($this->configuraciongeneral[1]);   
        }//Try Transaction
        catch(\Exception $e){
          //  // ROLLBACK
            DB::rollback();
            Session::flash('message', 'El registro no pudo ser elminado!');
            return Redirect::to($this->configuraciongeneral[1]);   
        }
    }

    


    public function getTimeLine($id){
        $tabla=CoorObrasDetaModel::
        select("coor_tmov_obras_listado_deta.*","p.parroquia","c.nombres as contratista","to.tipo","u.name")
        ->join("coor_tmae_tipo_obra as to","to.id","=","coor_tmov_obras_listado_deta.id_tipo_obra")
        ->join("parroquia as p","p.id","=","coor_tmov_obras_listado_deta.id_parroquia")
        ->join("coor_tmae_contratista as c","c.id","=","coor_tmov_obras_listado_deta.id_contratista")
        ->join("users as u","u.id","=","coor_tmov_obras_listado_deta.id_usuario")
        ->where('id_cab_obra',$id)
        ->orderBy("coor_tmov_obras_listado_deta.updated_at","ASC")
        ->get();
        return $tabla;
    }
}