<?php namespace Modules\Coordinacioncronograma\Http\Controllers;


use Pingpong\Modules\Routing\Controller;
use App\ModulosModel;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Route;
use Auth;
use Modules\Coordinacioncronograma\Entities\CoorObrasModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;	
use DB;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;	

class CoordinacionCharsObrasController extends Controller {
	var $configuraciongeneral = array ("MONITOREO Y CONTROL - OBRAS", "coordinacioncronograma/coordinaciongraficosobras", "index");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[
		{"Tipo":"chart","Descripcion":"GRÁFICO DE OBRAS","Nombre":"chart2","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"chart2","Descripcion":"GRÁFICO DE OBRAS POR PARROQUIA","Nombre":"chart4","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"chart","Descripcion":"GRÁFICO DE OBRAS POR PARROQUIAS","Nombre":"chart3","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';

    
		public function __construct() {
			$this->middleware('auth');
		}

	public function index()
	{
        $direcciones=parroquiaModel::select('id','parroquia')->where('estado','=','ACT')->get();
		$objetos = json_decode($this->objetos);
		$objetos=array_values($objetos);
		
        //show($objetos);
       
            
        return view('vistas.indexchart',[
            "objetos"=>$objetos,
            "direcciones"=>$direcciones,
			"configuraciongeneral"=>$this->configuraciongeneral,
			"delete"=>"si",
            "create"=>"si"
            ]);
    }
    
	public function getValoresChartsObras(){
		$tipo_chart=Input::get("tipo");
		$parroquia=Input::get("direccion");
		$valores;
		switch($tipo_chart){
			case '2':

			$valores=CoorObrasModel::select(DB::raw("count(nombre_obra) as valor, estado_obra"))
			->where("estado", "ACT")->groupby("estado_obra")->get();
			return $valores;
			break;
			case '3':
				
			$valores=CoorObrasModel::
			join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
			->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,pa.parroquia"))
			->where("coor_tmov_obras_listado.estado", "ACT")->groupby("pa.parroquia")->get();

			return $valores;
			break;
			case '4':
			$valores=CoorObrasModel::
			join("parroquia as pa", "coor_tmov_obras_listado.id_parroquia", "=", "pa.id")
			->select(DB::raw("count(coor_tmov_obras_listado.nombre_obra) as valor,coor_tmov_obras_listado.estado_obra"))
			->where([["coor_tmov_obras_listado.estado","ACT"],["coor_tmov_obras_listado.id_parroquia",$parroquia]])->groupby("coor_tmov_obras_listado.estado_obra")->get();	
			return $valores;
			break;
	
			return response()->json($valores);
		}
	


	}
	
}