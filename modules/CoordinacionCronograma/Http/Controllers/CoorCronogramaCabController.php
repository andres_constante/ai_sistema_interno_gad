<?php namespace Modules\Coordinacioncronograma\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;    
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaCabModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaDetaModel;
use Modules\Coordinacioncronograma\Entities\CoorCronogramaDetaActModel;
use Modules\Coordinacioncronograma\Entities\MovimientoCronogramaPersonasModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use App\UsuariosModel;

use Auth;


class CoorCronogramaCabController extends Controller {
    var $configuraciongeneral = array ("Cronograma de Compromisos", "coordinacioncronograma/cronogramacompromisos", "index",6=>"null");
    var $escoja=array(null=>"Escoja opción...") ;

    var $objetos = '[
        {"Tipo":"select","Descripcion":"Estado Actividad","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Actividad","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Personas a cargo","Nombre":"idusuario","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Explicación Actividad","Nombre":"observacion","Clase":"mostrarobservaciones","Valor":"Null","ValorAnterior" :"Null" } ,
        {"Tipo":"select","Descripcion":"Avance %","Nombre":"avance","Clase":"porcentaje","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Prioridad","Nombre":"prioridad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Ultima Modificacion","Nombre":"updated_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }     
        ]';

        var $objetosdetalle = '[
            {"Tipo":"text","Descripcion":"Responsable","Nombre":"responsable","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"text","Descripcion":"Correo","Nombre":"correo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';
    var $botonesindex='
        [
            {"Descripcion":"Mostrar","Ruta":"","Tipo":"Editar" },
            {"Descripcion":"Editar","Ruta":"","Tipo":"Mostrar" }
        ]
    ';



//https://jqueryvalidation.org/validate/
    var $validarjs =array(
            "estado_actividad"=>"estado_actividad: {
                            required: true
                        }",
            "actividad"=>"actividad: {
                            required: true
                        }",
            "fecha_inicio"=>"fecha_inicio: {
                            required: true
                        }",
            "fecha_fin"=>"fecha_fin: {
                            required: true
                        }",
            "idusuario"=>"idusuario: {
                            required: true
                        }",


            "observacion"=>"observacion: {
                            required: false
                        }"
        );


    public function __construct() {
        $this->middleware('auth');
    }
    public function traspasoactividad()
    {        
        $tabla=CoorCronogramaCabModel::all();
        $pro=0;
        $nopro=0;
        foreach ($tabla as $key => $value) {
            # code...
            $guardar=CoorCronogramaDetaModel::where("id_crono_cab",$value->id)->first();
            if(!$guardar)
            {
                $timeline=new CoorCronogramaDetaModel;
                 $timeline->estado_actividad=$value->estado_actividad;
                $timeline->actividad=$value->actividad;
                $timeline->fecha_inicio=$value->fecha_inicio;
                $timeline->fecha_fin=$value->fecha_fin;                
                $timeline->observacion=$value->observacion;
                $timeline->id_usuario=$value->id_usuario;
                //$timeline->created_at=$value->created_at;
                //$timeline->updated_at=$value->updated_at;
                $timeline->ip=$value->ip;
                $timeline->pc=$value->pc;
                $timeline->estado="INA";
                $timeline->id_direccion=$value->id_direccion;
                $timeline->avance=$value->avance;
                $timeline->id_crono_cab=$value->id;
                $personas=MovimientoCronogramaPersonasModel::where("id_crono_cab",$value->id)->get();
                $json = [];
                foreach ($personas as $key => $value) {
                    # code...
                    $json[] = $value->id_usuario;
                }
                $timeline->personas_json=json_encode($json);
                $timeline->save();
                $pro++;
            }else
            $nopro++;
        }
        return array("PROCESADOS"=>$pro,"NO PROCESADOS"=>$nopro);
    }
    public function quitarcampos($array,$idacti)
        {
            $tbcamposocultos= actividadesModel::find($idacti);            
            if($tbcamposocultos)
            {                
                $camposocultos=explode("|",$tbcamposocultos->camposocultos);
                if(!count($array))
                    return $camposocultos;
                foreach ($camposocultos as $key => $value) {
                # code...
                    $valor=str_replace("div-","",$value);
                    unset($array[$valor]);                    
                }
            }
            return $array;
    }
    
    public function verificarPermiso() {
        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     

        $id_usuario=Auth::user()->id;
        switch($id_tipo_pefil->tipo){
            case 1:
         
            $tabla = CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*","a.direccion")
            ->where("coor_tmov_cronograma_cab.estado","ACT")
            ->get();
            $create='si';
            $delete='si';
            break;
            case 2:
           
            $tabla = CoorCronogramaCabModel::
            join("coor_tmov_cropnograma_resposables as cr","cr.id_crono_cab","=","coor_tmov_cronograma_cab.id")
            ->join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*","a.direccion")
            ->where([["coor_tmov_cronograma_cab.estado","ACT"],["cr.id_usuario",$id_usuario]])
            ->get();

            //show($tabla);
            $create='no';
            $delete='no';
            break;
            case 3:
            $tabla = CoorCronogramaCabModel::
            join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->join("users as u","u.id_direccion","=","a.id")
            ->select("coor_tmov_cronograma_cab.*","a.direccion")
            ->where([["coor_tmov_cronograma_cab.estado","ACT"],["u.id",$id_usuario]])
            ->get();
            $create='no';
            $delete='no';
            break;
            case 4:
            $tabla = CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*","a.direccion")
            ->where("coor_tmov_cronograma_cab.estado","ACT")
            ->get();
            $create='si';
            $delete='si';
            break;
            
        }
        
    }
        
    public function index()
    {        
        $objetos = json_decode($this->objetos);
        unset($objetos[5]);        
        //show($objetos);        
        $objetos[4]->Nombre="direccion";
        //$objetos[4]->Value=$this->escoja +$seleccionar_direccion;

        $objetos=array_values($objetos);
        //show($objetos);

        $create='';
        $delete='';
        $edit=null;
        
        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     

        $id_usuario=Auth::user()->id;
        switch($id_tipo_pefil->tipo){
            case 1:
         
            $tabla = CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*","a.direccion")
            ->where("coor_tmov_cronograma_cab.estado","ACT")
            ->orderBy("coor_tmov_cronograma_cab.estado","desc")
            ->get();
            $create='si';
            $delete='si';
            break;
            case 2:
           
            $tabla = CoorCronogramaCabModel::
            join("coor_tmov_cropnograma_resposables as cr","cr.id_crono_cab","=","coor_tmov_cronograma_cab.id")
            ->join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*","a.direccion")
            ->where([["coor_tmov_cronograma_cab.estado","ACT"],["cr.id_usuario",$id_usuario]])
            ->orderBy("coor_tmov_cronograma_cab.estado","desc")
            ->get();

            //show($tabla);
            $create='no';
            $delete='no';
            break;
            case 3:
            $tabla = CoorCronogramaCabModel::
            join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->join("users as u","u.id_direccion","=","a.id")
            ->select("coor_tmov_cronograma_cab.*","a.direccion")
            ->where([["coor_tmov_cronograma_cab.estado","ACT"],["u.id",$id_usuario]])
            ->orderBy("coor_tmov_cronograma_cab.estado","desc")
            ->get();
            $create='no';
            $delete='no';
            break;
            case 4:
            $tabla = CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*","a.direccion")
            ->orderBy("coor_tmov_cronograma_cab.estado","desc")
            ->where("coor_tmov_cronograma_cab.estado","ACT")
            ->get();
            $create='si';
            $delete='si';
            break;
            case 5:
            $tabla = CoorCronogramaCabModel::join("tmae_direcciones as a","a.id","=","coor_tmov_cronograma_cab.id_direccion")
            ->select("coor_tmov_cronograma_cab.*","a.direccion")
            ->orderBy("coor_tmov_cronograma_cab.estado","desc")
            ->where("coor_tmov_cronograma_cab.estado","ACT")
            ->get();
            $create='no';
            $delete='no';
            $edit='no';
            break;
            
        }
        unset($objetos[5]);
        return view('vistas.index',[
            "objetos"=>$objetos,
            "tabla"=>$tabla,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "delete"=>$create,
            "create"=>$delete,
            "edit"=>$edit
            ]);
    }


    public function create()
    {
        //        
        $this->configuraciongeneral[2]="crear";
        $objetos=json_decode($this->objetos);
        
        //$seleccionar_personas_cargo=UsuariosModel::where("estado", "ACT")->lists("name","id")->all();
        $seleccionar_direccion=direccionesModel::where("estado", "ACT")->orderby("direccion","asc")->lists("direccion","id")->all();     
        $objetos[4]->Valor=$this->escoja + $seleccionar_direccion;
        $objetos[5]->Valor=$this->escoja;
        //Estado
        $estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
        $prioridad=explodewords(ConfigSystem("prioridad"),"|");
        $Avance= explodewords(ConfigSystem("avance"),"|");

        //show($prioridad);
        
        $objetos[0]->Valor=$this->escoja + $estadoobras;
        $objetos[7]->Valor=$this->escoja +$Avance;
        $objetos[8]->Valor=$this->escoja + $prioridad;
       //show($objetos);
        unset($objetos[9]);
        
        
        
        
        return view('vistas.create',[
                "objetos"=>$objetos,
                "edit"=>null,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
        ]);
    }
public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }


    public function guardar($id)
        {    
            $input=Input::all();
            //return $input;
            $ruta=$this->configuraciongeneral[1];
            DB::beginTransaction();
            try{        
                if($id==0)
                {
                    $ruta.="/create";
                    $guardar= new CoorCronogramaCabModel;
                    $msg="Registro Creado Exitosamente...!";
                    $msgauditoria="Registro de Comunicación";
                }
                else{
                    $ruta.="/$id/edit";
                    $guardar= CoorCronogramaCabModel::find($id);
                    $msg="Registro Actualizado Exitosamente...!";
                    $msgauditoria="Edición Comunicación";
                }
                
                $input=Input::all();
                $arrapas=array();
                $validararry=CoorCronogramaCabModel::rules($id);
                //show($validararry,0);                
                //$validararry=$this->quitarcampos($validararry,Input::get("tema_principal"));
                //$input=$this->quitarcampos($input,Input::get("tema_principal"));                
                //show($validararry,0);
                //show($input);
                $validator = Validator::make($input, $validararry);
                //$validator = Validator::make($input);

                if ($validator->fails()) {
                    //die($ruta);
                    return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
                }else {                    
                    foreach($input as $key => $value)
                    {
                        
                        if($key != "_method" && $key != "_token" && $key!="idusuario")
                        {                        
                            $guardar->$key = $value;
                        }                        
                     }

                     $guardar->id_usuario= Auth::user()->id;
                     $guardar->ip=\Request::getClientIp();
                     $guardar->pc=\Request::getHost();

                     $guardar->save();
                     $idcab=$guardar->id;

                     //
                     $timeline=new CoorCronogramaDetaModel;
                     $timeline->estado_actividad=$guardar->estado_actividad;
                     $timeline->actividad=$guardar->actividad;
                     $timeline->fecha_inicio=$guardar->fecha_inicio;
                     $timeline->fecha_fin=$guardar->fecha_fin;
                     $timeline->fecha_fin=$guardar->fecha_fin;
                     $timeline->observacion=$guardar->observacion;
                     $timeline->id_usuario=$guardar->id_usuario;
                     //$timeline->created_at=$guardar->created_at;
                     //$timeline->updated_at=$guardar->updated_at;
                     $timeline->ip=\Request::getClientIp();
                     $timeline->pc=\Request::getHost();
                     $timeline->id_direccion=$guardar->id_direccion;
                    
                     $timeline->id_direccion=$guardar->id_direccion;
                     $timeline->avance=$guardar->avance;
                     $timeline->prioridad=$guardar->prioridad;
                     $timeline->id_crono_cab=$idcab;

                     unset($guardar);


                     /*Detalle Personas a Cargo / Fiscalizadores*/
                     if(Input::has("idusuario"))
                     {
                        $multiple=Input::get("idusuario");
                        $json = [];
                        foreach ($multiple as $key => $value) {
                            # code...
                            $guardar=MovimientoCronogramaPersonasModel::where("id_usuario",$value)
                            ->where("id_crono_cab",$idcab)
                            ->first();                        
                            if(!$guardar)
                                $guardar= new MovimientoCronogramaPersonasModel;
                            $guardar->id_usuario=$value;
                            $guardar->id_crono_cab=$idcab;
                            $guardar->save();
                            $json[] =$value;
                        }
                        $timeline->personas_json=json_encode($json);
                        //show($timeline);
                    }
                    $timeline->save();
                    
                }
                // DB::rollback();
                DB::commit();
            }//Try Transaction
            catch(\Exception $e){
                // ROLLBACK
                DB::rollback();
                $mensaje=$e->getMessage();
                return redirect()->back()->withErrors([$mensaje])->withInput();    
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
     public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }


    public function edit($id)
    {
        //

        $variableControl=null;
        $verObservaciones="si";        
        $tabla = CoorCronogramaCabModel::find($id);    
        //show($tabla);    
        //$zona=array("URBANA"=>"URBANA","RURAL"=>"RURAL");
        $this->configuraciongeneral[2]="editar";
        $objetos=json_decode($this->objetos);
        //$seleccionar_personas_cargo=UsuariosModel::where("estado", "ACT")->lists("name","id")->all();
        $seleccionar_direccion=direccionesModel::where("estado", "ACT")->orderby("direccion","asc")->lists("direccion","id")->all();     
        $objetos[4]->Valor=$this->escoja + $seleccionar_direccion;
        $objetos[5]->Valor=$this->escoja;

        
        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
        if($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3){
            
            unset($seleccionar_direccion);
            $id_usuario=Auth::user()->id;
            $seleccionar_direccion=direccionesModel::
            join("users as u","u.id_direccion","=","tmae_direcciones.id")
            ->where([["tmae_direcciones.estado", "ACT"],["u.id",$id_usuario]])->orderby("direccion","asc")->lists("direccion","tmae_direcciones.id")->all();     
            $objetos[4]->Valor= $seleccionar_direccion;
        }
        
        //Estado
        $estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
        $objetos[0]->Valor=$this->escoja + $estadoobras;

        $prioridades=explodewords(ConfigSystem("prioridad"),"|");
        $objetos[8]->Valor=$this->escoja + $prioridades;
        //show($objetos[8]);

        
        //Personas a cargo
        $valorAnteriorpersonas=MovimientoCronogramaPersonasModel::join("users as a","a.id","=","coor_tmov_cropnograma_resposables.id_usuario")
            ->where("coor_tmov_cropnograma_resposables.id_crono_cab",$id)
            ->select("a.*");
        //show($valorAnteriorpersonas);
        $objetos[5]->Valor=$valorAnteriorpersonas->lists("name","id")->all(); 
        $objetos[5]->ValorAnterior=$valorAnteriorpersonas->lists("id","id")->all();
        //show($valotAnterior);
        $btnguardar=0;
        if(Input::has("list"))
            $btnguardar=1;

        $timelineActividades=$this->getTimeLineCab($id,1);
        $timelineActividadesResp=$this->getTimeLineCab($id,2);
        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
        $id_usuario=Auth::user()->id;
        $edit=null;

        switch($id_tipo_pefil->tipo){
           
            case 2:
            $objetos[0]->Tipo="selectdisabled";
            $objetos[1]->Tipo="textdisabled2";
            $objetos[4]->Tipo="selectdisabled";
            $objetos[2]->Tipo="textdisabled2";
            $objetos[3]->Tipo="textdisabled2";
            $objetos[8]->Tipo="selectdisabled";
            unset($seleccionar_direccion);
            unset($estadoobras);
            
            //estados
            $estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
            $valor=$estadoobras[$tabla->estado_actividad];
            unset($estadoobras);
            $estadoobras=[$valor=>$valor];
            $objetos[0]->Valor=$estadoobras;
            
            //prioridades
            unset($prioridades);
            $prioridades=explodewords(ConfigSystem("prioridad"),"|");
            $valor2=$prioridades[$tabla->prioridad];
            unset($prioridades);
            $prioridades=[$valor2=>$valor2];
            $objetos[8]->Valor=$prioridades;
            //estados



            $seleccionar_direccion=direccionesModel::where("id",$tabla->id_direccion)->orderby("direccion","asc")->lists("direccion","id")->all() ;     
            $objetos[4]->Valor=$seleccionar_direccion;
            $edit="no";
            $variableControl="no";
            break;
            
            case 3:
            $objetos[0]->Tipo="selectdisabled";
            $objetos[1]->Tipo="textdisabled2";
            $objetos[4]->Tipo="selectdisabled";
            $objetos[2]->Tipo="textdisabled2";
            $objetos[3]->Tipo="textdisabled2";
            $objetos[8]->Tipo="selectdisabled";
            unset($seleccionar_direccion);
            unset($estadoobras);
            //estados
            $estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
            $valor=$estadoobras[$tabla->estado_actividad];
            unset($estadoobras);
            $estadoobras=[$valor=>$valor];
            $objetos[0]->Valor=$estadoobras;
            //estados

            //prioridades
            unset($prioridades);
            $prioridades=explodewords(ConfigSystem("prioridad"),"|");
            //show($prioridades);
            $valor2="";
            if($tabla->prioridad)
            {
                $valor2=$prioridades[$tabla->prioridad];
            }
            
            unset($prioridades);
            $prioridades=[$valor2=>$valor2];
            $objetos[8]->Valor=$prioridades;
            //estados

            $seleccionar_direccion=direccionesModel::where("id",$tabla->id_direccion)->orderby("direccion","asc")->lists("direccion","id")->all() ;     
            $objetos[4]->Valor=$seleccionar_direccion;
            $edit="no";
            $variableControl="no";
            break;
           
        }
        $Avance= explodewords(ConfigSystem("avance"),"|");
        $objetos[7]->Valor=$this->escoja + $Avance;
        unset($objetos[9]);
        

            $this->configuraciongeneral[3]="2";
            $this->configuraciongeneral[4]=null;
            
        return view('vistas.create',[
            "timelineActividades"=>$timelineActividades,
            "timelineActividadesResp"=>$timelineActividadesResp,
            "objetos"=>$objetos,
            "variableControl"=>$variableControl,
            "tabla"=>$tabla,
            "edit"=>$edit,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "validarjs"=>$this->validarjs,
            "btnguardar"=>$btnguardar,
            "verObservaciones"=>$verObservaciones
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $objetos = json_decode($this->objetos);
        $objetosdetalle = json_decode($this->objetosdetalle);
       
        unset($objetos[5]);
        //show($this->objetos);
        $objetos[0]->Nombre="tema_principal";
        $objetos[1]->Nombre="actividad";
        $objetos[2]->Nombre="fecha_inicio";
        $objetos[3]->Nombre="fecha_fin";
        $objetos[4]->Nombre="dias_restantes";
        $objetos[6]->Nombre="observacion";

        $objetos=array_values($objetos);
        //show($objetos);
        


        $tabla = CoorCronogramaCabModel::
                    select(DB::raw("id,actividad,fecha_inicio,fecha_fin,CONCAT(DATEDIFF(fecha_fin,fecha_inicio),' días') as dias_restantes,observacion"))
                    ->where("id",$id)
                    ->first();
        $tabla2 = MovimientoCronogramaPersonasModel::
                    join('coor_tmov_cronograma_cab as cro','coor_tmov_cropnograma_resposables.id_crono_cab','=','cro.id')
                    ->join('users as us','coor_tmov_cropnograma_resposables.id_usuario','=','us.id')
                    ->select(DB::raw("us.name,us.email"))
                    ->where("cro.id",$id)
                    ->groupBy('us.name')
                    ->get();;

        $timelineActividades=$this->getTimeLineCab($id,1);
        $timelineActividadesResp=$this->getTimeLineCab($id,2);

        
            
        return view('vistas.show',[
            "timelineActividades"=>$timelineActividades,
            "timelineActividadesResp"=>$timelineActividadesResp,
                "objetos"=>$objetos,
                "objetosdetalle"=>$objetosdetalle,
                "tabla"=>$tabla,
                "tabla2"=>$tabla2,
                "configuraciongeneral"=>$this->configuraciongeneral
                //"validararry"=>$validararry
                ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();
        try{
            $tabla=CoorCronogramaCabModel::find($id);
            $tabla->estado='INA';
            $tabla->id_usuario= Auth::user()->id;
            //$tabla->delete();
            $tabla->save();
            // demas modelos
            //$tabla2->delete();
            $tabla3=CoorCronogramaDetaActModel::where("id_crono_cab",$id)->get();
            foreach ($tabla3 as $key => $value) {
                # code...
                $dele=CoorCronogramaDetaActModel::find($value->id);
                $dele->estado='INA';
                $dele->save();
            }
            // MovimientoComunicacionPersonasModel
            // CabPresupuestoModel
            // PresupuestoDetalleModel
            // MovComunicacionParroquiaModel

            //->update(array('estado' => 'INACTIVO'));
        DB::commit();
        Session::flash('message', 'El registro se eliminó Exitosamente!');
        if(Input::has('vista')){
            return 'El registro se eliminó Exitosamente!';
        }
            return Redirect::to($this->configuraciongeneral[1]);   
        }//Try Transaction
        catch(\Exception $e){
          //  // ROLLBACK
            DB::rollback();
            Session::flash('message', 'El registro no pudo ser elminado!');
            return Redirect::to($this->configuraciongeneral[1]);   
        }
    }
     

    //timeline edit and show cronograma
    public function getTimeLineCab($id,$tipo){
        $sw=0;
        switch($tipo){
            case 1:
            $tabla=CoorCronogramaDetaModel::
            select("coor_tmov_cronograma_deta.*","u.name","dir.direccion")
            ->join("users as u","u.id","=","coor_tmov_cronograma_deta.id_usuario")
            ->join("tmae_direcciones as dir","dir.id","=","coor_tmov_cronograma_deta.id_direccion")
            ->where('id_crono_cab',$id)
            ->orderBy("coor_tmov_cronograma_deta.updated_at","ASC")
            ->get();
            return $tabla;
            break;
            case 2:
            $personas[]=[];
            $tabla=CoorCronogramaDetaModel::
            select("personas_json")->where('id_crono_cab',$id)->get();
            foreach($tabla as $valor){
                
                $personas=json_decode($valor->personas_json);
                
                unset($tabla);
                foreach($personas as $p){
                    $tabla[]=DB::table("users")->where("id",$p)->select("name")->first();
                    $sw=1;
                }
            }
            if($sw==0){
                $tabla=array();
            }
                
            return $tabla;
            break;
        }
    }


    
}