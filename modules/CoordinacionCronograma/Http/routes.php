<?php
use JasperPHP\JasperPHP as JasperPHP; 
Route::group(['middleware' => 'web', 'prefix' => 'coordinacioncronograma', 'namespace' => 'Modules\CoordinacionCronograma\Http\Controllers'], function()
{
	/*JASPER*/

	Route::get('/', 'CoordinacionCronogramaController@index');
	Route::resource('contratista', 'CoorContratistaController');
	Route::resource('listobras', 'CoorObrasController');
	Route::resource('cronogramacompromisos', 'CoorCronogramaCabController');
	Route::resource('tipoobra', 'CoorTipoObraController');
	
	// Route::resource('componente', 'CoorTipoProyectoController');
	Route::resource('componente', 'CoorTipoProyectoController');
	Route::resource('proyecto', 'CoorProyectoEstrategicoController');
	// Route::get('image', 'ImagenController@index');
	
	//ajax
	Route::resource('contratistaajax', 'CoorContratistaController@contratistaajax');

	
	Route::resource('coordinacionlistaobras', 'CoordinacionListObrasController');
	Route::get("getValoresFiltroObras","CoordinacionListObrasController@getValoresFiltroObras");
	Route::get("getComunicacionesFiltroObras","CoordinacionListObrasController@getComunicacionesFiltroObras");

	Route::resource('coordinaciongraficosobras', 'CoordinacionCharsObrasController');
	Route::get("getValoresChartsObras","CoordinacionCharsObrasController@getValoresChartsObras");

	Route::resource('coordinaciongraficos', 'CoordinacionChartsController');
	Route::get("getValoresCharts","CoordinacionChartsController@getValoresCharts");
	
	
	Route::resource('coordinacionlista', 'CoordinacionListController');
	Route::get("getValoresFiltro","CoordinacionListController@getValoresFiltro");
	Route::get("getTimeLine","CoorObrasController@getTimeLine");
	Route::get("getComunicacionesFiltro","CoordinacionListController@getComunicacionesFiltro");
	Route::get("traspasoobras","CoorObrasController@traspasoobras");
	Route::get("traspasoactividad","CoorCronogramaCabController@traspasoactividad");
	
	Route::resource('dashboard', 'DashboardController');
	Route::get("getChartLine","DashboardController@getChartLine");

	Route::resource('dashboardproyectos', 'DashboardProyectosController');
	//Route::get("getChartLineProyectos","DashboardProyectosController@getChartLine");

	
	

});