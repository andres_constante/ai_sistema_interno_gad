<?php namespace Modules\Coordinacioncronograma\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CoorProyectoEstrategicoModel extends Model {

    protected $fillable = [];
    protected $table="coor_tmov_proyecto_estrategico";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'id_tipo_proyecto'=>'required',
                'nombre'=>'required',            
                'estado_proyecto' => 'required'
            ], $merge);
        }

}