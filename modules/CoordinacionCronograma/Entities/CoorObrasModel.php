<?php namespace Modules\Coordinacioncronograma\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CoorObrasModel extends Model {

    protected $fillable = [];
    protected $table="coor_tmov_obras_listado";
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'nombre_obra'=>'required|unique:coor_tmov_obras_listado'. ($id ? ",id,$id" : ''),
                'id_tipo_obra'=>'required|numeric',
                'calle'=>'required',
                'monto'=>'required|numeric',
                'estado_obra'=>'required',
                'avance'=>'required|numeric',
                'fecha_inicio'=>'required|date',
                'fecha_final'=>'required|date',
                'id_contratista'=>'required|numeric',
                'id_parroquia'=>'required|numeric'
            ], $merge);
        }
}