<?php namespace Modules\Coordinacioncronograma\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CoorCronogramaCabModel extends Model {

   
    protected $table = 'coor_tmov_cronograma_cab';
    protected $hidden = [];
    public static function rules ($id=0, $merge=[]) {
        return array_merge(
        [                            
            'estado_actividad'=>'required',
            'actividad'=>'required',
            'fecha_inicio'=> 'required|date', 
            'fecha_fin'=> 'required|date', 
            'idusuario'=> 'required',
            'id_direccion'=> 'required',
            'observacion'
            ], $merge);
    } 
}