<?php namespace Modules\Comunicacionalcaldia\Entities;
   
use Illuminate\Database\Eloquent\Model;

class CabPresupuestoModel extends Model {

    protected $table = 'com_tmov_presupuesto_cab';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [   'id_com_cab'=>'required',           
			'id_parroquia'=>'required',
            'monto_inicial'=>'required',
            'monto_final'=>'required',
            'idusuario'=>'required',
            'observacion'=>'required'
		], $merge);
    } 
    

}