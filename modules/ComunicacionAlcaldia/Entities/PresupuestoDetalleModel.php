<?php namespace Modules\Comunicacionalcaldia\Entities;
   
use Illuminate\Database\Eloquent\Model;

class PresupuestoDetalleModel extends Model {

    protected $table = 'com_tmov_presupuesto_deta';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [                
			'id_presu' => 'required',
			'monto' => 'required',
			'idusuario' => 'required',
			'observacion' => 'required'
		], $merge);
    } 

}