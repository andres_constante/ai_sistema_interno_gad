<?php namespace Modules\Comunicacionalcaldia\Entities;
   
use Illuminate\Database\Eloquent\Model;

class barrioModel extends Model {

    protected $table = 'barrio';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [                
			'id_parroquia'=>'required',
			'barrio'=>'required'
		], $merge);
    } 

}