<?php namespace Modules\Comunicacionalcaldia\Entities;
   
use Illuminate\Database\Eloquent\Model;

class MovComunicacionBarrioModel extends Model {

    protected $fillable = [];

    protected $table = 'com_tmov_comunicacion_barrios';
    public static function rules ($id=0, $merge=[]) {
            return array_merge(
            [                
                'id_cab_parroquia'=>'required',
                'id_barrio	'=>'required',
                'avance'=>'required|numeric'
            ], $merge);
        }

    

}