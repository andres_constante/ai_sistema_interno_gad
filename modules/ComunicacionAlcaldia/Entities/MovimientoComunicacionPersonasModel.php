<?php namespace Modules\Comunicacionalcaldia\Entities;
   
use Illuminate\Database\Eloquent\Model;

class MovimientoComunicacionPersonasModel extends Model {

    protected $table = 'com_tmov_comunicacion_personas';
 	protected $hidden = [];
 	public static function rules ($id=0, $merge=[]) {
		return array_merge(
        [                
			'id_com_cab'=>'required',
            'idusuario'=>'required'
		], $merge);
    } 

}