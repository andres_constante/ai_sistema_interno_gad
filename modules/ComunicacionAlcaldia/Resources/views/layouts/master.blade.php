<?php
//use Request;
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link href="{{ asset("favicon.ico") }} " rel="shortcut icon" type="image/vnd.microsoft.icon" />
   <title>@yield('titulo')</title>
   
    {!! HTML::style('css/bootstrap.min.css') !!}       
    {!! HTML::style('font-awesome/css/font-awesome.css') !!}
 
   <!-- Data Tables -->
    {{ HTML::style('css/plugins/dataTables/dataTables.bootstrap.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.responsive.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.tableTools.min.css') }}
       
<!-- Mensaje -->

{{ HTML::style('css/plugins/toastr/toastr.min.css') }}
    {{ HTML::style('css/animate.css') }}
    {{ HTML::style('css/style.css') }}

    <!--Wizard-->
{{ HTML::style('css/plugins/steps/jquery.steps.css')}}
{{ HTML::script('css/plugins/iCheck/custom.css')}}

    @yield('estilos')
<!-- Mainly scripts -->
{{ HTML::script('js/jquery-2.1.1.js') }} 
{{ HTML::script('js/bootstrap.min.js') }} 
{{ HTML::script('js/plugins/metisMenu/jquery.metisMenu.js') }} 
{{ HTML::script('js/plugins/slimscroll/jquery.slimscroll.min.js') }} 


<!-- Custom and plugin javascript -->
{{ HTML::script('js/inspinia.js') }} 
{{ HTML::script('js/plugins/pace/pace.min.js') }} 
{{ HTML::script('js/plugins/steps/jquery.steps.min.js') }}  
{{ HTML::script('js/plugins/validate/jquery.validate.min.js') }}  

 <!-- Data Tables -->
 {{ HTML::script('js/plugins/dataTables/jquery.dataTables.js') }}     
    {{ HTML::script("js/plugins/dataTables/dataTables.bootstrap.js") }} 
    {{ HTML::script("js/plugins/dataTables/dataTables.responsive.js") }} 
    {{ HTML::script("js/plugins/dataTables/dataTables.tableTools.min.js") }} 

 <!-- Mensaje -->
 {{ HTML::script('js/plugins/toastr/toastr.min.js') }}     
 
      <!-- CSS Notificacion -->
    {{ HTML::style('ventanas-modales/ventanas-modales.css') }}
    <!-- CSS Notificacion -->
    {{ HTML::script('ventanas-modales/ventanas-modales.js') }}  
    <!-- Graficos estadissticos -->
{{ HTML::style('css/plugins/morris/morris-0.4.3.min.css') }}
{{-- {{ HTML::script('js/plugins/morris/raphael-2.1.0.min.js') }} --}}
{{ HTML::script('js/plugins/morris/raphael.js') }}
{{ HTML::script('js/plugins/morris/morris.js') }}
<!-- Chosen -->
    {{ HTML::style('chosen/docsupport/prism.css') }}
    {{ HTML::style('chosen/chosen.css') }}
    
    {{ HTML::script('chosen/chosen.jquery.js') }}
    {{ HTML::script('chosen/docsupport/prism.js') }}
<!-- ColorBox -->
    {{ HTML::style('colorbox/colorbox.css') }}    
    
    {{ HTML::script('colorbox/jquery.colorbox.js') }} 
    <!-- Jquery Validate -->   
    {{ HTML::script('js/jquery.validate.min.js') }}   

    <!-- DatePicker -->    
    {{ HTML::style('bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ HTML::script('bootstrap-datepicker/bootstrap-datepicker.js') }}   
    {{ HTML::script('bootstrap-datepicker/locales/bootstrap-datepicker.es.js') }}   
    <!-- Selective -->
    {!! HTML::style('selective/selectize.bootstrap3.css'); !!}
    {!! Html::script('selective/selectize.js') !!}
    <!-- Tree -->
    {!! HTML::style('css/plugins/jsTree/style.css'); !!}
    {!! Html::script('js/plugins/jsTree/jstree.min.js') !!}

    <!-- dropzone -->
    {!! HTML::style('css/plugins/dropzone/dropzone.css'); !!}
    {!! Html::script('js/plugins/dropzone/dropzone.js') !!}

    {!! HTML::style('css/plugins/c3/c3.min.css'); !!}
    {!! Html::script('js/plugins/d3/d3.min.js') !!}
    {!! Html::script('js/plugins/c3/c3.min.js') !!}
    {!! Html::script('js/plugins/sparkline/jquery.sparkline.min.js') !!}

    
    {!! HTML::style('css/plugins/blueimp/css/blueimp-gallery.min.css'); !!}
    {!! Html::script('js/plugins/blueimp/jquery.blueimp-gallery.min.js') !!}



@yield('scripts')
 <script>

 function cargaatender()
 {
     
        $.ajax({
            type: "GET",
            url: '{{ URL::to("getnotificatodos") }}',
            data: '',
            error: function(objeto, quepaso, otroobj){
                $("#txttodos").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
            },              
            success: function(datos){
                $("#txttodos").html(datos);
          },
            statusCode: {
                404: function() {
                $("#txttodos").html("<div class='alert alert-danger'>No existe URL</div>");
            }   
            }     
    });      
 }
 function NotificacionTotal()
 {
        $.ajax({
            type: "GET",
            url: '{{ URL::to("getnotificacuenta") }}',
            data: '',
            error: function(objeto, quepaso, otroobj){
                $("#txtcontar").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
            },              
            success: function(datos){
                $("#txtcontar").html(datos);
          },
            statusCode: {
                404: function() {
                $("#txtcontar").html("<div class='alert alert-danger'>No existe URL</div>");
            }   
            }     
    }); 
     //setTimeout(NotificacionTotal, 5000);
 }
 //setTimeout(NotificacionTotal, 1000);


 </script>
 
</head>
<?php
    //Estas líneas hacen que el menú Inicio tenga la ruta Inicio según el modulo
    $rucu=Route::getCurrentRoute()->getPath();
    $vermodulo=explode("/",$rucu);
        //show($vermodulo);
    //$mod=App\ModulosModel::where("ruta",$vermodulo[0])->first();
    $mod=App\ModulosModel::where("ruta",$vermodulo[0])->first();
    $rutamod="/";
    if($mod)
        $rutamod=$mod->ruta;
    /*else
    {
        $mod=App\UsuarioModuloModel::join("ad_modulos as a","a.id","=","ad_usuario_modulo.id_modulo")
        ->where("ad_usuario_modulo.id_usuario",Auth::user()->id)
        ->select("a.nombre","a.id")
        ->first();
    }*/
?>
<body class="skin-1 {{ ($vermodulo[0]=="cambiardatos" ? 'pace-done body-small mini-navbar' : '') }}">

<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <span>
                            <img src="http://ciudadanodigital.manta.gob.ec/local/public/logo.png" class=" img-responsive" alt="">
                        </span>
       
                    </div>
                    <div class="logo-element">
                        MENÚ
                    </div>
                </li>
                <li {{ (Request::is('/') ? 'class="active"' : '') }}>
                    <a href="{{ URL::to($rutamod) }}"><i class="fa fa-home"></i> <span class="nav-label">Inicio</span></a>
                </li>    
<!-- Permisos segun el usuario-->  
<?php 
    $rucu=Route::getCurrentRoute()->getPath();
        $vermodulo=explode("/",$rucu);
        //show($vermodulo);
        //$mod=App\MenuModulosModel::where("ruta",$vermodulo[0])->first();
        //show($mod);
        //show($mod);
        $id_tipo_pefil=App\UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.id","ap.tipo")->where("users.id",Auth::user()->id)->first();

             if ($id_tipo_pefil->tipo==1)
             {
                $tablamen=App\MenuModulosModel::join("menu as a","a.id","=","ad_menu_modulo.id_menu")
                ->select("ad_menu_modulo.*","a.menu")
                ->where("ad_menu_modulo.nivel",1)->orderby("ad_menu_modulo.idmain")->get();

            }
            else {              
               $tablamen=App\MenuModulosModel::join("menu as a","a.id","=","ad_menu_modulo.id_menu")
                ->join("ad_menu_perfil as b","b.id_menu_modulo","=","ad_menu_modulo.id")
                ->select("ad_menu_modulo.*","a.menu")
                ->where("ad_menu_modulo.nivel",1)
                ->where("b.id_perfil",$id_tipo_pefil->id);
                if($mod){
                    $tablamen=$tablamen->where("ad_menu_modulo.id_modulo",$mod->id);
                }
                  
                $tablamen=$tablamen->orderby("ad_menu_modulo.orden")->get();  
              } 
             
               
                     
             //$sw=0;
            //show($tablamen);
              if($vermodulo[0]=="cambiardatos")
                $tablamen=array();
             ?>

            @foreach($tablamen as $key => $value)
                <?php
                    if ($id_tipo_pefil->tipo==1){
                        $tbnivel=App\MenuModulosModel::join("menu as a","a.id","=","ad_menu_modulo.id_menu")                            
                            ->select("ad_menu_modulo.*","a.menu")
                            ->where("ad_menu_modulo.nivel","<>",1) //->groupBy("ad_menu_modulo.id_menu");
                           ->where("ad_menu_modulo.idmain",$value->id)->get();
                        
                    }
                    else{

                        $tbnivel=App\MenuModulosModel::join("menu as a","a.id","=","ad_menu_modulo.id_menu") 
                        ->join("ad_menu_perfil as bb","bb.id_menu_modulo","=","ad_menu_modulo.id")  
                        ->join("ad_modulos as m","m.id","=","ad_menu_modulo.id_modulo") 
                        ->join("ad_usuario_modulo as um","um.id_modulo","=","m.id")  
                            ->join("users as u","u.id","=","um.id_usuario")                     
                            ->select("ad_menu_modulo.*","a.menu")
                            ->where("ad_menu_modulo.nivel","<>",1)
                            ->where("ad_menu_modulo.idmain",$value->id)
                            ->where("ad_menu_modulo.visible","SI")
                            ->where("u.id",Auth::user()->id)
                            //->where("u.id_perfil",Auth::user()->id)
                            ->where("bb.id_perfil",$id_tipo_pefil->id)
                            ->orderby("ad_menu_modulo.orden")
                            ->get();
                    }
                    $clase="";
                    foreach($tbnivel as $cla => $val)
                    {
                        //die(Request::path()."-".$val->ruta);
                        if(Request::is($val->ruta.'*'))
                                $clase="class=active";
                    }    
                //dd($tbnivel);                    
                ?> 
                <li {{ $clase }}>
                    @if($value->ruta=='#')                        
                        <a href="javascript::">
                    @else
                        <a href="{{ URL::to($value->ruta) }}" {{ $value->adicional }}>
                    @endif
                            <i class="{{ $value->icono }}"></i> <span class="nav-label">{{ $value->menu }}</span> <span class="fa arrow"></span></a>                    
                    @if(!empty($tbnivel))
                        <ul class="nav nav-second-level">
                            @foreach($tbnivel as $clave => $valor)
                            <li {{ (Request::is($valor->ruta.'*') ? 'class=active' : '') }}>
                                @if($valor->ruta=='#')                        
                                    <a href="javascript::" {{ $valor->adicional }}>
                                @else
                                    <a href="{{ URL::to($valor->ruta) }}" {{ $valor->adicional }}>
                                @endif
                                <i class="{{ $valor->icono }}"></i>
                                {{ $valor->menu }}</a>
                            </li>                            
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach  
<!-- Fin Permisos -->
                     <li {{ (Request::is('salir') ? 'class="active"' : '') }}>
                                <a href="{{ URL::to('auth/logout') }}">      <i class="fa fa-sign-out"></i>  Salir</a>
                              </li>
            </ul>

        </div>
    </nav>
<div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <!--form role="search" class="navbar-form-custom" method="post" action="#">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form-->
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <!-- Alerta Campana -->                 
                    @if (Session::has('tipousuario') && Session::get('tipousuario')=="ADMINISTRADOR")
                    <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="javascript::" onclick="cargaatender()">
                        <i class="fa fa-bell"></i>  <span class="label label-primary" id="txtcontar">0</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts" id="txttodos">
                    
                    </ul>
                </li>
                @endif              
                    <li>
                        @if(Auth::user()->id==1)
                        <a href="#">
                            <i class="fa fa-user"></i> {{ Auth::user()->name }}
                        </a>
                        @else
                        <a href="{{ URL::to('cambiardatos/'.Auth::user()->id.'/edit') }}">
                            <i class="fa fa-user"></i> {{ Auth::user()->name }}
                        </a>
                        @endif
                    </li>
                    <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-cubes"></i> Módulos
                            </a>
                            <?php
                            $modulos=App\UsuarioModuloModel::
                                join("ad_modulos as m","m.id","=","ad_usuario_modulo.id_modulo")                            
                                ->select("m.*")
                                ->where("ad_usuario_modulo.id_usuario",Auth::user()->id)
                                ->get();
                            ?>
                            <ul class="dropdown-menu dropdown-alerts">
                                @foreach ($modulos as $key => $item)
                                <li class="divider"></li>     
                                <li>
                                    <a href="{{ URL::to($item->ruta) }}">
                                            <div>
                                                <i class="fa fa-cube"></i> {{$item->nombre}}
                                            </div>
                                        </a>
                                    </li>
                                    <li class="divider"></li> 
                                @endforeach
                                
                            </ul>
                        </li>                    
                    <li>
                        <a href="{{ URL::to('auth/logout') }}">
                            <i class="fa fa-sign-out"></i> Salir
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">                
                <div class="col-lg-12">
                        @yield('contenido')
                </div>

            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                <!--10GB of <strong>250GB</strong> Free. -->
                {{ Auth::user()->name }}
            </div>
            <div>               
                {{ trans('html.main.copyright') }}
            </div>
        </div>

    </div>
</div>

</body>

</html>
