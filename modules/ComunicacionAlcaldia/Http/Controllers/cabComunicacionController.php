<?php namespace Modules\Comunicacionalcaldia\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use App\PerfilModel;
use Modules\Comunicacionalcaldia\Entities\cabComunicacionModel;
use Modules\Comunicacionalcaldia\Entities\actividadesModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Comunicacionalcaldia\Entities\MovimientoComunicacionPersonasModel;
use Modules\Comunicacionalcaldia\Entities\CabPresupuestoModel;
use Modules\Comunicacionalcaldia\Entities\PresupuestoDetalleModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use Modules\Comunicacionalcaldia\Entities\barrioModel;
use Modules\Comunicacionalcaldia\Entities\MovComunicacionParroquiaModel;
use Modules\Comunicacionalcaldia\Entities\MovComunicacionBarrioModel;
use App\UsuariosModel;
use Auth;


class cabComunicacionController extends Controller {
		var $configuraciongeneral = array ("Comunicación", "comunicacionalcaldia/comunicacion", "index",6=>"comunicacionalcaldia/comunicacionajax");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[ 
        {"Tipo":"select","Descripcion":"Tipo de actividad","Nombre":"id_tipo_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Descripción de actividad","Nombre":"nombre_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Dirección a Cargo","Nombre":"id_direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Zona","Nombre":"zona","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Número aproximado de Beneficiaros","Nombre":"beneficiarios","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Explicación Actividad / Obra","Nombre":"observacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Kilómetros a ser intervenidos","Nombre":"kilometros","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Estado Actividad","Nombre":"estado_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
		]';         
        
        // Objeto de la tabla cabecera presupuesto
        var $objetosCabPresupuesto = '[             
            {"Tipo":"select","Descripcion":"Parroquia","Nombre":"id_parroquia","Clase":"id_parroquia","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"text","Descripcion":"Monto inicial","Nombre":"monto_inicial","Clase":"moneda","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"text","Descripcion":"Monto final","Nombre":"monto_final","Clase":"moneda","Valor":"Null","ValorAnterior" :"Null" },            
            {"Tipo":"textarea","Descripcion":"Observación","Nombre":"observacionpresu","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"hidden","Descripcion":"id","Nombre":"idpapresu","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
            ]'; 
        
        // Objeto de la tabla cabecera con personas a cargo
        var $objetosCabComPersonas = '[            
        {"Tipo":"select-multiple","Descripcion":"Personas a cargo","Nombre":"idusuario","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" }
        ]';

        // Objeto de la tabla comunicacion con parroquia
        var $objetosCominicacionParroquia = '[            
            {"Tipo":"select","Descripcion":"Parroquia","Nombre":"parroquia_id","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"text","Descripcion":"Dirección / Calles","Nombre":"calles","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"hidden","Descripcion":"id","Nombre":"idpabaavance","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
            ]';

        var $objetosCominicacionBarrio = '[            
            {"Tipo":"select","Descripcion":"Barrio","Nombre":"id_barrio","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
            {"Tipo":"select","Descripcion":"Avance / Progreso(%)","Nombre":"avance","Clase":"null","Valor":"Null","ValorAnterior" :"Null" }
            ]';
            // {"Tipo":"text","Descripcion":"Usuario","Nombre":"idusuario12","Clase":"reonly","Valor":"Null","ValorAnterior" :"Null" }
            
// //https://jqueryvalidation.org/validate/
    var $validarjs =array(
            "id_tipo_actividad"=>"id_tipo_actividad: {
                            required: true
                        }",
            "nombre_actividad"=>"nombre_actividad: {
                            required: true
                        }",
            "id_direccion"=>"id_direccion: {
                            required: true
                        }"/*,
            "zona"=>"zona: {
                            required: true
                        }",
            "beneficiarios"=>"beneficiarios: {
                            required: true
                        }"*/,
            "observacion"=>"observacion: {
                            required: true
                        }",
            "fecha_inicio"=>"fecha_inicio: {
                            required: true
                        }",
            "fecha_fin"=>"fecha_fin: {
                            required: true
                        }",
            "id_barrio"=>"id_barrio: {
                            required: true
                        }",
            "idusuario"=>"idusuario: {
                            required: true
                        }"

		);
        var $wiz=array("Información Concreta","Personas a Cargo /  Fiscalizadores","Presupuesto por Parroquias", "Parroquias / Barrios Beneficiados");
        var $wizid=array("div-infoconcre","div-percar","div-txtpresupuesto","div-txtavancebapa");

		public function __construct() {
			$this->middleware('auth');
		} 
        public function quitarcampos($array,$idacti)
        {
            $tbcamposocultos= actividadesModel::find($idacti);            
            if($tbcamposocultos)
            {                
                $camposocultos=explode("|",$tbcamposocultos->camposocultos);
                if(!count($array))
                    return $camposocultos;
                foreach ($camposocultos as $key => $value) {
                # code...
                    $valor=str_replace("div-","",$value);
                    unset($array[$valor]);                    
                }
            }
            return $array;
        }
		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */

    public function comunicacionajax(Request $request)
        {
          $columns = array( 
                                0 =>'id', 
                                1 =>'nombre',
                                2=> 'nombre_actividad',
                                3=> 'direccion',
                                4=> 'zona',
                                5=> 'beneficiarios',
                                6=> 'observacion',
                                7=> 'fecha_inicio',
                                8=> 'fecha_fin',
                                9=> 'estado_actividad',
                                10=> 'acciones',
                            );
      
            $totalData = cabComunicacionModel::count();
                
            $totalFiltered = $totalData; 
    
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
            $id_usuario=Auth::user()->id;


            if(empty($request->input('search.value')))
            {     
                switch($id_tipo_pefil->tipo){
                    case 1:
                    $posts = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                            ->select("com_tmov_comunicacion_cab.*", "ac.nombre",
                                DB::raw("(select direccion from tmae_direcciones where id=com_tmov_comunicacion_cab.id_direccion) as direccion"))
                            ->where("com_tmov_comunicacion_cab.estado", "ACT")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                    break;
                    case 2:
                    
                    $posts = MovimientoComunicacionPersonasModel::
                            join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_personas.id_com_cab", "=", "com.id")
                            ->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
                            ->select("com.*", "ac.nombre",
                                DB::raw("(select direccion from tmae_direcciones where id=com.id_direccion) as direccion"))
                            ->where([["com.estado", "ACT"],["com_tmov_comunicacion_personas.idusuario",$id_usuario]])
                            ->groupBy("com.id")
                            ->offset($start)
                             ->limit($limit)
                             ->orderBy($order,$dir)
                             ->get();
                  
                    
                    break;
        
                    case 3:
                    $posts = MovimientoComunicacionPersonasModel::
                            join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_personas.id_com_cab", "=", "com.id")
                            ->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
                            ->join("tmae_direcciones as dir","com.id_direccion","=","dir.id")
                            ->join("users as u","dir.id","=","u.id_direccion")
                            ->select("com.*", "ac.nombre",
                                DB::raw("(select direccion from tmae_direcciones where id=com.id_direccion) as direccion"))
                            ->where([["com.estado", "ACT"],["u.id",$id_usuario]])
                            ->offset($start)
                             ->limit($limit)
                             ->orderBy($order,$dir)
                             ->get();
                             
                        break;
        
                    case 4:
                    
                    $posts = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                            ->select("com_tmov_comunicacion_cab.*", "ac.nombre",
                                DB::raw("(select direccion from tmae_direcciones where id=com_tmov_comunicacion_cab.id_direccion) as direccion"))
                            ->where("com_tmov_comunicacion_cab.estado", "ACT")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                        
                    break;

                    case 5:
                    
                    $posts = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                            ->select("com_tmov_comunicacion_cab.*", "ac.nombre",
                                DB::raw("(select direccion from tmae_direcciones where id=com_tmov_comunicacion_cab.id_direccion) as direccion"))
                            ->where("com_tmov_comunicacion_cab.estado", "ACT")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                        
                    break;
        
                }       
             
            }
            else {
                $search = $request->input('search.value'); 
                switch($id_tipo_pefil->tipo){
                    case 1:
                            $posts = cabComunicacionModel::
                            join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                            ->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
                            ->select("com_tmov_comunicacion_cab.*", "ac.nombre","dir.direccion")
                            ->where("com_tmov_comunicacion_cab.estado", "ACT")
                            ->where('com_tmov_comunicacion_cab.id','LIKE',"%{$search}%")
                            ->orWhere('ac.nombre', 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("nombre_actividad"), 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("direccion"), 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

                            $totalFiltered = cabComunicacionModel::
                            join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                            ->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
                            ->select("com_tmov_comunicacion_cab.*", "ac.nombre","dir.direccion")
                            ->where("com_tmov_comunicacion_cab.estado", "ACT")
                            ->where('com_tmov_comunicacion_cab.id','LIKE',"%{$search}%")
                            ->orWhere('ac.nombre', 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("nombre_actividad"), 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("direccion"), 'LIKE',"%{$search}%")
                             ->count();
                    
                    break;
                    case 2:
                    

                             $posts = MovimientoComunicacionPersonasModel::
                             join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_personas.id_com_cab", "=", "com.id")
                             ->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
                             ->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
                             ->select("com.*", "ac.nombre","dir.direccion")
                             ->where([["com.estado", "ACT"],["com_tmov_comunicacion_personas.idusuario",$id_usuario]])
                             ->where('com.id','LIKE',"%{$search}%")
                             ->orWhere('ac.nombre', 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("nombre_actividad"), 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("direccion"), 'LIKE',"%{$search}%")
                             ->offset($start)
                             ->limit($limit)
                             ->orderBy($order,$dir)
                             ->get();
 
                             $totalFiltered = MovimientoComunicacionPersonasModel::
                             join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_personas.id_com_cab", "=", "com.id")
                             ->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
                             ->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
                             ->select("com.*", "ac.nombre","dir.direccion")
                             ->where([["com.estado", "ACT"],["com_tmov_comunicacion_personas.idusuario",$id_usuario]])
                             ->where('com.id','LIKE',"%{$search}%")
                             ->orWhere('ac.nombre', 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("nombre_actividad"), 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("direccion"), 'LIKE',"%{$search}%")
                              ->count();
                  
                    
                    break;
        
                    case 3:


                             $posts = MovimientoComunicacionPersonasModel::
                             join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_personas.id_com_cab", "=", "com.id")
                             ->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
                             ->join("tmae_direcciones as dir","com.id_direccion","=","dir.id")
                             ->join("users as u","dir.id","=","u.id_direccion")
                             ->select("com.*", "ac.nombre","dir.direccion")
                             ->where([["com.estado", "ACT"],["u.id",$id_usuario]])
                             ->where('com.id','LIKE',"%{$search}%")
                             ->orWhere('ac.nombre', 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("nombre_actividad"), 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("direccion"), 'LIKE',"%{$search}%")
                             ->offset($start)
                             ->limit($limit)
                             ->orderBy($order,$dir)
                             ->get();
 
                             $totalFiltered =  MovimientoComunicacionPersonasModel::
                             join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_personas.id_com_cab", "=", "com.id")
                             ->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
                             ->join("tmae_direcciones as dir","com.id_direccion","=","dir.id")
                             ->join("users as u","dir.id","=","u.id_direccion")
                             >select("com.*", "ac.nombre","dir.direccion")
                             ->where([["com.estado", "ACT"],["u.id",$id_usuario]])
                             ->where('com.id','LIKE',"%{$search}%")
                             ->orWhere('ac.nombre', 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("nombre_actividad"), 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("direccion"), 'LIKE',"%{$search}%")
                              ->count();
                             
                        break;
        
                    case 4:
                    
                            $posts = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                            ->join("tmae_direcciones as dir","com_tmov_comunicacion_cab.id_direccion","=","dir.id")
                            ->select("com_tmov_comunicacion_cab.*", "ac.nombre","dir.direccion")
                            ->where("com_tmov_comunicacion_cab.estado", "ACT")
                            ->where('com_tmov_comunicacion_cab.id','LIKE',"%{$search}%")
                            ->orWhere('ac.nombre', 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("nombre_actividad"), 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("direccion"), 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

                            $totalFiltered =cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                            ->join("tmae_direcciones as dir","com_tmov_comunicacion_cab.id_direccion","=","dir.id")
                            ->select("com_tmov_comunicacion_cab.*", "ac.nombre","dir.direccion")
                            ->where("com_tmov_comunicacion_cab.estado", "ACT")
                            ->where('com_tmov_comunicacion_cab.id','LIKE',"%{$search}%")
                            ->orWhere('ac.nombre', 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("nombre_actividad"), 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("direccion"), 'LIKE',"%{$search}%")
                             ->count();
                        
                    break;

                    case 5:
                    
                            $posts = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                            ->join("tmae_direcciones as dir","com_tmov_comunicacion_cab.id_direccion","=","dir.id")
                            ->select("com_tmov_comunicacion_cab.*", "ac.nombre","dir.direccion")
                            ->where("com_tmov_comunicacion_cab.estado", "ACT")
                            ->where('com_tmov_comunicacion_cab.id','LIKE',"%{$search}%")
                            ->orWhere('ac.nombre', 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("nombre_actividad"), 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("direccion"), 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

                            $totalFiltered =cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                            ->join("tmae_direcciones as dir","com_tmov_comunicacion_cab.id_direccion","=","dir.id")
                            ->select("com_tmov_comunicacion_cab.*", "ac.nombre","dir.direccion")
                            ->where("com_tmov_comunicacion_cab.estado", "ACT")
                            ->where('com_tmov_comunicacion_cab.id','LIKE',"%{$search}%")
                            ->orWhere('ac.nombre', 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("nombre_actividad"), 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("direccion"), 'LIKE',"%{$search}%")
                             ->count();
                        
                    break;
        
                }  
               
            }
    
            $data = array();
            if(!empty($posts))
            {
                foreach ($posts as $post)
                {
                    switch($id_tipo_pefil->tipo){
                        case 1:
                       
                        $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                        link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.$post->id.')"><i class="fa fa-trash"></i></a> 
                        <div style="display: none;">
                        <form method="POST" action="comunicacion/'.$post->id.'" accept-charset="UTF-8" id="frmElimina'.$post->id.'" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                            <input name="_token" type="hidden" value="'.csrf_token().'">
                            <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                        </form>
                        </div>';
                        break;
                        case 2:
                        $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                        link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o'));
        
                        break;
            
                        case 3:
                        
                        $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                        link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o'));
                              
                                 
                        break;
            
                        case 4:
                        
                        $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                        link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.$post->id.')"><i class="fa fa-trash"></i></a> 
                        <div style="display: none;">
                        <form method="POST" action="comunicacion/'.$post->id.'" accept-charset="UTF-8" id="frmElimina'.$post->id.'" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                            <input name="_token" type="hidden" value="'.csrf_token().'">
                            <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                        </form>
                        </div>';
                       
                        break;
                        
                        case 5:
                        
                          $aciones=''.link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o'));
                        
                        break;
            
                    }  
                   
                    $nestedData['id'] = $post->id;
                    $nestedData['nombre'] = $post->nombre;
                    $nestedData['nombre_actividad'] = $post->nombre_actividad;
                    $nestedData['direccion'] = $post->direccion;
                    $nestedData['zona'] = $post->zona;
                    $nestedData['beneficiarios'] = $post->beneficiarios;
                    $nestedData['observacion'] = $post->observacion;
                    $nestedData['fecha_inicio'] = $post->fecha_inicio;
                    $nestedData['fecha_fin'] = $post->fecha_fin;
                    $nestedData['estado_actividad'] = $post->estado_actividad;
                    $nestedData['acciones'] = $aciones;
                    $data[] = $nestedData;
                    
                }
            }
            //show($data);
            $json_data = array(
                        "draw"            => intval($request->input('draw')),  
                        "recordsTotal"    => intval($totalData),  
                        "recordsFiltered" => intval($totalFiltered), 
                        "data"            => $data   
                        );
            
            return response()->json($json_data); 
        }
	public function index()
	{        
        $objetos = json_decode($this->objetos);
        unset($objetos[8]);
        //show($this->objetos);
        $objetos[0]->Nombre="nombre";
        $objetos[2]->Nombre="direccion";

        $objetos=array_values($objetos);
        
        
        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
        
        $id_usuario=Auth::user()->id;
        $tabla=[];
        $delete='';
        $create='';
        //show($objetos);
        switch($id_tipo_pefil->tipo){
            case 1:
            // $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
            //         ->select("com_tmov_comunicacion_cab.*", "ac.nombre",
            //             DB::raw("(select direccion from tmae_direcciones where id=com_tmov_comunicacion_cab.id_direccion) as direccion"))
            //         ->where("com_tmov_comunicacion_cab.estado", "ACT")
            //         ->get();
                    $delete='si';
                    $create='si';
            break;
            case 2:
            
            // $tabla = MovimientoComunicacionPersonasModel::
            //         join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_personas.id_com_cab", "=", "com.id")
            //         ->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
            //         ->select("com.*", "ac.nombre",
            //             DB::raw("(select direccion from tmae_direcciones where id=com.id_direccion) as direccion"))
            //         ->where([["com.estado", "ACT"],["com_tmov_comunicacion_personas.idusuario",$id_usuario]])
            //         ->groupBy("com.id")
            //         ->get();
            $delete='no';
            $create='no';
            
            break;

            case 3:
            // $tabla = MovimientoComunicacionPersonasModel::
            //         join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_personas.id_com_cab", "=", "com.id")
            //         ->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
            //         ->join("tmae_direcciones as dir","com.id_direccion","=","dir.id")
            //         ->join("users as u","dir.id","=","u.id_direccion")
            //         ->select("com.*", "ac.nombre",
            //             DB::raw("(select direccion from tmae_direcciones where id=com.id_direccion) as direccion"))
            //         ->where([["com.estado", "ACT"],["u.id",$id_usuario]])
            //         ->groupBy("com.id")
            //         ->get();
                    $delete='si';
                    $create='si';
            break;

            case 4:
            
            // $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
            //         ->select("com_tmov_comunicacion_cab.*", "ac.nombre",
            //             DB::raw("(select direccion from tmae_direcciones where id=com_tmov_comunicacion_cab.id_direccion) as direccion"))
            //         ->where("com_tmov_comunicacion_cab.estado", "ACT")
            //         ->get();
                    $delete='si';
                    $create='si';

            break;


            case 5:
            
            // $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
            //         ->select("com_tmov_comunicacion_cab.*", "ac.nombre",
            //             DB::raw("(select direccion from tmae_direcciones where id=com_tmov_comunicacion_cab.id_direccion) as direccion"))
            //         ->where("com_tmov_comunicacion_cab.estado", "ACT")
            //         ->get();
                    $delete='no';
                    $create='no';

            break;


        }
        
        //show($create);
        return view('vistas.index',[
            "objetos"=>$objetos,
            "tabla"=>$tabla,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "delete"=>$delete,
            "create"=>$create,
            ]);
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $objetos = json_decode($this->objetos);
        //unset($objetos[7]);
        //show($this->objetos);
        $objetos[0]->Nombre="nombre";
        $objetos[2]->Nombre="direccion";
        $objetos=array_values($objetos);
        //show($objetos);
        $tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
            ->select("com_tmov_comunicacion_cab.*", "ac.nombre",
                DB::raw("(select direccion from tmae_direcciones where id=com_tmov_comunicacion_cab.id_direccion) as direccion"))
            ->where("com_tmov_comunicacion_cab.estado", "ACT")
            ->where("com_tmov_comunicacion_cab.id",$id)
            ->first();
        //show($tabla);
        
        
        $avancesTimeline=$this->getTimeLineCab($id,1);
        
        $validararry=$this->quitarcampos(array(),$tabla->id_tipo_actividad);
        //show($validararry);
		return view('vistas.show',[
                "avancesTimeline"=>$avancesTimeline,
				"objetos"=>$objetos,
				"tabla"=>$tabla,
				"configuraciongeneral"=>$this->configuraciongeneral,
                "validararry"=>$validararry
				]);
	}
        
        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }
    
    /**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
        // 
        $zona=array("URBANA"=>"URBANA","RURAL"=>"RURAL","URBANA/RURAL"=>"URBANA/RURAL");
		$this->configuraciongeneral[2]="crear";
		$seleccionar_actividad=actividadesModel::where("estado", "ACT")->lists("nombre","id")->all();
        $seleccionar_direccion=direccionesModel::where("estado", "ACT")->orderby("direccion","asc")->lists("direccion","id")->all();
        $seleccionar_parroquia=parroquiaModel::where("estado", "ACT")->lists("parroquia","id")->all();
        $seleccionar_avance=explodewords(ConfigSystem("avance"),"|");
        $seleccionar_barrio=array();//barrioModel::where("estado", "ACT")->lists("barrio","id")->all();
        // $seleccionar_zona= $zona;
        
        
		$objetos=json_decode($this->objetos);
		$objetos[0]->Valor=$this->escoja + $seleccionar_actividad;
        $objetos[2]->Valor=$this->escoja + $seleccionar_direccion;
        $objetos[3]->Valor=$this->escoja + $zona;
        // show($objetos);
        
        /* nuevo objeto para manejar la seleccion de personas a cargo*/
        $seleccionar_personas_cargo=array();//UsuariosModel::where("estado", "ACT")->lists("name","id")->all();
        $objetosCabComPersonas=json_decode($this->objetosCabComPersonas);
		$objetosCabComPersonas[0]->Valor=$seleccionar_personas_cargo;
        
        /* nuevo objeto para manejar la tabla cabecera de presupuesto*/
        $objetosCabPresupuesto=json_decode($this->objetosCabPresupuesto);
        
        /* nuevo objeto para manejar la tabla de comunicacion con parroquia*/
        $objetosCominicacionParroquia=json_decode($this->objetosCominicacionParroquia);
        $objetosCominicacionParroquia[0]->Valor=$seleccionar_parroquia;
        
        
        /* nuevo objeto para manejar la tabla de comunicacion con barrio*/
        $objetosCominicacionBarrio=json_decode($this->objetosCominicacionBarrio);
        $objetosCominicacionBarrio[0]->Valor=$this->escoja +$seleccionar_barrio;
        $objetosCominicacionBarrio[1]->Valor=$this->escoja +$seleccionar_avance;

		
        $parroquia=array();//parroquiaModel::orderby("parroquia")->lists("parroquia","id")->all();
        $objetosCabPresupuesto[0]->Valor=$this->escoja + $parroquia;
        //Estado
        $estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
        $objetos[9]->Valor=$this->escoja + $estadoobras;
        // $tabla = CabPresupuestoModel::orderby("id","desc")->get();
        $create="si";
        return view('vistas.createcom',[
                "wiz"=>$this->wiz,
                "wizid"=> $this->wizid,
                "objetos"=>$objetos,
                "objetosCabComPersonas"=> $objetosCabComPersonas,
                "objetosCabPresupuesto"=> $objetosCabPresupuesto,
                "objetosCominicacionParroquia"=> $objetosCominicacionParroquia,
                "objetosCominicacionBarrio"=> $objetosCominicacionBarrio,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs,
                "create"=>$create,
                "save"=>'si',
		]);
    }
       
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       

        // $id_tipo_usuario=Auth::user()->id_tipo_usuario;
        // $delete='';
        // $create='';
        // switch($id_usuario){
        //     case 1:
        //     break;
        // }

        //para obtener el tipo de usuario
        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();

      
        
        $delete='';
        $create='';
        $save='';
        $objetos=[];
        $objetosCabComPersonas=[];
        $objetosCabPresupuesto=[];
        $objetosCominicacionParroquia=[];
        $objetosCominicacionBarrio=[];
        $btnguardar=0;

        
        switch($id_tipo_pefil->tipo){
            case 1:
            //
            $tabla = cabComunicacionModel::find($id);        
            //$zona=array("URBANA"=>"URBANA","RURAL"=>"RURAL");
            $zona=array($tabla->zona=>$tabla->zona);
            $this->configuraciongeneral[2]="editar";
            $wiz=$this->wiz;
            $seleccionar_actividad=actividadesModel::where("id",$tabla->id_tipo_actividad)->where("estado", "ACT")->lists("nombre","id")->all();
            $seleccionar_direccion=direccionesModel::where("id",$tabla->id_direccion)->where("estado", "ACT")->orderby("direccion","asc")->lists("direccion","id")->all();
            $seleccionar_parroquia=parroquiaModel::where("estado", "ACT")->lists("parroquia","id")->all();
            $seleccionar_barrio=array();barrioModel::where("estado", "ACT")->lists("barrio","id")->all();
            $seleccionar_avance=explodewords(ConfigSystem("avance"),"|");
        
            // $seleccionar_zona= $zona;

            $objetos=json_decode($this->objetos);
            $objetos[0]->Valor=/*$this->escoja + */$seleccionar_actividad;
            $objetos[2]->Valor=/*$this->escoja +*/ $seleccionar_direccion;
            $objetos[3]->Valor=/*$this->escoja +*/ $zona;
            
            /* nuevo objeto para manejar la seleccion de personas a cargo*/
            $seleccionar_personas_cargo=UsuariosModel::where("estado", "ACT")->lists("name","id")->all();
            //Personas a cargo
            $valorAnteriorpersonas=MovimientoComunicacionPersonasModel::join("users as a","a.id","=","com_tmov_comunicacion_personas.idusuario")
                ->where("com_tmov_comunicacion_personas.id_com_cab",$id)
                ->select("a.*")
                ->lists("id","id")
                ->all();
            $objetosCabComPersonas=json_decode($this->objetosCabComPersonas);
            $objetosCabComPersonas[0]->Valor=$seleccionar_personas_cargo;
            //show($objetosCabComPersonas);
            $objetosCabComPersonas[0]->ValorAnterior=$valorAnteriorpersonas;        
            //show($valotAnterior);
            /* nuevo objeto para manejar la tabla cabecera de presupuesto*/
            $objetosCabPresupuesto=json_decode($this->objetosCabPresupuesto);
            
            /* nuevo objeto para manejar la tabla de comunicacion con parroquia*/
            $objetosCominicacionParroquia=json_decode($this->objetosCominicacionParroquia);
            $objetosCominicacionParroquia[0]->Valor=$seleccionar_parroquia;
            /* nuevo objeto para manejar la tabla de comunicacion con barrio*/
            $objetosCominicacionBarrio=json_decode($this->objetosCominicacionBarrio);
            $objetosCominicacionBarrio[0]->Valor=$this->escoja + $seleccionar_barrio;            
            $objetosCominicacionBarrio[1]->Valor=$this->escoja + $seleccionar_avance;            
            $parroquia=array();//parroquiaModel::orderby("parroquia")->lists("parroquia","id")->all();
            $objetosCabPresupuesto[0]->Valor=$this->escoja + $parroquia;
            $estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
            $objetos[9]->Valor=$this->escoja + $estadoobras;
            
                
            if(Input::has("list"))
                $btnguardar=1;
                $create='si';
                break;
            break;
            case 2:
                //
            $tabla = cabComunicacionModel::find($id);        
            //$zona=array("URBANA"=>"URBANA","RURAL"=>"RURAL");
            $zona=array($tabla->zona=>$tabla->zona);
            $this->configuraciongeneral[2]="editar";
            $wiz=$this->wiz;
            $seleccionar_actividad=actividadesModel::where("id",$tabla->id_tipo_actividad)->where("estado", "ACT")->lists("nombre","id")->all();
            $seleccionar_direccion=direccionesModel::where("id",$tabla->id_direccion)->where("estado", "ACT")->orderby("direccion","asc")->lists("direccion","id")->all();
            $seleccionar_parroquia=parroquiaModel::where("estado", "ACT")->lists("parroquia","id")->all();
            $seleccionar_barrio=array();barrioModel::where("estado", "ACT")->lists("barrio","id")->all();
            $seleccionar_avance=explodewords(ConfigSystem("avance"),"|");
        
            // $seleccionar_zona= $zona;
            
            $objetos=json_decode($this->objetos);
            $objetos[0]->Valor=/*$this->escoja + */$seleccionar_actividad;
            $objetos[2]->Valor=/*$this->escoja +*/ $seleccionar_direccion;
            $objetos[3]->Valor=/*$this->escoja +*/ $zona;
          
            /* nuevo objeto para manejar la seleccion de personas a cargo*/
            $seleccionar_personas_cargo=UsuariosModel::where("estado", "ACT")->lists("name","id")->all();
            //Personas a cargo
            $valorAnteriorpersonas=MovimientoComunicacionPersonasModel::join("users as a","a.id","=","com_tmov_comunicacion_personas.idusuario")
                ->where("com_tmov_comunicacion_personas.id_com_cab",$id)
                ->select("a.*")
                ->lists("id","id")
                ->all();
            $objetosCabComPersonas=json_decode($this->objetosCabComPersonas);
            $objetosCabComPersonas[0]->Valor=$seleccionar_personas_cargo;
            // //show($objetosCabComPersonas);
            $objetosCabComPersonas[0]->ValorAnterior=$valorAnteriorpersonas;        
            //show($valotAnterior);
            /* nuevo objeto para manejar la tabla cabecera de presupuesto*/
            $objetosCabPresupuesto=json_decode($this->objetosCabPresupuesto);
            
            /* nuevo objeto para manejar la tabla de comunicacion con parroquia*/
            $objetosCominicacionParroquia=json_decode($this->objetosCominicacionParroquia);
            $objetosCominicacionParroquia[0]->Valor=$seleccionar_parroquia;
            
            
            /* nuevo objeto para manejar la tabla de comunicacion con barrio*/
            $objetosCominicacionBarrio=json_decode($this->objetosCominicacionBarrio);
            $objetosCominicacionBarrio[0]->Valor=$this->escoja + $seleccionar_barrio;
            $objetosCominicacionBarrio[1]->Valor=$this->escoja + $seleccionar_avance;  
    
            
            $parroquia=array();//parroquiaModel::orderby("parroquia")->lists("parroquia","id")->all();
            $objetosCabPresupuesto[0]->Valor=$this->escoja + $parroquia;
            $estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
            
            $objetos[9]->Valor=$this->escoja + $estadoobras;
           //desactivando campos
           $objetos[4]->Tipo='textdisabled2';
           $objetos[5]->Tipo="textarea-disabled";
           $objetos[6]->Tipo="textdisabled2";
           $objetos[7]->Tipo="textdisabled2";
           

           //no puede crear
           $create='no';
           $save='no';

           //quitamos el objeto titulo
           $wiz=$this->wiz;
           $wiz[1]='';
        break;
            
            
            
        case 3:
                //
            $tabla = cabComunicacionModel::find($id);        
            //$zona=array("URBANA"=>"URBANA","RURAL"=>"RURAL");
            $zona=array($tabla->zona=>$tabla->zona);
            $this->configuraciongeneral[2]="editar";
            $wiz=$this->wiz;
            $seleccionar_actividad=actividadesModel::where("id",$tabla->id_tipo_actividad)->where("estado", "ACT")->lists("nombre","id")->all();
            $seleccionar_direccion=direccionesModel::where("id",$tabla->id_direccion)->where("estado", "ACT")->orderby("direccion","asc")->lists("direccion","id")->all();
            $seleccionar_parroquia=parroquiaModel::where("estado", "ACT")->lists("parroquia","id")->all();
            $seleccionar_barrio=array();barrioModel::where("estado", "ACT")->lists("barrio","id")->all();
            $seleccionar_avance=explodewords(ConfigSystem("avance"),"|");
        
            // $seleccionar_zona= $zona;
            
            
            $objetos=json_decode($this->objetos);
            $objetos[0]->Valor=/*$this->escoja + */$seleccionar_actividad;
            $objetos[2]->Valor=/*$this->escoja +*/ $seleccionar_direccion;
            $objetos[3]->Valor=/*$this->escoja +*/ $zona;
            
            /* nuevo objeto para manejar la seleccion de personas a cargo*/
            $seleccionar_personas_cargo=UsuariosModel::where("estado", "ACT")->lists("name","id")->all();
            //Personas a cargo
            $valorAnteriorpersonas=MovimientoComunicacionPersonasModel::join("users as a","a.id","=","com_tmov_comunicacion_personas.idusuario")
                ->where("com_tmov_comunicacion_personas.id_com_cab",$id)
                ->select("a.*")
                ->lists("id","id")
                ->all();
            $objetosCabComPersonas=json_decode($this->objetosCabComPersonas);
            $objetosCabComPersonas[0]->Valor=$seleccionar_personas_cargo;
            //show($objetosCabComPersonas);
            $objetosCabComPersonas[0]->ValorAnterior=$valorAnteriorpersonas;        
            //show($valotAnterior);
            /* nuevo objeto para manejar la tabla cabecera de presupuesto*/
            $objetosCabPresupuesto=json_decode($this->objetosCabPresupuesto);
            
            /* nuevo objeto para manejar la tabla de comunicacion con parroquia*/
            $objetosCominicacionParroquia=json_decode($this->objetosCominicacionParroquia);
            $objetosCominicacionParroquia[0]->Valor=$seleccionar_parroquia;
            /* nuevo objeto para manejar la tabla de comunicacion con barrio*/
            $objetosCominicacionBarrio=json_decode($this->objetosCominicacionBarrio);
            $objetosCominicacionBarrio[0]->Valor=$this->escoja + $seleccionar_barrio;
            $objetosCominicacionBarrio[1]->Valor=$this->escoja + $seleccionar_avance;  
    
            
            $parroquia=array();//parroquiaModel::orderby("parroquia")->lists("parroquia","id")->all();
            $objetosCabPresupuesto[0]->Valor=$this->escoja + $parroquia;
            $estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
            $objetos[9]->Valor=$this->escoja + $estadoobras;
            
            
            if(Input::has("list"))
                $btnguardar=1;
                $create='si';
                break;
          case 4:
                //
            $tabla = cabComunicacionModel::find($id);        
            //$zona=array("URBANA"=>"URBANA","RURAL"=>"RURAL");
            $zona=array($tabla->zona=>$tabla->zona);
            $this->configuraciongeneral[2]="editar";
            $wiz=$this->wiz;
            $seleccionar_actividad=actividadesModel::where("id",$tabla->id_tipo_actividad)->where("estado", "ACT")->lists("nombre","id")->all();
            $seleccionar_direccion=direccionesModel::where("id",$tabla->id_direccion)->where("estado", "ACT")->orderby("direccion","asc")->lists("direccion","id")->all();
            $seleccionar_parroquia=parroquiaModel::where("estado", "ACT")->lists("parroquia","id")->all();
            $seleccionar_barrio=array();barrioModel::where("estado", "ACT")->lists("barrio","id")->all();
            $seleccionar_avance=explodewords(ConfigSystem("avance"),"|");
            // $seleccionar_zona= $zona;
            
            
            $objetos=json_decode($this->objetos);
            $objetos[0]->Valor=/*$this->escoja + */$seleccionar_actividad;
            $objetos[2]->Valor=/*$this->escoja +*/ $seleccionar_direccion;
            $objetos[3]->Valor=/*$this->escoja +*/ $zona;
            
            /* nuevo objeto para manejar la seleccion de personas a cargo*/
            $seleccionar_personas_cargo=UsuariosModel::where("estado", "ACT")->lists("name","id")->all();
            //Personas a cargo
            $valorAnteriorpersonas=MovimientoComunicacionPersonasModel::join("users as a","a.id","=","com_tmov_comunicacion_personas.idusuario")
                ->where("com_tmov_comunicacion_personas.id_com_cab",$id)
                ->select("a.*")
                ->lists("id","id")
                ->all();
            $objetosCabComPersonas=json_decode($this->objetosCabComPersonas);
            $objetosCabComPersonas[0]->Valor=$seleccionar_personas_cargo;
            //show($objetosCabComPersonas);
            $objetosCabComPersonas[0]->ValorAnterior=$valorAnteriorpersonas;        
            //show($valotAnterior);
            /* nuevo objeto para manejar la tabla cabecera de presupuesto*/
            $objetosCabPresupuesto=json_decode($this->objetosCabPresupuesto);
            
            /* nuevo objeto para manejar la tabla de comunicacion con parroquia*/
            $objetosCominicacionParroquia=json_decode($this->objetosCominicacionParroquia);
            $objetosCominicacionParroquia[0]->Valor=$seleccionar_parroquia;
            /* nuevo objeto para manejar la tabla de comunicacion con barrio*/
            $objetosCominicacionBarrio=json_decode($this->objetosCominicacionBarrio);
            $objetosCominicacionBarrio[0]->Valor=$this->escoja + $seleccionar_barrio;
            $objetosCominicacionBarrio[1]->Valor=$this->escoja + $seleccionar_avance;  
    
            
            $parroquia=array();//parroquiaModel::orderby("parroquia")->lists("parroquia","id")->all();
            $objetosCabPresupuesto[0]->Valor=$this->escoja + $parroquia;
            $estadoobras=explodewords(ConfigSystem("estadoobras"),"|");
            $objetos[9]->Valor=$this->escoja + $estadoobras;
            
        
            break;
        if(Input::has("list"))
            $btnguardar=1;
            $create='si';
            break;
        
        
        if(Input::has("list"))
            $btnguardar=1;
            break;
        }
        //show($objetos);
        
           //show($objetosCominicacionBarrio);
		return view('vistas.createcom',[
            "wiz"=>$wiz,
            "wizid"=> $this->wizid,
            "objetos"=>$objetos,
            "objetosCabComPersonas"=> $objetosCabComPersonas,
            "objetosCabPresupuesto"=> $objetosCabPresupuesto,
            "objetosCominicacionParroquia"=> $objetosCominicacionParroquia,
            "objetosCominicacionBarrio"=> $objetosCominicacionBarrio,
            "tabla"=>$tabla,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "validarjs"=>$this->validarjs,
            "btnguardar"=>$btnguardar,
            "create"=>$create,
            "save"=>$save
		]);
    }


    public function guardar($id)
        {    
            $input=Input::all();
            //return $input;
            $ruta=$this->configuraciongeneral[1];
            DB::beginTransaction();
            try{        
                if($id==0)
                {
                    $ruta.="/create";
                    $guardar= new cabComunicacionModel;
                    $msg="Registro Creado Exitosamente...!";
                    $msgauditoria="Registro de Comunicación";
                }
                else{
                    $ruta.="/$id/edit";
                    $guardar= cabComunicacionModel::find($id);
                    $msg="Registro Actualizado Exitosamente...!";
                    $msgauditoria="Edición Comunicación";
                }
                
                $input=Input::all();
                $arrapas=array();
                $validararry=cabComunicacionModel::rules($id);
                //show($validararry,0);                
                $validararry=$this->quitarcampos($validararry,Input::get("id_tipo_actividad"));
                $input=$this->quitarcampos($input,Input::get("id_tipo_actividad"));                
                //show($validararry,0);
                //show($input);
                $validator = Validator::make($input, $validararry);
                
                if ($validator->fails()) {
                    //die($ruta);
                    return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
                }else {                    
                    foreach($input as $key => $value)
                    {
                        
                        if($key != "_method" && $key != "_token" && $key!="idusuario" && $key!="avance" && $key!="calles" && $key!="idusuario12" && $key!="txtavancebapa" && $key!="txtpresupuesto")
                        {                        
                            $guardar->$key = $value;
                        }                        
                     }
                     $guardar->idusuario= Auth::user()->id;
                     $guardar->ip=\Request::getClientIp();
                     $guardar->pc=\Request::getHost();
                     $guardar->save();
                     $idcab=$guardar->id;
                     unset($guardar);
                     /*Detalle Personas a Cargo / Fiscalizadores*/
                     if(Input::has("idusuario"))
                     {
                        $multiple=Input::get("idusuario");
                        foreach ($multiple as $key => $value) {
                            # code...
                            $guardar=MovimientoComunicacionPersonasModel::where("idusuario",$value)
                            ->where("id_com_cab",$idcab)
                            ->first();                        
                            if(!$guardar)
                                $guardar= new MovimientoComunicacionPersonasModel;
                            $guardar->idusuario=$value;
                            $guardar->id_com_cab=$idcab;
                            $guardar->save();
                        }
                    }//Input Has
                    /*Presupuesto*/ 
                    if(Input::has("txtpresupuesto"))
                    {
                        $presujson=json_decode(Input::get("txtpresupuesto"));
                         //DB::rollback();
                        //show($presujson); 
                        unset($guardar);
                        foreach ($presujson as $key => $value) {
                            #Cabecera
                            $ver=CabPresupuestoModel::where("id_com_cab",$idcab)
                                ->where("id_parroquia",$value->id_parroquia)
                                ->first();
                            if(!$ver)
                                $guardar= new CabPresupuestoModel;
                            else
                                $guardar=$ver;
                            $guardar->id_com_cab=$idcab;
                            $guardar->id_parroquia=$value->id_parroquia;
                            $guardar->monto_inicial=$value->monto_inicial;
                            $guardar->monto_final=$value->monto_final;
                            $guardar->idusuario= Auth::user()->id;
                            $guardar->observacion= $value->observacion;
                            $guardar->save();
                            $idpresu=$guardar->id;
                            #Detalle
                            unset($guardar);
                            $actualizar=PresupuestoDetalleModel::find($idpresu);
                            if($actualizar)
                            {
                                $actualizar->estado='INA';
                                $actualizar->save();
                            }
                            $guardar= new PresupuestoDetalleModel;
                            $guardar->id_presu=$idpresu;
                            $guardar->monto=$value->monto_final;
                            $guardar->idusuario=Auth::user()->id;
                            $guardar->observacion=$value->observacion;
                            $guardar->ip=\Request::getClientIp();
                            $guardar->pc=\Request::getHost();
                            $guardar->save();
                        }  
                    }//Has Input                 
                    /**/
                    /*Avance Por Barrio*/
                    if(Input::has("txtavancebapa"))
                    {
                        unset($presujson);
                        $presujson=json_decode(Input::get("txtavancebapa"));
                        //DB::rollback();
                        //show($presujson);                    
                        unset($guardar);
                        //MovComunicacionParroquiaModel;
                        //MovComunicacionBarrioModel;
                        $idpaba=0;

                        foreach ($presujson as $key => $value) {
                            #Cabecera
                            $ver=MovComunicacionParroquiaModel::where("id_com_cab",$idcab)
                                ->where("id_parroquia",$value->parroquia_id)->first();
                            if($ver)
                                $guardar= $ver;
                            else
                                $guardar= new MovComunicacionParroquiaModel;
                            $guardar->id_com_cab=$idcab;
                            $guardar->id_parroquia=$value->parroquia_id;
                            $guardar->avance=0;
                            $guardar->calles=$value->calles;
                            $guardar->save();
                            $idpaba=$guardar->id;
                            #Detalle
                            unset($guardar);
                            unset($ver);
                            $ver=MovComunicacionBarrioModel::where("id_cab_parroquia",$idpaba)
                            ->where("id_barrio",$value->id_barrio)
                            ->where("estado","ACT")
                            ->first();
                            //DB::rollback();
                            //  show($ver); 
                            $sw=0;                           
                            if($ver)
                            {                            
                                if(floatval($ver->avance)!=floatval($value->avance))
                                {
                                    $ver->estado="INA";
                                    $ver->save();                               
                                }else{
                                    $guardar=$ver;
                                    $sw=1;
                                }
                            }
                            if($sw==0)
                            {                        
                                $guardar= new MovComunicacionBarrioModel;
                                $guardar->id_cab_parroquia=$idpaba;
                                $guardar->id_barrio=$value->id_barrio;
                                $guardar->avance=$value->avance;
                                $guardar->idusuario=Auth::user()->id;
                                $guardar->ip=\Request::getClientIp();
                                $guardar->pc=\Request::getHost();
                                $guardar->save();
                            }
                        } 
                        $promedioavance=MovComunicacionParroquiaModel::
                        join("com_tmov_comunicacion_barrios as a","a.id_cab_parroquia","=","com_tmov_comunicacion_parroquia.id")
                        ->where("com_tmov_comunicacion_parroquia.id_com_cab",$idcab)
                        ->where("a.estado","ACT")
                        ->select("com_tmov_comunicacion_parroquia.id","com_tmov_comunicacion_parroquia.id_parroquia",DB::raw("avg(a.avance) as total"))
                        ->groupby("com_tmov_comunicacion_parroquia.id","com_tmov_comunicacion_parroquia.id_parroquia")
                        ->get();
                        //show($promedioavance->toarray()); 
                        foreach ($promedioavance as $key => $value) {
                            # code...
                            $upavance=MovComunicacionParroquiaModel::find($value->id);
                            $upavance->avance=$value->total;
                            $upavance->save();
                        }
                    }//Input Has
                    /**/
                     Auditoria($msgauditoria);   
                }
                // DB::rollback();
                DB::commit();
            }//Try Transaction
            catch(\Exception $e){
                // ROLLBACK
                DB::rollback();
                $mensaje=$e->getMessage();
                return redirect()->back()->withErrors([$mensaje])->withInput();    
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }

  public function getPersonas(){
        $id=Input::get("id");
        $personas=MovimientoComunicacionPersonasModel::join("users as u", "com_tmov_comunicacion_personas.id", "=", "u.id")
        ->select("com_tmov_comunicacion_personas.*", "u.name")
        ->where("id_com_cab",$id)->lists("u.name","idusuario")->all();
        return $personas;
  }
  
  public function getPresupuesto(){
        $id=Input::get("id");
        $CabPresupuesto=CabPresupuestoModel::join("parroquia as p", "com_tmov_presupuesto_cab.id_parroquia", "=", "p.id")
        ->select("p.parroquia", "com_tmov_presupuesto_cab.id_parroquia", "com_tmov_presupuesto_cab.monto_inicial", "com_tmov_presupuesto_cab.monto_final", "com_tmov_presupuesto_cab.observacion")
        ->where("id_com_cab",$id)
        ->orderby("p.parroquia")
        ->get();
        //show($barrio);

        //$presupuesto=CabPresupuestoModel::where("id_com_cab",$id)->orderBy("created_at")->lists("id_parroquia","monto_inicial", "monto_final", "observacion")->get();
        
        return $CabPresupuesto;
  }
  
  public function getBeneficiarios(){
        $id=Input::get("id");
        $CabPresupuesto=MovComunicacionParroquiaModel::join("com_tmov_comunicacion_barrios as b", "com_tmov_comunicacion_parroquia.id", "=", "b.id_cab_parroquia")
        ->select("com_tmov_comunicacion_parroquia.id_parroquia as parroquia_id", "com_tmov_comunicacion_parroquia.calles", "b.id_barrio", "b.avance", "b.idusuario",
            DB::raw("(select parroquia from parroquia where id=com_tmov_comunicacion_parroquia.id_parroquia) as parroquia"),
            DB::raw("(select barrio from barrio where id=b.id_barrio) as barrioName")
            )
        ->where("id_com_cab",$id)
        ->where("b.estado","ACT")
        ->orderby("com_tmov_comunicacion_parroquia.id_parroquia")
        ->get();
        //show($barrio);

        //$presupuesto=CabPresupuestoModel::where("id_com_cab",$id)->orderBy("created_at")->lists("id_parroquia","monto_inicial", "monto_final", "observacion")->get();
        
        return $CabPresupuesto;
  }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::beginTransaction();
        try{
            $tabla=cabComunicacionModel::find($id);
            $tabla->estado='INA';
            $tabla->save();
            // demas modelos

            // MovimientoComunicacionPersonasModel
            // CabPresupuestoModel
            // PresupuestoDetalleModel
            // MovComunicacionParroquiaModel

            //->update(array('estado' => 'INACTIVO'));
        DB::commit();
        }//Try Transaction
        catch(\Exception $e){
            // ROLLBACK
            DB::rollback();
            Session::flash('message', 'El registro no pudo ser elminado!');
            return Redirect::to($this->configuraciongeneral[1]);   
        }
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }

    public function getTimeLineCab($id,$tipo){
        switch($tipo){
            case 1:
            $tabla=MovComunicacionParroquiaModel::
            join("com_tmov_comunicacion_barrios as a","a.id_cab_parroquia","=","com_tmov_comunicacion_parroquia.id")
            ->join("parroquia as p","p.id","=","com_tmov_comunicacion_parroquia.id_parroquia")
            ->join("users as u","u.id","=","a.idusuario")
            ->join("barrio as b","b.id","=","a.id_barrio")
            ->select("com_tmov_comunicacion_parroquia.id","p.parroquia","b.barrio","a.avance","u.name","a.updated_at")
            ->where("com_tmov_comunicacion_parroquia.id_com_cab",$id)
            ->orderBy("com_tmov_comunicacion_parroquia.updated_at","ASC")
            //->groupby("com_tmov_comunicacion_parroquia.id","com_tmov_comunicacion_parroquia.id_parroquia")
            ->get();
            //show($tabla);
            return $tabla;
            break;
            case 2:
            $personas[]=[];
            $tabla==MovComunicacionParroquiaModel::
            select()
            ->where("id_com_cab",$idcab)
            ->where("id_parroquia",$value->parroquia_id)->first();
       
            return $tabla;
            break;
        }
    }
}