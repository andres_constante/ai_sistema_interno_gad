<?php namespace Modules\Comunicacionalcaldia\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Modules\Comunicacionalcaldia\Entities\barrioModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use App\PerfilModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Auth;
use App\UsuariosModel;




class barrioController extends Controller {
	
	var $configuraciongeneral = array ("Asignar barrios", "comunicacionalcaldia/barrios", "index",6=>"comunicacionalcaldia/barrioajax");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[ 
		{"Tipo":"text","Descripcion":"Nombre del barrio","Nombre":"barrio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Parroquia ","Nombre":"id_parroquia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }
				  ]'; 
				  

		//https://jqueryvalidation.org/validate/
		var $validarjs =array(
			"barrio"=>"parroquia: {
				required: true
			}",
			"barrio"=>"parroquia: {
				required: true
			}"
		);
    public function __construct() {
        $this->middleware('auth');
    } 


    public function getBarrio()
    {
        $id=Input::get("parroquia_id");
       // $barrio=barrioModel::where("id_parroquia",$id)->orderBy("barrio")->lists("barrio","id")->all();
        //show($barrio);
        return $barrio;
    }

	public function index()
	{

		$objetos=json_decode($this->objetos);
		$objetos[1]->Nombre="parroquia";
		// $tabla = barrioModel::join("parroquia as p","barrio.id_parroquia","=","p.id")
		// 			->select("barrio.*","p.parroquia")
		// 			->where("barrio.estado","ACT")
        //             ->get();
        $objetos=array_values($objetos);

        //para obtener el tipo de usuario
        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
        
        $delete='';
        $create='';
        switch($id_tipo_pefil->tipo){
            case 1:
            $delete='si';
            $create='si';
            $edit='';
            break;
            case 2:
            $delete='no';
            $create='no';
            $edit='';
            break;
            
            case 3:
            $delete='si';
            $create='si';
            $edit='';
            break;
            
            case 4:
            $delete='si';
            $create='si';
            $edit='';
            break;
            
        }
        $tabla=[];
		return view('vistas.index',[
				"objetos"=>$objetos,
				"tabla"=>$tabla,
				"configuraciongeneral"=>$this->configuraciongeneral,
                "delete"=>$delete,
                "create"=>$create
				]);
    }
    
    public function barrioajax(Request $request)
    {
      $columns = array( 
                            0 =>'id', 
                            1 =>'barrio',     
                            2 =>'parroquia',
                            3=> 'acciones',
                        );
  
        $totalData = barrioModel::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = barrioModel::
            select("barrio.*","p.parroquia")
            ->join("parroquia as p","p.id","=","barrio.id_parroquia")
            ->where("p.estado","ACT") 
                       
                        ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $posts =  barrioModel::
                            select("barrio.*","p.parroquia")
                            ->join("parroquia as p","p.id","=","barrio.id_parroquia")
                            ->where("p.estado","ACT")
                            ->where('id','LIKE',"%{$search}%")
                            ->orWhere('barrio', 'LIKE',"%{$search}%")
                            ->orWhere('parroquia', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = barrioModel::
                            select("barrio.*","p.parroquia")
                            ->join("parroquia as p","p.id","=","barrio.id_parroquia")
                            ->where("p.estado","ACT")
                            ->where('id','LIKE',"%{$search}%")
                            ->orWhere('barrio', 'LIKE',"%{$search}%")
                            ->orWhere('parroquia', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            //show($posts);
            foreach ($posts as $post)
            {
                $aciones=link_to_route('usuarios.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                link_to_route('usuarios.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.$post->id.')"><i class="fa fa-trash"></i></a> ';


                $nestedData['id'] = $post->id;
                $nestedData['barrio'] = $post->barrio;
                $nestedData['parroquia'] = $post->parroquia;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
              
            }
        }
        //show($data);
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
        
        return response()->json($json_data); 
    }
      
	public function guardar($id)
        {         
            $input=Input::all();
            
            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new barrioModel;
                $msg="Registro Creado Exitosamente...!";
                $msgauditoria="Registro Variable de Configuración";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= barrioModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición Variable de Configuración";
            }
            
            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, barrioModel::rules($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
            }else {
                foreach($input as $key => $value)
                {
                    
                    if($key != "_method" && $key != "_token")
                    {
                        
                        $guardar->$key = $value;
                    
                    }                        
                 }
                 
                 $guardar->save();
                 /*Permisos por Perfil*/
                        
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->barrio));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
	

	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $objetos=json_decode($this->objetos);
        $objetos[1]->Nombre="parroquia";
        $objetos=array_values($objetos);
        //show($objetos);
        $tabla = barrioModel::join("parroquia as p","barrio.id_parroquia","=","p.id")
					->select("barrio.*","p.parroquia")
					->where("barrio.estado","ACT")
					->first();
		return view('vistas.show',[
				"objetos"=>$objetos,
				"tabla"=>$tabla,
				"configuraciongeneral"=>$this->configuraciongeneral
				]);
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
		$this->configuraciongeneral[2]="crear";
		$seleccionar_parroquia=parroquiaModel::where("estado", "ACT")->lists("parroquia","id")->all();
		$objetos=json_decode($this->objetos);
		$objetos[1]->Valor=$this->escoja + $seleccionar_parroquia;
		return view('vistas.create',[
				"objetos"=>$objetos,
				"configuraciongeneral"=>$this->configuraciongeneral,
				"validarjs"=>$this->validarjs
		]);
	}

	    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
	}
	

	/*
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->configuraciongeneral[2]="editar";
        $seleccionar_parroquia=parroquiaModel::where("estado", "ACT")->lists("parroquia","id")->all();
        $objetos=json_decode($this->objetos);
        $objetos[1]->Valor=$this->escoja + $seleccionar_parroquia;
        $tabla=barrioModel::find($id);
        return view('vistas.create',[
                "tabla"=>$tabla,
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
	}
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla=barrioModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}