<?php namespace Modules\Comunicacionalcaldia\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\PerfilModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Auth;
use App\UsuariosModel;

use Modules\Comunicacionalcaldia\Entities\parroquiaModel;

class parroquiaController extends Controller {
	
	var $configuraciongeneral = array ("Registro de parroquias", "comunicacionalcaldia/parroquias", "Index",6=>"comunicacionalcaldia/parroquiaajax");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[ 
        {"Tipo":"text","Descripcion":"Nombre de la parroquia","Nombre":"parroquia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Zona","Nombre":"zona","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
		]'; 
		//https://jqueryvalidation.org/validate/
		var $validarjs =array(
			"parroquia"=>"parroquia: {
				required: true
			}",
            "zona"=>"zona: {
                required: true
            }"

		);
		public function __construct() {
			$this->middleware('auth');
		}
        public function getparroquiafiltro()
        {
            $json=json_decode(Input::get("zona"),true);            
            $varid=array();
            foreach ($json as $key => $value) {
                # code...                
                $varid[]=$value["id_parroquia"];
            }
            $parro=parroquiaModel::whereIn("id",$varid)->lists("parroquia","id")->all();
            return $parro;
            //show($parro);
        }

        public function getparroquia()
        {
            $id=Input::get("zona");
            if($id=="URBANA/RURAL")
                $parro=parroquiaModel::lists("parroquia","id")->all();
            else
                $parro=parroquiaModel::where("zona",$id)->lists("parroquia","id")->all();
            return $parro;
        }
		/**
		* Display a listing of the resource.
		*
		* @return \Illuminate\Http\Response
		*/
		public function index()
		{
            //
           //para obtener el tipo de usuario
           $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
            $delete='';
            $create='';
            $edit='';
            switch($id_tipo_pefil->tipo){
				case 1:
				$delete='si';
				$create='si';
				$edit='';
				break;
				case 2:
				$delete='no';
				$create='no';
				//$edit='';
				break;
				
				case 3:
				$delete='si';
				$create='si';
				//$edit='';
				break;
				
				case 4:
				$delete='si';
				$create='si';
				//$edit='';
				break;
				
			}
            //$tabla=parroquiaModel::where("estado","ACT")->orderby("id","desc")->get();//->paginate(500);
            $tabla=[];
			return view('vistas.index',[
					"objetos"=>json_decode($this->objetos),
					"tabla"=>$tabla,
					"configuraciongeneral"=>$this->configuraciongeneral,
                    "delete"=>"yes",
                    "create"=>$create,
                   "delete"=>$delete
					]);
        } 
        
    public function parroquiaajax(Request $request)
        {
            $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
          $columns = array( 
                                0 =>'id', 
                                1 =>'parroquia',     
                                2 =>'zona',
                                3=> 'acciones',
                            );
      
            $totalData = parroquiaModel::count();
                
            $totalFiltered = $totalData; 
    
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
                
            if(empty($request->input('search.value')))
            {            
                $posts = parroquiaModel::where("estado","ACT")   
                            ->offset($start)
                             ->limit($limit)
                             ->orderBy($order,$dir)
                             ->get();
            }
            else {
                $search = $request->input('search.value'); 
    
                $posts =  parroquiaModel::where("estado","ACT")
                                ->where('id','LIKE',"%{$search}%")
                                ->orWhere('parroquia', 'LIKE',"%{$search}%")
                                ->orWhere('zona', 'LIKE',"%{$search}%")
                                ->offset($start)
                                ->limit($limit)
                                ->orderBy($order,$dir)
                                ->get();
    
                $totalFiltered = parroquiaModel::where("estado","ACT")
                                ->where('id','LIKE',"%{$search}%")
                                ->orWhere('parroquia', 'LIKE',"%{$search}%")
                                ->orWhere('zona', 'LIKE',"%{$search}%")
                                 ->count();
            }
    
            $data = array();
            if(!empty($posts))
            {
                //show($posts);
                foreach ($posts as $post)
                {
                    switch($id_tipo_pefil->tipo){
                        case 1:
                       
                        $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                        link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.$post->id.')"><i class="fa fa-trash"></i></a> 
                        <div style="display: none;">
                        <form method="POST" action="parroquias/'.$post->id.'" accept-charset="UTF-8" id="frmElimina'.$post->id.'" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                            <input name="_token" type="hidden" value="'.csrf_token().'">
                            <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                        </form>
                        </div>';
                        break;
                        case 2:
                        $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                        link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o'));
        
                        break;
            
                        case 3:
                        
                        $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                        link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o'));
                              
                                 
                        break;
            
                        case 4:
                        
                        $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                        link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.$post->id.')"><i class="fa fa-trash"></i></a> 
                        <div style="display: none;">
                        <form method="POST" action="parroquias/'.$post->id.'" accept-charset="UTF-8" id="frmElimina'.$post->id.'" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                            <input name="_token" type="hidden" value="'.csrf_token().'">
                            <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                        </form>
                        </div>';
                       
                        break;
            
                    }  
    
                    $nestedData['id'] = $post->id;
                    $nestedData['parroquia'] = $post->parroquia;
                    $nestedData['zona'] = $post->zona;
                    $nestedData['acciones'] = $aciones;
                    $data[] = $nestedData;
                  
                }
            }
            //show($data);
            $json_data = array(
                        "draw"            => intval($request->input('draw')),  
                        "recordsTotal"    => intval($totalData),  
                        "recordsFiltered" => intval($totalFiltered), 
                        "data"            => $data   
                        );
            
            return response()->json($json_data); 
        }
     

		public function guardar($id)
    {         
           $input=Input::all();

            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new parroquiaModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro Variable de Configuración";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= parroquiaModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición Variable de Configuración";
            }

            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, parroquiaModel::rules($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                 foreach($input as $key => $value)
                 {
                   
                    if($key != "_method" && $key != "_token")
                    {
                        $guardar->$key = $value;
                    }                        
                 }

                 $guardar->save();
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->parroquia));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
	
  
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
	  //
    $objetos=json_decode($this->objetos);
    $zona=array("URBANA"=>"URBANA","RURAL"=>"RURAL");
    $objetos[1]->Valor=$this->escoja+$zona;
	  $this->configuraciongeneral[2]="crear";
	  return view('vistas.create',[
		  "objetos"=>$objetos,
		  "configuraciongeneral"=>$this->configuraciongeneral,
		  "validarjs"=>$this->validarjs
		  ]);
		}


			  /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
	}
	
  /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tabla = parroquiaModel::find($id);
        return view('vistas.show',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
	}
	
	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $objetos=json_decode($this->objetos);
        $zona=array("URBANA"=>"URBANA","RURAL"=>"RURAL");
        $objetos[1]->Valor=$this->escoja+$zona;
        $this->configuraciongeneral[2]="editar";
        $tabla = parroquiaModel::find($id);
        return view('vistas.create',[
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "tabla"=>$tabla,
                "validarjs"=>$this->validarjs
                ]);
    }
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
	}

	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla=parroquiaModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
	}