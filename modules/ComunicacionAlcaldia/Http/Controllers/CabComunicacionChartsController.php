<?php namespace Modules\Comunicacionalcaldia\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Route;
use Auth;
use App\PerfilModel;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use App\UsuariosModel;
use Modules\Comunicacionalcaldia\Entities\cabComunicacionModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;

class CabComunicacionChartsController extends Controller {
	var $configuraciongeneral = array ("MONITOREO Y CONTROL", "comunicacionalcaldia/comunicaciongraficos", "index",6=>"null");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[
        {"Tipo":"chart","Descripcion":"GRÁFICO DE ACTIVIDADES","Nombre":"chart2","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"chart2","Descripcion":"GRÁFICO DE ACTIVIDADES POR DIRECCIÓN","Nombre":"chart4","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"chart","Descripcion":"GRÁFICO DE ACTIVIDADES POR DIRECCIONES","Nombre":"chart3","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';

    
		public function __construct() {
			$this->middleware('auth');
		}


		public function index()
		{
			
			$objetos = json_decode($this->objetos);
			$objetos=array_values($objetos);
			
			//show($objetos);
		//para obtener el tipo de usuario
		$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
		$id_usuario=Auth::user()->id;
		
		
			$delete='';
			$create='';
			$edit='';
			switch($id_tipo_pefil->tipo){
				case 1:
				$direcciones=direccionesModel::select('id','direccion')->where('estado','=','ACT')->orderby("direccion","asc")->get();
				$delete='si';
				$create='si';
				$edit='';
				break;
				case 2:
				$direcciones=direccionesModel::
				join("users as u","u.id_direccion","=","tmae_direcciones.id")
				->select('tmae_direcciones.id','tmae_direcciones.direccion')->where([['tmae_direcciones.estado','=','ACT'],["u.id",$id_usuario]])->orderby("direccion","asc")->get();
				$delete='no';
				$create='no';
				$edit='';
				break;
				
				case 3:
				$direcciones=direccionesModel::
				join("users as u","u.id_direccion","=","tmae_direcciones.id")
				->select('tmae_direcciones.id','tmae_direcciones.direccion')->where([['tmae_direcciones.estado','=','ACT'],["u.id",$id_usuario]])->orderby("direccion","asc")->get();
				$delete='si';
				$create='si';
				$edit='';
				break;
				
				case 4:
				$direcciones=direccionesModel::select('id','direccion')->where('estado','=','ACT')->orderby("direccion","asc")->get();
				$delete='si';
				$create='si';
				$edit='';
				break;
				
			}	
			return view('vistas.indexchart',[
				"objetos"=>$objetos,
				"direcciones"=>$direcciones,
				"configuraciongeneral"=>$this->configuraciongeneral,
				"create"=>$create,
                 "delete"=>$delete
				]);
		}


		public function getValoresCharts(){
			$tipo_chart=Input::get("tipo");
			$direccion=Input::get("direccion");
			$valores;
			$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
			$id_usuario=Auth::user()->id;
			switch($tipo_chart){
				case '2':
				switch($id_tipo_pefil->tipo){
					case 1:
					$valores=cabComunicacionModel::select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
					->where("com_tmov_comunicacion_cab.estado", "ACT")->groupby("estado_actividad")->get();
					return $valores;
					break;
					
					case 2:
					break;
					
					case 3:
					$valores=cabComunicacionModel::select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
					->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
					->join("users as u", "u.id_direccion", "=", "dir.id")
					->where([["com_tmov_comunicacion_cab.estado", "ACT"],["u.id",$id_usuario]])->groupby("estado_actividad")->get();
					return $valores;

					break;
					
					case 4:
					$valores=cabComunicacionModel::select(DB::raw('count(estado_actividad) as valor, estado_actividad'))
					->where("com_tmov_comunicacion_cab.estado", "ACT")->groupby("estado_actividad")->get();
					return $valores;
					break;
					
				}

				
				break;
				case '3':
				switch($id_tipo_pefil->tipo){
					case 1:
					$valores=cabComunicacionModel::join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
					->select(DB::raw('count(com_tmov_comunicacion_cab.id_direccion) as valor, dir.direccion'))
					->where("com_tmov_comunicacion_cab.estado", "ACT")->groupby("com_tmov_comunicacion_cab.estado_actividad")->get();
					return $valores;
					break;
					case 2:
					break;
					$valores=cabComunicacionModel::join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
					->join("users as u", "u.id_direccion", "=", "dir.id")
					->select(DB::raw('count(com_tmov_comunicacion_cab.id_direccion) as valor, dir.direccion'))
					->where([["com_tmov_comunicacion_cab.estado", "ACT"],["u.id",$id_usuario]])
					->groupby("com_tmov_comunicacion_cab.estado_actividad")->get();
					//show($id_usuario);
					return $valores;
					case 3:
					$valores=cabComunicacionModel::join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
					->join("users as u", "u.id_direccion", "=", "dir.id")
					->select(DB::raw('count(com_tmov_comunicacion_cab.id_direccion) as valor, dir.direccion'))
					->where([["com_tmov_comunicacion_cab.estado", "ACT"],["u.id",$id_usuario]])
					->groupby("com_tmov_comunicacion_cab.estado_actividad")->get();

					//show($id_usuario);
					return $valores;
					break;
					
					case 4:
					$valores=cabComunicacionModel::join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
					->select(DB::raw('count(com_tmov_comunicacion_cab.id_direccion) as valor, dir.direccion'))
					->where("com_tmov_comunicacion_cab.estado", "ACT")->groupby("com_tmov_comunicacion_cab.estado_actividad")->get();
					return $valores;
					break;
					
				}
					

				
				break;
				case '4':
				switch($id_tipo_pefil->tipo){
					case 1:
					$valores=cabComunicacionModel::join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
					->select(DB::raw('count(com_tmov_comunicacion_cab.estado_actividad) as valor, com_tmov_comunicacion_cab.estado_actividad'))
					->where([["com_tmov_comunicacion_cab.estado","ACT"],["com_tmov_comunicacion_cab.id_direccion",$direccion]])->groupby("com_tmov_comunicacion_cab.estado_actividad")->get();	
					return $valores;	
					break;
					case 2:
					$valores=cabComunicacionModel::join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
					->select(DB::raw('count(com_tmov_comunicacion_cab.estado_actividad) as valor, com_tmov_comunicacion_cab.estado_actividad'))
					->where([["com_tmov_comunicacion_cab.estado","ACT"],["com_tmov_comunicacion_cab.id_direccion",$direccion]])->groupby("com_tmov_comunicacion_cab.estado_actividad")->get();	
					return $valores;
					break;
					
					case 3:
					$valores=cabComunicacionModel::join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
					->select(DB::raw('count(com_tmov_comunicacion_cab.estado_actividad) as valor, com_tmov_comunicacion_cab.estado_actividad'))
					->where([["com_tmov_comunicacion_cab.estado","ACT"],["com_tmov_comunicacion_cab.id_direccion",$direccion]])->groupby("com_tmov_comunicacion_cab.estado_actividad")->get();	
					return $valores;
					
					break;
					
					case 4:
					$valores=cabComunicacionModel::join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
					->select(DB::raw('count(com_tmov_comunicacion_cab.estado_actividad) as valor, com_tmov_comunicacion_cab.estado_actividad'))
					->where([["com_tmov_comunicacion_cab.estado","ACT"],["com_tmov_comunicacion_cab.id_direccion",$direccion]])->groupby("com_tmov_comunicacion_cab.estado_actividad")->get();	
					return $valores;
					break;
					
				}
				
				break;
				

				
		
				return response()->json($valores);
			}
		
	
	
		}
}