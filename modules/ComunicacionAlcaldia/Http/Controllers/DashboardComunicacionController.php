<?php namespace Modules\Comunicacionalcaldia\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\Comunicacionalcaldia\Entities\cabComunicacionModel;
use Modules\Comunicacionalcaldia\Entities\MovComunicacionParroquiaModel;
use DB;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use App\UsuariosModel;
use Route;
use Auth;
use Illuminate\Support\Facades\Input;

class DashboardComunicacionController extends Controller {
	var $configuraciongeneral = array ("DASHBOARD", "comunicacionalcaldia/dashboard", "index");
	public function index()
	{
	
	$fechainicial=date("Y-m-d")." 00:00:00";
	//$fechainicial="2019-06-01 00:00:00";
	$fechafinal=date("Y-m-d")." 23:59:59";


	$modificacionesHoy=cabComunicacionModel::
		join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
		->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
		->join("users as u","u.id","=","com_tmov_comunicacion_cab.idusuario")
		->join("ad_perfil as p","u.id_perfil","=","p.id")
		->select('dir.direccion',DB::raw("count(*) as total"))
		->whereBetween('com_tmov_comunicacion_cab.updated_at', [$fechainicial,$fechafinal])
		->where("p.tipo","<>",3)
		->where("p.tipo","<>",1)
		->groupBy("dir.direccion")
		->get();




	$datos=cabComunicacionModel::select(DB::raw("count(id) as total, estado_actividad,'0' as color,0 as porcentaje"))
	->groupBy("estado_actividad")
	->where("estado", "ACT")
	->get();

	$total=cabComunicacionModel::select(DB::raw("count(id) as total"))
	->where("estado","ACT")
	->first();		
	
	$iboxComunicaciones=$this->getComunicaciones($datos,$total);
	$comunicacionesActualizadas=$this->getTimeLineCab(0,1);

		$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
        $id_usuario=Auth::user()->id;
        if($id_tipo_pefil->tipo ==2 ||$id_tipo_pefil->tipo==3 ){

			$direcciones=cabComunicacionModel::select(DB::raw("dir.id,direccion"))
			->join("tmae_direcciones as dir","dir.id","=","com_tmov_comunicacion_cab.id_direccion")
			->join("users as us","us.id_direccion","=","dir.id")
			->where([['dir.estado','=','ACT'],["us.id",$id_usuario]])
			->groupBy("com_tmov_comunicacion_cab.id_direccion")
			->orderby("direccion","asc")
			->get();
           
        }else{

			$direcciones=cabComunicacionModel::select(DB::raw("dir.id,direccion"))
			->join("tmae_direcciones as dir","dir.id","=","com_tmov_comunicacion_cab.id_direccion")
			->join("users as us","us.id_direccion","=","dir.id")
			->where([['dir.estado','=','ACT']])
			->groupBy("com_tmov_comunicacion_cab.id_direccion")
			->orderby("direccion","asc")
			->get();
            //show($direcciones);
		}


	
	return view('vistas.dashboard.dashboardcomunicacion',[
		"direcciones"=>$direcciones,
		"comunicacionesActualizadas"=>$comunicacionesActualizadas,
		"modificacionesHoy"=>$modificacionesHoy,
		"iboxComunicaciones"=>$iboxComunicaciones,
		"configuraciongeneral"=>$this->configuraciongeneral,
		"total"=>$total

		]);
	}
	
	public function getTimeLineCab($id,$tipo){
        switch($tipo){
            case 1:
			$tabla=MovComunicacionParroquiaModel::
			join("com_tmov_comunicacion_cab as com_tmov_comunicacion_cab", "com_tmov_comunicacion_parroquia.id_com_cab", "=", "com_tmov_comunicacion_cab.id")
			->join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
            ->join("com_tmov_comunicacion_barrios as a","a.id_cab_parroquia","=","com_tmov_comunicacion_parroquia.id")
            ->join("parroquia as p","p.id","=","com_tmov_comunicacion_parroquia.id_parroquia")
            ->join("users as u","u.id","=","a.idusuario")
            ->join("barrio as b","b.id","=","a.id_barrio")
            ->select("com_tmov_comunicacion_parroquia.id","com_tmov_comunicacion_cab.nombre_actividad","p.parroquia","b.barrio","a.avance","u.name","a.updated_at")
            //->where("com_tmov_comunicacion_parroquia.id_com_cab",$id)
            ->orderBy("com_tmov_comunicacion_parroquia.updated_at","ASC")
            //->groupby("com_tmov_comunicacion_parroquia.id","com_tmov_comunicacion_parroquia.id_parroquia")
            ->get();
            //show($tabla);
            return $tabla;
            break;
           
		}
	}
		


	public function getComunicaciones($datos,$total){

		$estadoobras=explode("|",ConfigSystem("estadoobras"));
		$estadoobrascolores=explode("|",ConfigSystem("estadoobrascolor"));
		
	
		foreach($datos as $item){
			switch($item->estado_actividad){

				case $estadoobras[0]:
					$item->estado_actividad='EJECUTADO VENCIDO';
					$item->color=$estadoobrascolores[0];
					$item->porcentaje=round((($item->total/$total->total)*100),2);

				break;
				case $estadoobras[1]:
					$item->estado_actividad='EJECUTADO';
					$item->color=$estadoobrascolores[1];
					$item->porcentaje=round((($item->total/$total->total)*100),2);
				break;
				case $estadoobras[2]:
					$item->estado_actividad='EJECUCIÓN';
					$item->color=$estadoobrascolores[2];
					$item->porcentaje=round((($item->total/$total->total)*100),2);

				break;
				case $estadoobras[3]:
					$item->estado_actividad='POR EJECUTAR';
					$item->color=$estadoobrascolores[3];
					$item->porcentaje=round((($item->total/$total->total)*100),2);

				break;
				case $estadoobras[4]:
					$item->color=$estadoobrascolores[4];
					$item->porcentaje=round((($item->total/$total->total)*100),2);
				break;

			}


		}
		return $datos;
		
	}
	public function getChartLine(){
		$id_direccion= Input::get('id');
		$tipo= Input::get('tipo');
		//show($tipo);
		$datos=null;
		if($tipo==1){
			$direcciones=direccionesModel::select("id","direccion")->where("estado", "ACT")->get(); 
			foreach($direcciones as $direccion){
				$EJECUCION=cabComunicacionModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","EJECUCION"],["id_direccion",$direccion->id]])
				->first();
				
				$EJECUTADO=cabComunicacionModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","EJECUTADO"],["id_direccion",$direccion->id]])
				->first();
				$EJECUTADO_VENCIDO=cabComunicacionModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","EJECUTADO_VENCIDO"],["id_direccion",$direccion->id]])
				->first();
				$POR_EJECUTAR=cabComunicacionModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","POR_EJECUTAR"],["id_direccion",$direccion->id]])
				->first();
				$SUSPENDIDO=cabComunicacionModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","SUSPENDIDO"],["id_direccion",$direccion->id]])
				->first();
				
				$datos[]=[$direccion->direccion,$EJECUCION->total,$EJECUTADO->total,$EJECUTADO_VENCIDO->total,$POR_EJECUTAR->total,$SUSPENDIDO->total];
				
			}
			
		}else if($tipo==2){
			$direcciones=direccionesModel::select("id","direccion")->where([["estado", "ACT"],["id",$id_direccion]])->get(); 
			$EJECUCION=cabComunicacionModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","EJECUCION"],["id_direccion",$id_direccion]])
				->first();

				$EJECUTADO=cabComunicacionModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","EJECUTADO"],["id_direccion",$id_direccion]])
				->first();
				$EJECUTADO_VENCIDO=cabComunicacionModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","EJECUTADO_VENCIDO"],["id_direccion",$id_direccion]])
				->first();
				$POR_EJECUTAR=cabComunicacionModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","POR_EJECUTAR"],["id_direccion",$id_direccion]])
				->first();
				$SUSPENDIDO=cabComunicacionModel::select(DB::raw("count(id) as total"))
				->where([["estado","ACT"],["estado_actividad","SUSPENDIDO"],["id_direccion",$id_direccion]])
				->first();
				
				$datos[]=[$direcciones[0]->direccion,$EJECUCION->total,$EJECUTADO->total,$EJECUTADO_VENCIDO->total,$POR_EJECUTAR->total,$SUSPENDIDO->total];
		}
		else if($tipo==3){



			$avances=MovComunicacionParroquiaModel::
			join("com_tmov_comunicacion_cab as com_tmov_comunicacion_cab", "com_tmov_comunicacion_parroquia.id_com_cab", "=", "com_tmov_comunicacion_cab.id")
            ->select("com_tmov_comunicacion_cab.id_direccion")
            ->groupBy("com_tmov_comunicacion_cab.id_direccion")
			->get();
			


			foreach($avances as $item){
				//show($item->id);
				$valor=MovComunicacionParroquiaModel::select(DB::raw("dir.direccion, avg(com_tmov_comunicacion_parroquia.avance) as porcentaje"))
				->join("com_tmov_comunicacion_cab as com_tmov_comunicacion_cab", "com_tmov_comunicacion_parroquia.id_com_cab", "=", "com_tmov_comunicacion_cab.id")
				->join("tmae_direcciones as dir","dir.id","=","com_tmov_comunicacion_cab.id_direccion")
				->where([["com_tmov_comunicacion_cab.estado","ACT"],["dir.id",$item->id]])
				->first();
				if($valor->porcentaje==0){
					$datos[]=[$valor->direccion,null];
				}else{
					$datos[]=[$valor->direccion,round($valor->porcentaje,2)];
				}				
				
				
			}
			

		}
		else if($tipo==4){
			$fechainicial=date("Y-m-d")." 00:00:00";
		//$fechainicial="2019-06-01 00:00:00";
		$fechafinal=date("Y-m-d")." 23:59:59";
			$modificacionesHoy=MovComunicacionParroquiaModel::
				join("com_tmov_comunicacion_cab as com_tmov_comunicacion_cab", "com_tmov_comunicacion_parroquia.id_com_cab", "=", "com_tmov_comunicacion_cab.id")
				->join("tmae_direcciones as dir","dir.id","=","com_tmov_comunicacion_cab.id_direccion")
				->join("users as u","u.id","=","com_tmov_comunicacion_cab.idusuario")
				->join("ad_perfil as p","u.id_perfil","=","p.id")
				->select("dir.direccion",DB::raw("count(*) as total"))
				->whereBetween("com_tmov_comunicacion_cab.updated_at",[$fechainicial,$fechafinal])
				->where("p.tipo","<>",3)
				->where("p.tipo","<>",1)
				->groupBy("dir.direccion")
				->get();
			
			
			
			foreach($modificacionesHoy as $item){

				$datos[]=[$item["direccion"],$item["total"]];
			}

		
		}
	
		return response()->json($datos);
		
	}

}