<?php namespace Modules\Comunicacionalcaldia\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\PerfilModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Auth;
use App\UsuariosModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;

class direccionesController extends Controller {
	
	var $configuraciongeneral = array ("Direcciones Municipales", "comunicacionalcaldia/direcciones", "index",6=>"comunicacionalcaldia/direcionesajax");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[ 
        {"Tipo":"text","Descripcion":"Dirección","Nombre":"direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Alias","Nombre":"alias","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
		]'; 
		//https://jqueryvalidation.org/validate/
		var $validarjs =array(
			"direccion"=>"direccion: {
				required: true
			}"
		);
		public function __construct() {
			$this->middleware('auth');
		} 
		
		public function getusuariosdireccion(){
            $id=Input::get("id");
            $tabla=UsuariosModel::where("estado", "ACT")->where("id_direccion",$id)->lists("name","id")->all();
            return $tabla;
        }
		/**
		* Display a listing of the resource.
		*
		* @return \Illuminate\Http\Response
		*/
	   public function index()
	   {
           //
           $usuario=Auth::user();
           //para obtener el tipo de usuario
           $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
            $delete='';
            $create='';
            $edit='';
           switch($id_tipo_pefil->tipo){
               case 1:
               $delete='si';
               $create='si';
               break;
               case 2:
               $delete='no';
               $create='no';
               break;
               
               case 3:
               //$delete='si';
               $create='si';
               break;
               
               case 4:
               $delete='si';
               $create='si';
               break;
   
           }
           //$tabla=direccionesModel::where("estado","ACT")->orderby("id","asc")->get();//->paginate(500);
           
           $tabla=[];
		   return view('vistas.index',[
				   "objetos"=>json_decode($this->objetos),
				   "tabla"=>$tabla,
				   "configuraciongeneral"=>$this->configuraciongeneral,
                   "delete"=>"yes", 
                   "create"=>$create,
                   "delete"=>$delete
				   ]);
       } 
       

       public function direcionesajax(Request $request)
       {
        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
         $columns = array( 
                               0 =>'id', 
                               1 =>'direccion',
                               2 =>'alias',     
                               3=> 'acciones',
                           );
     
           $totalData = direccionesModel::count();
               
           $totalFiltered = $totalData; 
   
           $limit = $request->input('length');
           $start = $request->input('start');
           $order = $columns[$request->input('order.0.column')];
           $dir = $request->input('order.0.dir');
               
           if(empty($request->input('search.value')))
           {            
               $posts = direccionesModel::where("estado","ACT")   
                           ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
           }
           else {
               $search = $request->input('search.value'); 
   
               $posts =  direccionesModel::where("estado","ACT")
                               ->where('id','LIKE',"%{$search}%")
                               ->orWhere('direccion', 'LIKE',"%{$search}%")
                               ->offset($start)
                               ->limit($limit)
                               ->orderBy($order,$dir)
                               ->get();
   
               $totalFiltered = direccionesModel::where("estado","ACT")
                               ->where('id','LIKE',"%{$search}%")
                               ->orWhere('direccion', 'LIKE',"%{$search}%")
                                ->count();
           }
   
           $data = array();
           if(!empty($posts))
           {
               //show($posts);
               foreach ($posts as $post)
               {
                switch($id_tipo_pefil->tipo){
                    case 1:
                   
                    $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                    link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.$post->id.')"><i class="fa fa-trash"></i></a> 
                    <div style="display: none;">
                    <form method="POST" action="direcciones/'.$post->id.'" accept-charset="UTF-8" id="frmElimina'.$post->id.'" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="'.csrf_token().'">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                    break;
                    case 2:
                    $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                    link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o'));
    
                    break;
        
                    case 3:
                    
                    $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                    link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o'));
                          
                             
                    break;
        
                    case 4:
                    
                    $aciones=link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                    link_to_route(str_replace("/",".",$this->configuraciongeneral[1]).'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.$post->id.')"><i class="fa fa-trash"></i></a> 
                    <div style="display: none;">
                    <form method="POST" action="direcciones/'.$post->id.'" accept-charset="UTF-8" id="frmElimina'.$post->id.'" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="'.csrf_token().'">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                   
                    break;
        
                }  
   
   
                   $nestedData['id'] = $post->id;
                   $nestedData['direccion'] = $post->direccion;
                   $nestedData['alias'] = $post->alias;
                   $nestedData['acciones'] = $aciones;
                   $data[] = $nestedData;
                 
               }
           }
           //show($data);
           $json_data = array(
                       "draw"            => intval($request->input('draw')),  
                       "recordsTotal"    => intval($totalData),  
                       "recordsFiltered" => intval($totalFiltered), 
                       "data"            => $data   
                       );
           
           return response()->json($json_data); 
       }
	
	public function guardar($id)
    {         
           $input=Input::all();

            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new direccionesModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro Variable de Configuración";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= direccionesModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición Variable de Configuración";
            }

            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, direccionesModel::rules($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                 foreach($input as $key => $value)
                 {
                   
                    if($key != "_method" && $key != "_token")
                    {
                        $guardar->$key = $value;
                    }                        
                 }

                 $guardar->save();
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->direccion));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
	

	 /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $this->configuraciongeneral[2]="crear";
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
    }

	  /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
	}
	
  /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tabla = direccionesModel::find($id);
        return view('vistas.show',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->configuraciongeneral[2]="editar";
        $tabla = direccionesModel::find($id);
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "tabla"=>$tabla,
                "validarjs"=>$this->validarjs
                ]);
    }
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }

	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $tabla=direccionesModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
	
}
