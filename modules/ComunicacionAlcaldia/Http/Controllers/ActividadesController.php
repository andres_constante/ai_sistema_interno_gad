<?php namespace Modules\Comunicacionalcaldia\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
use Auth;
use App\PerfilModel;
use App\UsuariosModel;
use Modules\Comunicacionalcaldia\Entities\actividadesModel;


class actividadesController extends Controller {
	
	var $configuraciongeneral = array ("Registro de actividades", "comunicacionalcaldia/actividades", "index",6=>"comunicacionalcaldia/actividadesajax");
    var $escoja=array(null=>"Escoja opción...") ;
	var $objetos = '[ 
        {"Tipo":"text","Descripcion":"Descripción de actividad","Nombre":"nombre","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Presupuesto","Nombre":"presupuesto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Campos Ocultos","Nombre":"camposocultos","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
		]';  

	//https://jqueryvalidation.org/validate/
	var $validarjs =array(
		"nombre"=>"nombre: {
			required: true
		}"
	);
    public function getacticampos()
    {
        $id=Input::get("id");
        $cam="";
        if($id==0)
        {
            $tabla=actividadesModel::where("estado","ACT")->get();
            foreach ($tabla as $key => $value) {
                # code...
                if($value->camposocultos!="")
                    $cam.=$value->camposocultos."|";
            }
            $cam=substr($cam, 0, -1);
            $campos=explode("|",$cam);
        }else{
            $tabla=actividadesModel::find($id);
            $campos=explode("|",$tabla->camposocultos);
        }
        $res=$campos;
        //$res=array($tabla->toarray(),$campos);
        //show($res);
        return $res;
    }
    public function __construct() {
        $this->middleware('auth');
    } 

	public function index()
	{
        //$tabla=actividadesModel::where("estado","ACT")->orderby("id","desc")->get();//->paginate(500);
        $tabla=[];
		$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
        $delete='';
        $create='';
        $edit='';
        switch($id_tipo_pefil->tipo){
            case 1:
            $delete='si';
            $create='si';
            $edit='';
            break;
            case 2:
            $delete='no';
            $create='no';
            $edit='';
            break;
            
            case 3:
            $delete='si';
            $create='si';
            $edit='';
            break;
            
            case 4:
            $delete='si';
            $create='si';
            $edit='';
            break;
            
        }
        
		   return view('vistas.index',[
				   "objetos"=>json_decode($this->objetos),
				   "tabla"=>$tabla,
				   "configuraciongeneral"=>$this->configuraciongeneral,
                   "create"=>$create,
                   "delete"=>$delete
				   ]);
	}

    public function actividadesajax(Request $request)
    {
      $columns = array( 
                            0 =>'id', 
                            1 =>'nombre',     
                            2 =>'presupuesto',
                            3 =>'camposocultos',
                            4=> 'acciones',
                        );
  
        $totalData = actividadesModel::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = actividadesModel::
            where("estado","ACT")
                       
                        ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $posts =  actividadesModel::
                                where("estado","ACT")
                            ->where('id','LIKE',"%{$search}%")
                            ->orWhere('nombre', 'LIKE',"%{$search}%")
                            ->orWhere('presupuesto', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = actividadesModel::
                            where("estado","ACT")
                            ->where('id','LIKE',"%{$search}%")
                            ->orWhere('nombre', 'LIKE',"%{$search}%")
                            ->orWhere('presupuesto', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            //show($posts);
            foreach ($posts as $post)
            {
                $aciones=link_to_route('usuarios.show','', array($post->id), array('class' => 'fa fa-newspaper-o')).'&nbsp;&nbsp;'.
                link_to_route('usuarios.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.$post->id.')"><i class="fa fa-trash"></i></a> ';


                $nestedData['id'] = $post->id;
                $nestedData['nombre'] = $post->nombre;
                $nestedData['presupuesto'] = $post->presupuesto;
                $nestedData['camposocultos'] = $post->camposocultos;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
              
            }
        }
        //show($data);
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
        
        return response()->json($json_data); 
    }
	public function guardar($id)
    {         
           $input=Input::all();

            $ruta=$this->configuraciongeneral[1];
            
            if($id==0)
            {
                $ruta.="/create";
                $guardar= new actividadesModel;
                 $msg="Registro Creado Exitosamente...!";
                 $msgauditoria="Registro Variable de Configuración";
            }
            else{
                $ruta.="/$id/edit";
                $guardar= actividadesModel::find($id);
                $msg="Registro Actualizado Exitosamente...!";
                $msgauditoria="Edición Variable de Configuración";
            }

            $input=Input::all();
            $arrapas=array();
            
            $validator = Validator::make($input, actividadesModel::rules($id));
            
            if ($validator->fails()) {
                //die($ruta);
                return Redirect::to("$ruta")
                    ->withErrors($validator)
                    ->withInput();
            }else {
                 foreach($input as $key => $value)
                 {
                   
                    if($key != "_method" && $key != "_token")
                    {
                        $guardar->$key = $value;
                    }                        
                 }

                 $guardar->save();
                 Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->nombre));   
            }
           Session::flash('message', $msg);
           return Redirect::to($this->configuraciongeneral[1]);
  }
	


	 /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $sino=array("SI"=>"SI","NO"=>"NO");
        $objetos=json_decode($this->objetos);
        $objetos[1]->Valor=$this->escoja +  $sino;
        $this->configuraciongeneral[2]="crear";
        return view('vistas.create',[
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
                ]);
	}
	
	 /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
	}
	
  /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tabla = actividadesModel::find($id);
        return view('vistas.show',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
	}
	
	/**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $sino=array("SI"=>"SI","NO"=>"NO");
        $objetos=json_decode($this->objetos);
        $objetos[1]->Valor=$this->escoja +  $sino;
        $this->configuraciongeneral[2]="editar";
        $tabla = actividadesModel::find($id);
        return view('vistas.create',[
                "objetos"=>$objetos,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "tabla"=>$tabla,
                "validarjs"=>$this->validarjs
                ]);
    }
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
	}
	
	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla=actividadesModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
	 
}