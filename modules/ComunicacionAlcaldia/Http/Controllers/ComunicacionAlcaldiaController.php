<?php namespace Modules\Comunicacionalcaldia\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use App\MenuModulosModel;
use Route;
use Auth;
use DB;
use App\PerfilModel;
use App\ModulosModel;
use App\UsuariosModel;


class ComunicacionAlcaldiaController extends Controller {
	public function __construct() {
        $this->middleware('auth');
    } 
	public function index()
	{
		$rucu=Route::getCurrentRoute()->getPath();
        $vermodulo=explode("/",$rucu);
       
        $mod=ModulosModel::where("ruta",$vermodulo[0])->first();
        
        //show($mod);
        $usuario=Auth::user();
       //para obtener el tipo de usuario
       $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo","ap.id")->where("users.id",Auth::user()->id)->first();


       
        $delete='';
        $create='';
        $edit='';
        switch($id_tipo_pefil->tipo){
            case 1:
            $delete='si';
            $create='si';
            $edit='';
            break;
            case 2:
            $delete='no';
            $create='no';
            $edit='';
            break;
            
            case 3:
            $delete='si';
            $create='si';
            $edit='';
            break;
            
            case 4:
            $delete='si';
            $create='si';
            $edit='';
            break;
            
        }
        $tbnivel=MenuModulosModel::join("menu as a","a.id","=","ad_menu_modulo.id_menu")
                ->join("ad_menu_perfil as b","b.id_menu_modulo","=","ad_menu_modulo.id")
                ->select("ad_menu_modulo.*","a.menu")
                ->where("b.id_perfil",Auth::user()->id_perfil);
                if($mod)
                    $tbnivel=$tbnivel->where("ad_menu_modulo.id_modulo",$mod->id);
        $tbnivel=$tbnivel->orderby("ad_menu_modulo.orden")->get();
        //show($tbnivel);
        return view('comunicacionalcaldia::index',["modulo"=>$mod,"usuario"=>$usuario,"iconos"=>$tbnivel,
        "create"=>$create,
        "delete"=>$delete]);
	}
	
}