<?php namespace Modules\Comunicacionalcaldia\Http\Controllers;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Modules\Comunicacionalcaldia\Entities\cabComunicacionModel;
use Modules\Comunicacionalcaldia\Entities\actividadesModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\Comunicacionalcaldia\Entities\MovComunicacionParroquiaModel;
use Modules\Comunicacionalcaldia\Entities\parroquiaModel;
use Modules\Comunicacionalcaldia\Entities\MovComunicacionBarrioModel;
use Modules\Comunicacionalcaldia\Entities\barrioModel;
use Modules\Comunicacionalcaldia\Entities\MovimientoComunicacionPersonasModel;
use Session;
use DB;
use App\UsuariosModel;
use Auth;
use App\PerfilModel;
use Carbon\Carbon;

class CabComunicacionesListController extends Controller {
	var $configuraciongeneral = array ("Comunicaciones - Búsqueda", "comunicacionalcaldia/comunicacionlista", "index");
    var $escoja=array(0=>"TODOS");
	var $objetos = '[{"Tipo":"select","Descripcion":"Tipo de Filtro","Nombre":"tipofiltro","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]'; 	
		
	var $objetosTable = '[
		
				{"Tipo":"text","Descripcion":"Tipo de actividad","Nombre":"tipo_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Descripción de actividad","Nombre":"nombre_actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Dirección a Cargo","Nombre":"id_direccion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Zona","Nombre":"zona","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Número aproximado de Beneficiaros","Nombre":"beneficiarios","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Explicación Actividad / Obra","Nombre":"observacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Fecha de inicio","Nombre":"fecha_inicio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Fecha fin","Nombre":"fecha_fin","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Dias Restantes","Nombre":"dias_restantes","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
				{"Tipo":"text","Descripcion":"Kilómetros a ser intervenidos","Nombre":"kilometros","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" }]';
				
	public function __construct() {
				$this->middleware('auth');
	} 
	
	/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
	*/
	public function index()
	{

		$objetos = json_decode($this->objetos);
		//show($this->objetos);
		$objetos[0]->Nombre="tipofiltro";
		

		$tipoFiltro=array(1=>"TIPO ACTIVIDAD",2=>"DIRECCION",3=>"ESTADO",4=>'PARROQUIA',5=>'BARRIO');
		$objetos[0]->Valor=$this->escoja + $tipoFiltro;
			

		$objetosTable = json_decode($this->objetosTable);
		$objetos=array_values($objetos);
		//show($objetos);
		//return view('comunicacionalcaldia::index');
		$tabla = cabComunicacionModel::join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
                    ->select("com_tmov_comunicacion_cab.*", "ac.nombre",
                        DB::raw("(select direccion from tmae_direcciones where id=com_tmov_comunicacion_cab.id_direccion) as direccion"))
                    ->where("com_tmov_comunicacion_cab.estado", "ACT")
					->get();
		//para obtener el tipo de usuario
        //para obtener el tipo de usuario
		$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
        $delete='';
        $create='';
        $edit='';
					switch($id_tipo_pefil->tipo){
						case 1:
						$delete='si';
						$create='si';
						$edit='';
						break;
						case 2:
						$delete='no';
						$create='no';
						$edit='';
						break;
						
						case 3:
						$delete='si';
						$create='si';
						$edit='';
						break;
						
						case 4:
						$delete='si';
						$create='si';
						$edit='';
						break;
						
					}
        return view('vistas.indexlist',[
			"objetos"=>$objetos,
			"tabla"=>$tabla,	
			"objetosTable"=>$objetosTable,
			"configuraciongeneral"=>$this->configuraciongeneral,
			"create"=>$create,
            "delete"=>$delete
            ]);


	}

	public function getValoresFiltro(){
		$tipo_filtro=Input::get("tipo_filtro");
		$html = '';
		switch($tipo_filtro){
			case 'TIPO ACTIVIDAD':
			$valores=actividadesModel::where("estado", "ACT")->select("nombre","id")->get();
			$html .= '<label class="col-lg-1 control-label">Actividad</label> <div class="col-lg-4">';

			$html .= '<select class="form-control chosen-select" id="valorfiltro">';
			foreach ($valores as $item) {
				$html .= '<option value="'.$item->id.'">'.$item->nombre.'</option>';
			}
			$html .= '</select></div>';
			return $html;
			break;

			case 'DIRECCION':
			$valores=direccionesModel::join("com_tmov_comunicacion_cab as a","a.id_direccion","=","tmae_direcciones.id")->where("tmae_direcciones.estado", "ACT")->select("tmae_direcciones.direccion","tmae_direcciones.id")->orderby("tmae_direcciones.direccion","asc")->get();
			
			$html .= '<label class="col-lg-1 control-label">Dirección</label> <div class="col-lg-4">';
			$html .= '<select class="form-control chosen-select"  id="valorfiltro">';
			foreach ($valores as $item) {
				$html .= '<option value="'.$item->id.'">'.$item->direccion.'</option>';
			}
			$html .= '</select></div>';
			return $html;
			break;

			case 'ESTADO':
			$valores=explodewords(ConfigSystem("estadoobras"),"|");
			$html .= '<label class="col-lg-1 control-label">Estado</label> <div class="col-lg-4">';
			$html .= '<select class="form-control chosen-select" id="valorfiltro">';
			foreach ($valores as $item) {
				$html .= '<option value="'.$item.'">'.$item.'</option>';
			}
			$html .= '</select></div>';
			return $html;
			break;

			case 'PARROQUIA':
			$valores=ParroquiaModel::where("estado", "ACT")->select("parroquia","id","zona")->get();
			$html .= '<label class="col-lg-1 control-label">Parroquia</label> <div class="col-lg-4">';
			$html .= '<select class="form-control chosen-select" id="valorfiltro">';
			foreach ($valores as $item) {
				$html .= '<option value="'.$item->id.'">'.$item->parroquia.'</option>';
			}
			$html .= '</select></div>';
			return $html;
			break;

			case 'BARRIO':
			$valores=BarrioModel::
			join("parroquia as parr", "barrio.id_parroquia", "=", "parr.id")
			->where("barrio.estado", "ACT")->select("parroquia","barrio.id","zona","barrio")->get();
			$html .= '<label class="col-lg-1 control-label">Barrio</label> <div class="col-lg-4">';
			$html .= '<select class="form-control chosen-select" id="valorfiltro">';
			foreach ($valores as $item) {
				$html .= '<option value="'.$item->id.'">'.$item->parroquia.' - '.$item->barrio.'</option>';
			}
			$html .= '</select></div>';
			return $html;
			break;
		}
		return response()->json(['html' => $html]);
		
  }

  public function getComunicacionesFiltro(){
	$tipo_filtro=Input::get("tipo_filtro");
	$valor_filtro=Input::get("valor_filtro");
	$desde=Carbon::parse(Input::get("desde"));
	$hasta=Carbon::parse(Input::get("hasta"));

	$valores=[];
	$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
	$id_usuario=Auth::user()->id;
	


	switch($tipo_filtro){
		case 'TODOS':

		switch($id_tipo_pefil->tipo){
			case 1:
			$valores=cabComunicacionModel::
			join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com_tmov_comunicacion_cab.id,act.nombre as tipo_actividad,com_tmov_comunicacion_cab.nombre_actividad,dir.direccion as direccion,
					com_tmov_comunicacion_cab.zona,com_tmov_comunicacion_cab.beneficiarios,com_tmov_comunicacion_cab.observacion,
					com_tmov_comunicacion_cab.fecha_inicio,com_tmov_comunicacion_cab.fecha_fin,CONCAT(DATEDIFF(com_tmov_comunicacion_cab.fecha_fin,com_tmov_comunicacion_cab.fecha_inicio),' días') as dias_restantes,com_tmov_comunicacion_cab.kilometros"))
			
			->whereBetween(DB::raw("date_format(com_tmov_comunicacion_cab.created_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->where("com_tmov_comunicacion_cab.estado", "ACT")
			->get();
			break;

			case 2:
			$valores = MovimientoComunicacionPersonasModel::
			join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_personas.id_com_cab", "=", "com.id")
			->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com.id,act.nombre as tipo_actividad,com.nombre_actividad,dir.direccion as direccion,
					com.zona,com.beneficiarios,com.observacion,
					com.fecha_inicio,com.fecha_fin,CONCAT(DATEDIFF(com.fecha_fin,com.fecha_inicio),' días') as dias_restantes,com.kilometros"))
			->where([["com.estado", "ACT"],["com_tmov_comunicacion_personas.idusuario",$id_usuario]])
			->whereBetween('com.fecha_fin', [$desde->format('Y-m-d H:m:s'),$hasta->format('Y-m-d  H:m:s')])
			->groupBy("com.id")
			->get();
			break;


			case 3:
			$valores = MovimientoComunicacionPersonasModel::
			join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_personas.id_com_cab", "=", "com.id")
			->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com.id_tipo_actividad", "=", "act.id")
			->join("users as u", "u.id_direccion", "=", "dir.id")
			->select(DB::raw("com.id,act.nombre as tipo_actividad,com.nombre_actividad,dir.direccion as direccion,
					com.zona,com.beneficiarios,com.observacion,
					com.fecha_inicio,com.fecha_fin,CONCAT(DATEDIFF(com.fecha_fin,com.fecha_inicio),' días') as dias_restantes,com.kilometros"))
			->where([["com.estado", "ACT"],["u.id",$id_usuario]])
			->whereBetween('com.fecha_fin', [$desde->format('Y-m-d H:m:s'),$hasta->format('Y-m-d  H:m:s')])
			->groupBy("com.id")
			->get();
			break;
			case 4:

			$valores = MovimientoComunicacionPersonasModel::
			join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_personas.id_com_cab", "=", "com.id")
			->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com.id_tipo_actividad", "=", "act.id")
			->join("users as u", "u.id_direccion", "=", "dir.id")
			->select(DB::raw("com.id,act.nombre as tipo_actividad,com.nombre_actividad,dir.direccion as direccion,
					com.zona,com.beneficiarios,com.observacion,
					com.fecha_inicio,com.fecha_fin,CONCAT(DATEDIFF(com.fecha_fin,com.fecha_inicio),' días') as dias_restantes,com.kilometros"))
			->where([["com.estado", "ACT"]])
			->whereBetween('com.fecha_fin', [$desde->format('Y-m-d H:m:s'),$hasta->format('Y-m-d  H:m:s')])
			->groupBy("com.id")
			->get();
			break;

		}


		
		
		return $valores;

		break;

		case 'TIPO ACTIVIDAD':
		
		switch($id_tipo_pefil->tipo){
			case 1:
			$valores=cabComunicacionModel::
			join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com_tmov_comunicacion_cab.id,act.nombre as tipo_actividad,com_tmov_comunicacion_cab.nombre_actividad,dir.direccion as direccion,
					com_tmov_comunicacion_cab.zona,com_tmov_comunicacion_cab.beneficiarios,com_tmov_comunicacion_cab.observacion,
					com_tmov_comunicacion_cab.fecha_inicio,com_tmov_comunicacion_cab.fecha_fin,CONCAT(DATEDIFF(com_tmov_comunicacion_cab.fecha_fin,com_tmov_comunicacion_cab.fecha_inicio),' días') as dias_restantes,com_tmov_comunicacion_cab.kilometros"))
			->where([["com_tmov_comunicacion_cab.estado", "ACT"],["act.id",$valor_filtro]])
			->whereBetween(DB::raw("date_format(com_tmov_comunicacion_cab.created_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->get();
			break;

			case 2:
			$valores=cabComunicacionModel::
			join("com_tmov_comunicacion_personas as comper", "comper.id_com_cab", "=", "com_tmov_comunicacion_cab.id")
			->join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com_tmov_comunicacion_cab.id,act.nombre as tipo_actividad,com_tmov_comunicacion_cab.nombre_actividad,dir.direccion as direccion,
					com_tmov_comunicacion_cab.zona,com_tmov_comunicacion_cab.beneficiarios,com_tmov_comunicacion_cab.observacion,
					com_tmov_comunicacion_cab.fecha_inicio,com_tmov_comunicacion_cab.fecha_fin,CONCAT(DATEDIFF(com_tmov_comunicacion_cab.fecha_fin,com_tmov_comunicacion_cab.fecha_inicio),' días') as dias_restantes,com_tmov_comunicacion_cab.kilometros"))
			->where([["com_tmov_comunicacion_cab.estado", "ACT"],["act.id",$valor_filtro],["comper.idusuario",$id_usuario]])
			->whereBetween(DB::raw("date_format(com_tmov_comunicacion_cab.created_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->groupBy("com_tmov_comunicacion_cab.id")
			->get();
			break;

			case 3:
			$valores=cabComunicacionModel::
			join("com_tmov_comunicacion_personas as comper", "comper.id_com_cab", "=", "com_tmov_comunicacion_cab.id")
			->join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
			->join("users as u", "u.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com_tmov_comunicacion_cab.id,act.nombre as tipo_actividad,com_tmov_comunicacion_cab.nombre_actividad,dir.direccion as direccion,
					com_tmov_comunicacion_cab.zona,com_tmov_comunicacion_cab.beneficiarios,com_tmov_comunicacion_cab.observacion,
					com_tmov_comunicacion_cab.fecha_inicio,com_tmov_comunicacion_cab.fecha_fin,CONCAT(DATEDIFF(com_tmov_comunicacion_cab.fecha_fin,com_tmov_comunicacion_cab.fecha_inicio),' días') as dias_restantes,com_tmov_comunicacion_cab.kilometros"))
			->where([["com_tmov_comunicacion_cab.estado", "ACT"],["act.id",$valor_filtro],["u.id",$id_usuario]])
			->whereBetween(DB::raw("date_format(com_tmov_comunicacion_cab.created_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->groupBy("com_tmov_comunicacion_cab.id")
			->get();
			break;
			case 4:
			$valores=cabComunicacionModel::
			join("com_tmov_comunicacion_personas as comper", "comper.id_com_cab", "=", "com_tmov_comunicacion_cab.id")
			->join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com_tmov_comunicacion_cab.id,act.nombre as tipo_actividad,com_tmov_comunicacion_cab.nombre_actividad,dir.direccion as direccion,
					com_tmov_comunicacion_cab.zona,com_tmov_comunicacion_cab.beneficiarios,com_tmov_comunicacion_cab.observacion,
					com_tmov_comunicacion_cab.fecha_inicio,com_tmov_comunicacion_cab.fecha_fin,CONCAT(DATEDIFF(com_tmov_comunicacion_cab.fecha_fin,com_tmov_comunicacion_cab.fecha_inicio),' días') as dias_restantes,com_tmov_comunicacion_cab.kilometros"))
			->where([["com_tmov_comunicacion_cab.estado", "ACT"],["act.id",$valor_filtro]])
			->whereBetween(DB::raw("date_format(com_tmov_comunicacion_cab.created_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->groupBy("com_tmov_comunicacion_cab.id")
			->get();
			break;
		}

		

		return $valores;
		break;

		case 'DIRECCION':
		
			switch($id_tipo_pefil->tipo){
				case 1:
				$valores=cabComunicacionModel::
				join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
				->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
				->join("com_tmae_actividades as act", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "act.id")
				->select(DB::raw("com_tmov_comunicacion_cab.id,act.nombre as tipo_actividad,com_tmov_comunicacion_cab.nombre_actividad,dir.direccion as direccion,
						com_tmov_comunicacion_cab.zona,com_tmov_comunicacion_cab.beneficiarios,com_tmov_comunicacion_cab.observacion,
						com_tmov_comunicacion_cab.fecha_inicio,com_tmov_comunicacion_cab.fecha_fin,CONCAT(DATEDIFF(com_tmov_comunicacion_cab.fecha_fin,com_tmov_comunicacion_cab.fecha_inicio),' días') as dias_restantes,com_tmov_comunicacion_cab.kilometros"))
				->where([["com_tmov_comunicacion_cab.estado", "ACT"],["dir.id",$valor_filtro]])
				->whereBetween(DB::raw("date_format(com_tmov_comunicacion_cab.created_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
				->get();
				break;
				case 4:
				$valores=cabComunicacionModel::
				join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
				->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
				->join("com_tmae_actividades as act", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "act.id")
				->select(DB::raw("com_tmov_comunicacion_cab.id,act.nombre as tipo_actividad,com_tmov_comunicacion_cab.nombre_actividad,dir.direccion as direccion,
						com_tmov_comunicacion_cab.zona,com_tmov_comunicacion_cab.beneficiarios,com_tmov_comunicacion_cab.observacion,
						com_tmov_comunicacion_cab.fecha_inicio,com_tmov_comunicacion_cab.fecha_fin,CONCAT(DATEDIFF(com_tmov_comunicacion_cab.fecha_fin,com_tmov_comunicacion_cab.fecha_inicio),' días') as dias_restantes,com_tmov_comunicacion_cab.kilometros"))
				->where([["com_tmov_comunicacion_cab.estado", "ACT"],["dir.id",$valor_filtro]])
				->whereBetween(DB::raw("date_format(com_tmov_comunicacion_cab.created_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
				->get();
				break;

			}
			break;
		
		

		return $valores;
		break;

		case 'ESTADO':
		switch($id_tipo_pefil->tipo){
			case 1:
			$valores=cabComunicacionModel::
			join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com_tmov_comunicacion_cab.id,act.nombre as tipo_actividad,com_tmov_comunicacion_cab.nombre_actividad,dir.direccion as direccion,
					com_tmov_comunicacion_cab.zona,com_tmov_comunicacion_cab.beneficiarios,com_tmov_comunicacion_cab.observacion,
					com_tmov_comunicacion_cab.fecha_inicio,com_tmov_comunicacion_cab.fecha_fin,CONCAT(DATEDIFF(com_tmov_comunicacion_cab.fecha_fin,com_tmov_comunicacion_cab.fecha_inicio),' días') as dias_restantes,com_tmov_comunicacion_cab.kilometros"))
			->where([["com_tmov_comunicacion_cab.estado", "ACT"],["com_tmov_comunicacion_cab.estado_actividad",$valor_filtro]])
			->whereBetween(DB::raw("date_format(com_tmov_comunicacion_cab.created_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->get();
			break;
			case 2:
			$valores=cabComunicacionModel::
			join("com_tmov_comunicacion_personas as comper", "comper.id_com_cab", "=", "com_tmov_comunicacion_cab.id")
			->join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com_tmov_comunicacion_cab.id,act.nombre as tipo_actividad,com_tmov_comunicacion_cab.nombre_actividad,dir.direccion as direccion,
					com_tmov_comunicacion_cab.zona,com_tmov_comunicacion_cab.beneficiarios,com_tmov_comunicacion_cab.observacion,
					com_tmov_comunicacion_cab.fecha_inicio,com_tmov_comunicacion_cab.fecha_fin,CONCAT(DATEDIFF(com_tmov_comunicacion_cab.fecha_fin,com_tmov_comunicacion_cab.fecha_inicio),' días') as dias_restantes,com_tmov_comunicacion_cab.kilometros"))
			->where([["com_tmov_comunicacion_cab.estado", "ACT"],["com_tmov_comunicacion_cab.estado_actividad",$valor_filtro],["comper.idusuario",$id_usuario]])
			->whereBetween(DB::raw("date_format(com_tmov_comunicacion_cab.created_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->get();
			
			break;
			case 3:
			$valores=cabComunicacionModel::
			join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
			->join("users as u", "u.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com_tmov_comunicacion_cab.id,act.nombre as tipo_actividad,com_tmov_comunicacion_cab.nombre_actividad,dir.direccion as direccion,
					com_tmov_comunicacion_cab.zona,com_tmov_comunicacion_cab.beneficiarios,com_tmov_comunicacion_cab.observacion,
					com_tmov_comunicacion_cab.fecha_inicio,com_tmov_comunicacion_cab.fecha_fin,CONCAT(DATEDIFF(com_tmov_comunicacion_cab.fecha_fin,com_tmov_comunicacion_cab.fecha_inicio),' días') as dias_restantes,com_tmov_comunicacion_cab.kilometros"))
			->where([["com_tmov_comunicacion_cab.estado", "ACT"],["com_tmov_comunicacion_cab.estado_actividad",$valor_filtro],["u.id",$id_usuario]])
			->whereBetween(DB::raw("date_format(com_tmov_comunicacion_cab.created_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->get();
			//show($valores);
			break;
			case 4:
			$valores=cabComunicacionModel::
			join("com_tmae_actividades as ac", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com_tmov_comunicacion_cab.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com_tmov_comunicacion_cab.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com_tmov_comunicacion_cab.id,act.nombre as tipo_actividad,com_tmov_comunicacion_cab.nombre_actividad,dir.direccion as direccion,
					com_tmov_comunicacion_cab.zona,com_tmov_comunicacion_cab.beneficiarios,com_tmov_comunicacion_cab.observacion,
					com_tmov_comunicacion_cab.fecha_inicio,com_tmov_comunicacion_cab.fecha_fin,CONCAT(DATEDIFF(com_tmov_comunicacion_cab.fecha_fin,com_tmov_comunicacion_cab.fecha_inicio),' días') as dias_restantes,com_tmov_comunicacion_cab.kilometros"))
			->where([["com_tmov_comunicacion_cab.estado", "ACT"],["com_tmov_comunicacion_cab.estado_actividad",$valor_filtro]])
			->whereBetween(DB::raw("date_format(com_tmov_comunicacion_cab.created_at,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])
			->get();
			break;
		}
		

		return $valores;
		case 'PARROQUIA':
		switch($id_tipo_pefil->tipo){
			case 1:	
			$valores=MovComunicacionParroquiaModel::
			join("parroquia as parr", "com_tmov_comunicacion_parroquia.id_parroquia", "=", "parr.id")
			->join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_parroquia.id_com_cab", "=", "com.id")
			->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com.id,act.nombre as tipo_actividad,com.nombre_actividad,dir.direccion as direccion,
					com.zona,com.beneficiarios,com.observacion,
					com.fecha_inicio,com.fecha_fin,CONCAT(DATEDIFF(com.fecha_fin,com.fecha_inicio),' días') as dias_restantes,com.kilometros"))
			->where([["com.estado", "ACT"],["parr.id",$valor_filtro]])
			->whereBetween('com.fecha_fin', [$desde->format('Y-m-d H:m:s'),$hasta->format('Y-m-d  H:m:s')])
			->get();
			break;
			case 2:
			$valores=MovComunicacionParroquiaModel::
			join("com_tmov_comunicacion_personas as comper", "comper.id_com_cab", "=", "com_tmov_comunicacion_cab.id")
			->join("parroquia as parr", "com_tmov_comunicacion_parroquia.id_parroquia", "=", "parr.id")
			->join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_parroquia.id_com_cab", "=", "com.id")
			->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com.id,act.nombre as tipo_actividad,com.nombre_actividad,dir.direccion as direccion,
					com.zona,com.beneficiarios,com.observacion,
					com.fecha_inicio,com.fecha_fin,CONCAT(DATEDIFF(com.fecha_fin,com.fecha_inicio),' días') as dias_restantes,com.kilometros"))
			->where([["com.estado", "ACT"],["parr.id",$valor_filtro],["comper.idusuario",$id_usuario]])
			->whereBetween('com.fecha_fin', [$desde->format('Y-m-d H:m:s'),$hasta->format('Y-m-d  H:m:s')])
			->get();
			break;
			case 3:
			$valores=MovComunicacionParroquiaModel::
			
			join("parroquia as parr", "com_tmov_comunicacion_parroquia.id_parroquia", "=", "parr.id")
			->join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_parroquia.id_com_cab", "=", "com.id")
			->join("com_tmov_comunicacion_personas as comper", "comper.id_com_cab", "=", "com.id")
			->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com.id,act.nombre as tipo_actividad,com.nombre_actividad,dir.direccion as direccion,
					com.zona,com.beneficiarios,com.observacion,
					com.fecha_inicio,com.fecha_fin,CONCAT(DATEDIFF(com.fecha_fin,com.fecha_inicio),' días') as dias_restantes,com.kilometros"))
			->where([["com.estado", "ACT"],["parr.id",$valor_filtro],["comper.idusuario",$id_usuario]])
			->whereBetween('com.fecha_fin', [$desde->format('Y-m-d H:m:s'),$hasta->format('Y-m-d  H:m:s')])
			->get();
			break;
			case 4:
				
			$valores=MovComunicacionParroquiaModel::
			join("parroquia as parr", "com_tmov_comunicacion_parroquia.id_parroquia", "=", "parr.id")
			->join("com_tmov_comunicacion_cab as com", "com_tmov_comunicacion_parroquia.id_com_cab", "=", "com.id")
			->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
			->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
			->join("com_tmae_actividades as act", "com.id_tipo_actividad", "=", "act.id")
			->select(DB::raw("com.id,act.nombre as tipo_actividad,com.nombre_actividad,dir.direccion as direccion,
					com.zona,com.beneficiarios,com.observacion,
					com.fecha_inicio,com.fecha_fin,CONCAT(DATEDIFF(com.fecha_fin,com.fecha_inicio),' días') as dias_restantes,com.kilometros"))
			->where([["com.estado", "ACT"],["parr.id",$valor_filtro]])
			->whereBetween('com.fecha_fin', [$desde->format('Y-m-d H:m:s'),$hasta->format('Y-m-d  H:m:s')])
			->get();
			
			break;
		}

		return $valores;
		case 'BARRIO':
		
		
			switch($id_tipo_pefil->tipo){
				case 1:
				$valores=MovComunicacionBarrioModel::
				join("com_tmov_comunicacion_parroquia as com_parr", "com_tmov_comunicacion_barrios.id_cab_parroquia", "=", "com_parr.id")
				->join("com_tmov_comunicacion_cab as com", "com_parr.id_com_cab", "=", "com.id")
				->join("parroquia as parr", "com_parr.id_parroquia", "=", "parr.id")
				->join("barrio as barr", "com_tmov_comunicacion_barrios.id_barrio", "=", "barr.id")
				->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
				->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
				->join("com_tmae_actividades as act", "com.id_tipo_actividad", "=", "act.id")
				->select(DB::raw("com.id,act.nombre as tipo_actividad,com.nombre_actividad,dir.direccion as direccion,
						com.zona,com.beneficiarios,com.observacion,
						com.fecha_inicio,com.fecha_fin,CONCAT(DATEDIFF(com.fecha_fin,com.fecha_inicio),' días') as dias_restantes,com.kilometros"))
				->where([["com.estado", "ACT"],["barr.id",$valor_filtro]])
				->whereBetween('com.fecha_fin', [$desde->format('Y-m-d H:m:s'),$hasta->format('Y-m-d  H:m:s')])
				->get();
				break;
				case 2:
				$valores=MovComunicacionBarrioModel::
				join("com_tmov_comunicacion_parroquia as com_parr", "com_tmov_comunicacion_barrios.id_cab_parroquia", "=", "com_parr.id")
				->join("com_tmov_comunicacion_cab as com", "com_parr.id_com_cab", "=", "com.id")
				->join("com_tmov_comunicacion_personas as comper", "comper.id_com_cab", "=", "com.id")
				->join("parroquia as parr", "com_parr.id_parroquia", "=", "parr.id")
				->join("barrio as barr", "com_tmov_comunicacion_barrios.id_barrio", "=", "barr.id")
				->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
				->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
				->join("com_tmae_actividades as act", "com.id_tipo_actividad", "=", "act.id")
				->select(DB::raw("com.id,act.nombre as tipo_actividad,com.nombre_actividad,dir.direccion as direccion,
						com.zona,com.beneficiarios,com.observacion,
						com.fecha_inicio,com.fecha_fin,CONCAT(DATEDIFF(com.fecha_fin,com.fecha_inicio),' días') as dias_restantes,com.kilometros"))
				->where([["com.estado", "ACT"],["barr.id",$valor_filtro],"comper.idusuario",$id_usuario])
				->whereBetween('com.fecha_fin', [$desde->format('Y-m-d H:m:s'),$hasta->format('Y-m-d  H:m:s')])
				->get();
				break;
				case 3:
				$valores=MovComunicacionBarrioModel::
				join("com_tmov_comunicacion_parroquia as com_parr", "com_tmov_comunicacion_barrios.id_cab_parroquia", "=", "com_parr.id")
				->join("com_tmov_comunicacion_cab as com", "com_parr.id_com_cab", "=", "com.id")
				->join("parroquia as parr", "com_parr.id_parroquia", "=", "parr.id")
				->join("barrio as barr", "com_tmov_comunicacion_barrios.id_barrio", "=", "barr.id")
				->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
				->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
				->join("users as u", "u.id_direccion", "=", "dir.id")
				->join("com_tmae_actividades as act", "com.id_tipo_actividad", "=", "act.id")
				->select(DB::raw("com.id,act.nombre as tipo_actividad,com.nombre_actividad,dir.direccion as direccion,
						com.zona,com.beneficiarios,com.observacion,
						com.fecha_inicio,com.fecha_fin,CONCAT(DATEDIFF(com.fecha_fin,com.fecha_inicio),' días') as dias_restantes,com.kilometros"))
				->where([["com.estado", "ACT"],["barr.id",$valor_filtro],["u.id",$id_usuario]])
				->whereBetween('com.fecha_fin', [$desde->format('Y-m-d H:m:s'),$hasta->format('Y-m-d  H:m:s')])
				->get();
				break;
				case 4:
				$valores=MovComunicacionBarrioModel::
				join("com_tmov_comunicacion_parroquia as com_parr", "com_tmov_comunicacion_barrios.id_cab_parroquia", "=", "com_parr.id")
				->join("com_tmov_comunicacion_cab as com", "com_parr.id_com_cab", "=", "com.id")
				->join("parroquia as parr", "com_parr.id_parroquia", "=", "parr.id")
				->join("barrio as barr", "com_tmov_comunicacion_barrios.id_barrio", "=", "barr.id")
				->join("com_tmae_actividades as ac", "com.id_tipo_actividad", "=", "ac.id")
				->join("tmae_direcciones as dir", "com.id_direccion", "=", "dir.id")
				->join("com_tmae_actividades as act", "com.id_tipo_actividad", "=", "act.id")
				->select(DB::raw("com.id,act.nombre as tipo_actividad,com.nombre_actividad,dir.direccion as direccion,
						com.zona,com.beneficiarios,com.observacion,
						com.fecha_inicio,com.fecha_fin,CONCAT(DATEDIFF(com.fecha_fin,com.fecha_inicio),' días') as dias_restantes,com.kilometros"))
				->where([["com.estado", "ACT"],["barr.id",$valor_filtro]])
				->whereBetween('com.fecha_fin', [$desde->format('Y-m-d H:m:s'),$hasta->format('Y-m-d  H:m:s')])
				->get();
				
				break;
			}
			

		return $valores;
	}
	return response()->json($valores);
}
	
}