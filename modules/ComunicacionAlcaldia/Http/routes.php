<?php

Route::group(['middleware' => 'web', 'prefix' => 'comunicacionalcaldia', 'namespace' => 'Modules\ComunicacionAlcaldia\Http\Controllers'], function()
{
	Route::get('/', 'ComunicacionAlcaldiaController@index');
	Route::resource('direcciones', 'DireccionesController');
	Route::get("getusuariosdireccion","DireccionesController@getusuariosdireccion");
	//AJAX
	Route::get("direcionesajax", "DireccionesController@direcionesajax");
	Route::get("parroquiaajax", "ParroquiaController@parroquiaajax");
	Route::get("barrioajax", "BarrioController@barrioajax");
	Route::get("actividadesajax", "ActividadesController@actividadesajax");
	Route::get("comunicacionajax", "cabComunicacionController@comunicacionajax");
	Route::resource('parroquias', 'ParroquiaController');
	Route::get("getparroquia","ParroquiaController@getparroquia");
	Route::get("getparroquiafiltro","ParroquiaController@getparroquiafiltro");
	Route::get("getBarrio","BarrioController@getBarrio");
	Route::get("getPersonas","cabComunicacionController@getPersonas");
	Route::get("getPresupuesto","cabComunicacionController@getPresupuesto");
	Route::get("getBeneficiarios","cabComunicacionController@getBeneficiarios");
	Route::resource('barrios', 'BarrioController');
	Route::resource('actividades', 'ActividadesController');
	Route::get("getacticampos","ActividadesController@getacticampos");
	Route::resource('comunicacion', 'cabComunicacionController');
	Route::resource('comunicacionlista', 'CabComunicacionesListController');
	Route::get("getValoresFiltro","CabComunicacionesListController@getValoresFiltro");
	Route::get("getComunicacionesFiltro","CabComunicacionesListController@getComunicacionesFiltro");
	Route::resource('comunicaciongraficos', 'CabComunicacionChartsController');
	Route::get("getValoresCharts","CabComunicacionChartsController@getValoresCharts");
	Route::resource('dashboard', 'DashboardComunicacionController');
	Route::get("getChartLine","DashboardComunicacionController@getChartLine");


	
});

