<?php namespace Modules\Tramitesalcaldia\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaCabModel;
use DB;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaCabDetaModel;
class DashboardTramitesController extends Controller {


	var $configuraciongeneral = array ("DASHBOARD TRAMITES", "tramitesalcaldia/dashboard", "index");
	
	public function index()
	{
		$fechainicial=date("Y-m-d")." 00:00:00";
		//$fechainicial="2019-06-01 00:00:00";
		$fechafinal=date("Y-m-d")." 23:59:59";


        //show($modificacionesHoy);
		$datos=TramAlcaldiaCabModel::select(DB::raw("count(id) as total,prioridad,'0' as color,0 as porcentaje"))
		->groupBy("prioridad")
		->where([["estado","ACT"],["disposicion","pendiente"]])
		->get();
		$total=TramAlcaldiaCabModel::select(DB::raw("count(id) as total"))
		->where([["estado","ACT"],["disposicion","pendiente"]])
		->first();		
		$iboxTramites=$this->getTramites(1,$datos,$total);
		
		//unset($datos);
		//($total);

		$datos=TramAlcaldiaCabModel::select(DB::raw("count(id) as total,disposicion,'0' as color,0 as porcentaje"))
		->groupBy("disposicion")
		->where([["estado","ACT"]])
		->get();
		$totalDisp=TramAlcaldiaCabModel::select(DB::raw("count(id) as total"))
		->where([["estado","ACT"]])
		->first();		
		$iboxTramitesDisposicion=$this->getTramites(2,$datos,$totalDisp);

	
		$TramitesRespondidos=$this->getTimeLineCab(0,1);
		//shoW($iboxTramitesDisposicion);

		return view('vistas.dashboard.dashboardtramites',[
			"tramitesRespondidos"=>$TramitesRespondidos,
			"iboxTramites"=>$iboxTramites,
			"iboxTramitesDisposicion"=>$iboxTramitesDisposicion,
			"configuraciongeneral"=>$this->configuraciongeneral,
			"total"=>$total,
			"totalDisp"=>$totalDisp
			]);
	}


	public function getTimeLineCab($id,$tipo){
        $sw=0;
        switch($tipo){
            case 1:
            $tabla=TramAlcaldiaCabDetaModel::
            select("tram_peticiones_cab_deta.*","u.name")
            ->join("users as u","u.id","=","tram_peticiones_cab_deta.id_usuario")
			//->where('id_crono_cab',$id)
			->offset(1)
            ->limit(20)
			->orderBy("tram_peticiones_cab_deta.updated_at","DESC")
            ->get();
            return $tabla;
            break;
        }
	}
	
	public function getTramites($tipo,$datos,$total){

		if($tipo==1){
			$prioridad=explode("|",ConfigSystem("prioridadtramites"));
			$prioridadcolores=explode("|",ConfigSystem("prioridadtramitescolor"));
			foreach($datos as $item){
				switch($item->prioridad){
	
					case $prioridad[0]:
						$item->prioridad=$prioridad[0];
						$item->color=$prioridadcolores[0];
						$item->porcentaje=round((($item->total/$total->total)*100),2);
	
					break;
					case $prioridad[1]:
						$item->prioridad= $prioridad[1];
						$item->color=$prioridadcolores[1];
						$item->porcentaje=round((($item->total/$total->total)*100),2);
					break;
					case $prioridad[2]:
						$item->prioridad= $prioridad[2];
						$item->color=$prioridadcolores[2];
						$item->porcentaje=round((($item->total/$total->total)*100),2);
	
					break;
	
				}
	
	
			}
		
			return $datos;
		}else if($tipo==2){

			$disposicion=explode("|",ConfigSystem("disposicion"));
			$disposicioncolores=explode("|",ConfigSystem("disposicioncolor"));
			//show($disposicion);
			foreach($datos as $item){
				switch($item->disposicion){
	
					case $disposicion[0]:
						$item->disposicion=$disposicion[0];
						$item->color=$disposicioncolores[0];
						$item->porcentaje=round((($item->total/$total->total)*100),2);
	
					break;
					case $disposicion[1]:
						$item->disposicion= $disposicion[1];
						$item->color=$disposicioncolores[1];
						$item->porcentaje=round((($item->total/$total->total)*100),2);
					break;
					case $disposicion[2]:
						$item->disposicion= $disposicion[2];
						$item->color=$disposicioncolores[2];
						$item->porcentaje=round((($item->total/$total->total)*100),2);
	
					break;
	
				}
	
	
			}
			return $datos;
		}
		
		
	}
	
}