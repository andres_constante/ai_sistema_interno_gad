<?php namespace Modules\Tramitesalcaldia\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaCabModel;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaCabDetaModel;
use Modules\Tramitesalcaldia\Entities\TramAlcaldiaDelModel;
use Session;    
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\NotificacionesController;
use App\UsuariosModel;
use Mail;


class TramAlcaldiaCabController extends Controller {
	var $configuraciongeneral = array ("Despacho de Alcaldía", "tramitesalcaldia/tramites", "index",6=>"null");
	var $escoja=array(null=>"Escoja opción...") ;
	var $objetos = '[
        {"Tipo":"datetext","Descripcion":"Fecha","Nombre":"fecha_ingreso","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Número de Trámite","Nombre":"numtramite","Clase":"numeros-letras-guion mayuscula","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Remitente","Nombre":"remitente","Clase":"mayuscula","Valor":"Null","ValorAnterior" :"Null" }, 
		{"Tipo":"text","Descripcion":"Asunto","Nombre":"asunto","Clase":"mayuscula","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"textarea","Descripcion":"Petición","Nombre":"peticion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
		{"Tipo":"select","Descripcion":"Prioridad","Nombre":"prioridad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
		{"Tipo":"textarea","Descripcion":"Recomendación","Nombre":"recomendacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Disposición","Nombre":"disposicion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Observación","Nombre":"observacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },		
        {"Tipo":"datetext","Descripcion":"Fecha de Respuesta","Nombre":"fecha","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha de Ingreso","Nombre":"created_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Adjunto","Nombre":"archivo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Adjunto de Finalización","Nombre":"archivo_finalizacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Finzalizar","Nombre":"estado_proceso","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
		]';
		var $validarjs =array(
            "fecha_ingreso"=>"fecha_ingreso: {
                required: true
            }",
            "numtramite"=>"numtramite: {
                            required: true
                        }",
            "remitente"=>"remitente: {
                            required: true
                        }",
            "asunto"=>"asunto: {
                            required: true
                        }",
            "peticion"=>"peticion: {
                            required: true
                        }",
            "prioridad"=>"prioridad: {
                            required: true
                        }",
            "recomendacion"=>"recomendacion: {
                            required: false
                        }"
        );

        protected $Notificacion; 
        public function __construct(NotificacionesController $Notificacion) {  
           // $this->middleware('cors');     
            $this->middleware('auth'); 
             $this->Notificacion = $Notificacion;
        }

	public function index()
	{     
		$objetos = json_decode($this->objetos);
        unset($objetos[7]);
        unset($objetos[8]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[12]);
        unset($objetos[13]);
        $objetos=array_values($objetos);
        
		//show($objetos);
		$tabla=TramAlcaldiaCabModel::where([["estado","ACT"],["disposicion","=","PENDIENTE"]])->get();
        $edit=null;
        $verid="no";

        
        return view('vistas.index',[
            "objetos"=>$objetos,
            "tabla"=>$tabla,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "delete"=>'si',
            "edit"=>$edit,
            "verid"=>$verid,
            "create"=>"si",
            
            ]);
	
	}

	public function index2()
	{     
		$objetos = json_decode($this->objetos);
		//show($this->objetos);
        $tabla=TramAlcaldiaCabModel::where([["estado","ACT"],["disposicion","<>","PENDIENTE"],["estado_proceso","<>",1]])
        ->orderBy("created_at","DESC")
        ->get();
       
        $edit='no';
        $verid="no";
        $editfinalizartramite="si";

        $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
        if ($id_tipo_pefil->tipo==2) {
            $editfinalizartramite=null;
           // show($editfinalizartramite);
        }
        unset($objetos[10]);
        unset($objetos[12]);
        unset($objetos[13]);
        $configuraciongeneral = $this->configuraciongeneral;
        $configuraciongeneral[0]="Trámites Despachados";
        $imprimir="si";
        return view('vistas.index',[
            "objetos"=>$objetos,
            "tabla"=>$tabla,
            "configuraciongeneral"=>$configuraciongeneral,
            "delete"=>null,
            "edit"=>$edit,
            "verid"=>$verid,
            "create"=>"no",
            "editfinalizartramite"=>$editfinalizartramite,
            "imprimir"=>$imprimir
            ]);
	
    }
    public function indexFinalizados()
	{     
		$objetos = json_decode($this->objetos);
		//show($this->objetos);
        $tabla=TramAlcaldiaCabModel::where([["estado","ACT"],["estado_proceso","=",1]])
        ->orderBy("created_at","DESC")
        ->get();
		
        $edit='no';
        $verid="no";
        unset($objetos[10]);
        unset($objetos[13]);
        $configuraciongeneral = $this->configuraciongeneral;
        $configuraciongeneral[0]="Trámites finalizados";
        $imprimir="si";
        return view('vistas.index',[
            "objetos"=>$objetos,
            "tabla"=>$tabla,
            "configuraciongeneral"=>$configuraciongeneral,
            "delete"=>null,
            "edit"=>$edit,
            "verid"=>$verid,
            "create"=>"no",
            "imprimir"=>$imprimir
            ]);
	
    }
    // function EnviarEmail($id,$email,$nombres,$asunto="Trámite Despachado"){
       

    //         $tabla=TramAlcaldiaCabModel::where([["estado","ACT"],["id",$id]])->get();
    //             $tabladel=TramAlcaldiaDelModel::
    //             join("users as us","us.id","=","tram_peticion_del.id_usuario")
    //             ->where([["tram_peticion_del.estado","ACT"],["id_tram_cab",$id]])->get();
                

           
    //         Mail::send(
    //         'vistas.reporteshtml.reportemail',
    //         ["tabla"=>$tabla,"tabladel"=>$tabladel],
    //         function($m) use ($email,$nombres,$asunto){
    //         $m->from("alcaldia@manta.gob.ec","GAD Municipal Manta");
    //         $m->to($email,$nombres)->subject($asunto);
    //         });
    //         return "";
           
    // }
    public function reporte($id)
	{    
                       
               // $this->EnviarEmail($id,'cgpg94@gmail.com',"Gonzalo");
                $tabla=TramAlcaldiaCabModel::where([["estado","ACT"],["id",$id]])->get();
                $tabladel=TramAlcaldiaDelModel::
                join("users as us","us.id","=","tram_peticion_del.id_usuario")
                ->where([["tram_peticion_del.estado","ACT"],["id_tram_cab",$id]])->get();
                if(count($tabladel)==0){
                    $tabladel=null;
                }
                return view('vistas.reporteshtml.reporte',[
            
                    "tabla"=>$tabla,
                    "tabladel"=>$tabladel
                    ]);
                

    }
    
    



	public function create()
    {
        //        
        $this->configuraciongeneral[2]="crear";
        $objetos=json_decode($this->objetos);
    
		
		$prioridad=explodewords(ConfigSystem("prioridadtramites"),"|");
		$objetos[5]->Valor=$this->escoja + $prioridad;
        unset($objetos[7]);
        unset($objetos[8]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[13]);
        unset($objetos[12]);

        $numero=TramAlcaldiaCabModel::where("estado","ACT")->orderBy("numtramite",'desc')->first();
       // $sinletras = str_replace("[a-zA-Z-]","",$numero['numtramite']);
        $sinletras = preg_replace("/[a-zA-Z-]-/","",$numero['numtramite']);
        $proxinumero=(int)$sinletras+1;
        $objetos[1]->ValorAnterior='T-'.$proxinumero;
		
        // show($proxinumero+1,0);
        //show($proxinumero);
        return view('vistas.create',[
                "objetos"=>$objetos,
                "edit"=>null,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "validarjs"=>$this->validarjs
        ]);
	}
	public function store(Request $request)
    {
        //
        return $this->guardar(0);
	}
	public function guardar($id)
    {    
            $input=Input::all();            
            //show($input);
            $ruta=$this->configuraciongeneral[1];            
            DB::beginTransaction();
           
           
          
            try{     
                $input=Input::all();
                $arrapas=array();
               
                     
                if($id==0)
                {
                    $ruta.="/create";
                    $guardar= new TramAlcaldiaCabModel;
                    $msg="Registro Creado Exitosamente...!";
                    $msgauditoria="Registro de Comunicación";
                    $existe= TramAlcaldiaCabModel::where([["estado",'ACT'],["numtramite",$input['numtramite']]])->first();

                    $numero=TramAlcaldiaCabModel::where("estado","ACT")->orderBy("numtramite",'desc')->first();
                    $sinletras = preg_replace("/[a-zA-Z-]-/","",$numero['numtramite']);
                    $proxinumero=(int)$sinletras+1;
                    $sinletras2 = preg_replace("/[a-zA-Z-]-/","",$input['numtramite']);
                    $codigo='T-'.$proxinumero;
                   
                    $msg2="Registro Creado Exitosamente...!";
                    if ($proxinumero!=(int)$sinletras2) {
                        $msg2="El número de tramite que debío ser es: ".$codigo;
                        
                    }


                   // Session::flash('message', "El número de tramite que debio ser es: ".$codigo);


                    
                   
                    if(isset($existe->id)){
                       
		
                        return Redirect::to("$ruta")
                        ->withErrors('El número de tramite ya exisiste')
                        ->withInput();
                        
                    }else{
                        foreach($input as $key => $value)
                        {
                            
                            if($key != "_method" && $key != "_token" && $key!="idusuario" && $key!="archivo")
                            {                        
                                $guardar->$key = $value;
                            }                        
                         }
    
                         $guardar->id_usuario= Auth::user()->id;
                         $guardar->ip=\Request::getClientIp();
                         $guardar->pc=\Request::getHost();
    
                         $guardar->save();
                        
                         $idcab=$guardar->id;
    /*ARCHIVO*/
                        if(Input::hasFile("archivo"))
                        {
                            $dir = public_path().'/archivos_sistema/';                        
                            $docs = Input::file("archivo");                        
                            if($docs)
                            {
                                    $fileName = "tramite-$idcab-".date("YmdHis").".".$docs->getClientOriginalExtension();//$docs->getClientOriginalName();
                                    $ext=$docs->getClientOriginalExtension();
                                    $perfiles=explode("|",ConfigSystem("archivospermitidos"));
                                    if(!in_array($ext,$perfiles))
                                    {
                                        DB::rollback();
                                        $mensaje="No se admite el tipo de archivo ".strtoupper($ext);
                                        return redirect()->back()->withErrors([$mensaje])->withInput();    
                                    }                                
    
                                    $file=TramAlcaldiaCabModel::find($idcab);
                                    /*Tratamiento de Archivo Anterior*/
                                    $oldfile=$dir.$file->archivo;
                                    //unlink($oldfile);
                                    File::delete($oldfile);
                                    /**/
                                    $file->archivo=$fileName;
                                    $file->save();
                                    $docs->move($dir, $fileName);
                            } 
                        }
                         //
                         $timeline=new TramAlcaldiaCabDetaModel;
                         $timeline->numtramite=$guardar->numtramite;
                         $timeline->remitente=$guardar->remitente;
                         $timeline->asunto=$guardar->asunto;
                         $timeline->peticion=$guardar->peticion;
                         $timeline->prioridad=$guardar->prioridad;
                         $timeline->recomendacion=$guardar->recomendacion;
                         $timeline->disposicion=$guardar->disposicion;
                        
                         $timeline->observacion=$guardar->observacion;
                         $timeline->id_usuario=$guardar->id_usuario;
                         $timeline->ip=\Request::getClientIp();
                         $timeline->pc=\Request::getHost();
                         $timeline->id_tram_cab=$idcab;
                         $timeline->archivo=$guardar->archivo;
                         $timeline->fecha_ingreso=$guardar->fecha_ingreso;
                         $timeline->save();


                        $to='fbd9f165-f6e1-42ad-8336-f48b58706e63';
                        $title='Existe un Nuevo Trámite por Atender';
                        $body='Asunto: '.$guardar->asunto.' Prioridad'.$guardar->prioridad;

                        

                         $this->Notificacion->notificacion($to, $title,$body);
                            
                    }
                }
                else if($id>0){

                    $ruta.="/$id/edit";
                    $guardar= TramAlcaldiaCabModel::find($id);
                    $msg="Registro Actualizado Exitosamente...!";
                    $msg2="Registro Actualizado Exitosamente...!";
                    $msgauditoria="Edición Comunicación";

                    
                        foreach($input as $key => $value)
                        {
                            
                            if($key != "_method" && $key != "_token" && $key!="idusuario" && $key!="archivo")
                            {                        
                                $guardar->$key = $value;
                            }                        
                         }
    
                         $guardar->id_usuario= Auth::user()->id;
                         $guardar->ip=\Request::getClientIp();
                         $guardar->pc=\Request::getHost();
        
                         $guardar->save();
                        
                         $idcab=$guardar->id;
    /*ARCHIVO*/
                                              
                        if(Input::hasFile("archivo"))
                        { 
                            $docs = Input::file("archivo"); 
                            $dir = public_path().'/archivos_sistema/';                    
                            if($docs)
                            {
                                    $fileName = "tramite-$idcab-".date("YmdHis").".".$docs->getClientOriginalExtension();//$docs->getClientOriginalName();
                                    $ext=$docs->getClientOriginalExtension();
                                    $perfiles=explode("|",ConfigSystem("archivospermitidos"));
                                    if(!in_array($ext,$perfiles))
                                    {
                                        DB::rollback();
                                        $mensaje="No se admite el tipo de archivo ".strtoupper($ext);
                                        return redirect()->back()->withErrors([$mensaje])->withInput();    
                                    }                                
    
                                    $file=TramAlcaldiaCabModel::find($idcab);
                                    /*Tratamiento de Archivo Anterior*/
                                    $oldfile=$dir.$file->archivo;
                                    //unlink($oldfile);
                                    File::delete($oldfile);
                                    /**/
                                    $file->archivo=$fileName;
                                    $file->save();
                                    $docs->move($dir, $fileName);
                            } 
                        }else if(Input::hasFile("archivo_finalizacion")){
                            $docs = Input::file("archivo_finalizacion"); 
                            $dir = public_path().'/archivos_sistema/';                    
                            if($docs)
                            {
                                    $fileName = "Finalizacion_tramite-$idcab-".date("YmdHis").".".$docs->getClientOriginalExtension();//$docs->getClientOriginalName();
                                    $ext=$docs->getClientOriginalExtension();
                                    $perfiles=explode("|",ConfigSystem("archivospermitidos"));
                                    if(!in_array($ext,$perfiles))
                                    {
                                        DB::rollback();
                                        $mensaje="No se admite el tipo de archivo ".strtoupper($ext);
                                        return redirect()->back()->withErrors([$mensaje])->withInput();    
                                    }                                
    
                                    $file=TramAlcaldiaCabModel::find($idcab);
                                    /*Tratamiento de Archivo Anterior*/
                                    $oldfile=$dir.$file->archivo_finalizacion;
                                    //unlink($oldfile);
                                    File::delete($oldfile);
                                    /**/
                                    $file->archivo_finalizacion=$fileName;
                                    $file->save();
                                    $docs->move($dir, $fileName);
                            }
                        }
                         //
                         $timeline=new TramAlcaldiaCabDetaModel;
                         $timeline->numtramite=$guardar->numtramite;
                         $timeline->remitente=$guardar->remitente;
                         $timeline->asunto=$guardar->asunto;
                         $timeline->peticion=$guardar->peticion;
                         $timeline->prioridad=$guardar->prioridad;
                         $timeline->recomendacion=$guardar->recomendacion;
                         $timeline->disposicion=$guardar->disposicion;
                        
                         $timeline->observacion=$guardar->observacion;
                         $timeline->id_usuario=$guardar->id_usuario;
                         $timeline->ip=\Request::getClientIp();
                         $timeline->pc=\Request::getHost();
                         $timeline->id_tram_cab=$idcab;
                         $timeline->archivo=$guardar->archivo;
                         $timeline->fecha_ingreso=$guardar->fecha_ingreso;
                        $timeline->save();
                            
                    
                }
                
               
               // show($existe->id);
                
                
               
                      
                
                 //DB::rollback();
                DB::commit();
            }//Try Transaction
            catch(\Exception $e){
                // ROLLBACK
                DB::rollback();
                $mensaje=$e->getMessage();
                return redirect()->back()->withErrors([$mensaje])->withInput();    
            }
            // show($mes2,0);
            // show($msg);
            if($msg!=$msg2){
                Session::flash('message', $msg2);
            }else{
                Session::flash('message', $msg);
            }
           if($guardar->estado_proceso==1){
            return Redirect::to('tramitesalcaldia/tramitesfinalizados');
           }else{
            
            return Redirect::to($this->configuraciongeneral[1]);
           }
           
  }
  public function show($id)
  {
      //
      $objetos=json_decode($this->objetos);
      $tabla=TramAlcaldiaCabModel::where("estado","ACT")
      ->where("id",$id)
      ->first();//->paginate(500);
      $tabla2 = TramAlcaldiaDelModel::
      join('tram_peticiones_cab as tra','tram_peticion_del.id_tram_cab','=','tra.id')
      ->join('users as us','tram_peticion_del.id_usuario','=','us.id')
      ->select(DB::raw("us.name,us.email"))
      ->where("tra.id",$id)
      ->groupBy('us.name')
      ->get();
      
    //   para qeu no se muestre si no tiene responsable
      if (count($tabla2)==0) {
        $tabla2=null;
      }
      unset($objetos[11]);
      unset($objetos[13]);
      unset($objetos[12]);

    
      $timelineTramite=$this->getTimeLineCab($id,1);
      $timelineTramiteResp=$this->getTimeLineCab($id,2);


 
      return view('vistas.show',[
              "timelineTramite"=>$timelineTramite,
               "timelineTramiteResp"=>$timelineTramiteResp,
              "objetos"=>$objetos,
              "tabla"=>$tabla,
              "tabla2"=>$tabla2,
              "configuraciongeneral"=>$this->configuraciongeneral
              ]);
  }
  public function update(Request $request, $id)
    {
        //
        return $this->guardar($id);
    }
	public function edit($id)
    {
        //

        $variableControl=null;
		$tabla = TramAlcaldiaCabModel::find($id);    
		$this->configuraciongeneral[2]="editar";
		
        $objetos=json_decode($this->objetos);
        $objetos[1]->Tipo="textdisabled2";
        unset($objetos[7]);
        unset($objetos[8]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[13]);
        unset($objetos[12]);

		
    
        $prioridades=explodewords(ConfigSystem("prioridadtramites"),"|");
        $objetos[5]->Valor=$this->escoja + $prioridades;
		//show($objetos[8]);
		



        $edit=null;
		$btnguardar=0;
		$variableControl=null;
        if(Input::has("list"))
            $btnguardar=1;
            $this->configuraciongeneral[3]="2";
            $this->configuraciongeneral[4]=null;
        return view('vistas.create',[
            //"timelineActividades"=>$timelineActividades,
            //"timelineActividadesResp"=>$timelineActividadesResp,
            "objetos"=>$objetos,
            "variableControl"=>$variableControl,
            "tabla"=>$tabla,
            "edit"=>$edit,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "validarjs"=>$this->validarjs,
            "btnguardar"=>$btnguardar
        ]);
	}
	public function editFinalizar()
    {   
        $id=Input::get("id");
        $variableControl=null;
		$tabla = TramAlcaldiaCabModel::find($id);    
        $this->configuraciongeneral[2]="editar";
        $this->configuraciongeneral[0]="Finalizar Trámite";
		
        $objetos=json_decode($this->objetos);
        $objetos[1]->Tipo="textdisabled2";
        unset($objetos[0]);
        unset($objetos[2]);
        unset($objetos[3]);
        unset($objetos[4]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[8]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]); 
        $finalizar=[0=>"NO",1=>"SI"];
        $objetos[13]->Valor=$this->escoja + $finalizar;
        $edit=null;
		$btnguardar=0;
		$variableControl="NO";
        if(Input::has("list"))
            $btnguardar=1;
            $this->configuraciongeneral[3]="2";
            $this->configuraciongeneral[4]=null;
        return view('vistas.create',[
            //"timelineActividades"=>$timelineActividades,
            //"timelineActividadesResp"=>$timelineActividadesResp,
            "objetos"=>$objetos,
            "variableControl"=>$variableControl,
            "tabla"=>$tabla,
            "edit"=>$edit,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "validarjs"=>$this->validarjs,
            "btnguardar"=>$btnguardar
        ]);
	}
	/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        DB::beginTransaction();
        try{
            $tabla=TramAlcaldiaCabModel::find($id);
            $tabla->estado='INA';
            $tabla->id_usuario= Auth::user()->id;
            //$tabla->delete();
            $tabla->save();
            // demas modelos
            //$tabla2->delete();
            $tabla3=TramAlcaldiaCabDetaModel::where("id_tram_cab",$id)->get();
            foreach ($tabla3 as $key => $value) {
                # code...
                $dele=TramAlcaldiaCabDetaModel::find($value->id);
                $dele->estado='INA';
                $dele->save();
            }
            // MovimientoComunicacionPersonasModel
            // CabPresupuestoModel
            // PresupuestoDetalleModel
            // MovComunicacionParroquiaModel

            //->update(array('estado' => 'INACTIVO'));
        DB::commit();
        Session::flash('message', 'El registro se eliminó Exitosamente!');
        if(Input::has('vista')){
            return 'El registro se eliminó Exitosamente!';
        }
            return Redirect::to($this->configuraciongeneral[1]);   
        }//Try Transaction
        catch(\Exception $e){
          //  // ROLLBACK
            DB::rollback();
            Session::flash('message', 'El registro no pudo ser elminado!');
            return Redirect::to($this->configuraciongeneral[1]);   
        }
    }
    public function getTimeLineCab($id,$tipo){
        $sw=0;
        switch($tipo){
            case 1:
            
            $tabla=TramAlcaldiaCabDetaModel::
            select("tram_peticiones_cab_deta.*","u.name")
            ->join("users as u","u.id","=","tram_peticiones_cab_deta.id_usuario")
            ->where('id_tram_cab',$id)
            ->orderBy("tram_peticiones_cab_deta.updated_at","ASC")
            ->get();
            
            return $tabla;
            break;
            case 2:
           
            $personas[]=[];
            $tabla=TramAlcaldiaCabDetaModel::
            select("delegados_json")->where('id_tram_cab',$id)->get();
            

//show($tabla2);
            foreach($tabla as $valor){
                
                $personas=json_decode($valor->delegados_json);
                //show($personas);
                unset($tabla);
               // isset(personas)
                if(isset($personas)){
                    foreach($personas as $p){
                        // show($p->id);
                        if($p->id=" "){
                            $tabla[]=["name"=>" "];
                        }else{
                            $tabla[]=DB::table("users")->where("id",$p->$id)->select("name")->get();
                            $sw=1;
                        }
                       
                    }
                }
                
            }
            if($sw==0){
                $tabla=array();
            }
            return $tabla;
            break;
        }
    }

}