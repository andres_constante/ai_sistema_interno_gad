<?php namespace Modules\Tramitesalcaldia\Http\Controllers;
use Pingpong\Modules\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;


use Route;
use Auth;
use Session;
use DB;
use App\UsuariosModel;
use App\PerfilModel;
use Modules\Comunicacionalcaldia\Entities\cabComunicacionModel;
use Modules\Comunicacionalcaldia\Entities\direccionesModel;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaCabModel;
use Carbon\Carbon;


class TramAlcaldiaChartCabController extends Controller {
	
	var $configuraciongeneral = array ("MONITOREO Y CONTROL", "tramitesalcaldia/tramitesgraficos", "index");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[
        {"Tipo":"chart","Descripcion":"GRÁFICO DE TRÁMITES POR DISPOSICIÓN","Nombre":"chart2","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"chart2","Descripcion":"GRÁFICO DE TRÁMITES POR PRIORIDAD","Nombre":"chart4","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';

    
		public function __construct() {
			$this->middleware('cors');
			$this->middleware('auth',['except' => ['getValoresCharts']]);
		}

		public function index()
		{
			
			$objetos = json_decode($this->objetos);
			$objetos=array_values($objetos);
			
			//show($objetos);
		//para obtener el tipo de usuario
		$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
		$id_usuario=Auth::user()->id;
		
		
			$delete='';
			$create='';
			$edit='';
			switch($id_tipo_pefil->tipo){
				case 1:
				$direcciones=direccionesModel::select('id','direccion')->where('estado','=','ACT')->orderby("direccion","asc")->get();
				$delete='si';
				$create='si';
				$edit='';
				break;
				case 2:
				$direcciones=direccionesModel::
				join("users as u","u.id_direccion","=","tmae_direcciones.id")
				->select('tmae_direcciones.id','tmae_direcciones.direccion')->where([['tmae_direcciones.estado','=','ACT'],["u.id",$id_usuario]])->orderby("direccion","asc")->get();
				$delete='no';
				$create='no';
				$edit='';
				break;
				
				case 3:
				$direcciones=direccionesModel::
				join("users as u","u.id_direccion","=","tmae_direcciones.id")
				->select('tmae_direcciones.id','tmae_direcciones.direccion')->where([['tmae_direcciones.estado','=','ACT'],["u.id",$id_usuario]])->orderby("direccion","asc")->get();
				$delete='si';
				$create='si';
				$edit='';
				break;
				
				case 4:
				$direcciones=direccionesModel::select('id','direccion')->where('estado','=','ACT')->orderby("direccion","asc")->get();
				$delete='si';
				$create='si';
				$edit='';
				break;
				
			}
			//show($objetos);
			return view('vistas.indexchart',[
				"objetos"=>$objetos,
				"direcciones"=>$direcciones,
				"configuraciongeneral"=>$this->configuraciongeneral,
				"create"=>$create,
                 "delete"=>$delete
				]);
		}


	public function getValoresCharts($filtro="TODAS"){
		$tipo_chart=Input::get("tipo");
		$valores;
		$desde= Carbon::now();
		
		switch($tipo_chart){
			case '2':
			$valores=TramAlcaldiaCabModel::select(DB::raw("count(*) as valor, disposicion"))
			->where("estado", "ACT")->groupby("disposicion");
			if($filtro!="TODAS")
				$valores=$valores->where("disposicion",$filtro);
			$valores=$valores->get();
			return $valores;
			break;
			case '3':
				
			$valores=TramAlcaldiaCabModel::select(DB::raw("count(*) as valor, prioridad"))
			->where("estado", "ACT")->groupby("prioridad");
			if($filtro!="TODAS")
				$valores=$valores->where("disposicion",$filtro);
			$valores=$valores->get();
			return $valores;
			break;

			case '4':
			$valores=TramAlcaldiaCabModel::select(DB::raw("count(*) as valor, created_at"))
			->where([["estado", "ACT"]])
			->groupby("created_at")->get();
			//show($valores);
			return $valores;
			break;
	
			return response()->json($valores);
		}
	


	}



	
}