<?php namespace Modules\Tramitesalcaldia\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use App\UsuariosModel;
use Auth;

use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Modules\TramitesAlcaldia\Entities\TramAlcaldiaCabModel;

class TramAlcaldiaListCabController extends Controller {
	var $configuraciongeneral = array ("DESPACHO DE ALACALDÍA - BUSQUEDA", "tramitesalcaldia/tramiteslista", "index");
	var $escoja=array(0=>"TODOS");
	

	var $objetos = '[{"Tipo":"select","Descripcion":"Tipo de Filtro","Nombre":"tipofiltro","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
	{"Tipo":"datetext","Descripcion":"Desde","Nombre":"desde","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
	{"Tipo":"datetext","Descripcion":"Hasta","Nombre":"hasta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]'; 	
		
	var $objetosTable = '[
		{"Tipo":"text","Descripcion":"Número de Trámite","Nombre":"numtramite","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Remitente","Nombre":"remitente","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
		{"Tipo":"text","Descripcion":"Asunto","Nombre":"asunto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Petición","Nombre":"peticion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }, 
		{"Tipo":"text","Descripcion":"Prioridad","Nombre":"prioridad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
		{"Tipo":"text","Descripcion":"Recomendación","Nombre":"recomendacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Disposición","Nombre":"disposicion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Observación","Nombre":"observacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },		
		{"Tipo":"text","Descripcion":"Fecha de Ingreso","Nombre":"created_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Fecha de Respuesta","Nombre":"updated_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Adjunto","Nombre":"archivo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
		]';

		
		public function __construct() {
			$this->middleware('auth');
} 

/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
*/
public function index()
{

	$objetos = json_decode($this->objetos);
	//show($this->objetos);
	$objetos[0]->Nombre="tipofiltro";
	$tipoFiltro=array(1=>"PRIORIDAD",2=>"DISPOSICION");
	
	$objetos[0]->Valor=$this->escoja + $tipoFiltro;
	$objetosTable = json_decode($this->objetosTable);
	$objetos=array_values($objetos);
	$delete=null;

	$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
	if($id_tipo_pefil->tipo==2||$id_tipo_pefil->tipo==3){
		$delete=null;
	}
    //show($objetos);
	return view('vistas.indexlist',[
		"objetos"=>$objetos,
		"objetosTable"=>$objetosTable,
		"configuraciongeneral"=>$this->configuraciongeneral,
		"delete"=>$delete,
		"create"=>"si"
		]);


}
public function getValoresFiltro(){
	$tipo_filtro=Input::get("tipo_filtro");
	$html = '';
	$id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();
	//($id_tipo_pefil->tipo);
	$id_usuario=Auth::user()->id;
	switch($tipo_filtro){
		
		case 'PRIORIDAD':
		$valores=explodewords(ConfigSystem("prioridad"),"|");
		$html .= '<label class="col-lg-1 control-label">Estado</label> <div class="col-lg-4">';
		$html .= '<select class="form-control chosen-select" id="valorfiltro">';
		foreach ($valores as $item) {
			$html .= '<option value="'.$item.'">'.$item.'</option>';
		}
		$html .= '</select></div>';
		return $html;
		break;

		case 'DISPOSICION':
		$valores=explodewords(ConfigSystem("disposiciones"),"|");
		$html .= '<label class="col-lg-1 control-label">Estado</label> <div class="col-lg-4">';
		$html .= '<select class="form-control chosen-select" id="valorfiltro">';
		foreach ($valores as $item) {
			$html .= '<option value="'.$item.'">'.$item.'</option>';
		}
		$html .= '</select></div>';
		return $html;
		break;
		

		
	}
	return response()->json(['html' => $html]);
	
}

public function getTramitesFiltro(){
	$tipo_filtro=Input::get("tipo_filtro");
	$valor_filtro=Input::get("valor_filtro");
	$desde=Carbon::parse(Input::get("desde"));
	$hasta=Carbon::parse(Input::get("hasta"));
	$valores=[];
	$create='';
    $delete='';
   
    $id_tipo_pefil=UsuariosModel::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.tipo")->where("users.id",Auth::user()->id)->first();     
	$id_usuario=Auth::user()->id;


	switch($tipo_filtro){

		
		case 'TODOS':
		//show($hasta->format('Y-m-d H:s:m'));
		
		$valores=TramAlcaldiaCabModel::where("estado","ACT")
		->whereBetween(DB::raw("date_format(fecha_ingreso,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])->get();
		//show($valores);

		break;

		case 'PRIORIDAD':
		$valores=TramAlcaldiaCabModel::where([["estado","ACT"],["prioridad",$valor_filtro]])
		->whereBetween(DB::raw("date_format(fecha_ingreso,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])->get();

		break;

		case 'DISPOSICION':
		$valores=TramAlcaldiaCabModel::where([["estado","ACT"],["disposicion",$valor_filtro]])
		->whereBetween(DB::raw("date_format(fecha_ingreso,'%Y-%m-%d')"), [$desde->format('Y-m-d'),$hasta->format('Y-m-d')])->get();
		
		break;


		
		
	}
	return response()->json($valores);
}

	
}