<?php

Route::group(['middleware' => 'web', 'prefix' => 'tramitesalcaldia', 'namespace' => 'Modules\TramitesAlcaldia\Http\Controllers'], function()
{
	Route::get('/', 'TramitesAlcaldiaController@index');
	Route::resource('tramites', 'TramAlcaldiaCabController');

	Route::get('vertramite/{id}', 'TramAlcaldiaCabController@reporte');

	Route::get('tramitesrespondidos', 'TramAlcaldiaCabController@index2');
	Route::get('tramitesfinalizados', 'TramAlcaldiaCabController@indexFinalizados');
	
	Route::get('finalizartramite', 'TramAlcaldiaCabController@editFinalizar');

	Route::resource('tramiteslista', 'TramAlcaldiaListCabController');
	Route::get("getValoresFiltro","TramAlcaldiaListCabController@getValoresFiltro");

	Route::get("getTramitesFiltro","TramAlcaldiaListCabController@getTramitesFiltro");


	Route::resource('tramitesgraficos', 'TramAlcaldiaChartCabController');
	Route::get("getValoresCharts","TramAlcaldiaChartCabController@getValoresCharts");



	Route::resource('dashboard', 'DashboardTramitesController');
	Route::get("getChartLine","DashboardTramitesController@getChartLine");
});