@extends('tramitesalcaldia::layouts.master')
@section ('titulo') {{ trans('html.main.sistema') }} @stop
@section ('scripts')
<script type="text/javascript">
 $(document).ready(function(){
            $('.contact-box').each(function() {
                animationHover(this, 'pulse');
            });
        });
</script>
@stop
@section ('contenido')
	<h1 class="text-center">
		{{ $modulo->nombre }}
	</h1>
	<h1 class="titulo2">
                {{ $modulo->detalle }}
	</h1>
@if (Session::has('message'))
    <script>    
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>    
	   <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-9"> 
		<!--Logo -->
     		<img src="{{asset('http://manta.gob.ec/images/manta-firmes.png') }}" alt="" class="col-lg-12 img-responsive" style="width: 450px;">
     	<!--Iconos -->
     @foreach($iconos as $key => $value)
      @if($value->nivel==1)
        <div class="col-lg-12 wrapper border-bottom">
              <h2>{{ $value->menu }}</h2>        
          </div>      
      @else
     <div class="col-lg-6 col-md-6">
                  <div class="contact-box">
                      <a href="{{ URL::asset($value->ruta) }}">
                      <h3 style="text-align: center;"><strong>{{ $value->menu }}</strong></h3>
                      <div class="col-sm-4">
                          <div class="text-center">
                              <i class="{{ $value->icono }} fa-5x"></i>                              
                          </div>
                      </div>
                      <div class="col-sm-8">
                          <p><i class="fa fa-check"></i>{{ $value->menu }}</p>
                      </div>
                      <div class="clearfix"></div>
                          </a>
                  </div>
              </div>
      @endif
     @endforeach 

     	</div>
     	<div class="col-lg-3">
                        <div class="widget-head-color-box navy-bg p-lg text-center">
                            <div class="m-b-md">
                            <h2 class="font-bold no-margins">
                                {{$usuario->name}}
                            </h2>
                                <small>Información</small>
                            </div>
                            <p style="text-align: center;">

                              <img class="sobre" src="{{ asset('img/actualizarf.png') }}"  style="width: 25%;" />
                              <img style="width: 60%;" src="{{ asset('img/avatar.jpg') }}"  class=" img-circle  circle-border m-b-md" alt="profile">
                            </p>
                            <div>
                                <span>Email: {{$usuario->email}}</span> <br>
                                <span>Teléfono:  {{$usuario->telefono}}</span> 
                              
                            </div>
                            <br>
                             <div>
                              
                                    <span ><a class="btn btn-default btn-xs" style="font-size: 10px;"   href="{{ URL::to('cambiardatos/'.Auth::user()->id.'/edit') }}">Editar Datos</a> </span>  
                                    <span hidden><a class="btn btn-default btn-xs" style="font-size: 10px;"   href="javascript:">Cambiar Contraseña</a> </span> 
                            </div>
                        </div>
                        <div class="widget-text-box">
                            
                        </div>
        </div>
    </div>
</div>
@if (isset($estadistica)) <!-- [0]=Titulo [1]=Ruta [2]=Valor [3]=Numero hasta 4 [4]=Clase Texto: success info danger warning -->
    <div class="col-lg-2">
        <div class="ibox float-e-margins">
            <div class="ibox-title  boton-{{ $estadistica[3] }}">
                <h5>{{ $estadistica[0] }}</h5>
            </div>
            <div class="ibox-content cuadro-{{ $estadistica[3] }}" style="text-align: center;">
                @if(Auth::user()->id==1)
                    <a href="{{ URL::to($estadistica[1]) }}">
                        <h1 class="no-margins"><div class="font-bold text-{{ $estadistica[4] }}">{{ $estadistica[2] }}</div></h1> <!-- La clase de text-info se cambia a text-success, etc -->
                    </a>
                @else
                    <h1 class="no-margins"><div class="font-bold text-{{ $estadistica[4] }}">{{ $estadistica[2] }}</div></h1>
                @endif
            </div>
        </div>
    </div>    
@endif
@stop